select 
	`cheques_comprobantes`.`id_comprobante` AS `id_comprobante`,
    `cheques_comprobantes`.`id_orden` AS `id_orden`,
    `empresa_comprobante`.`nro` AS `nro_fact`,
    `empresa_comprobante`.`razonSocial` AS `razonSocial`,
    `empresa_comprobante`.`actividad` AS `actividad`,
    `empresa_comprobante`.`tipo_comprobante` AS `tipo_comprobante`,
    `empresa_comprobante`.`id_empresa` AS `id_empresa`,
    `empresa_comprobante`.`total` AS `total`,
    `cheques_cuenta_empresa`.`razonSocial` AS `razonSocial2`,
    `cheques_cuenta_empresa`.`Banco` AS `Banco`,
    `cheques_cuenta_empresa`.`id_banco` AS `id_banco`,
    `cheques_cuenta_empresa`.`cuenta` AS `cuenta`,
    `cheques_cuenta_empresa`.`nro` AS `nro`,
    `cheques_cuenta_empresa`.`emision` AS `emision`,
    `cheques_cuenta_empresa`.`tipo` AS `tipo`,
    `cheques_cuenta_empresa`.`paguese` AS `paguese`,
    `cheques_cuenta_empresa`.`cantidad` AS `cantidad`,
    `cheques_comprobantes`.`saldo` AS `saldo`,
    `cheques_comprobantes`.`observaciones` AS `observaciones` 
    
 from 
 	`cheques_cuenta_empresa`,`cheques_comprobantes`,`empresa_comprobante`
where
	`cheques_comprobantes`.`id_comprobante` = `empresa_comprobante`.`id_comprobante` 
	and`cheques_comprobantes`.`banco` = `cheques_cuenta_empresa`.`id_banco` 
	and  cheques_comprobantes.cuenta = cheques_cuenta_empresa.cuenta
	and cheques_comprobantes.nro = cheques_cuenta_empresa.nro
	
-- movimientos`--	
select 
	`movimientos`.`id` AS `id`,
	`movimientos`.`id_banco` AS `id_banco`,
	`movimientos`.`cuenta` AS `cuenta`,
	`movimientos`.`movimiento` AS `movimiento`,
	`movimientos`.`concepto` AS `concepto`,
	`movimientos`.`valor` AS `valor`,
	`movimientos`.`fecha` AS `fecha`,
	`movimientos`.`tipo` AS `tipo`,
	`movimientos`.`observaciones` AS `observaciones`,
	`cuenta_empresa`.`razonSocial` AS `razonSocial`,
	`cuenta_empresa`.`actividad` AS `actividad`,
	`cuenta_empresa`.`Banco` AS `Banco`,
	`concepto`.`concepto` AS `concepto_detalle`,
	`concepto`.`codigo` AS `codigo` 
from 
	((`movimientos` join `cuenta_empresa`) join `concepto`) 
where 
	((`cuenta_empresa`.`id_banco` = `movimientos`.`id_banco`)
and (`cuenta_empresa`.`nro_cuenta` = `movimientos`.`cuenta`) 
and (`concepto`.`id_concepto` = `movimientos`.`concepto`))	