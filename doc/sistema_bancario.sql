-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-02-2020 a las 15:06:34
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_bancario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cheques`
--

CREATE TABLE `cheques` (
  `banco` int(11) NOT NULL,
  `cuenta` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `nro` int(11) NOT NULL,
  `tipo` enum('A','P') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `paguese` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'destinatario a quien va dirigido el cheque',
  `estado` enum('activo','anulado','conciliados','cancelado') COLLATE utf8_spanish2_ci DEFAULT 'activo',
  `emision` date DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `conciliacion` date DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `confecciono` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'responzable quien realizo el cheque',
  `clase` enum('no corresponde','sin factura','con factura') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `impreso` enum('true','false') COLLATE utf8_spanish2_ci DEFAULT 'false',
  `responsable` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `observaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cheques`
--

INSERT INTO `cheques` (`banco`, `cuenta`, `nro`, `tipo`, `paguese`, `estado`, `emision`, `vencimiento`, `conciliacion`, `cantidad`, `confecciono`, `clase`, `impreso`, `responsable`, `observaciones`) VALUES
(1, '1680-00138-38497-0173-3', 491, 'A', 'juan  roberto  solis  y/o juan carlos  pereyra De los castillos', 'conciliados', '2019-11-27', '2019-11-28', '2020-01-03', 65035, 'jua', 'con factura', 'true', 'PROVEDOR', ''),
(1, '1680-00138-38497-0173-3', 492, 'A', 'juan perez fdsg sgs', 'conciliados', '2019-11-27', '2019-11-28', '2020-01-03', 480, 'jua', 'con factura', 'true', 'PROVEDOR', ''),
(1, '1680-00138-38497-0173-3', 493, 'A', '', 'conciliados', '2019-11-27', '2019-11-28', '2020-01-03', 6980, 'jua', 'con factura', 'true', 'PROVEDOR', ''),
(1, '1680-00138-38497-0173-3', 494, 'A', 'juan perez fdsg sgs', 'conciliados', '2019-11-27', '2019-11-28', '2020-01-03', 5642156, 'jua', 'con factura', 'true', 'PROVEDOR', ''),
(1, '1680-00138-38497-0173-3', 495, 'P', 'MAMANI JAIME', 'conciliados', '2019-11-29', '2019-11-30', '2019-11-01', 50000, '', 'con factura', 'true', 'J', ''),
(1, '1680-00138-38497-0173-3', 496, 'A', 'ROBERTO ISMAEL MARCILESE', 'conciliados', '2019-11-29', '2019-11-30', '2019-12-19', 40000, 'V', 'no corresponde', 'true', 'J', ''),
(1, '1680-00138-38497-0173-3', 497, 'A', 'Julio Lopez', 'conciliados', '2019-12-06', '2019-12-07', '2020-01-02', 80500, 'jua', 'con factura', 'false', 'No se', ''),
(1, '1680-00138-38497-0173-3', 498, 'A', 'Martin Perez de los Castillos', 'conciliados', '2019-12-06', '2019-12-07', '2019-12-24', 169826.5, 'jua', 'con factura', 'false', 'No se', ''),
(1, '1680-00138-38497-0173-3', 499, 'A', 'Martin Lopez jose Castro JOHIHGB  GHFYHF GDHGD', 'conciliados', '2019-12-06', '2019-12-07', '2020-01-09', 5689.59, 'jua', 'con factura', 'false', 'No se', ''),
(1, '1680-00138-38497-0173-3', 500, 'A', 'LAS LOMAS SRL', 'conciliados', '2019-12-06', '2019-12-07', '1970-01-01', 52682000.69, 'jua', 'con factura', 'false', 'No se', ''),
(1, '1680-00138-38497-0173-3', 501, 'A', 'juan  roberto  solis  y/o juan carlos  pereyra De los castillos', 'conciliados', '2019-12-06', '2019-12-07', '2019-12-19', 698730, 'jua', 'con factura', 'true', 'No se', ''),
(2, '0931-02003891-75', 14529, 'A', 'juan perez fdsg sgs sadasd sadasdas', 'activo', '2019-11-19', '2019-11-20', '0000-00-00', 320120.5, 'jua', 'con factura', 'true', 'javier', ''),
(2, '0931-02003891-75', 14590, 'A', 'LAS LOMAS SRL', 'activo', '2019-11-14', '2019-11-21', '0000-00-00', 703131.6, 'javier', 'con factura', 'true', 'javier', ''),
(2, '0931-02003891-75', 14591, 'A', 'LAS LOMAS SRL', 'activo', '2019-11-14', '2019-11-21', '0000-00-00', 703131.6, 'javier', 'con factura', 'true', 'javier', ''),
(2, '0931-02003891-75', 14594, 'A', 'PETRANOR SRL.', 'cancelado', '2019-11-24', '2019-11-25', '0000-00-00', 1654686.78, 'V', 'con factura', 'true', 'J', ''),
(2, '0931-02003891-75', 14595, 'P', 'TITO PERSONAL', 'anulado', '2019-11-24', '2019-11-25', '0000-00-00', 15000, 'V', 'no corresponde', 'true', 'J', ''),
(2, '0931-02003891-75', 14601, 'A', 'Martinez & Martinez', 'activo', '2019-12-17', '2019-12-18', NULL, 2000, 'j', 'con factura', 'true', 'a', ''),
(2, '0931-02003891-75', 14602, 'A', 'Mario Luiz', 'activo', '2019-12-17', '2019-12-18', NULL, 23000, 'j', 'con factura', 'true', 'a', ''),
(2, '0931-02003891-75', 14603, 'A', 'Julio Cesar Chavez', 'activo', '2019-12-17', '2019-12-18', NULL, 233350, 'j', 'con factura', 'true', 'a', ''),
(2, '0931-02003891-75', 14604, 'A', 'ñandu srl &', 'activo', '2019-12-17', '2019-12-18', NULL, 245235, 'j', 'con factura', 'true', 'a', ''),
(2, '0931-02003891-75', 14605, 'A', 'Por ellita', 'activo', '2019-12-17', '2019-12-18', NULL, 2325235, 'j', 'con factura', 'true', 'a', ''),
(2, '0931-02003891-75', 14606, 'A', 'Gustavo No lo se Abudabi', 'activo', '2019-12-17', '2019-12-18', NULL, 3453463, 'j', 'con factura', 'true', 'a', ''),
(2, '0931-02003891-75', 14607, 'A', 'Hierro Nort SRL', 'activo', '2019-12-17', '2019-12-18', NULL, 2345602.244, 'j', 'con factura', 'true', 'a', ''),
(4, '1680-00138-38497-0173-3', 195266, 'A', 'FLAVIO JAVIER AGUILERA', 'activo', '2019-11-19', '2019-11-20', '0000-00-00', 5686020.6, 'jua', 'con factura', 'true', 'javier', ''),
(4, '1680-00138-38497-0173-3', 195267, 'A', 'POLVER SRL', 'cancelado', '2019-11-19', '2019-11-20', '0000-00-00', 2656496, 'jua', 'con factura', 'true', 'javier', ''),
(10, '3-206-0000012540-5', 921836, 'A', 'juan  roberto  solis  y/o juan carlos  pereyra De los castillos', 'activo', '2019-11-19', '2019-11-20', NULL, 658130, 'jua', 'con factura', 'false', 'javier', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cheques_comprobantes`
--

CREATE TABLE `cheques_comprobantes` (
  `id_comprobante` int(11) NOT NULL,
  `banco` int(11) NOT NULL,
  `cuenta` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `nro` int(11) NOT NULL,
  `saldo` float DEFAULT NULL,
  `observaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cheques_comprobantes`
--

INSERT INTO `cheques_comprobantes` (`id_comprobante`, `banco`, `cuenta`, `nro`, `saldo`, `observaciones`, `id_orden`) VALUES
(0, 2, '0931-02003891-75', 14594, 0, 'Cheque cantidad:14594', 11),
(0, 2, '0931-02003891-75', 14601, 0, 'Cheque cantidad:14601', 11),
(0, 2, '0931-02003891-75', 14603, 0, 'Cheque cantidad:14603', 11),
(0, 2, '0931-02003891-75', 14604, 0, 'Cheque cantidad:14604', 11),
(0, 2, '0931-02003891-75', 14605, 0, 'Cheque cantidad:14605', 11),
(0, 2, '0931-02003891-75', 14606, 0, 'Cheque cantidad:14606', 11),
(0, 2, '0931-02003891-75', 14607, 0, 'Cheque cantidad:14607', 11),
(0, 4, '1680-00138-38497-0173-3', 195266, 0, 'Cheque cantidad:195266', 11),
(0, 4, '1680-00138-38497-0173-3', 195267, 0, 'Cheque cantidad:2656500', 31),
(11, 2, '0931-02003891-75', 14601, 0, 'Cheque cantidad:14601', 10),
(11, 2, '0931-02003891-75', 14602, 0, 'Cheque cantidad:14602', 9),
(14, 1, '1680-00138-38497-0173-3', 502, 0, 'Cheque cantidad:502', 31),
(15, 2, '0931-02003891-75', 14601, 0, 'Cheque cantidad:14601', 10),
(15, 2, '0931-02003891-75', 14602, 0, 'Cheque cantidad:14602', 9);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `cheques_comprobantes_vista`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `cheques_comprobantes_vista` (
`id_comprobante` int(11)
,`id_orden` int(11)
,`nro_fact` varchar(255)
,`razonSocial` varchar(255)
,`actividad` varchar(255)
,`tipo_comprobante` enum('Notas de crédito','Notas de débito','Remito','Orden de compra','Recibos','Comprobante','Tickets','factura','')
,`id_empresa` int(11)
,`total` float
,`razonSocial2` varchar(255)
,`Banco` varchar(255)
,`id_banco` int(11)
,`cuenta` varchar(255)
,`nro` int(11)
,`emision` date
,`tipo` enum('A','P')
,`paguese` varchar(255)
,`cantidad` double
,`saldo` float
,`observaciones` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `cheques_cuenta_empresa`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `cheques_cuenta_empresa` (
`razonSocial` varchar(255)
,`Banco` varchar(255)
,`tipo_cuenta` enum('Otros','Cuenta Corriente especial','Caja de Ahorros','Cuenta Corrientes','')
,`id_banco` int(11)
,`cuenta` varchar(255)
,`nro` int(11)
,`tipo` enum('A','P')
,`paguese` varchar(255)
,`estado` enum('activo','anulado','conciliados','cancelado')
,`emision` date
,`vencimiento` date
,`conciliacion` date
,`cantidad` double
,`confecciono` varchar(255)
,`clase` enum('no corresponde','sin factura','con factura')
,`impreso` enum('true','false')
,`responsable` varchar(255)
,`observaciones` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobantes`
--

CREATE TABLE `comprobantes` (
  `id_comprobante` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `tipo_comprobante` enum('Notas de crédito','Notas de débito','Remito','Orden de compra','Recibos','Comprobante','Tickets','factura','') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nro` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `id_empresa_dest` int(11) DEFAULT NULL,
  `iva` float DEFAULT NULL,
  `neto` double DEFAULT NULL,
  `iva_valor` double DEFAULT NULL,
  `neto2` double DEFAULT NULL,
  `imp_internos` double DEFAULT NULL,
  `typo` enum('cliente','proveedor') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` enum('cancelado','anulado','activo') COLLATE utf8_spanish2_ci DEFAULT 'activo',
  `observaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `iva2` double DEFAULT NULL,
  `iva_valor2` double DEFAULT NULL,
  `concep_no_grabados` double DEFAULT NULL,
  `total` float DEFAULT NULL,
  `pago` enum('pendiente','parcial','completado') COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comprobantes`
--

INSERT INTO `comprobantes` (`id_comprobante`, `id_empresa`, `tipo_comprobante`, `nro`, `fecha`, `id_empresa_dest`, `iva`, `neto`, `iva_valor`, `neto2`, `imp_internos`, `typo`, `estado`, `observaciones`, `iva2`, `iva_valor2`, `concep_no_grabados`, `total`, `pago`) VALUES
(11, 11, 'Comprobante', '', '2019-11-28', 8, 21, 100, 21, 50, 20, 'proveedor', 'activo', '', 10.5, 5.25, 10, 206.25, 'pendiente'),
(12, 13, 'Notas de débito', '2', '2019-11-15', NULL, 21, 175072.41, 36765.206099999996, 0, 0, 'cliente', 'activo', '', 0, 0, 0, 175072, 'pendiente'),
(13, 13, 'factura', '0003-0000146', '2019-11-15', NULL, 21, 2165066.55, 454663.97549999994, 0, 0, 'proveedor', 'activo', '', 0, 0, 0, 2165070, 'parcial'),
(14, 14, 'factura', '0002-00004613', '2019-11-05', NULL, 0, 500, 0, 0, 0, 'proveedor', 'activo', '', 0, 0, 0, 500, 'parcial'),
(15, 11, 'factura', '32423', '2019-12-13', NULL, 21, 2000, 420, 0, 400, 'proveedor', 'activo', '', 0, 0, 300, 3120, 'pendiente'),
(16, 14, 'factura', '34324-32432', '2020-01-16', 6, 21, 2000, 420, 0, 5, 'proveedor', 'activo', 'test', 0, 0, 10, 2435, 'pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepto`
--

CREATE TABLE `concepto` (
  `id_concepto` int(11) NOT NULL,
  `concepto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `concepto`
--

INSERT INTO `concepto` (`id_concepto`, `concepto`, `codigo`, `descripcion`) VALUES
(1, 'Deposito en efectivo', 'CDE', 'Ingreso de dinero en efectivo'),
(2, 'Deposito cheque', 'CDC', 'Ingreso mediante cheque'),
(3, 'Deposito Transferencia Electronica', 'CTE', 'Se realizo una transferencia electronica'),
(4, 'Debito Cheque Corriente', 'DCC', NULL),
(5, 'Debito Cheque Diferido', 'DCD', NULL),
(6, 'Debito Transferencia Electronica', 'DTE', NULL),
(7, 'Compra Dolares', 'CD', NULL),
(8, 'Pago Proveedores', 'PP', NULL),
(9, 'Debito por Intereses', 'DIN', NULL),
(10, 'Debito Fiscal Iva', 'DFI', NULL),
(11, 'Debito Fiscal DGR', 'DFD', NULL),
(12, 'Debito Fiscal 6 X Mil', 'DF6', NULL),
(13, 'Pago Impuestos', 'PGI', NULL),
(14, 'Debitos Varios', 'DVS', NULL),
(15, 'Comisiones', 'COM', NULL),
(16, 'Intereses', 'INT', NULL),
(17, 'COMMOVEFEC', 'COE', NULL),
(18, 'Otros conceptos', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `nro_cuenta` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `id_banco` int(11) NOT NULL COMMENT 'fk a empresas',
  `cbu` int(11) DEFAULT NULL,
  `id_empresa` int(11) NOT NULL COMMENT 'fk a empresas',
  `tipo` enum('Otros','Cuenta Corriente especial','Caja de Ahorros','Cuenta Corrientes','') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `sucursal` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `saldo` float DEFAULT NULL,
  `ultimo_cheque` int(11) DEFAULT NULL,
  `observaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` enum('cancelada','activa') COLLATE utf8_spanish2_ci DEFAULT 'activa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`nro_cuenta`, `id_banco`, `cbu`, `id_empresa`, `tipo`, `sucursal`, `saldo`, `ultimo_cheque`, `observaciones`, `estado`) VALUES
('087-20-319590-2-00', 3, 2147483647, 7, 'Cuenta Corrientes', 'SALTA', -1, 384465, '', NULL),
('0931-02003891-75', 2, 2147483647, 9, 'Cuenta Corrientes', 'SALTA', -1, 14607, '  					  					', 'activa'),
('10917-2173-0', 4, 2147483647, 6, 'Cuenta Corrientes', 'SALTA', -1, 761089, '', NULL),
('1680-00106-2', 1, 2147483647, 6, 'Cuenta Corrientes', 'SALTA', 500, 1450, '', NULL),
('1680-00110-9', 1, 2147483647, 7, 'Cuenta Corrientes', 'SALTA', -1, 1627, '', NULL),
('1680-00138-38497-0173-3', 1, 2147483647, 5, 'Cuenta Corrientes', 'SALTA', -800845, 502, '  					  					  					  					', 'activa'),
('1680-00138-38497-0173-3', 4, 2147483647, 8, 'Cuenta Corrientes', 'SALTA', -1, 195267, '', NULL),
('3-206-0000012540-5', 10, 2147483647, 9, 'Cuenta Corrientes', 'PALPALA - JUJUY', -1, 921836, '', NULL),
('476-20-000281-4-00', 3, 2147483647, 6, 'Cuenta Corrientes', 'SALTA', -1, 851949, '', NULL),
('476-20-000611-3-00', 3, 2147483647, 5, 'Cuenta Corrientes', 'SALTA', -1, 926596, '', NULL);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `cuenta_empresa`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `cuenta_empresa` (
`razonSocial` varchar(255)
,`actividad` varchar(255)
,`nro_cuenta` varchar(255)
,`id_banco` int(11)
,`cbu` int(11)
,`Banco` varchar(255)
,`id_empresa` int(11)
,`tipo` enum('Otros','Cuenta Corriente especial','Caja de Ahorros','Cuenta Corrientes','')
,`sucursal` varchar(255)
,`saldo` float
,`ultimo_cheque` int(11)
,`observaciones` varchar(255)
,`estado` enum('cancelada','activa')
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_comprobante`
--

CREATE TABLE `detalle_comprobante` (
  `id_comprobante` int(11) NOT NULL,
  `id_detalle` int(11) NOT NULL,
  `cantidad` float DEFAULT NULL,
  `concepto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pago`
--

CREATE TABLE `detalle_pago` (
  `id_pago` int(11) NOT NULL,
  `id_detalle` int(11) NOT NULL,
  `tipo` enum('efectivo','transferencia bancaria','cheque','credito','otros') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `detalle` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cantidad` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalle_pago`
--

INSERT INTO `detalle_pago` (`id_pago`, `id_detalle`, `tipo`, `detalle`, `cantidad`) VALUES
(1, 1, 'efectivo', '', 4000),
(1, 2, NULL, '2345688', 2390),
(1, 3, 'efectivo', '', 5000),
(1, 4, 'efectivo', '', 2323),
(1, 6, 'credito', 'fer', 2323),
(2, 4, 'efectivo', '', 2000),
(2, 5, 'cheque', 'nro 343 banco rio', 4000),
(2, 9, 'otros', 'pago con especias', 3400),
(3, 2, 'cheque', '432r2r', 5000),
(3, 3, 'credito', 'r3r3', 5000),
(4, 2, 'efectivo', '', 5000);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `deudas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `deudas` (
`id_empresa` int(11)
,`id` int(11)
,`fecha` date
,`tipo` varchar(16)
,`clase` varchar(9)
,`total` double
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `cuit` varchar(255) COLLATE utf8_spanish2_ci DEFAULT '',
  `tipo` enum('Responsable Inscripto','Responzable monotributo','Consumidor Final','Otros') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `razonSocial` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `actividad` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `clase` enum('otros','banco','empresa','proveedor','cliente') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `domicilio` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono2` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `representante` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `tel_representante` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `otros` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `cuit`, `tipo`, `razonSocial`, `actividad`, `clase`, `domicilio`, `telefono`, `telefono2`, `celular`, `representante`, `tel_representante`, `otros`) VALUES
(1, '30604731018', 'Responsable Inscripto', 'BANCO COMAFI S.A.', 'Actividad Bancaria', 'banco', 'ESPAÑA 661', '4221307', '4229319', '0387-156004304', 'CHANETON MARIANO DANIEL', '', ''),
(2, '30709447846', 'Responsable Inscripto', 'BANCO ICBC S.A.', 'Actividad Bancaria', 'banco', 'ESPAÑA 771', '4226825', '4226834', '0387-155852311', 'CABANILLAS NICOLAS', '', ''),
(3, '30500003193', 'Responsable Inscripto', 'BANCO FRANCES S.A.', 'Actividad Bancaria', 'banco', 'ESPAÑA 642', '4000230', '', '', 'HUSSEIN CRISTIAN', '', ''),
(4, '30500001735', 'Responsable Inscripto', 'BANCO DE GALICIA Y BS. AS. SA', 'Actividad Bancaria', 'banco', 'BALCARCE 101', '4311131', '4311535', '0387-155077690', 'ALVARADO RODRIGO', '', ''),
(5, '', 'Otros', 'TRANSPORTE MARCILESE SA', NULL, 'otros', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '', 'Otros', 'TRANMAR SRL', NULL, 'otros', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '', 'Otros', 'PABLO MARCILESE SRL', NULL, 'otros', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '', 'Otros', 'AGRO-INVERSORA MOJOTORO SRL', NULL, 'otros', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '', 'Otros', 'PROCESADORA DE BORATOS ARG. SA', NULL, 'otros', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '30500010084', 'Responsable Inscripto', 'BANCO MACRO S.A.', 'Actividad Bancaria', 'banco', 'MIRABAL ESQ. SAN MARTIN - PALPALA', '', '', '', '', '', ''),
(11, '30663379913', 'Responsable Inscripto', 'POLVER SRL', 'FILTROS', 'proveedor', 'ALVARADO 1499', '4314761', '', '', '', '', ''),
(12, '33701937479', 'Responsable Inscripto', 'PROCESADORA DE BORATOS ARGENTINOS SA', 'MINERIA', 'cliente', '', '', '', '', '', '', ''),
(13, '30707209840', 'Responsable Inscripto', ' PROSAL SA', 'ZAFRA', 'cliente', 'SARGENTO CABRAL   CAMPÒ SANTO', '', '', '', 'JUAN PABLO SOTO MAYOR', '', ''),
(14, '20108321262', 'Responsable Inscripto', 'jorge ferrero', 'gestoria', 'proveedor', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `empresa_comprobante`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `empresa_comprobante` (
`id_comprobante` int(11)
,`pago` enum('pendiente','parcial','completado')
,`id_empresa` int(11)
,`id_empresa_dest` int(11)
,`tipo_comprobante` enum('Notas de crédito','Notas de débito','Remito','Orden de compra','Recibos','Comprobante','Tickets','factura','')
,`nro` varchar(255)
,`fecha` date
,`iva` float
,`total` float
,`observaciones` varchar(255)
,`estado` enum('cancelado','anulado','activo')
,`typo` enum('cliente','proveedor')
,`id` int(11)
,`cuit` varchar(255)
,`tipo` enum('Responsable Inscripto','Responzable monotributo','Consumidor Final','Otros')
,`razonSocial` varchar(255)
,`actividad` varchar(255)
,`clase` enum('otros','banco','empresa','proveedor','cliente')
,`domicilio` varchar(255)
,`telefono` varchar(50)
,`telefono2` varchar(50)
,`celular` varchar(50)
,`representante` varchar(255)
,`tel_representante` varchar(255)
,`otros` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impresion`
--

CREATE TABLE `impresion` (
  `id` int(11) NOT NULL,
  `cant_x` float DEFAULT NULL,
  `cant_y` float DEFAULT NULL,
  `emision_x` float DEFAULT NULL,
  `emision_y` float DEFAULT NULL,
  `vencimiento_x` float DEFAULT NULL,
  `vencimiento_y` float DEFAULT NULL,
  `paguese_x` float DEFAULT NULL,
  `paguese_y` float DEFAULT NULL,
  `cant_letras_x` float DEFAULT NULL,
  `cant_letras_y` float DEFAULT NULL,
  `saltos` float DEFAULT NULL COMMENT 'los saltos entre cheques'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `impresion`
--

INSERT INTO `impresion` (`id`, `cant_x`, `cant_y`, `emision_x`, `emision_y`, `vencimiento_x`, `vencimiento_y`, `paguese_x`, `paguese_y`, `cant_letras_x`, `cant_letras_y`, `saltos`) VALUES
(1, 12, 0, 2, 0, 8, 0, 15, 0, 50, -18, -12),
(2, 10, 10, 5, 12, 5, 14, 15, 12, 55, 0, -12),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 10, 0, 10, -10, 10, -10, 20, -10, 45, -20, 0),
(10, 15, 0, 0, -10, 5, -8, 20, -7, 40, -20, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` int(11) NOT NULL,
  `id_banco` int(11) DEFAULT NULL,
  `cuenta` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `movimiento` int(11) DEFAULT NULL,
  `concepto` int(255) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipo` enum('ingreso','egreso') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `observaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id`, `id_banco`, `cuenta`, `movimiento`, `concepto`, `valor`, `fecha`, `tipo`, `observaciones`) VALUES
(9, 1, '1680-00106-2', 2, 2, 500, '2019-12-31', 'ingreso', ''),
(10, 1, '1680-00138-38497-0173-3', NULL, 4, 80500, '2020-01-02', 'egreso', 'Cheque Nº497'),
(11, 1, '1680-00138-38497-0173-3', NULL, 4, 65035, '2020-01-03', 'egreso', 'Cheque Nº491'),
(12, 1, '1680-00138-38497-0173-3', NULL, 4, 480, '2020-01-03', 'egreso', 'Cheque Nº492'),
(13, 1, '1680-00138-38497-0173-3', NULL, 4, 6980, '2020-01-03', 'egreso', 'Cheque Nº493'),
(14, 1, '1680-00138-38497-0173-3', NULL, 4, 5642160, '2020-01-03', 'egreso', 'Cheque Nº494'),
(15, 1, '1680-00138-38497-0173-3', NULL, 4, 5689.59, '2020-01-09', 'egreso', 'Cheque Nº499'),
(16, 1, '1680-00138-38497-0173-3', 0, 1, 5000000, '2020-01-09', 'ingreso', '');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `movimiento_cuenta_empresa`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `movimiento_cuenta_empresa` (
`id` int(11)
,`id_banco` int(11)
,`cuenta` varchar(255)
,`movimiento` int(11)
,`concepto` int(255)
,`valor` float
,`fecha` date
,`tipo` enum('ingreso','egreso')
,`observaciones` varchar(255)
,`razonSocial` varchar(255)
,`actividad` varchar(255)
,`Banco` varchar(255)
,`concepto_detalle` varchar(255)
,`codigo` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_comprobante`
--

CREATE TABLE `orden_comprobante` (
  `id_orden` int(11) NOT NULL,
  `id_comprobante` int(11) NOT NULL,
  `tipo_comprobante` enum('Notas de crédito','Notas de débito','Remito','Orden de compra','Recibos','Comprobante','Tickets','factura','') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nro` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orden_comprobante`
--

INSERT INTO `orden_comprobante` (`id_orden`, `id_comprobante`, `tipo_comprobante`, `nro`, `fecha`, `total`) VALUES
(27, 3, 'factura', '0002-00004613', '2019-11-05', 500),
(9, 11, 'Comprobante', '', '2019-11-28', 206.25),
(10, 11, 'Comprobante', '', '2019-11-28', 206.25),
(11, 11, 'Comprobante', '', '2019-11-28', 206.25),
(31, 14, 'factura', '0002-00004613', '2019-11-05', 500),
(9, 15, 'factura', '32423', '2019-12-13', 3120),
(10, 15, 'factura', '32423', '2019-12-13', 3120),
(11, 15, 'factura', '32423', '2019-12-13', 3120);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_pago`
--

CREATE TABLE `orden_pago` (
  `id_orden` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL COMMENT 'fk  empresa',
  `pago_efectivo` float DEFAULT NULL,
  `pago_retenciones` float DEFAULT NULL,
  `nro_transferencia` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `pago_transferencia` float DEFAULT NULL,
  `pago_nota` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL,
  `estado` enum('realizado','cancelado','pendiente') COLLATE utf8_spanish2_ci DEFAULT 'pendiente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orden_pago`
--

INSERT INTO `orden_pago` (`id_orden`, `id_empresa`, `pago_efectivo`, `pago_retenciones`, `nro_transferencia`, `pago_transferencia`, `pago_nota`, `fecha`, `total`, `estado`) VALUES
(9, 11, 200, 0, '', 0, 100, '2019-12-13', 19974, 'realizado'),
(10, 11, 200, 0, '324', 32423, 100, '2019-12-13', 29397, 'pendiente'),
(11, 11, 200, 0, '324', 32423, 100, '2019-12-13', 15998000, 'realizado'),
(12, 11, 200, NULL, '324', 32423, 100, '2019-12-13', 89525, 'pendiente'),
(13, 11, 200, NULL, '324', 32423, 100, '2019-12-13', 89525, 'pendiente'),
(14, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2709980, 'pendiente'),
(15, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2713300, 'pendiente'),
(16, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2713300, 'pendiente'),
(17, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2713300, 'pendiente'),
(18, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2713300, 'pendiente'),
(19, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2713300, 'pendiente'),
(20, 11, 0, NULL, '00', 0, 0, '2019-12-13', 2713300, 'pendiente'),
(21, 11, 200, NULL, '24', 4, 4, '2019-12-14', 53683.8, 'pendiente'),
(22, 11, 200, NULL, '24', 4, 4, '2019-12-14', 57010, 'pendiente'),
(23, 11, 100, NULL, '00', 0, 600, '2019-12-16', 2653870, 'pendiente'),
(24, 11, 100, NULL, '00', 0, 600, '2019-12-16', 2657200, 'pendiente'),
(25, 11, 100, NULL, '00', 0, 600, '2019-12-16', 2657200, 'pendiente'),
(26, 11, 100, NULL, '00', 0, 600, '2019-12-16', 2657200, 'pendiente'),
(27, 14, 100, 100, '233423', 100, 100, '2019-12-16', 2656400, 'pendiente'),
(28, 14, 100, 100, '233423', 100, 100, '2019-12-16', 2656900, 'pendiente'),
(29, 14, 100, 100, '233423', 100, 100, '2019-12-16', 2656900, 'pendiente'),
(30, 14, 100, 100, '233423', 100, 100, '2019-12-16', 2656900, 'pendiente'),
(31, 14, 10, 100, '233423', 100, 100, '2019-12-16', 2713110, 'pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id_pago` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `total` double DEFAULT NULL,
  `observaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` enum('realizado','cancelado','pendiente') COLLATE utf8_spanish2_ci DEFAULT 'pendiente'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id_pago`, `id_empresa`, `fecha`, `total`, `observaciones`, `estado`) VALUES
(1, 8, '2020-01-08', 0, 'test', 'pendiente'),
(2, 8, '2020-01-16', 0, 'test', 'pendiente'),
(3, 12, '2020-01-16', 0, '', 'pendiente'),
(4, 12, '2020-01-16', 0, '', 'pendiente'),
(5, 12, NULL, NULL, NULL, 'pendiente'),
(6, 12, NULL, NULL, NULL, 'pendiente'),
(7, 12, NULL, NULL, NULL, 'pendiente'),
(8, 12, NULL, NULL, NULL, 'pendiente'),
(9, 12, NULL, NULL, NULL, 'pendiente'),
(10, 12, NULL, NULL, NULL, 'pendiente'),
(11, 7, NULL, NULL, NULL, 'pendiente'),
(12, 8, '2020-01-10', 0, 'pago atrasado', 'pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_comprobantes`
--

CREATE TABLE `pagos_comprobantes` (
  `id_pago` int(11) NOT NULL,
  `id_comprobante` int(11) NOT NULL,
  `estado` enum('activo','cancelado') COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `pagos_empresas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `pagos_empresas` (
`id_pago` int(11)
,`id_empresa` int(11)
,`fecha` date
,`total` double
,`observaciones` varchar(255)
,`razonSocial` varchar(255)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp_comprobantes`
--

CREATE TABLE `temp_comprobantes` (
  `id_comprobante` int(11) NOT NULL,
  `tipo_comprobante` enum('Notas de crédito','Notas de débito','Remito','Orden de compra','Recibos','Comprobante','Tickets','factura','') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nro` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `temp_comprobantes`
--

INSERT INTO `temp_comprobantes` (`id_comprobante`, `tipo_comprobante`, `nro`, `fecha`, `total`) VALUES
(1, 'factura', '32423', '2019-12-13', 3120),
(2, 'Comprobante', '', '2019-11-28', 206.25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp_conciliacion`
--

CREATE TABLE `temp_conciliacion` (
  `banco` int(11) NOT NULL,
  `cuenta` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `nro` int(11) NOT NULL,
  `tipo` enum('A','P') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `paguese` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'destinatario a quien va dirigido el cheque',
  `emision` date DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `cantidad` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `nickname` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `type_user` enum('administrador','proveedores','ventas','cajero') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idpersona` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`nickname`, `password`, `type_user`, `idpersona`) VALUES
('admin', '15860d108b92df2d94ce86c862b06d65e575846e7b92f90601d1c9905432a7858580b997adeeaf08814c51c4614a1215b8f1d1f6fcbbc015131c1884cfc91589', 'administrador', NULL),
('proyect', '15860d108b92df2d94ce86c862b06d65e575846e7b92f90601d1c9905432a7858580b997adeeaf08814c51c4614a1215b8f1d1f6fcbbc015131c1884cfc91589', 'administrador', NULL);

-- --------------------------------------------------------

--
-- Estructura para la vista `cheques_comprobantes_vista`
--
DROP TABLE IF EXISTS `cheques_comprobantes_vista`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cheques_comprobantes_vista`  AS  select `cheques_comprobantes`.`id_comprobante` AS `id_comprobante`,`cheques_comprobantes`.`id_orden` AS `id_orden`,`empresa_comprobante`.`nro` AS `nro_fact`,`empresa_comprobante`.`razonSocial` AS `razonSocial`,`empresa_comprobante`.`actividad` AS `actividad`,`empresa_comprobante`.`tipo_comprobante` AS `tipo_comprobante`,`empresa_comprobante`.`id_empresa` AS `id_empresa`,`empresa_comprobante`.`total` AS `total`,`cheques_cuenta_empresa`.`razonSocial` AS `razonSocial2`,`cheques_cuenta_empresa`.`Banco` AS `Banco`,`cheques_cuenta_empresa`.`id_banco` AS `id_banco`,`cheques_cuenta_empresa`.`cuenta` AS `cuenta`,`cheques_cuenta_empresa`.`nro` AS `nro`,`cheques_cuenta_empresa`.`emision` AS `emision`,`cheques_cuenta_empresa`.`tipo` AS `tipo`,`cheques_cuenta_empresa`.`paguese` AS `paguese`,`cheques_cuenta_empresa`.`cantidad` AS `cantidad`,`cheques_comprobantes`.`saldo` AS `saldo`,`cheques_comprobantes`.`observaciones` AS `observaciones` from ((`cheques_cuenta_empresa` join `cheques_comprobantes`) join `empresa_comprobante`) where ((`cheques_comprobantes`.`id_comprobante` = `empresa_comprobante`.`id_comprobante`) and (`cheques_comprobantes`.`banco` = `cheques_cuenta_empresa`.`id_banco`) and (`cheques_comprobantes`.`nro` = `cheques_cuenta_empresa`.`nro`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `cheques_cuenta_empresa`
--
DROP TABLE IF EXISTS `cheques_cuenta_empresa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cheques_cuenta_empresa`  AS  select `cuenta_empresa`.`razonSocial` AS `razonSocial`,`cuenta_empresa`.`Banco` AS `Banco`,`cuenta_empresa`.`tipo` AS `tipo_cuenta`,`cheques`.`banco` AS `id_banco`,`cheques`.`cuenta` AS `cuenta`,`cheques`.`nro` AS `nro`,`cheques`.`tipo` AS `tipo`,`cheques`.`paguese` AS `paguese`,`cheques`.`estado` AS `estado`,`cheques`.`emision` AS `emision`,`cheques`.`vencimiento` AS `vencimiento`,`cheques`.`conciliacion` AS `conciliacion`,`cheques`.`cantidad` AS `cantidad`,`cheques`.`confecciono` AS `confecciono`,`cheques`.`clase` AS `clase`,`cheques`.`impreso` AS `impreso`,`cheques`.`responsable` AS `responsable`,`cheques`.`observaciones` AS `observaciones` from (`cheques` join `cuenta_empresa` on(((`cuenta_empresa`.`id_banco` = `cheques`.`banco`) and (`cuenta_empresa`.`nro_cuenta` = `cheques`.`cuenta`)))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `cuenta_empresa`
--
DROP TABLE IF EXISTS `cuenta_empresa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cuenta_empresa`  AS  select `empresas`.`razonSocial` AS `razonSocial`,`empresas`.`actividad` AS `actividad`,`cuenta`.`nro_cuenta` AS `nro_cuenta`,`cuenta`.`id_banco` AS `id_banco`,`cuenta`.`cbu` AS `cbu`,(select `empresas`.`razonSocial` from `empresas` where (`cuenta`.`id_banco` = `empresas`.`id`)) AS `Banco`,`cuenta`.`id_empresa` AS `id_empresa`,`cuenta`.`tipo` AS `tipo`,`cuenta`.`sucursal` AS `sucursal`,`cuenta`.`saldo` AS `saldo`,`cuenta`.`ultimo_cheque` AS `ultimo_cheque`,`cuenta`.`observaciones` AS `observaciones`,`cuenta`.`estado` AS `estado` from (`cuenta` join `empresas` on((`cuenta`.`id_empresa` = `empresas`.`id`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `deudas`
--
DROP TABLE IF EXISTS `deudas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `deudas`  AS  (select `comprobantes`.`id_empresa` AS `id_empresa`,`comprobantes`.`id_comprobante` AS `id`,`comprobantes`.`fecha` AS `fecha`,`comprobantes`.`tipo_comprobante` AS `tipo`,`comprobantes`.`typo` AS `clase`,`comprobantes`.`total` AS `total` from `comprobantes`) union (select `orden_pago`.`id_empresa` AS `id_empresa`,`orden_pago`.`id_orden` AS `id`,`orden_pago`.`fecha` AS `fecha`,'Orden de Pago' AS `tipo`,'proveedor' AS `clase`,`orden_pago`.`total` AS `total` from `orden_pago`) union (select `pagos`.`id_empresa` AS `id_empresa`,`pagos`.`id_pago` AS `id`,`pagos`.`fecha` AS `fecha`,'Pago' AS `tipo`,'cliente' AS `clase`,`pagos`.`total` AS `total` from `pagos`) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `empresa_comprobante`
--
DROP TABLE IF EXISTS `empresa_comprobante`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `empresa_comprobante`  AS  select `comprobantes`.`id_comprobante` AS `id_comprobante`,`comprobantes`.`pago` AS `pago`,`comprobantes`.`id_empresa` AS `id_empresa`,`comprobantes`.`id_empresa_dest` AS `id_empresa_dest`,`comprobantes`.`tipo_comprobante` AS `tipo_comprobante`,`comprobantes`.`nro` AS `nro`,`comprobantes`.`fecha` AS `fecha`,`comprobantes`.`iva` AS `iva`,`comprobantes`.`total` AS `total`,`comprobantes`.`observaciones` AS `observaciones`,`comprobantes`.`estado` AS `estado`,`comprobantes`.`typo` AS `typo`,`empresas`.`id` AS `id`,`empresas`.`cuit` AS `cuit`,`empresas`.`tipo` AS `tipo`,`empresas`.`razonSocial` AS `razonSocial`,`empresas`.`actividad` AS `actividad`,`empresas`.`clase` AS `clase`,`empresas`.`domicilio` AS `domicilio`,`empresas`.`telefono` AS `telefono`,`empresas`.`telefono2` AS `telefono2`,`empresas`.`celular` AS `celular`,`empresas`.`representante` AS `representante`,`empresas`.`tel_representante` AS `tel_representante`,`empresas`.`otros` AS `otros` from (`comprobantes` join `empresas`) where (`comprobantes`.`id_empresa` = `empresas`.`id`) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `movimiento_cuenta_empresa`
--
DROP TABLE IF EXISTS `movimiento_cuenta_empresa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `movimiento_cuenta_empresa`  AS  select `movimientos`.`id` AS `id`,`movimientos`.`id_banco` AS `id_banco`,`movimientos`.`cuenta` AS `cuenta`,`movimientos`.`movimiento` AS `movimiento`,`movimientos`.`concepto` AS `concepto`,`movimientos`.`valor` AS `valor`,`movimientos`.`fecha` AS `fecha`,`movimientos`.`tipo` AS `tipo`,`movimientos`.`observaciones` AS `observaciones`,`cuenta_empresa`.`razonSocial` AS `razonSocial`,`cuenta_empresa`.`actividad` AS `actividad`,`cuenta_empresa`.`Banco` AS `Banco`,`concepto`.`concepto` AS `concepto_detalle`,`concepto`.`codigo` AS `codigo` from ((`movimientos` join `cuenta_empresa`) join `concepto`) where ((`cuenta_empresa`.`id_banco` = `movimientos`.`id_banco`) and (`cuenta_empresa`.`nro_cuenta` = `movimientos`.`cuenta`) and (`concepto`.`id_concepto` = `movimientos`.`concepto`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `pagos_empresas`
--
DROP TABLE IF EXISTS `pagos_empresas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pagos_empresas`  AS  select `pagos`.`id_pago` AS `id_pago`,`pagos`.`id_empresa` AS `id_empresa`,`pagos`.`fecha` AS `fecha`,`pagos`.`total` AS `total`,`pagos`.`observaciones` AS `observaciones`,`empresas`.`razonSocial` AS `razonSocial` from (`pagos` join `empresas` on((`pagos`.`id_empresa` = `empresas`.`id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cheques`
--
ALTER TABLE `cheques`
  ADD PRIMARY KEY (`banco`,`cuenta`,`nro`);

--
-- Indices de la tabla `cheques_comprobantes`
--
ALTER TABLE `cheques_comprobantes`
  ADD PRIMARY KEY (`id_comprobante`,`banco`,`cuenta`,`nro`);

--
-- Indices de la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  ADD PRIMARY KEY (`id_comprobante`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `concepto`
--
ALTER TABLE `concepto`
  ADD PRIMARY KEY (`id_concepto`),
  ADD UNIQUE KEY `concepto` (`concepto`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`nro_cuenta`,`id_banco`);

--
-- Indices de la tabla `detalle_comprobante`
--
ALTER TABLE `detalle_comprobante`
  ADD PRIMARY KEY (`id_comprobante`,`id_detalle`);

--
-- Indices de la tabla `detalle_pago`
--
ALTER TABLE `detalle_pago`
  ADD PRIMARY KEY (`id_pago`,`id_detalle`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `razonSocial` (`razonSocial`),
  ADD KEY `cuit` (`cuit`) USING BTREE;

--
-- Indices de la tabla `impresion`
--
ALTER TABLE `impresion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orden_comprobante`
--
ALTER TABLE `orden_comprobante`
  ADD PRIMARY KEY (`id_comprobante`,`id_orden`);

--
-- Indices de la tabla `orden_pago`
--
ALTER TABLE `orden_pago`
  ADD PRIMARY KEY (`id_orden`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id_pago`);

--
-- Indices de la tabla `pagos_comprobantes`
--
ALTER TABLE `pagos_comprobantes`
  ADD PRIMARY KEY (`id_pago`,`id_comprobante`);

--
-- Indices de la tabla `temp_comprobantes`
--
ALTER TABLE `temp_comprobantes`
  ADD PRIMARY KEY (`id_comprobante`);

--
-- Indices de la tabla `temp_conciliacion`
--
ALTER TABLE `temp_conciliacion`
  ADD PRIMARY KEY (`banco`,`cuenta`,`nro`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`nickname`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  MODIFY `id_comprobante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `concepto`
--
ALTER TABLE `concepto`
  MODIFY `id_concepto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `orden_pago`
--
ALTER TABLE `orden_pago`
  MODIFY `id_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `temp_comprobantes`
--
ALTER TABLE `temp_comprobantes`
  MODIFY `id_comprobante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  ADD CONSTRAINT `comprobantes_ibfk_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresas` (`id`);

--
-- Filtros para la tabla `detalle_comprobante`
--
ALTER TABLE `detalle_comprobante`
  ADD CONSTRAINT `detalle_comprobante_ibfk_1` FOREIGN KEY (`id_comprobante`) REFERENCES `comprobantes` (`id_comprobante`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
