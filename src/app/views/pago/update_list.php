<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="text-center container">
	<?php echo $mensaje;?>
	<h1>Modificar Pago</h1>
	<div class="alert alert-dark" >
		<h4>Razon Social: <b><?=$empresa[0]->razonSocial;?></b>    CUIT: <?=$empresa[0]->cuit;?></h4>		
	</div>
	
	<a href= "<?=base_url();?>index.php/pagos/update_pay/<?=$id_pago;?>/new_facture/">
			<button type="button" class="btn btn-success">
				<i class="fas fa-file-invoice"></i> Agregar Facturas
			</button>
	</a>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>                  	                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
						OP
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th>                  	                  
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
                    	OP
                    </th> 
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0;
						foreach ($pagos_comprobantes as $row) 
						{
							$comprobante = $this->Comprobantes_mdl->consulta("id_comprobante='".$row->id_comprobante."'");
							echo "<tr>
									<td>".$row->id_comprobante."</td>
									<td>".$comprobante[0]->tipo_comprobante."</td>
									<td>".$comprobante[0]->nro."</td>
									<td>".invierte_fecha($comprobante[0]->fecha)."</td>
									<td>".number_format($comprobante[0]->total,2,",",".")."</td>
									<td>".number_format($comprobante[0]->saldo,2,",",".")."</td>
									<td>
										<a href='".base_url()."index.php/pagos/update_pay/".$id_pago."/delete_facture/".
											$comprobante[0]->id_comprobante."/'   title='Eliminar, Factura NRO:".
											$comprobante[0]->nro."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>
									</td>";
							$total += $comprobante[0]->saldo;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>          
          
        </div>
        <p>Total saldo de comprobantes: <b>$<?=number_format($total,2,",",".");?></b> </p>
        
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"update","name"=>"update"));?>
		<input type="hidden" name="id_empresa" id="id_empresa" value="<?=$empresa[0]->id;?>"/>
		<input type="hidden" name="id_pago" id="id_pago"  value="<?php echo ($pago[0]->id_pago);?>"/>
		
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  				</div>
  				<input type="date" class="form-control" placeholder="ingrese fecha" id="fecha" name="fecha" 
  						value="<?php echo ($pago[0]->fecha);?>" required/>  					
			</div>
			<div class="form-group">	
				<label>Observaciones</label>
				<textarea class="form-control" id="observaciones" name="observaciones"><?php echo ($pago[0]->observaciones);?></textarea> 	
			</div>
		</div>
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >				
						<option <?php if($pago[0]->estado=="realizado"){echo "selected";}?>>realizado</option>
						<option <?php if($pago[0]->estado=="cancelado"){echo "selected";}?>>cancelado</option>
						<option <?php if($pago[0]->estado=="pendiente"){echo "selected";}?>>pendiente</option>
					</select>
				</div>
		<div class="container-fluid">
			
		<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de pagos
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Tipo</th>
                    <th>Detalle</th>
                    <th>Cantidad</th>                                        
                    <th title="Realizar operaciones con los diferentes pagos">Operar</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>id</th>
                    <th>Tipo</th>
                    <th>Detalle</th>
                    <th>Cantidad</th>                                        
                    <th title="Realizar operaciones con los diferentes pagos">Operar</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php $cont = null;
                		foreach ($detalle as $row) 
                		{
                			$row->cantidad = round($row->cantidad,2);                			
							echo "<tr>
									<td>".$row->id_detalle."</td>
									<td>".estado_detalles($row->tipo,$row->id_detalle)."</td>
									<td><input type=\"text\" class=\"form-control\" placeholder=\"Detalle de pago\" 
  										id=\"detalle".$row->id_detalle."\" name=\"detalle".$row->id_detalle."\" 
  										value=\"".$row->detalle."\"></td>
									<td><input type=\"text\" class=\"form-control\" placeholder=\"Cantidad\" 
  										id=\"cantidad".$row->id_detalle."\" name=\"cantidad".$row->id_detalle."\" 
  										value=\"".$row->cantidad."\"></td>																	
									<td>										
										<a href='".base_url()."index.php/pagos/delete_pay/".$row->id_pago."/".$row->id_detalle."/'
											  title='Eliminacion detalle NRO:".$row->id_detalle."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a> 										
									</td>
								  </tr>";
							$cont += $row->cantidad;
						}
						
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        	<p><b>Total: </b>$<?=number_format($cont,2,",",".");?></p>
        </div>
        <?=btn_ocultar("Agregar Pagos","opc",'	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo105" name="tipo105" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle105" name="detalle105" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad105" name="cantidad105" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo106" name="tipo106" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle106" name="detalle106" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad106" name="cantidad106" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo107" name="tipo107" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle107" name="detalle107" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad107" name="cantidad107" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo108" name="tipo108" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle108" name="detalle108" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad108" name="cantidad108" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo109" name="tipo109" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle109" name="detalle109" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad109" name="cantidad109" />  					
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo1010" name="tipo1010" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle1010" name="detalle1010" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad1010" name="cantidad1010" />  					
			</div>
		</div>'); ?>
        
		<button type="submit" class="btn btn-primary">
			<i class="fas fa-pen"></i> Actualizar
		</button>
	<?=form_close()?>
		<hr /><br /><br /><br /><br />
</div>