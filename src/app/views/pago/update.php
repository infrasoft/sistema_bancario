<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
//print_r($detalle);
?>
<div class="text-center container">
	<?php echo $mensaje;?>
	<h1>Registrar nuevo pago</h1>
	<div class="alert alert-dark" >
		<h4>Razon Social: <b><?=$empresa[0]->razonSocial;?></b>    CUIT: <?=$empresa[0]->cuit;?></h4>		
	</div>
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new"));?>
		
		
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo<?php echo ($detalle[0]->id_detalle);?>" name="tipo<?php echo ($detalle[0]->id_detalle);?>" >
					<option <?php if($detalle[0]->tipo == "efectivo") {echo "selected";}?>>efectivo</option>
					<option <?php if($detalle[0]->tipo == "transferencia bancaria") {echo "selected";}?>>transferencia bancaria</option>	
					<option <?php if($detalle[0]->tipo == "cheque") {echo "selected";}?>>cheque</option>	
					<option <?php if($detalle[0]->tipo == "credito") {echo "selected";}?>>credito</option>
					<option <?php if($detalle[0]->tipo == "otros") {echo "selected";}?>>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" placeholder="Detalle de pago" 
  					id="detalle<?php echo ($detalle[0]->id_detalle);?>" name="detalle<?php echo ($detalle[0]->id_detalle);?>" 
  					value="<?php echo ($detalle[0]->detalle);?>"/>  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad<?php echo ($detalle[0]->id_detalle);?>" 
  					name="cantidad<?php echo ($detalle[0]->id_detalle);?>" value="<?php echo ($detalle[0]->cantidad);?>" required/>  					
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo1" name="tipo1" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle1" name="detalle1" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad1" name="cantidad1" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo2" name="tipo2" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle2" name="detalle2" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad2" name="cantidad2"/>  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo3" name="tipo3" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle3" name="detalle3" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad3" name="cantidad3" />  					
			</div>
		</div>
		<div >
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo4" name="tipo4" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle4" name="detalle4" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad4" name="cantidad4" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo5" name="tipo5" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle5" name="detalle5" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad5" name="cantidad5" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo6" name="tipo6" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle6" name="detalle6" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad6" name="cantidad6" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo7" name="tipo7" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle7" name="detalle7" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad7" name="cantidad7" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo8" name="tipo8" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle8" name="detalle8" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad8" name="cantidad8" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo9" name="tipo9" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle9" name="detalle9" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad9" name="cantidad9" />  					
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo10" name="tipo10" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle10" name="detalle10" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad10" name="cantidad10" />  					
			</div>
		</div>	
		
		<button type="submit" class="btn btn-primary">
 			<i class="fas fa-plus-circle"></i> Registrar pago
 		</button>
 		</div>						
	<?=form_close()?>
	<hr /><br /><br /><br /><br />
</div> 