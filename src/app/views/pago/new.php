<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
$fecha_actual = date("d-m-Y");
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
?>
<div class="text-center container">
	<?php echo $mensaje;?>
	<h1>Registrar nuevo pago</h1>
	<div class="alert alert-dark" >
		<h4>Razon Social: <b><?=$empresa[0]->razonSocial;?></b>    CUIT: <?=$empresa[0]->cuit;?></h4>			
	</div>
	<?php //print_r($_SESSION["lista"]); ?>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>                  	                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th>                  	                  
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                     
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total_facturas = 0; $total_saldo = 0; $total_nota =0;
                	
                		foreach ($_SESSION["lista"] as $row) 
                		{
                			$row["total"] = round($row["total"],2);
                			$row["saldo"] = round($row["saldo"],2);												
							echo "<tr>
									<td>".$row["id_comprobante"]."</td>																
									<td>".$row["tipo_comprobante"]."</td>
									<td>".$row["nro"]."</td>
									<td>".$row["fecha"]."</td>
									<td>".$row["total"]."</td>
									<td>".$row["saldo"]."</td>									
									"; 
							
							if ($row["tipo_comprobante"] == "Notas de crédito") 
							{
								$total_nota += $row["saldo"];	
							} else {
								$total_facturas += $row["total"]; 
								$total_saldo += $row["saldo"];
							}
							 
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>          
          
        </div>
         <p>Total: $<?=$total_facturas;?></p>  
         <p>Total Saldo Pendiente: $<?=$total_saldo;?></p>
         <p>Total de nota de credito: $<?=$total_nota;?></p>
         <p>Total saldo - nota de credito: <?=$total_saldo - $total_nota;?></p>
        <hr />
	
	<?php echo form_open( base_url().'index.php/pagos/new_pay/'.$empresa[0]->id.'/2/', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new"));?>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  				</div>
  				<input type="date" class="form-control" placeholder="ingrese fecha" id="fecha" name="fecha" 
  					value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>" required/>  					
			</div>
			<div class="form-group">	
				<label>Observaciones</label>
				<textarea class="form-control" id="observaciones" name="observaciones"></textarea> 	
			</div>
			<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Empresa</span>
  					</div>  					
  					<select class="form-control" id="id_empresa_dest" name="id_empresa_dest" >
  						<?php
  						foreach ($cuentas as $row) 
  						{							
							if ($_SESSION["id_empresa_dest"] == $row->id_empresa) 
							{
								echo "<option value='".$row->id_empresa."' selected>".$row->razonSocial."</option>";	
							} else {
								echo "<option value='".$row->id_empresa."'>".$row->razonSocial."</option>";
							}
							
						}
  						?>
  					</select>	
				</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo0" name="tipo0" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle0" name="detalle0" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad0" name="cantidad0" required/>  					
			</div>
		</div> 
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo1" name="tipo1" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle1" name="detalle1" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad1" name="cantidad1" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo2" name="tipo2" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle2" name="detalle2" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad2" name="cantidad2"/>  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo3" name="tipo3" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle3" name="detalle3" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad3" name="cantidad3" />  					
			</div>
		</div>
		<div >
		<?=btn_ocultar("Mas Opciones","opc",'<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo4" name="tipo4" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle4" name="detalle4" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad4" name="cantidad4" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo5" name="tipo5" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle5" name="detalle5" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad5" name="cantidad5" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo6" name="tipo6" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle6" name="detalle6" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad6" name="cantidad6" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo7" name="tipo7" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle7" name="detalle7" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad7" name="cantidad7" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo8" name="tipo8" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle8" name="detalle8" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad8" name="cantidad8" />  					
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo9" name="tipo9" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle9" name="detalle9" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad9" name="cantidad9" />  					
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo10" name="tipo10" >
					<option >efectivo</option>
					<option >transferencia bancaria</option>	
					<option>cheque</option>	
					<option>credito</option>
					<option>otros</option>				
				</select>
			</div>
				
			<div class="input-group mb-3">
	  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Detalle</span>
  				</div>
  				<input type="text" class="form-control" 
  					placeholder="Detalle de pago" id="detalle10" name="detalle10" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Cantidad" id="cantidad10" name="cantidad10" />  					
			</div>
		</div>'); ?>	
		
		<button type="submit" class="btn btn-primary">
 			<i class="fas fa-plus-circle"></i> Registrar pago
 		</button>
 		</div>						
	<?=form_close()?>
	<hr /><br /><br /><br /><br />
</div> 