<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container">
	<?php echo $mensaje;?>
	<h1>Generar nueva orden de pago</h1>
	<h3>Seleccione la empresa</h3>
	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"company_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">        	
        	    <option>id</option>
        	    <option value="razonSocial">Razon Social</option>
        	    <option>fecha</option>
        	    <option>total</option>    	
        </select>
      </div>
      <button type="submit" class="btn btn-primary">
      	<i class="fas fa-search"></i> Buscar
      </button>
<?=form_close();?>
	<a href="<?=base_url();?>index.php/empresas/company_list/list/" alt="Realiza la busqueda avanzada">
		<p>Busqueda Avanzada</p>
	</a>
	</div>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de pagos
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Razon Social</th>
                    <th>Fecha</th>
                    <th>Total</th>                                        
                    <th title="Realizar operaciones con los diferentes pagos">Operar</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>id</th>
                    <th>Razon Social</th>
                    <th>Fecha</th>
                    <th>Total</th>                                        
                    <th title="Realizar operaciones con los diferentes pagos">Operar</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php $cont = null;
                		foreach ($lista as $row) 
                		{
                			$row->total = round($row->total,2);                			
							echo "<tr>
									<td>".$row->id_pago."</td>
									<td>".$row->razonSocial."</td>
									<td>".invierte_fecha($row->fecha)."</td>
									<td>".number_format($row->total,2,",",".")."</td>																	
									<td>
										<a href='".base_url()."index.php/pagos/update_pay/".$row->id_pago."/' title='Modificar Datos del Pago'>
											<i class='fas fa-address-book'></i>
										</a> -
										<a href='".base_url()."index.php/imprimir/pay/".$row->id_pago."/'
											title='Imprimir Comprobante Nro: ".$row->id_pago."' target='_blank'>
											<i class='far fa-file-pdf' style='color:red;'></i>
										</a> - 
										<a href='".base_url()."index.php/pagos/delete_pay_complete/".$row->id_pago."/'
											  title='Eliminacion de pago NRO:".$row->id_pago."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>										
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
		
</div>