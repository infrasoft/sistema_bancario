<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="container text-center">	
	<?php echo $mensaje; ?>
	<h1>Modificar Concepto</h1>
	<div class="container-fluid">
		<div class="alert alert-dark" >
			<h4>Concepto Nº: <b><?php echo $vector[0]->id_concepto;?></b>	</h4>
		</div>
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Concepto</span>
  					</div>
  					<input type="text" class="form-control" placeholder="ingrese el concepto" id="concepto" name="concepto" 
  					  value="<?=$vector[0]->concepto;?>" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Codigo</span>
  					</div>
  					<input type="text" class="form-control" placeholder="ingrese el codigo" id="codigo" name="codigo" 
  						value="<?=$vector[0]->codigo;?>" required/>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Descripcion</span>
  					</div>
  					<textarea class="form-control" id="descripcion" name="descripcion"><?=$vector[0]->descripcion;?></textarea>
				</div>
				
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-pen"></i> Actualizar
				</button>
			</div>
		<?=form_close();?>
	</div><br/><br/><hr />
</div>