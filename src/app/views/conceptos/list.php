<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>

<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Lista de Conceptos</h1>

	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="lista_venta" name="lista_venta">
        	<option value="id_concepto">id</option>
        	<option >concepto</option>
        	<option >codigo</option>        	
        	<option>descripcion</option>
        	 	
        </select>
      </div>
      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
<?=form_close();?>
	<div class="text-center">
		<a href="<?=base_url();?>index.php/conceptos/new_concept/">
			<button class="btn btn-primary">
				Nuevo concepto
			</button>
		</a>
	</div>
	
	</div>

<div class="container" id="content-wrapper">
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Conceptos
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>                    
                    <th>Concepto</th>
                    <th>codigo</th>
                    <th>Descripcion</th>                   
                    <th>OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th>                    
                    <th>Concepto</th>
                    <th>codigo</th>
                    <th>Descripcion</th>                   
                    <th>OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		foreach ($lista as $row) 
                		{
							echo "<tr>
									<td>".$row->id_concepto."</td>									
									<td>".$row->concepto."</td>
									<td>".$row->codigo."</td>
									<td>".$row->descripcion."</td>																		
									<td>
										<a href='".base_url()."index.php/conceptos/update_concept/".$row->id_concepto."/' title='Modificar Datos'>
											<i class='fas fa-address-book'></i>
										</a> - 
										<a href='".base_url()."index.php/conceptos/delete_concept/".$row->id_concepto."/'
											  title='Eliminacion del concepto NRO:".$row->id_concepto."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>  
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>
</div>