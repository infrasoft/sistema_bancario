<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="container text-center">	
	<?php echo $mensaje; ?>
	<h1>Nuevo Concepto</h1>
	<div class="container-fluid">
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Concepto</span>
  					</div>
  					<input type="text" class="form-control" placeholder="ingrese el concepto" id="concepto" name="concepto" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Codigo</span>
  					</div>
  					<input type="text" class="form-control" placeholder="ingrese el codigo" id="codigo" name="codigo" required/>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Descripcion</span>
  					</div>
  					<textarea class="form-control" id="descripcion" name="descripcion"></textarea>
				</div>
				
				<button type="submit" class="btn btn-primary">
  					<i class="far fa-plus-square"></i> Registrar
  				</button>
			</div>
		<?=form_close();?>
	</div><br/><br/><hr />
</div>