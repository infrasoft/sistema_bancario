<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Reporte de empresa</h1>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de empresas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>                    
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Referencias</th>
                    <th>Debe</th>
                    <th>Haber</th>
                    <th>Saldo</th>                    
                    <th title="Realizar operaciones con las diferentes empresas">Operar</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Referencias</th>
                    <th>Debe</th>
                    <th>Haber</th>
                    <th>Saldo</th>                    
                    <th title="Realizar operaciones con las diferentes empresas">Operar</th>
                  </tr>
                </tfoot>
                <tbody>
                	
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>