<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" >
	<?php echo $mensaje; ?>
	<h1>Eliminar Empresa</h1>
	<p>¿Realmente desea eliminar la empresa?  . Una vez realizado el proceso. No sería posible recuperar los datos eliminados</p>
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"delete","name"=>"delete"));?>
		<a href="<?=base_url();?>index.php/empresas/">
			<button type="button" class="btn btn-primary">
				<i class="fas fa-list-ul"></i> Volver a Lista de Empresas
			</button>
		</a>
		<button type="submit" class="btn btn-danger" id="delete" name="delete" value="delete" >
			<i class="far fa-trash-alt"></i> Eliminar empresas
		</button>
	<?=form_close();?>
</div>