<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
foreach ($vector as $row) 
{
?>
<div class ="container text-center">
	<?php echo $mensaje;?>
	<h1>Registro de empresas</h1>
	<div class="card-body">
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">CUIT</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de CUIT" id="cuit" name="cuit" value="<?php echo $row->cuit; ?>"/>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Razon Social</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese  la razon social" id="razonSocial" name="razonSocial" value="<?php echo $row->razonSocial; ?>" required/>
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  			</div>
  			<select class="form-control" id="tipo" name="tipo" >
				<option <?php if($row->tipo =="Responsable Inscripto"){ echo "selected";} ?>>Responsable Inscripto</option>
				<option <?php if($row->tipo =="Responzable Monotributo"){ echo "selected";} ?>>Responzable Monotributo</option>
				<option <?php if($row->tipo =="Consumidor Final"){ echo "selected";} ?>>Consumidor Final</option>				
				<option <?php if($row->tipo =="Otros"){ echo "selected";} ?>>Otros</option>
			</select>
		</div>			
	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Actividad</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese la Actividad" id="actividad" name="actividad" value="<?php echo $row->actividad; ?>" required/>
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  			</div>
  			<select class="form-control" id="clase" name="clase" >
				<option <?php if($row->clase =="banco"){ echo "selected";} ?>>banco</option>
				<option <?php if($row->clase =="cliente"){ echo "selected";} ?>>cliente</option>
				<option <?php if($row->clase =="empresa"){ echo "selected";} ?>>empresa</option>
				<option <?php if($row->clase =="proveedor"){ echo "selected";} ?>>proveedor</option>
				<option <?php if($row->clase =="otros"){ echo "selected";} ?>>otros</option>
			</select>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Domicilio</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Domicilio" id="domicilio" name="domicilio" value="<?php echo $row->domicilio; ?>" />
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de Telefono" id="telefono" name="telefono" value="<?php echo $row->telefono; ?>"/>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono 2</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de Telefono" id="telefono2" name="telefono2" value="<?php echo $row->telefono2; ?>"/>
		</div>
	</div> 
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Celular</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de Celular" id="celular" name="celular" value="<?php echo $row->celular; ?>"/>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Reprecentante</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el reprecentante" id="representante" name="representante" value="<?php echo $row->representante; ?>"/>
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono Reprecentante</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Telefono del reprecentante" id="tel_representante" name="tel_representante" value="<?php echo $row->tel_representante; ?>"/>
		</div>
	
		<input type="hidden" id="verificar" name="verificar" value="si"/>
			
		<div class="input-group">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Otros Datos</span>
  			</div>
  			<textarea class="form-control" aria-label="Otros Datos" id="otros" name="otros"><?php echo $row->otros; ?></textarea>
		</div>
	</div>
	<div class="form-group">	
		<button type="submit" class="btn btn-primary">
			<i class="fas fa-pen"></i> Actualizar
		</button>		
	</div>
        <?=form_close();?>
 </div> <br/> <hr />
</div>

<?php } ?>