<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class ="container text-center">
	<?php echo $mensaje;?>
	<h1><?php echo $titulo;?></h1>
	<div class="card-body">
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">CUIT</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de CUIT" id="cuit" name="cuit" required/>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Razon Social</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese  la razon social" id="razonSocial" name="razonSocial" required/>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  			</div>
  			<select class="form-control" id="tipo" name="tipo" >
				<option>Responsable Inscripto</option>
				<option>Responzable Monotributo</option>
				<option>Consumidor Final</option>				
				<option>Otros</option>
			</select>
		</div>			
	</div>	
	
	<div  class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Actividad</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese la Actividad" id="actividad" name="actividad"  required/>
		</div>
		
		<div class=" d-none">
		<div class="input-group mb-3 hidden">
  			<div class="input-group-prepend">
    			<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  			</div>
  			<select class="form-control" id="clase" name="clase" >
				<option <?php if($clase=="banco"){ echo "selected";}?>>banco</option>
				<option <?php if($clase=="cliente"){ echo "selected";}?>>cliente</option>
				<option <?php if($clase=="empresa"){ echo "selected";}?>>empresa</option>
				<option <?php if($clase=="proveedor"){ echo "selected";}?>>proveedor</option>
				<option <?php if($clase=="Otros"){ echo "selected";}?>>otros</option>
			</select>
		</div>		
		</div>
	</div> 
	
	<div  class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Domicilio</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Domicilio" id="domicilio" name="domicilio" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de Telefono" id="telefono" name="telefono" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono 2</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de Telefono" id="telefono2" name="telefono2" />
		</div>
	</div> 
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Celular</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el nro de Celular" id="celular" name="celular" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Reprecentante</span>
  			</div>
  			<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el reprecentante" id="representante" name="representante" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono Reprecentante</span>
  			</div>
  			<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  			placeholder="Telefono del reprecentante" id="tel_representante" name="tel_representante" />
		</div>
	</div>	
	<div class="form-group">	
		<div class="input-group">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Otros Datos</span>
  			</div>
  			<textarea class="form-control" aria-label="Otros Datos" id="otros" name="otros"></textarea>
		</div>
				
		<button type="submit" class="btn btn-primary">
			<i class="fas fa-user-plus"></i> Registrar
		</button>
	</div>
        <?=form_close();?>
 </div> <br/> <hr />
</div>