<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class ="container text-center">
	<?php echo $mensaje;?>
	<h1>Busqueda Avanzada</h1>
	
	<div class="card-body">
	<?php echo form_open(base_url()."index.php/empresas/company_list/", 
							array("class"=>"form-inline","role"=>"form", "id"=>"seach","name"=>"seach"));?>
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">
    				Cantidad de registros a buscar
    			</span>
  			</div>
  			<input type="tel" class="form-control" value="100" id="cant_reg" name="cant_reg" required>
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >CUIT</span>
  			</div>
  			<input type="tel" class="form-control" placeholder="Ingrese el nro de CUIT" 
  			id="cuit" name="cuit" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >Razon Social</span>
  			</div>
  			<input type="text" class="form-control" placeholder="Ingrese la razon social"
  				 id="razonSocial" name="razonSocial" />
		</div>
	</div>	
	<div  class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<label class="input-group-text" >Tipo</label>
  			</div>
  			<select class="form-control" id="tipo" name="tipo" >
  				<option></option>
				<option >Responsable Inscripto</option>
				<option >Responzable Monotributo</option>
				<option >Consumidor Final</option>				
				<option >Otros</option>
			</select>
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Actividad</span>
  			</div>
  			<input type="text" class="form-control"	placeholder="Ingrese la Actividad"
  				 id="actividad" name="actividad" />
		</div>
	</div>	
	<div  class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<label class="input-group-text" >Clase</label>
  			</div>
  			<select class="form-control" id="clase" name="clase" >
  				<option></option>
				<option>banco</option>
				<option>cliente</option>
				<option>empresa</option>
				<option>proveedor</option>
				<option>otros</option>
			</select>
		</div>
			
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Domicilio</span>
  			</div>
  			<input type="text" class="form-control" placeholder="Domicilio"
  				 id="domicilio" name="domicilio" />
		</div>
	</div>	
	<div  class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono</span>
  			</div>
  			<input type="tel" class="form-control" placeholder="Ingrese el nro de Telefono" 
  				id="telefono" name="telefono" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Telefono 2</span>
  			</div>
  			<input type="tel" class="form-control" placeholder="Ingrese el nro de Telefono" 
  				id="telefono2" name="telefono2" />
		</div>
	</div> 
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Celular</span>
  			</div>
  			<input type="tel" class="form-control" placeholder="Ingrese el nro de Celular" 
  				id="celular" name="celular" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >Reprecentante</span>
  			</div>
  			<input type="text" class="form-control" placeholder="Ingrese el reprecentante" 
  				id="representante" name="representante" />
		</div>
	</div>	
	<div  class="form-group">	
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >Telefono Reprecentante</span>
  			</div>
  			<input type="tel" class="form-control" placeholder="Telefono del reprecentante" 
  				id="tel_representante" name="tel_representante" />
		</div>
		
		<div class="input-group">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Otros Datos</span>
  			</div>
  			<textarea class="form-control" aria-label="Otros Datos" id="otros" name="otros"></textarea>
		</div>
	</div>	
	<div  class="form-group">
		<button type="submit" class="btn btn-primary" id="search" name="search" value="true">
			<i class="fas fa-search"></i> Buscar
		</button>				
	</div>
        <?=form_close();?>
 </div> <br/> <hr />
</div>