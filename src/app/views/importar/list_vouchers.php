<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

?>
<div class="container-fluid" id="content-wrapper">
	<?php echo $mensaje;?>
		<div class="text-center">
	    	<span id="none" class="spinner-border text-secondary"></span>
		</div>

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered d-none" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	
                  	<th>Razon Social</th>   
                  	<th>Empresa</th>                 
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>Pago</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>   
                    
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	
                  	<th>Razon Social</th>
                  	<th>Empresa</th>                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>Pago</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>                   
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0; $saldo = 0; $saldo_neg=""; $total_n_credito=0;
                		foreach ($lista as $row) 
                		{
                      $n=count($row)-1;
                      for($i=1; $i <= $n; $i++)
                		  {
                			  $fact = $this->Empresas_mdl->consulta("id='".$row[$i][3]."'");
							          if ( $row[$i][10] >0) 
							          {
								          $saldo_neg = "class = 'text-danger'";
							          }
							          else {
								          $saldo_neg = "";
							          }
							          $row[$i][9] = round($row[$i][9],2); //total
							          $row[$i][10] = round($row[$i][10],2); //saldo
							          echo "<tr ".$saldo_neg.">									
									            <td>".$row[$i][17]."</td>
									            <td>".$fact[0]->razonSocial."</td>									
									            <td>".$row[$i][13]."</td>
									            <td>".$row[$i][6]."</td>
									            <td>".$row[$i][1]."</td>
									            <td>".invierte_fecha($row[$i][7])."</td>
									            <td>".number_format($row[$i][9],2,",",".")."</td>
									            <td>".number_format($row[$i][10],2,",",".")."</td>									
													  </tr>";
                      }
                    }	
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>       
        </div>       
    
    <p>Total: <b>$<?=number_format($total,2,",",".");?></b></p>

        

        <a href="<?=base_url()?>index.php/importar/data_enter/vouchers/true/" title="Volver a menu principal">
                    <button class="btn btn-danger"><i class="fas fa-undo"></i> Volver</button>
        </a>
        <a href="<?=base_url()?>index.php/importar/process_upload_data/vouchers/" title="Realizar la importacion de datos">
            <button class="btn btn-primary" ><i class="fas fa-file-import"></i> Importar datos</button>
        </a>
</div>