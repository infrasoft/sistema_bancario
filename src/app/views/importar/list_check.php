<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

?>
<div class="container-fluid" id="content-wrapper">
	<?php echo $mensaje;?>
		<div class="text-center">
	    	<span id="none" class="spinner-border text-secondary"></span>
		</div>
		<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered d-none" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Vencimiento</th>
                    <th>A la orden </th>
                    <th>Importe</th>                    
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Vencimiento</th>
                    <th>A la orden </th>
                    <th>Importe</th>                    
                  </tr>
                </tfoot>
                <tbody>
                	<?php // print_r($lista);
                		$total = 0; 
                	
                		foreach ($lista as $row) 
                		{	$n=count($row)-1;
                		    for($i=0; $i <= $n; $i++)
                		    {//             		    print_r($row[$i][$i]); 
                		        $cantidad = round($row[$i][13],2);
                		        if($row[$i][8] == "anulado" OR $row[$i][8] == "cancelado")
							    {
							        echo "<tr class='text-danger'>
									<td><b>".$row[$i][0]."</b></td>
									<td><b>".$row[$i][1]."</b></td>
									<td><b>".$row[$i][6]."</b></td>									
									<td><b>".invierte_fecha($row[$i][10])."</b></td>
									<td><b>".invierte_fecha($row[$i][11])."</b></td>
									<td><b>".$row[$i][8]."</b></td>
									<td><b>".number_format($cantidad,2,",",".")."</b>";
							    }else
							    {
								    if ($row[$i][8] == "conciliados") 
								    {
									    echo "<tr class='text-primary'>
    										<td><b>".$row[$i][0]."</b></td>
	    									<td><b>".$row[$i][1]."</b></td>
		    								<td><b>".$row[$i][6]."</b></td>									
			    							<td><b>".invierte_fecha($row[$i][10])."</b></td>
									        <td><b>".invierte_fecha($row[$i][11])."</b></td>
									        <td><b>".$row[$i][8]."</b></td>
						    				<td><b>".number_format($cantidad,2,",",".")."</b>";
								    }
								    else
								    {
									    echo "<tr>
										    <td>".$row[$i][0]."</td>
										    <td>".$row[$i][1]."</td>
										    <td>".$row[$i][6]."</td>									
										    <td><b>".invierte_fecha($row[$i][10])."</b></td>
									        <td><b>".invierte_fecha($row[$i][11])."</b></td>
									        <td><b>".$row[$i][8]."</b></td>
										    <td>".number_format($cantidad,2,",",".");
								    }
                    		    }
                    		    $total += $cantidad;
                    		    echo	"</td>									
									<!--<td>
									    	<a href='".base_url()."index.php/cheques/update_check/".
																				$row[$i][4]."/".
																				$row[$i][5]."/".
																				$row[$i][6]."/'
											     title='Modificar dato de cheque NRO:".$row[$i][6]."'  target='_blank'>
											    <i class='fas fa-money-check'></i>
										    </a> - 
										    <a href='".base_url()."index.php/cheques_comprobantes/link_checks_to_receipts/0/".
													$row[$i][4]."/".	$row[$i][5]."/".$row[$i][6]."/".
													"/2/' title='Vincular a recibo'  target='_blank'>
											    <i class='fas fa-money-check' style='color:green'></i>
										    </a> -
										    <a href='".base_url()."index.php/imprimir/print_cheque/".$row[$i][4]."/".
																				$row[$i][5]."/".
																				$row[$i][6]."/'
											    title='Imprimir Cheque Nro: ".$row[$i][6]."' target='_blank'>
											    <i class='far fa-file-pdf' style='color:red;'></i>
										    </a> - 
										    <a href='".base_url()."index.php/cheques/delete_check/".
																				$row[$i][4]."/".
																				$row[$i][5]."/".
																				$row[$i][6]."/'
											      title='Eliminacion Fisica del cheque NRO:".$row[$i][6]."'  target='_blank'>
											    <i class='far fa-trash-alt' style='color:black;'></i>
										    </a> 
									</td>-->
								        </tr>";
                		    }
                		}
                		
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: <b>$<?=number_format($total,2,",",".");?></b></p>
        <a href="<?=base_url()?>index.php/importar/data_enter/cheques/true/" title="Volver a menu principal">
                    <button class="btn btn-danger"><i class="fas fa-undo"></i> Volver</button>
        </a>
        <a href="<?=base_url()?>index.php/importar/process_upload_data/cheques/" title="Realizar la importacion de datos">
            <button class="btn btn-primary" ><i class="fas fa-file-import"></i> Importar datos</button>
        </a>
</div>	
</div>	
	    