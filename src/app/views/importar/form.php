<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
?>

<div class ="container-fluid text-center">
	<div class="text-left">
		<a href="<?=base_url(); ?>index.php/importar/"> 
			<button class ="btn btn-primary">
				<i class="fa fa-backward"></i>	Volver
			</button>
		</a>
	</div>
	<h1><?= $title;?></h1>
	<?=$mensaje;?>
    <p> Se recomienda realizar una copia de la base de datos previa a realizar el proceso. 
        Luego se proceda a realizar el correspondiente control de los datos ingresados</p>
        
        
	<div class="card-body">
	    
    <?php echo form_open(base_url()."index.php/importar/process_upload/".$session."/", 
							array("class"=>"form-inline","role"=>"form", "id"=>"import_file",
                            "name"=>"import_file","onkeypress"=>"return anular(event)",
							"enctype" => "multipart/form-data"));?>
    <div class="form-group">
        <div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Subir el archivo</span>
  			</div>
  			<input type="file" class="form-control" aria-label="Sizing example input" 
			  aria-describedby="inputGroup-sizing-default"
  			placeholder="Ingrese el archivo .csv" id="file_upload" name="file_upload"
			  accept=".csv" required/>
		</div>
		<div class="input-group mb-3">
			<input type="text" class="form-control" id="comment" name="comment"/>
		</div>
        <button type="submit" class="btn btn-primary" id="send-data">
			<i class="fas fa-file-import"></i>  Subir Archivo
		</button>
    </div>
    <?=form_close();?>
    </div> <br/> <hr />
</div>
