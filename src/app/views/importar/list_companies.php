<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

?>
<div class="container-fluid" id="content-wrapper">
	<?php echo $mensaje;?>
    <div class="text-center">
      <span id="none" class="spinner-border text-secondary"></span>
    </div>

    <!-- DataTables Example -->
    <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de empresas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered d-none" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Razon Social</th>
                    <th>Tipo</th>
                    <th>Clase</th>
                    <th>Telefono</th>
                    <th>domicilio</th>              
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Razon Social</th>
                    <th>Tipo</th>
                    <th>Clase</th>
                    <th>Telefono</th>
                    <th>domicilio</th>                    
                  </tr>
                </tfoot>
                <tbody>
                <?php // print_r($lista);
                	 
                	foreach ($lista as $row) 
                	{	$n=count($row)-1;
                		for($i=0; $i <= $n; $i++)
                	    {
                            echo "<tr>
                                    <td>".$row[$i][3]."</td>
                                    <td>".$row[$i][4]."</td>
                                    <td>".$row[$i][5]."</td>
                                    <td>".$row[$i][7]."</td>
                                    <td>".$row[$i][6]."</td>
                                </tr>";               
                        }
                    }
                ?>   
                </tbody>
                </table> 
            </div>
          </div>   

        <a href="<?=base_url()?>index.php/importar/data_enter/companies/true/" title="Volver a menu principal">
                    <button class="btn btn-danger"><i class="fas fa-undo"></i> Volver</button>
        </a>
        <a href="<?=base_url()?>index.php/importar/process_upload_data/companies/" title="Realizar la importacion de datos">
            <button class="btn btn-primary" ><i class="fas fa-file-import"></i> Importar datos</button>
        </a>

</div>