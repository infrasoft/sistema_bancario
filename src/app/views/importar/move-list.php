<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

?>
<div class="container-fluid" id="content-wrapper">
	<?php echo $mensaje;?>
    <div class="text-center">
      <span id="none" class="spinner-border text-secondary"></span>
    </div>
    <!-- DataTables Example -->
    <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Movimientos Bancarios
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered d-none" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>banco</th>
                  	<th>titular</th>
                    <th>cuenta</th>                   
                    <th>concepto</th>
                    <th>valor</th>
                    <th>fecha</th>               
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>banco</th>
                  	<th>titular</th>
                    <th>cuenta</th>                   
                    <th>concepto</th>
                    <th>valor</th>
                    <th>fecha</th>                    
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0;
                		foreach ($lista as $row) 
                		{
                        $n=count($row)-1;
                        for($i=1; $i <= $n; $i++)
                        {
                          //$cantidad = round($row[$i][13],2);
                          echo "<tr>
									        <td>".$row[$i][11]."</td>
									        <td>".$row[$i][9]."</td>
									        <td>".$row[$i][2]."</td>								
									        <td>".$row[$i][12]."</td>
									        <td>".number_format(floatval($row[$i][5]),2,",",".")."</td>
									        <td>".invierte_fecha($row[$i][6])."</td>	
								          </tr>";
							            $total += floatval($row[$i][5]);
                        }    						
						        }
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: <b>$<? number_format($total,2,",",".");?></b></p>
        <a href="<?=base_url()?>index.php/importar/data_enter/cheques/true/" title="Volver a menu principal">
                    <button class="btn btn-danger"><i class="fas fa-undo"></i> Volver</button>
        </a>
        <a href="<?=base_url()?>index.php/importar/process_upload_data/cheques/" title="Realizar la importacion de datos">
            <button class="btn btn-primary" ><i class="fas fa-file-import"></i> Importar datos</button>
        </a>
</div>