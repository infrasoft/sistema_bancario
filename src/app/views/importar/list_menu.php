<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/
?>
<div class ="container text-center">
	<?php echo $mensaje;?>
	<h1>Lista de Importaciones</h1>
    <div class = "container-fluid">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item">
                <a href="<?=base_url();?>index.php/Importar/data_enter/">
                    <button type="button" class="btn btn-primary btn-sm"> <i class="fas fa-file-import"></i> Cheques</button>
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?=base_url();?>index.php/Importar/data_enter/vouchers/">
                    <button type="button" class="btn btn-primary btn-sm"> <i class="fas fa-file-import"></i> Pagos</button>
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?=base_url();?>index.php/Importar/data_enter/vouchers/">
                    <button type="button" class="btn btn-primary btn-sm"> <i class="fas fa-file-import"></i> Comprobantes</button>
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?=base_url();?>index.php/Importar/data_enter/movements/">
                    <button type="button" class="btn btn-primary btn-sm"> <i class="fas fa-file-import"></i> Movimientos</button>
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?=base_url();?>index.php/Importar/data_enter/companies/">
                    <button type="button" class="btn btn-primary btn-sm"> <i class="fas fa-file-import"></i> Clientes</button>
                </a>
            </li>
            <li class="list-group-item">
                <a href="<?=base_url();?>index.php/Importar/data_enter/companies/">
                    <button type="button" class="btn btn-primary btn-sm"> <i class="fas fa-file-import"></i> Proveedores</button>
                </a>
            </li>
        </ul>
    </div>
</div>