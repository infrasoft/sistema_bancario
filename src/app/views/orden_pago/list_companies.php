<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Generar nueva orden de pago</h1>
	<h3>Seleccione la empresa</h3>
	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"company_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="lista_venta" name="lista_venta">        	
        	    <option>id</option>
        	    <option>cuit</option>
        	    <option selected="">razonSocial</option>
        	    <option>tipo</option>
        	    <option>clase</option>
        	    <option>actividad</option>
        	    <option>domicilio</option>
        	    <option>telefono</option>
        	    <option>telefono2</option>
        	    <option>celular</option>        	    
        	    <option>representante</option>
        	    <option>tel_representante</option>
        	    <option>otros</option>    	
        </select>
      </div>
      <button type="submit" class="btn btn-primary">
      	<i class="fas fa-search"></i> Buscar
      </button>
<?=form_close();?>
	<a href="<?=base_url();?>index.php/empresas/company_list/" alt="Realiza la busqueda avanzada">
		<p>Busqueda Avanzada</p>
	</a>
	</div>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de empresas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Razon Social</th>
                    <th>Tipo</th>
                    <th>Clase</th>
                    <th>Telefono</th>
                    <th>domicilio</th>                    
                    <th title="Realizar operaciones con las diferentes empresas">Operar</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Razon Social</th>
                    <th>Tipo</th>
                    <th>Clase</th>
                    <th>Telefono</th>
                    <th>domicilio</th>                    
                    <th title="Realizar operaciones con las diferentes empresas">Operar</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php $cont = null;
                		foreach ($lista as $row) 
                		{
                			if ($row->clase =="banco") 
                			{
								$cont = "-
										<a href='".base_url()."index.php/cuentas/avanced_seach/".$row->id."/' title='Cuentas vinculadas'>
											<i class='fas fa-money-check'></i>
										</a>";
							}
							else {
								$cont = "-
										<a href='".base_url()."index.php/orden_pago/new_payment_order/1/".$row->id.
											"/' title='Seleccionar Facturas Vinculadas' style='color:orange;'>
											<i class='fas fa-money-check'></i>
										</a>";
							}
							echo "<tr>
									<td>".$row->razonSocial."</td>
									<td>".$row->tipo."</td>
									<td>".$row->clase."</td>
									<td>".$row->telefono."</td>
									<td>".$row->domicilio."</td>									
									<td>
										<a href='".base_url()."index.php/empresas/company_update/".$row->id."/' title='Modificar Datos de la Empresa'>
											<i class='fas fa-address-book'></i>
										</a>".$cont." 										
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>