<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container text-center" id="content-wrapper">
		<?php echo $mensaje; ?>
	<h1>Lista de Ordenes de pago</h1>
	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"company_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">        	
        	    <option>id_orden</option>
        	    <option>fecha</option>        	  
        	    <option>total</option>
        	    <option>estado</option>        	      	
        </select>
      </div>
      <button type="submit" class="btn btn-primary">
      	<i class="fas fa-search"></i> Buscar
      </button>
<?=form_close();?>
	
	</div>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de empresas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Razon Social</th>
                    <th>Empresa</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>estado</th>
                                       
                    <th >Operar</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Razon Social</th>
                    <th>Empresa</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>estado</th>
                                       
                    <th >Operar</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php $cont = 0;
                		foreach ($lista as $row) 
                		{
                			$empresa = $this->Empresas_mdl->consulta("id='".$row->id_empresa."'");
							if ($row->facturacion != "") 
							{
								$facturacion = $this->Empresas_mdl->consulta("id='".$row->facturacion."'");
								$fact = $facturacion[0]->razonSocial;
							}else 
							{
								$fact = "";
							}
							$row->total = round($row->total,2); 							
							echo "<tr>
									<td>".$row->id_orden."</td>
									<td>".$empresa[0]->razonSocial."</td>
									<td>".$fact."</td>
									<td>".invierte_fecha($row->fecha)."</td>									
									<td>".number_format($row->total,2,",",".")."</td>
									<td>".$row->estado."</td>									
									<td>
										<a href='".base_url()."index.php/orden_pago/update_payment_order/".$row->id_orden.
												"/' title='Modificar Orden de Pago'>
											<i class='fas fa-address-book'></i>
										</a> -
										<a href='".base_url()."index.php/imprimir/pay_order/".$row->id_orden."/'
											title='Imprimir Orden de Pago Nro: ".$row->id_orden."' target='_blank'>
											<i class='far fa-file-pdf' style='color:red;'></i>
										</a> - 
										<a href='".base_url()."index.php/orden_pago/delete_payment/".$row->id_orden."/'
											  title='Eliminacion de la orden de pago NRO:".$row->id_orden."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>										
									</td>
								  </tr>";
								 $cont += $row->total;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
		<p>Total: <b>$<?=number_format($cont,2,",",".");?></b></p>
</div>