<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<script type="text/javascript">
	function marcar(source) 
	{
		checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
		for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
		{
			if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
			{
				checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
			}
		}
	}
</script>
<div class="container text-center" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1><?=$title;?></h1>
	<div class="form-group mb-3">
				<p>Empresa:  <b> <?=$empresa[0]->razonSocial; ?></b> Tipo: <b><?=$empresa[0]->tipo; ?></b> 
					CUIT: <b><?=$empresa[0]->cuit; ?></b>
					
				</p>				
			</div>
	<h3>Seleccione los cheques a adjuntar</h3>
	<div class="text-center">
		<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
			<div class="form-group text-center">
        		<input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 		name="buscar" required/>
        
        		<select class="form-control" id="campo" name="campo">
        			<option value="razonSocial">Razon Social</option>
        			<option>Banco</option>
        			<option value="tipo_cuenta">Tipo de Cuenta</option>
        			<option>cuenta</option>
        			<option value="nro">Nº de Cheque</option>
        			<option value="paguese">A la orden de</option>
        			<option value="estado">Estado</option>
        			<option>emision</option>
        			<option>vencimiento</option>
        			<option>cantidad</option>
        			<option>confecciono</option>
        		 	<option>clase</option>
        		 	<option>responsable</option>
        		 	<option>observaciones</option>
        		</select>
      		</div>
      		<button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
		<?=form_close();?>
		
	</div>
	<?php echo form_open('', 
							array("role"=>"form", "id"=>"new_form","name"=>"new_form","onkeypress"=>"return anular(event)"));?>
		
		<button type="submit" class="btn btn-primary" id="confirm" name="confirm" value="Agregar">
			<i class="fas fa-plus"></i> Agregar
		</button>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Agregar cheques">
						<div class="custom-control custom-checkbox">
    						<input type="checkbox" class="custom-control-input" id="todos2" name="todos2" onclick="marcar(this);">
    						<label class="custom-control-label" for="todos2">Todos</label>
						</div>
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Agregar cheques">
                    	<div class="custom-control custom-checkbox">
    						<input type="checkbox" class="custom-control-input" id="todos1" name="todos1" onclick="marcar(this);">
    						<label class="custom-control-label" for="todos1">Todos</label>
						</div>
                    </th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                			$i=0; 
                		foreach ($lista as $row) 
                		{
                			if(!$this->Cheques_Comprobantes_mdl->consulta(
                					array("banco"=>$row->id_banco,"cuenta"=>$row->cuenta,"nro"=>$row->nro )))
                		 	{             		 	
                		 	
							echo "<tr>
									<td>".$row->razonSocial."</td>
									<td>".$row->Banco."</td>
									<td>".$row->nro."</td>									
									<td>".$row->emision."</td>
									<td>".$row->tipo."</td>
									<td>".$row->paguese."</td>
									<td>".$row->cantidad."</td>									
									<td>
										<div class='custom-control custom-checkbox custom-control-inline'>
  											<input type='checkbox' class='custom-control-input case' id='check".$i."' name='check".$i."' >
   											<label class='custom-control-label' for='check".$i."'></label>
  											<input type='hidden' id='banco".$i."' name='banco".$i."' value='".$row->id_banco."'>
  											<input type='hidden' id='cuenta".$i."' name='cuenta".$i."' value='".$row->cuenta."'>
  											<input type='hidden' id='nro".$i."' name='nro".$i."' value='".$row->nro."'>
  											<input type='hidden' id='tipo".$i."' name='tipo".$i."' value='".$row->tipo."'>
  											<input type='hidden' id='paguese".$i."' name='paguese".$i."' value='".$row->paguese."'>
  											<input type='hidden' id='emision".$i."' name='emision".$i."' value='".$row->emision."'>
  											<input type='hidden' id='vencimiento".$i."' name='vencimiento".$i."' value='".$row->vencimiento."'>
  											<input type='hidden' id='cantidad".$i."' name='cantidad".$i."' value='".$row->cantidad."'>
										</div>										
									</td>
								  </tr>";
								++$i;
							}
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <?=form_close();?>
</div>	