<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
$fecha_actual = date("d-m-Y");
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
$_SESSION["id_empresa_dest"] = $orden_pago[0]->facturacion;
?>
<div class="container text-center" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Modificar orden de pago</h1>
	<h3>Confirmar orden de pago</h3>
	<a href= "<?=base_url();?>index.php/pagos/update_pay/<?=$id_orden;?>/1/<?php echo $orden_pago[0]->id_empresa;?>/">
			<button type="button" class="btn btn-success">
				<i class="fas fa-file-invoice"></i> Agregar Facturas
			</button>
	</a>
		
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>                  	                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
						OP
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th>                  	                  
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
                    	OP
                    </th> 
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total_facturas = 0; $total_saldo = 0;
                		foreach ($lista_comprobantes as $row) 
                		{
                			$row->total = round($row->total,2);
                			$saldo = $this->Comprobantes_mdl->consulta("id_comprobante='".$row->id_comprobante."'","saldo");
							$saldo[0]->saldo = round($saldo[0]->saldo,2);
							echo "<tr>
									<td>".$row->id_comprobante."</td>																
									<td>".$row->tipo_comprobante."</td>
									<td>".$row->nro."</td>
									<td>".$row->fecha."</td>
									<td>".$row->total."</td>
									<td>".$saldo[0]->saldo."</td>									
									<td>
										<a href='".base_url()."index.php/orden_pago/".$dir.$row->id_comprobante."/'
											  title='Eliminar, Factura NRO:".$row->nro."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>
									</td>"; 
							$total_facturas = $total_facturas + $row->total; 
							$total_saldo = $total_saldo + $saldo[0]->saldo; 
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>          
          
        </div>
        
        <p>Total: $<?=$total_facturas;?></p> <p>Total Saldo Pendiente: $<?=$total_saldo;?></p>
        <hr />
        
        <a href= "<?=base_url();?>index.php/orden_pago/update_payment_order/<?=$id_orden;?>/2/">
			<button type="button" class="btn btn-success">
				<i class="fas fa-money-check"></i> Agregar Cheques
			</button>
		</a>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Eliminar cheques">
						OP
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Eliminar cheques">
                    	OP
                    </th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total_cheques=0;
                		foreach ($lista_cheques as $row) 
                		{                			
                			$cheque = $this->Cheques_mdl->consulta_views(array("id_banco" =>$row->banco,
																				"cuenta" =>$row->cuenta,
																				"nro" =>$row->nro)); 
							$cheque[0]->cantidad = round($cheque[0]->cantidad,2);              			                										
							echo "<tr>
									<td>".$row->cuenta."</td>
									<td>".$cheque[0]->Banco."</td>
									<td>".$row->nro."</td>									
									<td>".$cheque[0]->emision."</td>
									<td>".$cheque[0]->tipo."</td>
									<td>".$cheque[0]->paguese."</td>
									<td>".$cheque[0]->cantidad."</td>									
									<td>
										<a href='".base_url()."index.php/orden_pago/".$dir.
												$row->banco."/".$row->cuenta."/".$row->nro."/'
											  title='Eliminar Cheque'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>										
									</td>
								  </tr>";
							$total_cheques = $total_cheques + $cheque[0]->cantidad;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: $<?=$total_cheques;?></p>
        
        <?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group col">
				<input type="hidden" name="total_facturas" id="total_facturas" value="<?=$total_saldo;?>" />
				<input type="hidden" name="total_cheques" id="total_cheques" value="<?=$total_cheques;?>" />
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Efectivo </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_efectivo" name="pago_efectivo" 
  					step="0.01" value="<?=$orden_pago[0]->pago_efectivo;?>" onChange="total_orden_pago()"/>
				</div>
			</div>
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Retenciones </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_retenciones" name="pago_retenciones" 
  						step="0.01" value="<?=$orden_pago[0]->pago_retenciones;?>" onChange="total_orden_pago()"/>
				</div>
			</div>
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Transferencias.</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Nro Transferencia" id="nro_transferencia" name="nro_transferencia" 
  					step="0.01" value="<?=$orden_pago[0]->nro_transferencia;?>" />
					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_transferencia" name="pago_transferencia" 
						step="0.01" value="<?=$orden_pago[0]->pago_transferencia;?>" onChange="total_orden_pago()"/>
				</div>
			</div>
			<div class="form-group col">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nota de Credito</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_nota" name="pago_nota" step="0.01"
  						value="<?=$orden_pago[0]->pago_nota;?>" onChange="total_orden_pago()"/>
				
				</div>
			</div>
			<div class="form-group col">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  					</div>
  					<input type="date" class="form-control" placeholder="Ingrese la fecha" id="fecha" name="fecha"   					
  					    value="<?=$orden_pago[0]->fecha;?>" required/>					
				</div>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >				
						<option <?php if($orden_pago[0]->estado=="realizado"){echo "selected";}?>>realizado</option>
						<option <?php if($orden_pago[0]->estado=="cancelado"){echo "selected";}?>>cancelado</option>
						<option <?php if($orden_pago[0]->estado=="pendiente"){echo "selected";}?>>pendiente</option>
					</select>
				</div>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Total</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="total" name="total" step="0.01"
  						 onChange="total_orden_pago()" value="<?=$orden_pago[0]->total;?>" required/>
				
				</div>
			</div>
			<p>Saldo pendiente de pago = $</p>
				<input type="tel" class="form-control" id="pendiente" name="pendiente" 
						value="<?= $orden_pago[0]->pendiente ;?>"/>
			<button type="submit" class="btn btn-lg btn-block btn-primary">
			<i class="fas fa-pen"></i> Modificar Orden de Pago
			</button>
		<?=form_close();?>	
</div>
