<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<script type="text/javascript">
	function marcar(source) 
	{
		checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
		for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
		{
			if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
			{
				checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
			}
		}
	}
</script>
<div class="container text-center">
	<h1><?=$title;?></h1>
	<?=$mensaje;?>
	<div class="form-group mb-3">
				<p> <b> <?=$empresa[0]->razonSocial; ?></b> Tipo: <b><?=$empresa[0]->tipo; ?></b> 
					CUIT: <b><?=$empresa[0]->cuit; ?></b>
					
				</p>				
			</div>
	<h3>Seleccionar las Facturas</h3>
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"receipts_list"));?>
	
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">
        	<option>id_empresa</option>
        	<option value="razonSocial" selected>Razon Social</option>				
	      	<option>tipo</option>
	      	<option>nro</option>
	      	<option>fecha</option>
	      	<option>total</option>
	      	<option>otros</option>
	      	<option>estado</option>
        </select>             
  		
      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
      </div>
<?=form_close();?>

	<?php echo form_open('', 
							array("role"=>"form", "id"=>"new_form","name"=>"new_form","onkeypress"=>"return anular(event)"));?>							
		
		<button type="submit" class="btn btn-primary" id="confirm" name="confirm" value="Agregar">
			<i class="fas fa-plus"></i>  Agregar
		</button>				
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>
                  	<th>Razon Social</th> 
                  	<th>Facturacion</th>                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
						<div class="custom-control custom-checkbox">
    						<input type="checkbox" class="custom-control-input" id="todos2" name="todos2" onclick="marcar(this);">
    						<label class="custom-control-label" for="todos2">OP</label>
						</div>
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th> 
                  	<th>Razon Social</th>
                  	<th>Facturacion</th>                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
                    	<div class="custom-control custom-checkbox">
    						<input type="checkbox" class="custom-control-input" id="todos1" name="todos1" onclick="marcar(this);">
    						<label class="custom-control-label" for="todos1">OP</label>
						</div>
                    </th> 
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$i=0; $total = 0;
                		foreach ($lista as $row) 
                		{
                			$fact = $this->Empresas_mdl->consulta("id='".$row->id_empresa_dest."'");
							$row->total = round($row->total,2);
							$row->saldo = round($row->saldo,2);
							echo "<tr>
									<td>".$row->id_comprobante."</td>
									<td>".$row->razonSocial."</td>	
									<td>".$fact[0]->razonSocial."</td>								
									<td>".$row->tipo_comprobante."</td>
									<td>".$row->nro."</td>
									<td>".invierte_fecha($row->fecha)."</td>
									<td>".number_format($row->total,2,",",".")."</td>
									<td>".number_format($row->saldo,2,",",".")."</td>										
									<td>
										<div class='custom-control custom-checkbox custom-control-inline'>
  											<input type='checkbox' class='custom-control-input case' id='check".$i."' name='check".$i."' >
   											<label class='custom-control-label' for='check".$i."'></label>
  											<input type='hidden' id='id_comprobante".$i."' name='id_comprobante".$i."' value='".$row->id_comprobante."'>
  											<input type='hidden' id='tipo_comprobante".$i."' name='tipo_comprobante".$i."' value='".$row->tipo_comprobante."'>
  											<input type='hidden' id='nro".$i."' name='nro".$i."' value='".$row->nro."'>
  											<input type='hidden' id='fecha".$i."' name='fecha".$i."' value='".$row->fecha."'>
  											<input type='hidden' id='total".$i."' name='total".$i."' value='".$row->total."'>
										</div>
									</td>"; 
							++$i; $total += $row->total;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>          
          
        </div>        
        <?=form_close(); ?>
        <p><b>Total: </b>$ <?=number_format($total,2,",","."); ?></p>
       <hr /><br /><br /><br /><br />
</div>