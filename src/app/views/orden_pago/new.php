<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
$fecha_actual = date("d-m-Y");
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");

if (!isset($_SESSION["id_empresa"])) 
{
	$_SESSION["id_empresa"]="";	
}
?>
<div class="container text-center" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Generar nueva orden de pago</h1>
	<p>Proveedor:  <b> <?=$empresa[0]->razonSocial; ?></b> Tipo: <b><?=$empresa[0]->tipo; ?></b> 
					CUIT: <b><?=$empresa[0]->cuit; ?></b>
					
				</p>
	<h3>Confirmar orden de pago</h3>
		<a href= "<?=base_url();?>index.php/orden_pago/new_payment_order/1/<?php echo $_SESSION["id_empresa"];?>/">
			<button type="button" class="btn btn-success">
				<i class="fas fa-file-invoice"></i> Agregar Facturas
			</button>
		</a>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>                  	                   
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
						OP
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th>                  	                  
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>Saldo</th>
                    <th>
                    	OP
                    </th> 
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total_facturas = 0; $total_saldo = 0; $total_nota = 0;
                	
                		foreach ($lista_comprobantes as $row) 
                		{
                			$saldo = $this->Comprobantes_mdl->consulta(array("id_comprobante"=>$row->id_comprobante),"saldo");	
							
							if (!isset($saldo[0]->saldo)) 
							{
								$saldo_sum = 0;
							}else 
							{
								$saldo_sum = $saldo[0]->saldo;	
							}
							$saldo_sum = round($saldo_sum,2); 
							$row->total = round($row->total,2); 						
							echo "<tr>
									<td>".$row->id_comprobante."</td>																
									<td>".$row->tipo_comprobante."</td>
									<td>".$row->nro."</td>
									<td>".$row->fecha."</td>
									<td>".$row->total."</td>
									<td>".$saldo_sum."</td>									
									<td>
										<a href='".base_url()."index.php/orden_pago/new_payment_order/3/".$row->id_comprobante."/'
											  title='Eliminar, Factura NRO:".$row->nro."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>
									</td>"; 
							if ($row->tipo_comprobante == "Notas de crédito") 
							{
								$total_nota += $saldo_sum;
							}else {
								$total_facturas =$total_facturas + $row->total; 
								$total_saldo =$total_saldo + $saldo_sum ; 
							}							
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>          
          
        </div>
        
        		<p>Total: $<?=$total_facturas;?></p>  
        		<p>Total Saldo Pendiente: $<?=$total_saldo;?></p>
        		<p>Total nota de credito: $<?=$total_nota;?></p>
        		<p>Total Saldo - Notas de credito : <?php  $total_saldo = $total_saldo - $total_nota;
        													echo $total_saldo;  ?></p>
        <hr />
        
        <a href= "<?=base_url();?>index.php/orden_pago/new_payment_order/2/<?php echo $_SESSION["id_empresa"];?>/">
			<button type="button" class="btn btn-success">
				<i class="fas fa-money-check"></i> Agregar Cheques
			</button>
		</a>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Eliminar cheques">
						OP
					</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Eliminar cheques">
                    	OP
                    </th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total_cheques=0;
						
                		foreach ($lista_cheques as $row) 
                		{
                			$aux = $this->Cuentas_mdl->consultavista("id_banco='".$row->banco."'");  
              				$row->cantidad = round($row->cantidad,2); 					
							echo "<tr>
									<td>".$row->cuenta."</td>
									<td>".$aux[0]->Banco."</td>
									<td>".$row->nro."</td>									
									<td>".$row->emision."</td>
									<td>".$row->tipo."</td>
									<td>".$row->paguese."</td>
									<td>".$row->cantidad."</td>									
									<td>
										<a href='".base_url()."index.php/orden_pago/new_payment_order/3/".
												$row->banco."/".$row->cuenta."/".$row->nro."/'
											  title='Eliminar Cheque'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>										
									</td>
								  </tr>";
							$total_cheques = $total_cheques + $row->cantidad;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: $<?=$total_cheques;?></p>
        
        <?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<input type="hidden" name="total_facturas" id="total_facturas" value="<?=$total_saldo;?>" />
			<input type="hidden" name="total_cheques" id="total_cheques" value="<?=$total_cheques;?>" />
			<input type="hidden" name="facturacion" id="facturacion" value="<?=$_SESSION["id_empresa_dest"];?>" />				
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Efectivo </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_efectivo" name="pago_efectivo" 
  					step="0.01" value="0" onChange="total_orden_pago()"/>
				</div>
			</div>
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Retenciones </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_retenciones" name="pago_retenciones" 
  					step="0.01" value="0" onChange="total_orden_pago()"/>
				</div>
			</div>
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Transferencias.</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Nro Transferencia" id="nro_transferencia" name="nro_transferencia" 
  					step="0.01" value="00"/>
					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_transferencia" 
					name="pago_transferencia" step="0.01" value="0"	 onChange="total_orden_pago()"/>
				</div>
			</div>
			<div class="form-group col">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nota de Credito</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_nota" name="pago_nota" step="0.01"
  						value="0" onChange="total_orden_pago()"/>					
				</div>
			</div>
			<div class="form-group col">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  					</div>
  					<input type="date" class="form-control" placeholder="Ingrese la fecha" id="fecha" name="fecha"   					
  					    value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>" required/>					
				</div>
								
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Total</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="total" name="total" step="0.01"
  						value="<?=$total_cheques; ?>" onchange="total_orden_pago()"/>
				
				</div>
				<p>Diferencias  = $</p>
				<input type="tel" class="form-control" id="pendiente" name="pendiente"  value="<?=$total_cheques - $total_saldo ?>"/>
			</div>
			<button type="submit" class="btn btn-lg btn-block btn-primary">
				<i class="fas fa-plus"></i>  Realizar Orden de Pago
			</button>
		<?=form_close();?>	
</div>
