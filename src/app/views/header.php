<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sistema Bancario</title>

  <!-- Custom fonts for this template-->
  <link href="<?=base_url();?>media/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?=base_url();?>media/vendor/bootstrap/scss/bootstrap.scss" rel="stylesheet">
  
   <!-- Page level plugin CSS-->
  <link href="<?=base_url();?>media/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?=base_url();?>media/css/sb-admin.css" rel="stylesheet">
  
  <!-- Calendar
  <link href="<?=base_url();?>media/css/calendario.css" rel="stylesheet">-->
  
  <!-- Calendarios 
  <script language='JavaScript' type='text/javascript' src='<?=base_url();?>media/js/calendar.js'></script>
  <script language='JavaScript' type='text/javascript' src='<?=base_url();?>media/js/calendar-es.js'></script>
  <script language='JavaScript' type='text/javascript' src='<?=base_url();?>media/js/calendar-setup.js'></script>-->
  
  <!-- Fs basicas para formularios -->
  <script language='JavaScript' type='text/javascript' src='<?=base_url();?>media/js/form.js'></script>



	<!-- Template faltante-->
  <?php echo $header; ?>
</head>
