<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container">
	<h1>Lista de pagos </h1>
	<div class="form-group mb-3">
				<p>Empresa:  <b> <?=$empresa[0]->razonSocial; ?></b> Tipo: <b><?=$empresa[0]->tipo; ?></b> 
					CUIT: <b><?=$empresa[0]->cuit; ?></b>
					
				</p>				
			</div>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de pagos
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th>
                  	<th>Empresa</th>
                    <th>Tipo</th>
                    <th>Nro</th>
                    <th>Fecha</th>               
                    <th>Debe</th>
                    <th>Haber</th>
                    <th>Saldo</th>                                        
                    <th title="Realizar operaciones con los diferentes pagos">Op</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th>
                  	<th>Empresa</th>
                    <th>Tipo</th>
                    <th>Nro</th>
                    <th>Fecha</th>               
                    <th>Debe</th>
                    <th>Haber</th>
                    <th>Saldo</th>                                        
                    <th title="Realizar operaciones con los diferentes pagos">Op</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php $total = 0; $debe = 0; $haber = 0; $link = "";
                		foreach ($lista as $row) 
                		{
                			$row->total = round($row->total,2);
                			if ($row->clase != "proveedor") 
                			{
								switch ($row->tipo) 
								{
									case 'Notas de crédito':
										$haber = $row->total; 
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Notas de crédito" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Notas de débito':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Notas de débito" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Remito':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Remito" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Orden de compra':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Orden de compra" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;									
									case 'Recibos':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Recibos" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Comprobante':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Comprobante" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Tickets':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Tickets" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'factura':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';	
									break;
									case 'Pago':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/pagos/update_pay/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
										break;																					
								}								
								
							} else {//proveedor
								switch ($row->tipo) 
								{
									case 'Notas de crédito':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Notas de débito':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Remito':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Orden de compra':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;									
									case 'Recibos':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Comprobante':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Comprobante" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'Tickets':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Ticket" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;
									case 'factura':
										$haber = $row->total;
										$link = '<a href="'.base_url().'index.php/comprobantes/receipts_update/'.
												$row->id.'/" title="Modificar Datos de Factura" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';	
									break;
									case 'Orden de Pago':
										$debe = $row->total;
										$link = '<a href="'.base_url().'index.php/orden_pago/update_payment_order/'.
												$row->id.'/" title="Modificar Orden de pago" target=\"_blank\">
											<i class="fas fa-receipt"></i>
										</a>';
									break;																					
								}
							}
							$total = $total - $debe + $haber; 
							$empresa = $this->Empresas_mdl->consulta("id='".$row->facturacion."'");
							               			
							echo "<tr>
									<td>".$row->id."</td>
									<td>".$empresa[0]->razonSocial."</td>
									<td>".$row->tipo."</td>
									<td>".$row->nro."</td>
									<td>".invierte_fecha($row->fecha)."</td>									
									<td>".number_format($debe,2,",",".")."</td>
									<td>".number_format($haber,2,",",".")."</td>
									<td>".number_format($total,2,",",".")."</td>																	
									<td>
										".$link."										
									</td>
								  </tr>";
							$debe = 0; $haber = 0; $link = "";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          <p>Total:<b> $<?=number_format($total,2,",",".");?></b></p>
        </div> <br /><br /><br /><br />
</div>
