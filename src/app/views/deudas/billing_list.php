<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="text-center container mt-3">	
	<h1 >Seleccione una empresa</h1>
	<div class="container-fluid">
		<?php echo form_open('', 
					array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
		<div class="form-group">
			<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Empresa</span>
  					</div>  					
  					<select class="form-control" id="id_empresa_dest" name="id_empresa_dest" >
  						<?php
  						foreach ($cuentas as $row) 
  						{
							echo "<option value='".$row->id_empresa."'>".$row->razonSocial."</option>";
						}
  						?>
  					</select>	
			</div>
			<button type="submit" class="btn btn-primary">
  				<i class="far fa-plus-square"></i> Seleccionar
  			</button>
		</div>
		<?=form_close();?>

