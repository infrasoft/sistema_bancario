<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Verificar deudas</h1>
	<h3>Seleccione la empresa</h3>
	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"company_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">        	
        	    <option>id</option>
        	    <option>cuit</option>
        	    <option selected="">razonSocial</option>
        	    <option>tipo</option>
        	    <option>clase</option>
        	    <option>actividad</option>
        	    <option>domicilio</option>
        	    <option>telefono</option>
        	    <option>telefono2</option>
        	    <option>celular</option>        	    
        	    <option>representante</option>
        	    <option>tel_representante</option>
        	    <option>otros</option>    	
        </select>
      </div>
      <button type="submit" class="btn btn-primary">
      	<i class="fas fa-search"></i> Buscar
      </button>
<?=form_close();?>
	
	</div>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de empresas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Razon Social</th>
                    <th>Tipo</th>
                    <th>Clase</th>
                    <th>Telefono</th>
                    <th>domicilio</th>                    
                    <th title="Realizar operaciones con las diferentes empresas">Operar</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Razon Social</th>
                    <th>Tipo</th>
                    <th>Clase</th>
                    <th>Telefono</th>
                    <th>domicilio</th>                    
                    <th title="Realizar operaciones con las diferentes empresas">Operar</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php $cont = null;
                		foreach ($lista as $row) 
                		{                			
							echo "<tr>
									<td>".$row->razonSocial."</td>
									<td>".$row->tipo."</td>
									<td>".$row->clase."</td>
									<td>".$row->telefono."</td>
									<td>".$row->domicilio."</td>									
									<td>
										<a href='".base_url()."index.php/deudas/debt_list/".$row->id."/' title='Seleccionar empresa'>
											<i class='fas fa-address-book'></i>
										</a> 										
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>
