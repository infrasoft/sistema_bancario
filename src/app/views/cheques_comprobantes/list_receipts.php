<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Lista de Recibos</h1>
	<h3>Seleccione el recibo a vincular con el cheque</h3>
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"receipts_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">
        	<option>id_empresa</option>			
	      	<option>tipo</option>
	      	<option>nro</option>
	      	<option>fecha</option>
	      	<option>total</option>
	      	<option>otros</option>
	      	<option>estado</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
<?=form_close();?>
	<a href="<?=base_url();?>index.php/comprobantes/receipts_list/" alt="Realiza la busqueda avanzada">
		<p>Busqueda Avanzada</p>
	</a>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes 
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th> 
                  	<th>Razon Social</th>                    
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th> 
                  	<th>Razon Social</th>                    
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		foreach ($lista as $row) 
                		{
							echo "<tr>
									<td>".$row->id_comprobante."</td>
									<td>".$row->razonSocial."</td>									
									<td>".$row->tipo_comprobante."</td>
									<td>".$row->nro."</td>
									<td>".$row->fecha."</td>
									<td>".$row->total."</td>									
									<td>
										<a href='".base_url()."index.php/comprobantes/receipts_update/".$row->id_comprobante."' alt='Modificar Datos'>
											<i class='fas fa-address-book'></i>
										</a> - 
										<a href='".base_url()."index.php/cheques_comprobantes/link_checks_to_receipts/".
																				$row->id_comprobante."/"
																				.$id_banco."/"
																				.$cuenta."/"
																				.$nro."/1/'".											  
												  "title='Vincular a cheques, recibo NRO:".$row->nro."'>
											<i class='fas fa-money-check'></i>
										</a>
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>