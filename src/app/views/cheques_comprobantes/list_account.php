<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Lista de cuentas</h1>
	<h3>Seleccione la cuenta</h3>
	
	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="lista_venta" name="lista_venta">
        	<option value="razonSocial">Razon Social</option>
        	<option >Banco</option>
        	<option value="nro_cuenta">Nº de cuenta</option>        	
        	<option>tipo</option>
        	<option>sucursal</option>
        	<option>saldo</option>
        	<option>ultimo_cheque</option>
        	<option>observaciones</option>        	
			<option>estado</option>  	
        </select>
      </div>
      <button type="submit" class="btn btn-primary">
      	<i class="fas fa-search"></i> Buscar
      </button>
<?=form_close();?>	
	</div>
	
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de cuentas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Banco</th>
                  	<th>Titular</th>
                  	<th>Tipo</th>
                    <th>Nro de cuenta</th>                 
                    <th>Saldo</th>
                    <th>Ultimo Cheque</th>
                    <th title="Operaciones con cuentas">OP</th>                   
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Banco</th>
                  	<th>Titular</th>
                  	<th>Tipo</th>
                    <th>Nro de cuenta</th>                 
                    <th>Saldo</th>
                    <th>Ultimo Cheque</th>
                    <th title="Operaciones con cuentas">OP</th> 
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		foreach ($lista as $row) 
                		{
							echo "<tr>
									<td>".$row->Banco."</td>
									<td>".$row->razonSocial."</td>
									<td>".$row->tipo."</td>
									<td>".$row->nro_cuenta."</td>									
									<td>".$row->saldo."</td>
									<td>".$row->ultimo_cheque."</td>																		
									<td>										
										<a href='".base_url()."index.php/cheques/".$link.$row->id_banco."/".$row->nro_cuenta.
												"/' title='Seleccionar cuenta Nº ".$row->nro_cuenta."'>
											<i class='fas fa-money-check'></i>
										</a>
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>