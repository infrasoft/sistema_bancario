<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Lista de Cheques</h1>
	<h3>Seleccione el cheque a vincular con el recibo</h3>
	<div class="text-center">
		<?=form_open(base_url().'index.php/cheques/', array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
			<div class="form-group text-center">
        		<input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 		name="buscar" required/>
        
        		<select class="form-control" id="campo" name="campo">
        			<option value="razonSocial">Razon Social</option>
        			<option>Banco</option>
        			<option value="tipo_cuenta">Tipo de Cuenta</option>
        			<option>cuenta</option>
        			<option value="nro">Nº de Cheque</option>
        			<option value="paguese">A la orden de</option>
        			<option value="estado">Estado</option>
        			<option>emision</option>
        			<option>vencimiento</option>
        			<option>cantidad</option>
        			<option>confecciono</option>
        		 	<option>clase</option>
        		 	<option>responsable</option>
        		 	<option>observaciones</option>
        		</select>
      		</div>
      		<button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
		<?=form_close();?>
		<a href="<?=base_url();?>index.php/cheques/avanced_seach/" 
			title="Realiza la busqueda avanzada por diferentes criterios">
			Busqueda Avanzada
		</a>
	</div>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		foreach ($lista as $row) 
                		{
							echo "<tr>
									<td>".$row->razonSocial."</td>
									<td>".$row->Banco."</td>
									<td>".$row->nro."</td>									
									<td>".$row->emision."</td>
									<td>".$row->tipo."</td>
									<td>".$row->paguese."</td>
									<td>".$row->cantidad."</td>									
									<td>
										<a href='".base_url()."index.php/cheques_comprobantes/link_checks_to_receipts/".$id_comprobante."/".
										$row->id_banco."/".$row->cuenta."/".$row->nro."/1/'
											  title='Vincular al cheque NRO:".$row->nro."'>
											<i class='fas fa-money-check'></i>
										</a>										
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>	