<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container text-center">
	<?php echo $mensaje; ?>
	<h1>Vincular</h1>
	<a href="#recibo" class="btn btn-success" data-toggle="collapse">Recibos vinculados al Cheque</a>
       <div id="recibo" class="collapse">
	<h3>Recibos vinculados al Cheque</h3>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes 
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>ID</th> 
                  	<th>Razon Social</th>                    
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>ID</th> 
                  	<th>Razon Social</th>                    
                    <th>Tipo</th>
                    <th>nro</th>
                    <th>fecha</th>
                    <th>total</th>
                    <th>OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php 
                		$total_comprobantes = 0;
                		foreach ($lista_comprobantes as $row) 
                		{
                			
							$row= $this->Comprobantes_mdl->consulta_views("id_comprobante='".$comprobante[0]->id_comprobante."'");	//
							
							if ($row !=null) 
							{							
							echo "<tr>
									<td>".$row[0]->id_comprobante."</td>
									<td>".$row[0]->razonSocial."</td>									
									<td>".$row[0]->tipo_comprobante."</td>
									<td>".$row[0]->nro."</td>
									<td>".$row[0]->fecha."</td>
									<td>".$row[0]->total."</td>									
									<td>
										<a href='".base_url()."index.php/comprobantes/receipts_update/".$row[0]->id_comprobante."' alt='Modificar Datos'>
											<i class='fas fa-address-book'></i>
										</a> - 
										
									</td>
								  </tr>"; 
								  $total_comprobantes += $row[0]->total;
							}
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: $<?=$total_comprobantes ?></p>
    </div>
    <a href="#cheques" class="btn btn-success" data-toggle="collapse">Cheques vinculados al recibo</a>
       <div id="cheques" class="collapse">    
	<h3>Cheques Vinculados al recibo</h3>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Tipo</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total_cheques = 0; $i =0;
                		//print_r($lista_cheques);
                		foreach ($lista_cheques as $row) 
                		{
                			
							
							echo "<tr>
									<td>".$row->razonSocial2."</td>
									<td>".$row->Banco."</td>
									<td>".$row->nro."</td>									
									<td>".$row->emision."</td>
									<td>".$row->tipo."</td>
									<td>".$row->paguese."</td>
									<td>".$row->cantidad."</td>									
									<td>
										<a href='".base_url()."index.php/cheques/update_check/".$row->id_banco."/".$row->cuenta."/".$row->nro."/' alt='Modificar Datos'>
											<i class='fas fa-address-book'></i>
										</a> -										
									</td>
								  </tr>";
								  $total_cheques += $row->cantidad; ++$i;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
		<p>Total: $<?=$total_cheques; ?></p>
		
	</div>
	<div class="container">
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Datos del recibo</h4>
          </div>
          <div class="card-body">
            
            <p><b>ip recibo: </b>	<?php echo($comprobante[0]->id_comprobante); ?></p>
			<p><b>Empresa: </b>	<?php echo($comprobante[0]->razonSocial); ?></p>
			<p><b>Cuit: </b>	<?php echo($comprobante[0]->cuit); ?></p>
			<p><b>Tipo Comprobante: </b>	<?php echo($comprobante[0]->tipo_comprobante); ?></p>
			<p><b>Nro comprobante: </b>	<?php echo($comprobante[0]->nro); ?></p>
			<p><b>Fecha: </b>	<?php echo(invierte_fecha($comprobante[0]->fecha)); ?></p>
			<p><b>Total: </b>$	<?php echo($comprobante[0]->total); ?></p>
			<p><b>Estado: </b>	<?php echo($comprobante[0]->estado); ?></p>
			<hr />
			
			<a href="<?=base_url();?>index.php/cheques_comprobantes/link_checks_to_receipts/0/<?=$cheque[0]->id_banco;?>/<?=$cheque[0]->cuenta;?>/
					<?=$cheque[0]->nro;?>/2/" >
            	<button type="button" class="btn btn-lg btn-block btn-outline-primary">
            		Cambiar recibo
            	</button>
            </a>
          </div>
        </div>
        
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Datos del cheque</h4>
          </div>
          <div class="card-body">
          	 
          	 <p><b>Razon Social:</b> <?php echo($cheque[0]->razonSocial); ?></p>
          	 <p><b>Banco:</b> <?php echo($cheque[0]->Banco); ?></p>
          	 <p><b>Tipo de Cuenta:</b> <?php echo($cheque[0]->tipo_cuenta); ?></p>
          	 <p><b>Cheque Nº:</b> <?php echo($cheque[0]->nro); ?></p>
          	 <p><b>Destinatario:</b> <?php echo($cheque[0]->paguese); ?></p>
          	 <p><b>Emision:</b> <?php echo($cheque[0]->emision); ?></p>
          	 <p><b>Vencimiento:</b> <?php echo($cheque[0]->vencimiento); ?></p>
          	 <p><b>Cantidad:</b> <?php echo($cheque[0]->cantidad); ?></p>
          	 <p><b>Impreso:</b> <?php echo($cheque[0]->impreso); ?></p> 
          	 <hr />   
            	 
          	<a href="<?=base_url();?>index.php/cheques_comprobantes/link_checks_to_receipts/<?=$comprobante[0]->id_comprobante;?>/0/0/0/0/"> 
            	<button type="button" class="btn btn-lg btn-block btn-primary">
            		Cambiar cheque
            	</button>
            </a>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Vincular</h4>
          </div>
          <div class="card-body">
            <p>¿Desea realizar la vinculacion?</p>
            <?php echo form_open('', 
							array("role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
				<div class="form-group">
					<div class="input-group mb-3">
  						<div class="input-group-prepend">
    						<span class="input-group-text" >saldo</span>
  						</div>
  						<input type="number" class="form-control"  id="saldo" name="saldo" value="<?php $tot = $total_comprobantes - $total_cheques; echo $tot; ?>" required="Valor requerido"/>
  					
					</div>
				
					<label>Observaciones</label>
  					<textarea class="form-control" id="observaciones" name="observaciones"></textarea>
  					  					
					<div class="input-group mb-3" >
  						<div class="input-group-prepend">
    						<label class="input-group-text" for="inputGroupSelect01">Orden de pago</label>
  						</div>
  						<select class="form-control" id="orden_pago" name="orden_pago" onchange="showCustomer(this.value)">
							<option value="realizar">Realizar</option>
							<option value="no_realizar" selected>No realizar</option>
							<option value="ya_realizada">Ya Realizada</option>								
						</select>
					</div>		
			<div id="txtHint">		
				
				</div>	
			</div>		
            <button type="submit" class="btn btn-lg btn-block btn-primary">Realizar vinculacion</button>
            <?=form_close();?>
          </div>
        </div>
      </div>	
     
</div><br /><br /><br /><br />