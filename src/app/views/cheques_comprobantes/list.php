<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container text-center" id="content-wrapper">
		<?php echo $mensaje; ?>
	<h1><?php echo $title; ?></h1>
	
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"receipts_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">
        	<option value="nro_fact">Nº Factura</option>
        	<option value="razonSocial">Razon Social</option>			
	      	<option>banco</option>
	      	<option>cuenta</option>
	      	<option>nro</option>
	      	<option>saldo</option>
	      	<option>observaciones</option>	      	
        </select>
      </div>
      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
<?=form_close();?>

<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Facturas y Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Nro  Fac</th>
                  	<th>Razon Social</th>
                  	<th>Total</th>
                  	<th>Banco</th>                    
                    <th>Cuenta</th>
                    <th>nro</th>
                    <th>Cantidad</th>
                    <th>Saldo</th>
                   
                    <th>OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Nro  Fac</th>
                  	<th>Razon Social</th>
                  	<th>Total</th>
                  	<th>Banco</th>                    
                    <th>Cuenta</th>
                    <th>nro</th>
                    <th>Cantidad</th>
                    <th>Saldo</th>
                   
                    <th>OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		foreach ($lista as $row) 
                		{
                			if ($row->razonSocial == $row->paguese) 
                			{
								$aux = $row->razonSocial;
							} else {
								$aux = "<p class='text-danger'>".$row->razonSocial." // ".$row->paguese."</p>";
							}
							
							echo "<tr>
									<td>".$row->nro_fact."</td>
									<td>".$aux."</td>
									<td>".number_format($row->total,2,",",".")."</td>
									<td>".$row->Banco."</td>									
									<td>".$row->cuenta."</td>
									<td>".$row->nro."</td>
									<td>".number_format($row->cantidad,2,",",".")."</td>
									<td>".number_format($row->saldo,2,",",".")."</td>
																		
									<td>
										<a href='".base_url()."index.php/comprobantes/receipts_update/".$row->id_comprobante.
												"' title='Modificar Datos de Factura'>
											<i class='fas fa-address-book'></i>
										</a> -	
										<a href='".base_url()."index.php/cheques/update_check/".
																				$row->id_banco."/".
																				$row->cuenta."/".
																				$row->nro."/'
											  title='Modificar dato de cheque NRO:".$row->nro."'>
											<i class='fas fa-money-check'></i>
										</a> -
										<a href='".base_url()."index.php/cheques_comprobantes/delete_vinculation/".
																				$row->id_comprobante."/".
																				$row->id_banco."/".
																				$row->cuenta."/".
																				$row->nro."/'
											  title='Eliminar Vinculacion de Factura NRO:".$row->nro."' target='_blank'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>									
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>