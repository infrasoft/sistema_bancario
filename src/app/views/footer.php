<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>


<!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">         
          	<a href="http://www.infrasoft.com.ar/" target="_blank">
            	<span>Copyright © Infrasoft Servicios Informaticos 2019 - http://www.infrasoft.com.ar/</span>
            </a>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 <!-- Bootstrap core JavaScript-->
  <script src="<?=base_url();?>media/vendor/jquery/jquery.min.js"></script>
  <script src="<?=base_url();?>media/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?=base_url();?>media/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?=base_url();?>media/js/sb-admin.min.js"></script>
  
  <?php echo $footer;?>

</body>

</html>