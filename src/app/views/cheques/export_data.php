<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if (!isset($dir2)) 
{
	$dir2="";
}
if (!isset($dir3)) 
{
	$dir3="";
}
?>
<div class="container-fluid">
<div class="btn-group " >
	<a class="btn btn-danger" href="<?=base_url();?>index.php/<?=$dir;?>" role="button"  target="_blank">
		<i class="far fa-file-pdf"></i> PDF
	</a>
	<a class="btn btn-light" href="<?=base_url();?>index.php/<?=$dir2;?>" role="button"  download="lista.csv">
		<i class="fas fa-file-csv"></i> CSV
	</a>
	<a class="btn btn-light" href="<?=base_url();?>index.php/importar/<?=$dir3;?>" role="button" >
		<i class="fas fa-file-import"></i> Importar
	</a>
</div>
