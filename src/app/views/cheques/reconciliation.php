<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
//print_r($titulo[0]);
$fecha_actual = date("d-m-Y");
?>
<script>
		function showDate(str) 
		{
  		if (str=="") 
  		{
    		document.getElementById("date").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("conciliar").innerHTML=this.responseText;
    		}
  		}
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/ya_conciliados/"+str+"/<?=$titulo[0]->id_banco;?>/<?=$titulo[0]->nro_cuenta;?>/", true);
  		xmlhttp.send();
		}		
</script>
<div class="container" id="content-wrapper">
	<?php echo mensajes(" Titular: <b>".$titulo[0]->razonSocial.
						"</b> Banco: <b>".$titulo[0]->Banco.
						"</b> Nº de Cuenta: <b>".$titulo[0]->nro_cuenta."</b>",
						"alert-info",""); ?>
	<h1>Conciliacion de cheques</h1>
	<?php echo $mensaje; ?>
	<div class="text-center">
		<?=form_open(base_url().'index.php/cheques/check_conciliation/'.$titulo[0]->id_banco."/".$titulo[0]->nro_cuenta."/", 
						array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
			<div class="form-group text-center">
        		<input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 		name="buscar" required/>
        
        		<select class="form-control" id="campo" name="campo">
        			
        			<option value="nro">Nº de Cheque</option>
        			<option value="paguese">A la orden de</option>
        			<option value="estado">Estado</option>
        			<option>emision</option>
        			<option>vencimiento</option>
        			<option>cantidad</option>
        			<option>confecciono</option>
        		 	<option>clase</option>
        		 	<option>responsable</option>
        		 	<option>observaciones</option>
        		</select>
      		</div>
      		<button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
		<?=form_close();?>
		
	</div>
	
		<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Vencimiento</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Vencimiento</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0;
                		foreach ($list_conciliacion as $row) 
                		{
							echo "<tr>									
									<td>".$row->nro."</td>									
									<td>".$row->emision."</td>
									<td>".$row->vencimiento."</td>
									<td>".$row->paguese."</td>
									<td>".number_format($row->cantidad,2,",",".")."</td>									
									<td>
																				
										<a href='".base_url()."index.php/cheques/delete_conciliation/".
																				$row->banco."/".
																				$titulo[0]->nro_cuenta."/".
																				$row->nro."/'
											  title='Eliminacion del cheque NRO:".$row->nro."'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a> 
									</td>
								  </tr>";
							$total += $row->cantidad;
						}
                	?>
                  	
                </tbody>
              </table>
            </div>
          </div>
          <div name="conciliar" id="conciliar"></div>
        </div>
        <p> Total: <b>$ <?=$total;?></b></p>
        <?=form_open("", 
						array("class"=> "form-inline", "role" => "form", "id"=>"conciliacion","name"=>"conciliacion"));?>
						
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de Conciliacion</span>
  					</div>
  					<input type="date" class="form-control" placeholder="ingrese fecha" id="fecha" name="fecha" 
  						value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>" onchange="showDate(this.value)" required/>
				</div>				
        <button type="submit" class="btn btn-primary" id="conciliacion" name="conciliacion" value="ok">
        	<i class="fas fa-money-check"></i> Conciliar
        </button>         
        <?=form_close();?>
</div>