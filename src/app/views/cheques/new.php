<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
$hoy = getdate();
$d = $hoy["mday"]."/".$hoy["mon"]."/".$hoy["year"];

$fecha_actual = date("d-m-Y");
?>
<div class="text-center container">
	<?php echo $mensaje; ?>
	<h1 >Nuevo Cheque</h1>
	<div class="container-fluid">
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				<div class="autocomplete mb-3">  					
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la cuenta" id="cbu" name="cbu" value=""/>  					
				</div>	
				
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nº de Cheque</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el nro" id="nro" name="nro" value=""/>  					
				</div>
				
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo" name="tipo" >				
						<option>A</option>
						<option>P</option>
					</select>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Paguese a </span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Destinatario" id="paguese" name="paguese" value="" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >					
					<option>activo</option>
					<option>anulado</option>
					<option>cancelado</option>
					</select>
				</div>
			</div>
			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de Emision</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					 id="emision" name="emision" value="<?php echo date("d/m/Y",strtotime($fecha_actual."- 1 days"));?>" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de Pago</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="ingrese fecha" id="vencimiento" name="vencimiento" value="<?php echo date("d/m/Y",strtotime($fecha_actual));?>" required/>
				</div>	
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Cantidad</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la cantidad" id="cantidad" name="cantidad" value="" required/>
				</div>
			</div>
			<div class="form-group">	
				<label>Observaciones</label>
				<textarea class="form-control"></textarea>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Confecciono</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Responzable" id="confecciono" name="confecciono" value="" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  					</div>
  					<select class="form-control" id="clase" name="clase" >					
					<option>con factura</option>
					<option>sin factura</option>
					<option>no corresponde</option>
					</select>
				</div>
				
				<input type="submit" class="btn btn-primary" value="Actualizar">
			</div>						
		<?=form_close(); setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
		
$fecha = strftime("%d de %B de %Y", strtotime($d));
echo $fecha;?>
	</div>
</div>