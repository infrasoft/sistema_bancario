
<div class="form-group">
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nº de Cheque</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el nro" id="nro" name="nro" value="" />  					
		</div>
		
		<div class="input-group mb-3">
  					
  					<select class="form-control" id="tipo" name="tipo" >
  						<option></option>				
						<option>A</option>
						<option>P</option>
					</select>
					<div class="input-group-append">
    					<label class="input-group-text" for="inputGroupSelect01">A la orden</label>
  					</div>
		</div>		
</div>
	<div class="form-group">		
		<div class="input-group mb-3 ">
  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Paguese a </span>
  			</div>
  			<input type="text" class="form-control" 
  					placeholder="Destinatario" id="paguese" name="paguese" value="" />
		</div>
		
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >
  					<option></option>					
					<option >activo</option>
					<option >anulado</option>
					<option >cancelado</option>
					<option >conciliados</option>
					</select>
				</div>
		
		<div class="input-group mb-3 ">
  			<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Fecha de conciliacion </span>
  			</div>
  			<input type="date" class="form-control" 
  					placeholder="Ingrese la fecha" id="conciliacion" name="conciliacion" />
		</div>
		<div class="form-check">
  			<input class="form-check-input" type="checkbox" id="no_conciliados" name="no_conciliados" checked/>  			
  			<label class="form-check-label" for="defaultCheck1">
    			Incluir no conciliados
  			</label>  			
		</div>
	</div>
	<div class="form-group">	
		
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  					</div>
  					<select class="form-control" id="clase" name="clase" >					
					<option></option>
					<option>con factura</option>
					<option>sin factura</option>
					<option>no corresponde</option>
					</select>
		</div>		
		
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Responsable</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el responsable" id="responsable" name="responsable" />  					
		</div>
		
		<div class="form-group">	
				<label>Observaciones</label>
				<textarea class="form-control" id="observaciones" name="observaciones"></textarea> 
				
				
  			</div>			
	</div>
	