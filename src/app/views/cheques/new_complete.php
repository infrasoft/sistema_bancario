<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

$fecha_actual = date("d-m-Y");
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
?>

<script>
		function showUser(str) 
		{
  		if (str=="") 
  		{
    		document.getElementById("cant_nro").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("cant_nro").innerHTML=this.responseText;
    		}
  		}
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/nros_a_letras/"+str, true);
  		xmlhttp.send();
		}
		
		function showUser_date(str) 
		{
  		if (str=="") 
  		{
    		document.getElementById("emision").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("d_emision").innerHTML=this.responseText;
    		}
  		}
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/fecha_a_texto/"+str, true);
  		xmlhttp.send();
		}
		
		function showUser_date2(str) 
		{
  		if (str=="") 
  		{
    		document.getElementById("vencimiento").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("d_vencimiento").innerHTML=this.responseText;
    		}
  		}
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/fecha_a_texto/"+str, true);
  		xmlhttp.send();
		}		
</script>

<div class="text-center container">
	<?php echo $mensaje; ?>
	<h1 >Nuevo Cheque</h1>
	<div class="container-fluid">
		<div class="alert alert-dark" >
			<h4>Banco: <b><?php echo $cuenta[0]->Banco;?></b> - 
			Cuenta: <b><?php echo $cuenta[0]->nro_cuenta;?></b> - Cheque Diferido </h4>
		</div>
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				<input type="hidden" id="banco" name="banco" value="<?php echo $cuenta[0]->id_banco;?>"/>
				<input type="hidden" id="cuenta" name="cuenta" value="<?php echo $cuenta[0]->nro_cuenta;?>"/>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nº de Cheque</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el nro" id="nro" name="nro" 
  					value="<?php echo $cuenta[0]->ultimo_cheque + 1;?>" readonly/>  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">$</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" step="0.01"
  					placeholder="Ingrese la cantidad" id="cantidad" name="cantidad" onchange="showUser(this.value)" required/>
				</div>
			</div> 
			<div class="form-group">
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de Emision Salta, </span>
  					</div>
  					<input type="date" class="form-control" id="emision" name="emision" 
  						value="<?php echo date("Y-m-d",strtotime($fecha_actual."- 1 days"));?>" onchange="showUser_date(this.value)" required/>
  					 
  					<div class="input-group-append">
  						<span class="input-group-text" id = "d_emision" name = "d_emision">
  							<?php echo strftime("%d de %B de %Y", strtotime($fecha_actual."- 1 days"));?>
  						</span>
  					</div>
				</div>
			</div>
			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de pago Salta,</span>
  					</div>
  					<input type="date" class="form-control" placeholder="ingrese fecha" id="vencimiento" name="vencimiento" 
  						value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>" onchange="showUser_date2(this.value)" required/>
  					<div class="input-group-append">
  						<span class="input-group-text" id = "d_vencimiento" name = "d_vencimiento">
  							<?php echo strftime("%d de %B de %Y", strtotime($fecha_actual));?>
  						</span>
  					</div>
				</div> 
				 			
  			</div>
  			<div class="form-group">
  				<div class="input-group mb-3">
  					
  					<select class="form-control" id="tipo" name="tipo" >				
						<option>A</option>
						<option>P</option>
					</select>
					<div class="input-group-append">
    					<label class="input-group-text" for="inputGroupSelect01">A la orden</label>
  					</div>
				</div>
				<div class="autocomplete">
				<div class="input-group mb-3 ">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Paguese a </span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Destinatario" id="paguese" name="paguese" />
				</div>
				</div>
  			</div>
  			<div class="form-group">
  				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Confecciono</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Responzable" id="confecciono" name="confecciono" value="<?php echo $_SESSION["confecciono"]; ?>"
  					 />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  					</div>
  					<select class="form-control" id="clase" name="clase" >					
					<option>con factura</option>
					<option>sin factura</option>
					<option>no corresponde</option>
					</select>
				</div>
			</div>
  			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Responsable</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el responsable" id="responsable" name="responsable" value="<?php echo $_SESSION["responsable"]; ?>" required/>  					
				</div>
							
			</div>
  			<div class="form-group">	
				<label>La cantidad de</label>
				<textarea class="form-control" id="cant_nro" name="cant_nro" cols="50"></textarea>
				
			</div>
  			<div class="form-group">	
				<label>Observaciones</label>
				<textarea class="form-control" id="observaciones" name="observaciones"></textarea> 	
			</div>
  			<div class="form-group">			
				<!-- btn Grabar -->				
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#grabarModal">
  					<i class="far fa-save"></i> Grabar
				</button>
				
				<!-- Modal  btn grabar-->
				<div class="modal fade" id="grabarModal" tabindex="-1" role="dialog" aria-labelledby="#grabarModal" aria-hidden="true">
  					<div class="modal-dialog" role="document">
    					<div class="modal-content">
      						<div class="modal-header">
        						<h5 class="modal-title" id="exampleModalLabel">Libro Banco</h5>
        						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          							<span aria-hidden="true">&times;</span>
        						</button>
						     </div>
      						<div class="modal-body">
        						¿Desea grabar los datos o bien grabar e imprimir?
      						</div>
      						<div class="modal-footer">
        						<button type="button" class="btn btn-secondary" data-dismiss="modal">
        							<i class="far fa-window-close"></i> Cerrar
        						</button>
        						<button type="submit" class="btn btn-primary">
        							<i class="far fa-save"></i> Grabar
        						</button>  
        						<button type="submit" class="btn btn-primary" id="print" name="print" value="Lote">
        							<i class="fas fa-print"></i> Grabar e Imprimir Lote
        						</button>      						
        						
      						</div>
    					</div>
  					</div>
				</div>
				<!-- btn Imprimir Lote -->	
				<a href="<?=base_url();?>index.php/imprimir/print_pending/<?php echo $cuenta[0]->id_banco;?>/<?php echo $cuenta[0]->nro_cuenta;?>/"
					target='_blank'>
					<button type="button" class="btn btn-danger"> <i class="fas fa-print"></i> Imprimir Lote </button>
				</a>
				<!-- btn Grabar -->				
				<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#limpiarModal">
  					<i class="fas fa-broom"></i> Limpiar Lote
				</button>
				
				<!-- Modal  btn limpiar-->
				<div class="modal fade" id="limpiarModal" tabindex="-1" role="dialog" aria-labelledby="#limpiarModal" aria-hidden="true">
  					<div class="modal-dialog" role="document">
    					<div class="modal-content">
      						<div class="modal-header">
        						<h5 class="modal-title" id="exampleModalLabel">Libro Banco</h5>
        						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          							<span aria-hidden="true">&times;</span>
        						</button>
						     </div>
      						<div class="modal-body">
        						¿Desea Limpiar los cheques en proceso de impresion?
      						</div>
      						<div class="modal-footer">
        						<button type="button" class="btn btn-secondary" data-dismiss="modal">
        							<i class="far fa-window-close"></i> Close
        						</button>
        						<a href="<?=base_url();?>index.php/cheques/clean_checks/<?php echo $cuenta[0]->id_banco;?>/<?php echo $cuenta[0]->nro_cuenta;?>/">
        							<button type="button" class="btn btn-danger" id="clean" name="clean">
        								<i class="fas fa-broom"></i> Limpiar Lote</button>        							 
				 				</a>
      						</div>
    					</div>
  					</div>
				</div>				
				
  			</div>	
		<?=form_close()?>
		<br/>
		<div class="alert alert-dark" >
			<h2><b><?php echo $cuenta[0]->razonSocial;?></b></h2>
		</div>
	</div>
</div> <br /><br /><br /> 
