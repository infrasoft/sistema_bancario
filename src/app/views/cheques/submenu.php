<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="container-fluid">
	<div class="btn-group " >
		<a class="btn btn-primary" href="<?=base_url();?>index.php/cheques/" role="button">
			<i class="fas fa-list"></i> Lista
		</a>	
		<a class="btn btn-primary" href="<?=base_url();?>index.php/cheques/avanced_seach/" role="button">
			<i class="fab fa-searchengin"></i> Busqueda Avanzada
		</a>
	</div>
	