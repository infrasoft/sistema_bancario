<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >Banco </span>
  			</div>
  			<input type="text" class="form-control" placeholder="Ingrese Banco" 
  			id="Banco" name="Banco" value="" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >Titular </span>
  			</div>
  			<input type="text" class="form-control" placeholder="Ingrese el titular" 
  			id="razonSocial" name="razonSocial" value="" />
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<label class="input-group-text" for="inputGroupSelect01">Tipo de Cuenta</label>
  			</div>
  			<select class="form-control" id="tipo_cuenta" name="tipo_cuenta" >
  				<option></option>
				<option>Caja de Ahorros</option>
				<option>Cuenta Corrientes</option>
				<option>Cuenta Corriente especial</option>						
				<option>otros</option>
			</select>
		</div>
	</div>