<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

$fecha_actual = date("d-m-Y");
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
//$this->load->helpers("vistas");
?>

<script>
		function showUser(str) 
		{
  		if (str=="") 
  		{
    		document.getElementById("cant_nro").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
 		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("cant_nro").innerHTML=this.responseText;
    		}
 		}
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/nros_a_letras/"+str, true);
  		xmlhttp.send();
    	}		
</script>
<div class="text-center container">
	<?php echo $mensaje; ?>
	<h1 >Modificar Cheque</h1>
	<div class="container-fluid">
		<div class="alert alert-dark" >
			<h4>Banco: <b><?php echo $cuenta[0]->Banco;?></b> - 
			Cuenta: <b><?php echo $cuenta[0]->nro_cuenta;?></b> - Cheque Diferido </h4>
		</div>
		
		<div class="alert alert-dark" >
			<h2><b><?php echo $cuenta[0]->razonSocial;?></b></h2>
		</div>
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				<input type="hidden" id="banco" name="banco" value="<?php echo $cuenta[0]->id_banco;?>"/>
				<input type="hidden" id="cuenta" name="cuenta" value="<?php echo $cuenta[0]->nro_cuenta;?>"/>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nº de Cheque</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el nro" id="nro" name="nro" value="<?php echo $cheques[0]->nro;?>" readonly/>  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">$</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la cantidad" id="cantidad" name="cantidad" value="<?php echo $cheques[0]->cantidad;?>" 
  					onchange="showUser(this.value)" required/>
				</div>
			</div> 
			<div class="form-group">
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de Emision Salta, </span>
  					</div>
  					<input type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					 id="emision" name="emision" value="<?php echo $cheques[0]->emision;?>" required/>
  					
				</div>
			</div>
  			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha de pago Salta,</span>
  					</div>
  					<input type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="ingrese fecha" id="vencimiento" name="vencimiento" value="<?php echo $cheques[0]->vencimiento;?>" required/>
  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Conciliacion</span>
  					</div>
  					<input type="date" class="form-control" placeholder="ingrese fecha" id="conciliacion" name="conciliacion" 
  					value="<?php echo $cheques[0]->conciliacion;?>" />
  					
				</div>
				 			
  			</div>
  			<div class="form-group">
  				<div class="input-group mb-3">  					
  					<select class="form-control" id="tipo" name="tipo" >				
						<option <?php if($cheques[0]->tipo =="A"){ echo "selected";}?>>A</option>
						<option <?php if($cheques[0]->tipo =="P"){ echo "selected";}?>>P</option>
					</select>
					<div class="input-group-append">
    					<label class="input-group-text" for="inputGroupSelect01">A la orden</label>
  					</div>
				</div>
				
				<div class="autocomplete">
				<div class="input-group mb-3 ">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Paguese a </span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Destinatario" id="paguese" name="paguese" value="<?php echo $cheques[0]->paguese;?>" required/>
				</div>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >					
					<option <?php if($cheques[0]->estado =="activo"){ echo "selected";}?>>activo</option>
					<option <?php if($cheques[0]->estado =="anulado"){ echo "selected";}?>>anulado</option>
					<option <?php if($cheques[0]->estado =="cancelado"){ echo "selected";}?>>cancelado</option>
					<option <?php if($cheques[0]->estado =="conciliados"){ echo "selected";}?>>conciliados</option>
					</select>
				</div>				
				
  			</div>
  			<div class="form-group">
  				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Impreso</label>
  					</div>
  					<select class="form-control" id="impreso" name="impreso" >					
					<option <?php if($cheques[0]->impreso =="true"){ echo "selected";}?> value="true">Si</option>
					<option <?php if($cheques[0]->impreso =="false"){ echo "selected";}?> value="false">No</option>					
					</select>
				</div>
				
  				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Confecciono</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Responzable" id="confecciono" name="confecciono" value="<?php echo $cheques[0]->confecciono;?>"
  					 readonly/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  					</div>
  					<select class="form-control" id="clase" name="clase" >					
					<option <?php if($cheques[0]->clase =="con factura"){ echo "selected";}?>>con factura</option>
					<option <?php if($cheques[0]->clase =="sin factura"){ echo "selected";}?>>sin factura</option>
					<option <?php if($cheques[0]->clase =="no corresponde"){ echo "selected";}?>>no corresponde</option>
					</select>
				</div>
			</div>
  			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Responsable</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el responsable" id="responsable" name="responsable" 
  					value="<?php echo $cheques[0]->responsable;?>" required/>  					
				</div>
							
			</div>
  			<div class="form-group">	
				<label>La cantidad de</label>
				<textarea class="form-control" id="cant_nro" name="cant_nro" cols="50"><?php echo $cant_nro;?>
				</textarea>
				
			</div>
  			<div class="form-group">	
				<label>Observaciones</label>
				<textarea class="form-control" id="observaciones" name="observaciones"><?php echo $cheques[0]->observaciones;?></textarea>
				
				<button type="submit" class="btn btn-primary"><i class="far fa-save"></i> Actualizar</button>
				<button class="btn btn-danger" id="print" name="print" value="ok"><i class="fas fa-print"></i> Imprimir</button>
				
  			</div>	
		<?=form_close()?>
		
	</div>
</div> <br /><br /><br />