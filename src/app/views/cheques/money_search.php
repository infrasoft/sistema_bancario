<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>

<?=btn_ocultar("<i class='fas fa-cash-register'></i> Rango de efectivo","cash_range", '<div class="form-group">
		<h3>Seleccionar el rango de dinero</h3><br/>
		<div class="form-group">
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Desde $</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la cantidad" id="desde_cantidad" name="desde_cantidad" value="0"/>
		</div>
		
		<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Hasta $</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la cantidad" id="hasta_cantidad" name="hasta_cantidad" />
		</div>
	</div>
	</div>'); ?>
