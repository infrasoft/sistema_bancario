<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Lista de cheques</h1>
	<div class="text-center">
		<?=form_open(base_url().'index.php/cheques/', array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
			<div class="form-group text-center">
        		<input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 		name="buscar" required/>
        
        		<select class="form-control" id="campo" name="campo">
        			<option value="razonSocial">Razon Social</option>
        			<option>Banco</option>
        			<option value="tipo_cuenta">Tipo de Cuenta</option>
        			<option>cuenta</option>
        			<option value="nro">Nº de Cheque</option>
        			<option value="paguese">A la orden de</option>
        			<option value="estado">Estado</option>
        			<option>emision</option>
        			<option>vencimiento</option>
        			<option>conciliacion</option>
        			<option>cantidad</option>
        			<option>confecciono</option>
        		 	<option>clase</option>
        		 	<option>responsable</option>
        		 	<option>observaciones</option>
        		</select>
      		</div>
      		<button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
		<?=form_close();?>
		<a href="<?=base_url();?>index.php/cheques/avanced_seach/" 
			title="Realiza la busqueda avanzada por diferentes criterios">
			<button type="button" class="btn btn-primary">
				<i class="fab fa-searchengin"></i> Busqueda Avanzada
			</button>
		</a>
	</div>
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Cheques
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Vencimiento</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Cuenta</th>
                  	<th>Banco</th>
                  	<th> Nº</th>                    
                    <th>Emitido</th>                   
                    <th>Vencimiento</th>
                    <th>A la orden </th>
                    <th>Importe</th>
                    <th title="Opciones para modificar el cheque">OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0;
                		foreach ($lista as $row) 
                		{					
							$row->cantidad = round($row->cantidad,2);								
							if($row->estado == "anulado" OR $row->estado == "cancelado")
							{		
								echo "<tr class='text-danger'>
									<td><b>".$row->razonSocial."</b></td>
									<td><b>".$row->Banco."</b></td>
									<td><b>".$row->nro."</b></td>									
									<td><b>".invierte_fecha($row->emision)."</b></td>
									<td><b>".invierte_fecha($row->vencimiento)."</b></td>
									<td><b>".$row->paguese."</b></td>
									<td><b>".number_format($row->cantidad,2,",",".")."</b>";
							}else
							{
								if ($row->estado == "conciliados") 
								{
									echo "<tr class='text-primary'>
										<td><b>".$row->razonSocial."</b></td>
										<td><b>".$row->Banco."</b></td>
										<td><b>".$row->nro."</b></td>									
										<td><b>".invierte_fecha($row->emision)."</b></td>
										<td><b>".invierte_fecha($row->vencimiento)."</b></td>
										<td><b>".$row->paguese."</b></td>
										<td><b>".number_format($row->cantidad,2,",",".")."</b>";
								}
								else
								{
									echo "<tr>
										<td>".$row->razonSocial."</td>
										<td>".$row->Banco."</td>
										<td>".$row->nro."</td>									
										<td>".invierte_fecha($row->emision)."</td>
										<td>".invierte_fecha($row->vencimiento)."</td>
										<td>".$row->paguese."</td>
										<td>".number_format($row->cantidad,2,",",".");
								}
							}
							$total += $row->cantidad;		
							echo	"</td>									
									<td>
										<a href='".base_url()."index.php/cheques/update_check/".
																				$row->id_banco."/".
																				$row->cuenta."/".
																				$row->nro."/'
											  title='Modificar dato de cheque NRO:".$row->nro."'  target='_blank'>
											<i class='fas fa-money-check'></i>
										</a> - 
										<a href='".base_url()."index.php/cheques_comprobantes/link_checks_to_receipts/0/".
													$row->id_banco."/".	$row->cuenta."/".$row->nro."/".
													"/2/' title='Vincular a recibo'  target='_blank'>
											<i class='fas fa-money-check' style='color:green'></i>
										</a> -
										<a href='".base_url()."index.php/imprimir/print_cheque/".$row->id_banco."/".
																				$row->cuenta."/".
																				$row->nro."/'
											title='Imprimir Cheque Nro: ".$row->nro."' target='_blank'>
											<i class='far fa-file-pdf' style='color:red;'></i>
										</a> - 
										<a href='".base_url()."index.php/cheques/delete_check/".
																				$row->id_banco."/".
																				$row->cuenta."/".
																				$row->nro."/'
											  title='Eliminacion Fisica del cheque NRO:".$row->nro."'  target='_blank'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a> 
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: <b>$<?=number_format($total,2,",",".");?></b></p>
</div>		