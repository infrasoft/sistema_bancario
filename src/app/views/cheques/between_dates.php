<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="form-group">
<?php
echo btn_ocultar("<i class='far fa-calendar-alt'></i> Emision y Cobro","date_range2",'<h3>Seleccionar el rango por fecha de emision y cobro</h3>
	<div class="form-group">		
			
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" >Fecha de emision menor igual a : </span>
  			</div>
  			<input type="date" class="form-control"  id="fecha_menor" name="fecha_menor" />  					
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text">Fecha de cobro mayor igual a :</span>
  			</div>
  			<input type="date" class="form-control" id="fecha_mayor" name="fecha_mayor" />  					
		</div>
	</div>');
?>
</div>