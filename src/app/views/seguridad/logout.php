<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="container-fluid">
  <h2 class="font-weight-bold"> Ha Salido del sistema </h2>
  
  <p>Por favor ingrese nuevamente al sistema haciendo 
  	<a href="<?=base_url();?>index.php/seguridad/" class="btn btn-primary"> click aqui </a></p>
 </div>
