<div class="container">
	<h1>Cambio de password</h1>
	<?php echo $mensaje; ?>
	<p>En esta seccion podremos cambiar el acceso al sistema</p>
	<?php echo form_open('', 
							array( "class"=>"form-inline","role"=>"form","id"=>"update","name"=>"update"));?>
	<div class="form-group">
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">Ingrese el password</span>
  			</div>
  			<input type="password" class="form-control" id="old" name="old" required/>  					
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">nuevo password</span>
  			</div>
  			<input type="password" class="form-control" id="new" name="new" required/>  					
		</div>
		
		<div class="input-group mb-3">
  			<div class="input-group-prepend">
    			<span class="input-group-text" id="inputGroup-sizing-default">repita el password</span>
  			</div>
  			<input type="password" class="form-control" id="verified" name="verified" required/>  					
		</div>
	</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary" value="Actualizar">
	</div>						
	<?=form_close();?>
</div>