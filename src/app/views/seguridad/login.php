<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <?php echo $mensaje; ?>
      <div class="card-body">
        <?php echo form_open('', 
							array( "id"=>"log_in","name"=>"log_in"));?>
          <div class="form-group">
           
            	
              <input type="text" id="loginname"  name="loginname" class="form-control" placeholder="Ingrese el Usuario" required="required" >
              
           
          </div>
          <div class="form-group">
            
              <input type="password" id="password" name="password" class="form-control" placeholder="Ingrese el Password" required="required">              
            
          </div>
          
          <input type="submit" class="btn btn-lg btn-primary btn-block" value="Ingresar al sistema">
        <?=form_close();?>
       
      </div>
    </div>
  </div>