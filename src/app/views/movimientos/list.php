<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1><?php echo $titulo;?></h1>
	<div class="text-center">
	<?=form_open(base_url().'index.php/movimientos/', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"company_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="lista_venta" name="lista_venta">
        	<option>Banco</option>
        	<option value="razonSocial">Titular</option>
        	<option>cuenta</option>
			<option>cbu</option>
			<option>movimiento</option>
			<option>concepto</option>
			<option>valor</option>
			<option>fecha</option> 
			<option>estado</option>
			<option>observaciones</option>  	
        </select>
      </div>
      <button type="submit" class="btn btn-primary">
      	<i class="fas fa-search"></i> Buscar 
      </button>
<?=form_close();?>
	<a href="<?=base_url();?>index.php/movimientos/avanced_seach_form/" 
		title="Realiza la busqueda avanzada">
		<button type="button" class="btn btn-primary">
		<i class="fab fa-searchengin"></i> Busqueda Avanzada
		</button>
	</a>
	</div>
	
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Movimientos Bancarios
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>banco</th>
                  	<th>titular</th>
                    <th>cuenta</th>                   
                    <th>concepto</th>
                    <th>valor</th>
                    <th>fecha</th>                    
                    <th title="Operaciones de movimientos bancarios">Op</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>banco</th>
                  	<th>titular</th>
                    <th>cuenta</th>                   
                    <th>concepto</th>
                    <th>valor</th>
                    <th>fecha</th>                    
                    <th title="Operaciones de movimientos bancarios">Op</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0;
                		foreach ($lista as $row) 
                		{
                			$row->valor = round($row->valor,2);                			
							echo "<tr>
									<td>".$row->Banco."</td>
									<td>".$row->razonSocial."</td>
									<td>".$row->cuenta."</td>								
									<td>".$row->concepto_detalle."</td>
									<td>".number_format($row->valor,2,",",".")."</td>
									<td>".invierte_fecha($row->fecha)."</td>									
									<td>										
										<a href='".base_url()."index.php/movimientos/movements_update/".$row->id.
											"/' title='Actualizar movimiento Nº ".$row->id."' target='_blank'>
											<i class='fas fa-file-invoice'></i>
										</a> - 
										<a href='".base_url()."index.php/movimientos/movements_delete/".$row->id."/'
											  title='Eliminacion del movimiento NRO:".$row->id."' target='_blank'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a>
									</td>
								  </tr>";
							$total += $row->valor;
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <p>Total: <b>$<?=number_format($total,2,",",".");?></b></p>
</div>