<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class ="container text-center">
	<?php echo $mensaje;?>
	<h1><?php echo $titulo;?></h1>
	<div class="card-body">
		<div class="alert alert-dark" >
			<h4>Banco: <b><?php echo $cuenta[0]->Banco;?></b> - 
			Cuenta: <b><?php echo $cuenta[0]->nro_cuenta;?></b> </h4>
		</div>
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
		<div class="form-group">
			<div class="autocomplete mb-3">  				
  				<input type="hidden" class="form-control" id="id" name="id" value="<?=$vector[0]->id;?>"/>  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Movimiento</span>
  				</div>
  				<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  				placeholder="Ingrese el Nro" id="movimiento" name="movimiento" value="<?=$vector[0]->movimiento;?>" required/>  					
			</div>
			
			<div class="autocomplete mb-3">  					
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Concepto" id="concepto" name="concepto" 
  					value="<?=$concepto[0]->concepto." - ".$concepto[0]->codigo."|".$concepto[0]->id_concepto;?>" required/>
				</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Valor $</span>
  				</div>
  				<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  				placeholder="Ingrese la Valor" id="valor" name="valor" value="<?=$vector[0]->valor;?>" step="0.01" required/>  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  				</div>
  				<input type="date" class="form-control" fecha" id="fecha" name="fecha" value="<?=$vector[0]->fecha;?>" required/>
			</div>
			
			<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo" name="tipo" >
						<option <?php if($vector[0]->tipo =="ingreso"){ echo "selected";}?> value="ingreso">credito</option>
						<option <?php if($vector[0]->tipo =="egreso"){ echo "selected";}?> value="egreso">débito</option>						
					</select>
				</div>
		</div>
		<div class="form-group">
			 <label for="observaciones">Observaciones</label>
 			<textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3"><?=$vector[0]->observaciones;?></textarea>
 			-
 			<button type="submit" class="btn btn-primary">
					<i class="fas fa-pen"></i> Actualizar
				</button>
		</div>	
	<?=form_close();?>
	<div class="alert alert-dark" >
			<h2><b><?php echo $cuenta[0]->razonSocial;?></b></h2>
		</div>
 </div> <br/> <hr />
</div>