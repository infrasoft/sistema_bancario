<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
$fecha_actual = date("d-m-Y");
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
?>
<div class="text-center container">
	<?php echo $mensaje; ?>
	<h1><?php echo $titulo;?></h1>
	<div class="container-fluid">
		<div class="alert alert-dark" >
			<h4>Banco: <b><?php echo $cuenta[0]->Banco;?></b> - 
			Cuenta: <b><?php echo $cuenta[0]->nro_cuenta;?></b> </h4>
		</div>
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new"));?>
			<input type="hidden" id="id_banco" name="id_banco" value="<?php echo $cuenta[0]->id_banco;?>"/> 
			<input type="hidden"  id="cuenta" name="cuenta" value="<?php echo $cuenta[0]->nro_cuenta;?>"/>
			<input type="hidden"  id="saldo" name="saldo" value="<?php echo $cuenta[0]->saldo;?>"/>
			<div class="form-group">
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha </span>
  					</div>
  					<input type="date" class="form-control" 
  					placeholder="ingrese fecha" id="fecha" name="fecha"  value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>" required/>
  					
				</div> 
				
				<div class="autocomplete mb-3">  					
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Concepto" id="concepto" name="concepto" value="" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
	    				<span class="input-group-text" id="inputGroup-sizing-default">Importe $</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la Valor" id="valor" name="valor" step="0.01" required/>  					
				</div>
			</div>
			<div class="form-group">
				
				<div class="input-group mb-3">
	  				<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Movimiento</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Nro" id="movimiento" name="movimiento" />  					
				</div>
							
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo" name="tipo" >
						<option value="ingreso">credito</option>
						<option value="egreso" selected>debito</option>						
					</select>
				</div>
			</div>
			<div class="form-group">	
				<label for="observaciones">Observaciones</label>
 				<textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3"></textarea>
 			 				
 				<button type="submit" class="btn btn-primary">
 					<i class="fas fa-plus-circle"></i> Registrar movimiento
 				</button>
			</div>
			
		<?=form_close()?>
		<br/>
		<div class="alert alert-dark" >
			<h2><b><?php echo $cuenta[0]->razonSocial;?></b></h2>
		</div>
	</div>
</div>