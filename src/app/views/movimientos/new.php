<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class ="container text-center">
	<?php echo $mensaje;?>
	<h1><?php echo $titulo;?></h1>
	<div class="card-body">
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
		<div class="form-group">
			<div class="autocomplete mb-3">  				
  				<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  				placeholder="Ingrese cuenta" id="id_banco" name="id_banco" />  					
			</div>
			
			<div class="autocomplete mb-3">  				
  				<input type="hidden"  id="cuenta" name="cuenta" value=""/>  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Movimiento</span>
  				</div>
  				<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  				placeholder="Ingrese el Nro" id="movimiento" name="movimiento" required/>  					
			</div>
			
			<div class="autocomplete mb-3">  					
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Concepto" id="concepto" name="concepto" value="" required/>
				</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Valor</span>
  				</div>
  				<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  				placeholder="Ingrese la Valor" id="valor" name="valor" step="0.01" required/>  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  				</div>
  				<input type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  				placeholder="ingrese fecha" id="fecha" name="fecha" required/>
			</div>
			
			<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo" name="tipo" >
						<option>ingreso</option>
						<option selected>egreso</option>						
					</select>
				</div>
		</div>
		<div class="form-group">
			 <label for="observaciones">Observaciones</label>
 			<textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3"></textarea>
 			-
 			<input type="submit" class="btn btn-primary" value="Actualizar">
		</div>	
	<?=form_close();?>
 </div> <br/> <hr />
</div>