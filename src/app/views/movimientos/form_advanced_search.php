<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<h1>Busqueda avanzada</h1>
	
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new"));?>
		<div class="form_group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Cuentas</span>
  				</div>  					
  				<select class="form-control" id="id_cuenta" name="id_cuenta" >
  					<option></option>
  					<?php
  					foreach ($cuentas as $row) 
  					{
						echo "<option value='".$row->id_banco."#".$row->nro_cuenta."'>".$row->razonSocial." - ".$row->Banco." - ".$row->nro_cuenta."</option>";
					}
  					?>
  				</select>	
			</div>
		</div>
		<div class="form-group">
				
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Desde </span>
  				</div>
  				<input type="date" class="form-control" 
  					placeholder="ingrese fecha" id="fecha_desde" name="fecha_desde"  />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Hasta </span>
  				</div>
  				<input type="date" class="form-control" 
  					placeholder="ingrese fecha" id="fecha_hasta" name="fecha_hasta"  />  					
			</div>  
			
			<div class="autocomplete mb-3">  					
  				<input type="text" class="form-control" placeholder="Ingrese el Concepto" id="concepto" name="concepto" />
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Movimiento</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese el Nro" id="movimiento" name="movimiento" />  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe desde $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Valor" id="valor_desde" name="valor_desde" step="0.01"/>  					
			</div>
			
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
	    			<span class="input-group-text" id="inputGroup-sizing-default">Importe hasta $</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="Ingrese la Valor" id="valor_hasta" name="valor_hasta" step="0.01"/>  					
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">
  				<div class="input-group-prepend">
    				<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  				</div>
  				<select class="form-control" id="tipo" name="tipo" >
  					<option ></option>
					<option value="ingreso">credito</option>
					<option value="egreso">debito</option>						
				</select>
			</div>			
				
				<label for="observaciones">Observaciones</label>
 				<textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3"></textarea>		
				
							
		</div>
		<div class="form-group">	
			<button type="submit" class="btn btn-primary" id="search" name="search" value="true">
				<i class="fas fa-search"></i> Buscar
			</button>
		</div>				
	<?=form_close()?>
</div>