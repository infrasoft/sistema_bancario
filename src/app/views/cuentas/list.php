<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container" id="content-wrapper">
	<?php echo $mensaje;?>
	<h1>Lista de cuentas</h1>
	<div class="text-center">
	<?=form_open('', array("class"=> "form-inline", "role" => "form", "id"=>"list","name"=>"list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="lista_venta" name="lista_venta">
        	<option value="razonSocial">Razon Social</option>
        	<option >Banco</option>
        	<option value="nro_cuenta">Nº de cuenta</option>        	
        	<option>tipo</option>
        	<option>sucursal</option>
        	<option>saldo</option>
        	<option>ultimo_cheque</option>
        	<option>observaciones</option>        	
			<option>estado</option>  	
        </select>
      </div>
      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
<?=form_close();?>
	<!--<a href="<?=base_url();?>index.php/empresas/company_list/" title="Realiza la busqueda avanzada">
		<p>Busqueda Avanzada</p>
	</a>-->
	</div>
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de cuentas
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	<th>Banco</th>
                  	<th>Titular</th>
                  	<th>Tipo</th>
                    <th>Nro de cuenta</th>                 
                    <th>Saldo</th>
                    <th>Ultimo Cheque</th>
                    <th title="Operaciones con cuentas">OP</th>                   
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	<th>Banco</th>
                  	<th>Titular</th>
                  	<th>Tipo</th>
                    <th>Nro de cuenta</th>                 
                    <th>Saldo</th>
                    <th>Ultimo Cheque</th>
                    <th title="Operaciones con cuentas">OP</th> 
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		foreach ($lista as $row) 
                		{
                			if ($row->saldo<0) 
                			{
								$clase = "class='text-danger'";
							}
							else {
								$clase = "";
							}
							echo "<tr ".$clase.">
									<td >".$row->Banco."</td>
									<td>".$row->razonSocial."</td>
									<td>".$row->tipo."</td>
									<td>".$row->nro_cuenta."</td>									
									<td>".number_format($row->saldo,2,",",".")."</td>
									<td>".$row->ultimo_cheque."</td>																		
									<td>
										<a href='".base_url()."index.php/cuentas/update_account/".$row->id_banco."/".$row->nro_cuenta.
												"/' title='Modificar cuenta Nº ".$row->nro_cuenta."'  target='_blank'>
											<i class='fas fa-address-book'></i>
										</a> - 
										<a href='".base_url()."index.php/cuentas/delete_account/".
																				$row->id_banco."/".$row->nro_cuenta."/'
											  title='Eliminar cuenta NRO:".$row->nro_cuenta."'  target='_blank'>
											<i class='far fa-trash-alt' style='color:black;'></i>
										</a> -
										<a href='".base_url()."index.php/movimientos/movements_new_list/".$row->id_banco."/".$row->nro_cuenta.
												"/' title='Crear nuevo movimiento en cuenta Nº ".$row->nro_cuenta."' target='_blank'>
											<i class='fas fa-file-invoice' style='color: gray;'></i>
										</a> -
										<a href='".base_url()."index.php/movimientos/avanced_seach/".$row->id_banco."/".$row->nro_cuenta.
												"/' title='Movimientos vinculados a la cuenta' target='_blank'>
											<i class='fas fa-money-check' style='color: orange;'></i>
										</a> -
										<a href='".base_url()."index.php/cheques/new_check/".$row->id_banco."/".$row->nro_cuenta.
												"/' title='Crear nuevo Cheque en cuenta Nº ".$row->nro_cuenta."' target='_blank'>
											<i class='fas fa-money-check' style='color: green;'></i>
										</a>-
										<a href='".base_url()."index.php/cheques/avanced_seach/2/".$row->id_banco."/".$row->nro_cuenta.
												"/' title='Muestra los cheques de la cuenta Nº ".$row->nro_cuenta."' target='_blank'>
											<i class='fas fa-money-check' style='color: blue;'></i>
										</a>
									</td>
								  </tr>";
						}
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
</div>