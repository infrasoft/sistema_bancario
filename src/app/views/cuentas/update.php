<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>

<div class="container-fluid">
	<?php echo $mensaje;?>
	<h1>Actualizar cuenta</h1>
	<div class="card-body">
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				<div class="autocomplete mb-3">  					
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Banco" id="banco" name="banco" value="<?=$vector[0]->Banco; ?>"/>
  					<input type="hidden" name="id_banco" id="id_banco" value="<?=$vector[0]->id_banco; ?>"/>
				</div>
				
				<div class="autocomplete mb-3">  					
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Titular" id="titular" name="titular" value="<?=$vector[0]->razonSocial; ?>"/>
  					<input type="hidden" name="id_empresa" id="id_empresa" value="<?=$vector[0]->id_empresa; ?>"/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nº de  Cuenta</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Nro" id="nro_cuenta" name="nro_cuenta" value="<?=$vector[0]->nro_cuenta; ?>" required/>  					
				</div>				
				
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo" name="tipo" >
						<option <?php if($vector[0]->tipo =="Caja de Ahorros"){ echo "selected";}?>>Caja de Ahorros</option>
						<option <?php if($vector[0]->tipo =="Cuenta Corrientes"){ echo "selected";}?>>Cuenta Corrientes</option>
						<option <?php if($vector[0]->tipo =="Cuenta Corriente especial"){ echo "selected";}?>>Cuenta Corriente especial</option>						
						<option <?php if($vector[0]->tipo =="otros"){ echo "selected";}?>>otros</option>
					</select>
				</div>	
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">CBU</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el CBU" id="cbu" name="cbu" value="<?=$vector[0]->cbu; ?>" required/>  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Sucursal</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la Sucursal" id="sucursal" name="sucursal" value="<?=$vector[0]->sucursal; ?>"/>  					
				</div>				
				
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Saldo</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la Saldo" id="saldo" name="saldo" value="<?=$vector[0]->saldo; ?>" required/>  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Ultimo Cheque</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el nro" id="ultimo_cheque" name="ultimo_cheque" value="<?=$vector[0]->ultimo_cheque; ?>" required/>  					
				</div>
				
				 <label for="observaciones">Observaciones</label>
  				<textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3">
  					<?=$vector[0]->observaciones; ?></textarea>  				
  				
			</div> 
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >
						<option <?php if($vector[0]->estado =="activa"){ echo "selected";}?>>activa</option>
						<option <?php if($vector[0]->estado =="cancelada"){ echo "selected";}?>>cancelada</option>					
					</select>
				</div>	
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-pen"></i> Actualizar
				</button>
  				
			</div>
		<?=form_close();?>
	</div>		
</div> <br /><br /><br />