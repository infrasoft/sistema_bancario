<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>

<div class="container-fluid">
	<?php echo $mensaje;?>
	<h1>Registrar cuenta</h1>
	<div class="card-body">
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<div class="form-group">
				<div class="autocomplete mb-3">  					
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Banco" id="banco" name="banco" />
  					<input type="hidden" id="id_banco" name="id_banco" value="0"/>  					
				</div>
				-
				<div class="autocomplete mb-3">  					
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el Titular" id="titular" name="titular"/>
  					<input type="hidden" id="id_empresa" name="id_empresa" value="0"/>  
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nº de  Cuenta</span>
  					</div>
  					<input type="text" class="form-control" placeholder="Ingrese el Nro" id="nro_cuenta" name="nro_cuenta" required/>  					
				</div>
								
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo" name="tipo" >
						<option>Caja de Ahorros</option>
						<option>Cuenta Corrientes</option>
						<option>Cuenta Corriente especial</option>						
						<option>otros</option>
					</select>
				</div>	
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">CBU</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el CBU" id="cbu" name="cbu" required/>  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" >Sucursal</span>
  					</div>
  					<input type="text" class="form-control" id="sucursal" name="sucursal" />  					
				</div>				
				
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Saldo</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese la Saldo" id="saldo" name="saldo" required/>  					
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Ultimo Cheque</span>
  					</div>
  					<input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Ingrese el nro" id="ultimo_cheque" name="ultimo_cheque" required/>  					
				</div>
			</div>
			<div class="form-group">	
				 <label for="observaciones">Observaciones</label>
  				<textarea class="form-control rounded-0" id="observaciones" name="observaciones" rows="3"></textarea>  				
  							
								  				
  				<button type="submit" class="btn btn-primary">
  					<i class="far fa-plus-square"></i> Registrar
  				</button>
			</div>
		<?=form_close();?>
	</div>		
</div>

