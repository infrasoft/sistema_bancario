<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
//print_r($vector);
foreach ($vector as $row) 
{
?>
<div class="text-center container">
	<?php echo $mensaje; ?>
	<h1 >Modificar comprobantes</h1>
	<div class="container-fluid">
		<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"update","name"=>"update","onkeypress"=>"return anular(event)"));?>
			<div class="form-group mb-3">
				<p><?=strtoupper($row->typo);?>:  <b> <?=$empresa[0]->razonSocial; ?></b> Tipo: <b><?=$empresa[0]->tipo; ?></b> 
					CUIT: <b><?=$empresa[0]->cuit; ?></b> 
				</p>
				<p>Comprobante: <b><?=$vector[0]->id_comprobante; ?></b></p>
			</div>
			<br/>				
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Facturacion</span>
  					</div>  					
  					<select class="form-control" id="id_empresa_dest" name="id_empresa_dest" >
  						<?php
  						foreach ($cuentas as $row2) 
  						{
  							if ($row2->id_empresa == $row->id_empresa_dest ) 
  							{
								echo "<option value='".$row2->id_empresa."' selected>".$row2->razonSocial."</option>";   
							} else {
								echo "<option value='".$row2->id_empresa."'>".$row2->razonSocial."</option>";  
							 }					  
							
						}
  						?>
  					</select>	
				</div>
				
				<div class="input-group mb-3">
					
					<input type="hidden"  id="id_empresa" name="id_empresa" value="<?=$empresa[0]->id; ?>"/>
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nro</span>
  					</div>
  					<input type="tel" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Nro de Factura" id="nro" name="nro" value="<?=$vector[0]->nro; ?>"/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo_comprobante" name="tipo_comprobante" >
					<option <?php if($row->tipo_comprobante =="Comprobante"){ echo "selected";}?>>Comprobante</option>
					<option <?php if($row->tipo_comprobante =="factura"){ echo "selected";}?>>factura</option>
					<option <?php if($row->tipo_comprobante =="Notas de crédito"){ echo "selected";}?>>Notas de crédito</option>
					<option <?php if($row->tipo_comprobante =="Notas de débito"){ echo "selected";}?>>Notas de débito</option>
					<option <?php if($row->tipo_comprobante =="Orden de compra"){ echo "selected";}?>>Orden de compra</option>
					<option <?php if($row->tipo_comprobante =="Recibos"){ echo "selected";}?>>Recibos</option>
					<option <?php if($row->tipo_comprobante =="Tickets"){ echo "selected";}?>>Tickets</option>
					</select>
				</div>
			</div>
			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  					</div>
  					<input type="date" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="ingrese fecha" id="fecha" name="fecha" value="<?=$vector[0]->fecha;?>" required/>
				</div>				
				
				<div class="input-group mb-3">
					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  					</div>
					<select class="form-control" name = "typo" id="typo">
						<option <?php if ($row->typo == "proveedor") {echo "selected";} ?>>proveedor</option>
						<option <?php if ($row->typo == "cliente") {echo "selected";} ?>>cliente</option>
					</select>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >					
					<option <?php if($row->estado =="activo"){ echo "selected";}?>>activo</option>
					<option <?php if($row->estado =="anulado"){ echo "selected";}?>>anulado</option>
					<option <?php if($row->estado =="cancelado"){ echo "selected";}?>>cancelado</option>
					</select>
				</div>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Pago</label>
  					</div>
  					<select class="form-control" id="pago" name="pago" >					
					<option <?php if($row->pago =="pendiente"){ echo "selected";}?>>pendiente</option>
					<option <?php if($row->pago =="parcial"){ echo "selected";}?>>parcial</option>
					<option <?php if($row->pago =="completado"){ echo "selected";}?>>completado</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Neto</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Neto" id="neto" name="neto" 
  					value="<?=$vector[0]->neto; ?>" step="0.01" onChange="calculo_iva()" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA %</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva" name="iva" value="<?=$vector[0]->iva; ?>" 
  					step="0.01" onChange="calculo_iva()" required/>
				</div>				
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA $</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva_valor" name="iva_valor"  step="0.01" 
  					value="<?=$vector[0]->iva_valor; ?>" onChange="iva_total()" required/>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Neto2</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Neto" id="neto2" name="neto2" 
  					value="<?=$vector[0]->neto2; ?>" step="0.01" onChange="calculo_iva2()" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA2 %</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva2" name="iva2" value="<?=$vector[0]->iva2; ?>" step="0.01" 
  					onChange="calculo_iva2()" />
				</div>				
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA2 $</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva_valor2" name="iva_valor2" 
  						value="<?=$vector[0]->iva_valor2; ?>" step="0.01" onChange="iva_total()"  />
				</div>
					
			</div>
			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Conceptos No Gravados</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="concep_no_grabados" name="concep_no_grabados" 
  					value="<?=$vector[0]->concep_no_grabados; ?>" step="0.01" onChange="iva_total()" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Impuestos internos </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="imp_internos" name="imp_internos" 
  					value="<?=$vector[0]->imp_internos; ?>" step="0.01" onChange="iva_total()" />
				</div>
			</div>
			<div class="form-group">					
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Total</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Total" id="total" name="total" 
  					onChange="iva_total()" value="<?=$vector[0]->total; ?>" required/>
				</div>				
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Saldo</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Saldo" id="saldo" name="saldo" step="0.01"
  					onChange="saldo_estado()" value="<?=$vector[0]->saldo; ?>" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Observaciones</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Otros Datos" id="observaciones" name="observaciones" value="<?=$vector[0]->observaciones; ?>"/>
				</div>
				
				
				<?php //$this->load->view("comprobantes/update-detalle",$detalle); ?>
				
			<div class="form-group">
				
				<button type="submit" class="btn btn-primary" onclick="verificar_saldo()">
					<i class="fas fa-pen"></i> Actualizar
				</button>
			</div>	
		<?=form_close(); }?>
	</div><br/><br/><hr />
</div>
