<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<div class="container text-center" id="content-wrapper">
		<?php echo $mensaje; ?>
	<h1><?php echo $title; ?></h1>
	
	<?=form_open(base_url().'index.php/comprobantes/', array("class"=> "form-inline", "role" => "form", "id"=>"company_list","name"=>"receipts_list"));?>
	<div class="form-group text-center">
        <input type="text" class="form-control" placeholder="Buscar" id="buscar"
        	 name="buscar" required/>
        
        <select class="form-control" id="campo" name="campo">        	
        	<option>cuit</option>
        	<option value="razonSocial">Razon Social</option>
        	<option>facturacion</option>
        	<option>actividad</option>
        	<option value="tipo_comprobante">Tipo Comprobante</option>			
	      	<option value="tipo">Condicion Fiscal</option>
	      	<option>nro</option>
	      	<option>fecha</option>
	      	<option>total</option>
	      	<option>otros</option>
	      	<option>observaciones</option>
        </select>
      </div>
      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Buscar</button>
<?=form_close();?>

	<a href="<?=base_url();?>index.php/comprobantes/receipts_list/<?=$clase ;?>/avanced/" alt="Realiza la busqueda avanzada">
		<button type="button" class="btn btn-primary">
			<i class="fab fa-searchengin"></i> Busqueda Avanzada
		</button>
	</a> 
	
	<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Lista de Comprobantes
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" name="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                  	
                  	<th>Razon Social</th>   
                  	<th>Empresa</th>                 
                    <th>Tipo</th>               
                    <th>Pago</th>                    
                    <th>total</th>
                    <th>Saldo</th>                    
                    <th>OP</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  	
                  	<th>Razon Social</th>
                  	<th>Empresa</th>                   
                    <th>Tipo</th>                  
                    <th>Pago</th>                    
                    <th>total</th>
                    <th>Saldo</th>
                    <th>OP</th>
                  </tr>
                </tfoot>
                <tbody>
                	<?php
                		$total = 0; $saldo = 0; $saldo_neg=""; $total_n_credito=0;
                		foreach ($lista as $row) 
                		{                			
							if ( $row->saldo >0) 
							{
								$saldo_neg = "class = 'text-danger'";
							}
							else {
								$saldo_neg = "";
							}
							echo "<tr ".$saldo_neg.">									
									<td>".$row->razonSocial."</td>
									<td>".$row->facturacion."</td>									
									<td>".$row->tipo_comprobante."</td>									
									<td>".$row->pago."</td>									
									<td>".number_format($row->total,2,",",".")."</td>
									<td>".number_format($row->saldo,2,",",".")."</td>									
									<td>
										-
									</td>
								  </tr>";
							
							if ($row->tipo_comprobante == "Notas de crédito") 
							{
								$total_n_credito += $row->total;
							} else {
								$total += $row->total; $saldo += $row->saldo;
							}							
						}
						$saldo = $saldo - $total_n_credito;
                	?>
                  
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
        <tbody>
    			<tr>
      				<th scope="row">Total</th>
      				<td><p>Total: $<?=number_format($total,2,",",".");?> </p></td>     
    			</tr>
    			<tr>
      				<th scope="row">Notas de crédito:</th>
      				<td><?php
          				if ($total_n_credito != 0) 
          				{
					  		echo "<p> $".number_format($total_n_credito,2,",",".")."</p>";
						}
          				?>
      				</td>      
    			</tr>
			    <tr>
      				<th scope="row">Saldo pendiente de pago:</th>
				      <td><p> $<?=number_format($saldo,2,",",".");?></p></td>     
    			</tr>
  			</tbody>
</div>