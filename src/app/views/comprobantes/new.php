<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
$sel_typo = "";
if (($typo == "cliente") OR ($typo == "proveedor")) 
{
	$sel_typo = '<input type="hidden" name="typo" id="typo" value="'.$typo.'"/>';
}
else {
	$sel_typo = '<div class="input-group mb-3">
					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Clase</label>
  					</div>
					<select class="form-control" name = "typo" id="typo">
						<option>proveedor</option>
						<option>cliente</option>
					</select>
				</div>';
}
?>

<div class="text-center container">
	<?php echo $mensaje; ?>
	<h1 >Nuevo comprobantes</h1>
	<div class="container-fluid">
		<?php echo form_open('', 
						array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new",
								"onkeypress"=>"return anular(event)"));?>
			<div class="form-group mb-3">
				<p><?=strtoupper($typo);?>:  <b> <?=$empresa[0]->razonSocial; ?></b> Tipo: <b><?=$empresa[0]->tipo; ?></b> 
					CUIT: <b><?=$empresa[0]->cuit; ?></b>
					
				</p>				
			</div>
			<br/>				
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Empresa</span>
  					</div>  					
  					<select class="form-control" id="id_empresa_dest" name="id_empresa_dest" >
  						<?php
  						foreach ($cuentas as $row) 
  						{
							echo "<option value='".$row->id_empresa."'>".$row->razonSocial."</option>";
						}
  						?>
  					</select>	
				</div>	
				<div class="input-group mb-3">
					
					<input type="hidden"  id="id_empresa" name="id_empresa" value="<?=$empresa[0]->id; ?>"/>
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Nro</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Nro de Factura" id="nro" name="nro" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo_comprobante" name="tipo_comprobante" >
					<option>Comprobante</option>
					<option selected="factura">factura</option>
					<option>Notas de crédito</option>
					<option>Notas de débito</option>
					<option>Orden de compra</option>
					<option>Recibos</option>
					<option>Tickets</option>
					</select>
				</div>
			</div>
			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Fecha</span>
  					</div>
  					<input type="date" class="form-control" placeholder="ingrese fecha" id="fecha" name="fecha" required/>
				</div>				
				
				<?=$sel_typo;?>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >					
					<option>activo</option>
					<option>anulado</option>
					<option>cancelado</option>
					</select>
				</div>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Pago</label>
  					</div>
  					<select class="form-control" id="pago" name="pago" >					
					<option>pendiente</option>
					<option>parcial</option>
					<option>completado</option>
					</select>
				</div>
				
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Neto</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Neto" id="neto" name="neto" value="0" 
  					step="0.01" onChange="calculo_iva()" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA %</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva" name="iva" value="" step="0.01" 
  					onChange="calculo_iva()" value="21" required/>
				</div>				
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA $</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva_valor" name="iva_valor" value="0"  					
  					 step="0.01" onChange="iva_total()"  required/>
				</div>					
			</div>
			<div class="form-group">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Neto2</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Neto" id="neto2" name="neto2" value="0" 
  					step="0.01" onChange="calculo_iva2()" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA2 %</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva2" name="iva2" value="" step="0.01" 
  					onChange="calculo_iva2()" value="0"/>
				</div>				
			
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">IVA2 $</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="iva_valor2" name="iva_valor2" value="0"  					
  					 step="0.01" onChange="iva_total()"  />
				</div>
			</div>
			<div class="form-group">						
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Conceptos No Gravados</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="concep_no_grabados" name="concep_no_grabados" 
  					value="0" step="0.01" onChange="iva_total()" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Impuestos internos </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el IVA" id="imp_internos" name="imp_internos" value="0" 
  					step="0.01" onChange="iva_total()" />
				</div>
			</div>
			<div class="form-group">				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Total</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Total" id="total" name="total" step="0.01"
  					onChange="iva_total()" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Saldo</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Saldo" id="saldo" name="saldo" step="0.01"
  					onChange="saldo_estado()" required/>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Observaciones</span>
  					</div>
  					<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
  					placeholder="Otros Datos" id="observaciones" name="observaciones" />
				</div>			
			
				<?php //$this->load->view("comprobantes/new-detalle"); ?>
			</div>				
				<button type="submit" class="btn btn-primary" value="Actualizar" onclick="verificar_saldo()">
					<i class="far fa-plus-square"></i> Actualizar
				</button>
			<div class="form-group">
				
			</div>
		<?=form_close();?>
	</div><br/><br/><hr />
</div>
