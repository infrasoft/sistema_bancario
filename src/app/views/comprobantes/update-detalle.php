<?php 
/***************************************************
           http:infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http:infrasoft.com.ar
***************************************************/

?>
<div class="container-fluid">
	<fieldset class="">
		<div class="input-group">
		<input type="text" class="form-control corto" value="Cantidad" disabled="no"/>
		<input type="text" class="form-control medio" value="Concepto" disabled="no"/>
		<input type="text" class="form-control corto" value="Precio" disabled="no"/>
		<input type="text" class="form-control corto" value="Total" disabled="no"/>
	</div>
	<div class="input-group">
		<input name="cant1" id="cant1"  type="number" class="form-control corto"  value="<?php if(isset($detalle[0]->cantidad)){echo $detalle[0]->cantidad;} ?>"/>
		<input name="concep1" id="concep1"  type="text" class="form-control medio" value="<?php if(isset($detalle[0]->concepto)){echo $detalle[0]->concepto;} ?>"/>
		<input name="precio1" id="precio1"  type="number" class="form-control corto" value="<?php if(isset($detalle[0]->precio)){echo $detalle[0]->precio;} ?>"/>
		<input name="tot1" id="tot1"  type="number" class="form-control corto" value="<?php if(isset($detalle[0]->total)){echo $detalle[0]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant2" id="cant2"  type="number" class="form-control corto" value="<?php if(isset($detalle[1]->cantidad)){ echo $detalle[1]->cantidad;} ?>"/>
		<input name="concep2" id="concep2"  type="text" class="form-control medio" value="<?php if(isset($detalle[1]->concepto)){ echo $detalle[1]->concepto;} ?>"/>
		<input name="precio2" id="precio2"  type="number" class="form-control corto" value="<?php if(isset($detalle[1]->precio)){ echo $detalle[1]->precio;} ?>"/>
		<input name="tot2" id="tot2"  type="number" class="form-control corto" value="<?php if(isset($detalle[1]->total)){ echo $detalle[1]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant3" id="cant3"  type="number" class="form-control corto" value="<?php if(isset($detalle[2]->cantidad)){echo $detalle[2]->cantidad;} ?>"/>
		<input name="concep3" id="concep3"  type="text" class="form-control medio" value="<?php if(isset($detalle[2]->concepto)){echo $detalle[2]->concepto;} ?>"/>
		<input name="precio3" id="precio3"  type="number" class="form-control corto" value="<?php if(isset($detalle[2]->precio)){echo $detalle[2]->precio;} ?>"/>
		<input name="tot3" id="tot3"  type="number" class="form-control corto" value="<?php if(isset($detalle[2]->total)){echo $detalle[2]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant4" id="cant4"  type="number" class="form-control corto" value="<?php if(isset($detalle[3]->cantidad)){echo $detalle[3]->cantidad;} ?>"/>
		<input name="concep4" id="concep4"  type="text" class="form-control medio" value="<?php if(isset($detalle[3]->concepto)){echo $detalle[3]->concepto;} ?>"/>
		<input name="precio4" id="precio4"  type="number" class="form-control corto" value="<?php if(isset($detalle[3]->precio)){echo $detalle[3]->precio;} ?>"/>
		<input name="tot4" id="tot4"  type="number" class="form-control corto" value="<?php if(isset($detalle[3]->total)){echo $detalle[3]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant5" id="cant5"  type="number" class="form-control corto" value="<?php if(isset($detalle[4]->cantidad)){echo $detalle[4]->cantidad;} ?>"/>
		<input name="concep5" id="concep5"  type="text" class="form-control medio" value="<?php if(isset($detalle[4]->concepto)){echo $detalle[4]->concepto;} ?>"/>
		<input name="precio5" id="precio5"  type="number" class="form-control corto" value="<?php if(isset($detalle[4]->precio)){echo $detalle[4]->precio;} ?>"/>
		<input name="tot5" id="tot5"  type="number" class="form-control corto" value="<?php if(isset($detalle[4]->total)){echo $detalle[4]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant6" id="cant6"  type="number" class="form-control corto" value="<?php if(isset($detalle[5]->cantidad)){echo $detalle[5]->cantidad;} ?>"/>
		<input name="concep6" id="concep6"  type="text" class="form-control medio" value="<?php if(isset($detalle[5]->concepto)){echo $detalle[5]->concepto;} ?>"/>
		<input name="precio6" id="precio6"  type="number" class="form-control corto" value="<?php if(isset($detalle[5]->precio)){echo $detalle[5]->precio;} ?>"/>
		<input name="tot6" id="tot6"  type="number" class="form-control corto" value="<?php if(isset($detalle[5]->total)){echo $detalle[5]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant7" id="cant7"  type="number" class="form-control corto" value="<?php if(isset($detalle[6]->cantidad)){echo $detalle[6]->cantidad;} ?>"/>
		<input name="concep7" id="concep7"  type="text" class="form-control medio" value="<?php if(isset($detalle[6]->concepto)){echo $detalle[6]->concepto;} ?>"/>
		<input name="precio7" id="precio7"  type="number" class="form-control corto" value="<?php if(isset($detalle[6]->precio)){echo $detalle[6]->precio;} ?>"/>
		<input name="tot7" id="tot7"  type="number" class="form-control corto" value="<?php if(isset($detalle[6]->total)){echo $detalle[6]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant8" id="cant8"  type="number" class="form-control corto" value="<?php if(isset($detalle[7]->cantidad)){echo $detalle[7]->cantidad;} ?>"/>
		<input name="concep8" id="concep8"  type="text" class="form-control medio" value="<?php if(isset($detalle[7]->concepto)){echo $detalle[7]->concepto;} ?>"/>
		<input name="precio8" id="precio8"  type="number" class="form-control corto" value="<?php if(isset($detalle[7]->precio)){echo $detalle[7]->precio;} ?>"/>
		<input name="tot8" id="tot8"  type="number" class="form-control corto" value="<?php if(isset($detalle[7]->total)){echo $detalle[7]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant9" id="cant9"  type="number" class="form-control corto" value="<?php if(isset($detalle[8]->cantidad)){echo $detalle[8]->cantidad;} ?>"/>
		<input name="concep9" id="concep9"  type="text" class="form-control medio" value="<?php if(isset($detalle[8]->concepto)){echo $detalle[8]->concepto;} ?>"/>
		<input name="precio9" id="precio9"  type="number" class="form-control corto" value="<?php if(isset($detalle[8]->precio)){echo $detalle[8]->precio;} ?>"/>
		<input name="tot9" id="tot9"  type="number" class="form-control corto" value="<?php if(isset($detalle[8]->total)){echo $detalle[8]->total;} ?>"/>
	</div>
	<div class="input-group">
		<input name="cant10" id="cant10"  type="number" class="form-control corto" value="<?php if(isset($detalle[9]->cantidad)){echo $detalle[9]->cantidad;} ?>"/>
		<input name="concep10" id="concep10"  type="text" class="form-control medio" value="<?php if(isset($detalle[9]->concepto)){echo $detalle[9]->concepto;} ?>"/>
		<input name="precio10" id="precio10"  type="number" class="form-control corto" value="<?php if(isset($detalle[9]->precio)){echo $detalle[9]->precio;} ?>"/>
		<input name="tot10" id="tot10"  type="number" class="form-control corto" value="<?php if(isset($detalle[9]->total)){echo $detalle[9]->total;} ?>"/>
	</div>
	</fieldset>
</div>