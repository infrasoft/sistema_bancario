<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="container">
	<h1>Estados de cuenta - <?=$typo;?></h1>
	<?php echo form_open(base_url().'index.php/comprobantes/account_status/'.$typo."/", 
						array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
			<input type="hidden" name="typo" id="typo" value="<?=$typo;?>"/>			
			<div class="form-group">
				<div class="form-group mb-3">
					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default"><?=$typo;?></span>
	  				</div>
  					<div class="autocomplete">
  						<input type="text" class="form-control" placeholder="Ingrese la empresa" id="razonSocial" name="razonSocial" />
  					</div>				
				</div>
						
			
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Empresa</span>
  					</div>  					
  					<select class="form-control" id="id_empresa_dest" name="id_empresa_dest" >
  						<option></option>
  						<?php
  						foreach ($cuentas as $row) 
  						{
							echo "<option value='".$row->id_empresa."'>".$row->razonSocial."</option>";
						}
  						?>
  					</select>	
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Tipo</label>
  					</div>
  					<select class="form-control" id="tipo_comprobante" name="tipo_comprobante" >
  						<option></option>
						<option>Comprobante</option>
						<option >factura</option>
						<option>Notas de crédito</option>
						<option>Notas de débito</option>
						<option>Orden de compra</option>
						<option>Recibos</option>
						<option>Tickets</option>
					</select>
				</div>
								
			</div>				
			<div class="form-group">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Estado</label>
  					</div>
  					<select class="form-control" id="estado" name="estado" >
  						<option></option>
  						<option>activo</option>
						<option>anulado</option>
						<option>cancelado</option>
					</select>
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<label class="input-group-text" for="inputGroupSelect01">Pago</label>
  					</div>
  					<select class="form-control" id="pago" name="pago" >
  						<option></option>
  						<option value="consaldo">con saldo</option>				
						<option>pendiente</option>
						<option>parcial</option>
						<option>completado</option>
					</select>
				</div>
			</div>	
			<div class="form-group">
				<div class="input-group mb-3">
					<h3>Fecha</h3>
				</div>	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Desde</span>
  					</div>
  					<input type="date" class="form-control" placeholder="ingrese fecha" id="fecha_desde" name="fecha_desde" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Hasta</span>
  					</div>
  					<input type="date" class="form-control" placeholder="ingrese fecha" id="fecha_hasta" name="fecha_hasta" />
				</div>			
				
			</div>	
			<div class="form-group">
				<div class="input-group mb-3">
					<h3>Neto</h3>
				</div>
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Desde</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Neto" id="neto_desde" name="neto_desde" 	step="0.01" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Hasta</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Neto" id="neto_hasta" name="neto_hasta" 	step="0.01" />
				</div>
			</div>
			<div class="form-group">				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Total desde</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Total" id="total_desde" name="total_desde" step="0.01" />
				</div>
				
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Total hasta</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese el Total" id="total_hasta" name="total_hasta" step="0.01" />
				</div>
			</div>	
				<button type="submit" class="btn btn-primary" id="search" name="search" value="Buscar">
					<i class="fas fa-search"></i> Buscar
				</button>
			
		<?=form_close();?>
		<br/><br/><hr />
</div>