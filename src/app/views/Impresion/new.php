<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>

<div class="container-fluid">
	<?php echo $mensaje;?>
	<h1>Modificar Impresion</h1>
	<h3><?=$Banco[0]->razonSocial;?></h3>
	<?php echo form_open('', 
							array("class"=>"form-inline","role"=>"form", "id"=>"new","name"=>"new","onkeypress"=>"return anular(event)"));?>
		<div class="form-group">
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>cantidad </b>:  coordenada x</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="cant_x" name="cant_x" 
  					value="<?php echo $datos[0]->cant_x;?>" required/>
			</div>
			
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>cantidad </b>:  coordenada y</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="cant_y" name="cant_y" 
  					value="<?php echo $datos[0]->cant_y;?>"  required/>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>emision </b>:   coordenada x</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="emision_x" name="emision_x" 
  					value="<?php echo $datos[0]->emision_x;?>"  required/>
			</div>
			
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>emision </b>:   coordenada y</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="emision_y" name="emision_y" 
  					value="<?php echo $datos[0]->emision_y;?>"  required/>
			</div>
		</div>	
		<div class="form-group">
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>vencimiento  </b>:   coordenada x</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="vencimiento_x" name="vencimiento_x" 
  					value="<?php echo $datos[0]->vencimiento_x;?>"  required/>
			</div>
			
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>vencimiento </b>:    coordenada y</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="vencimiento_y" name="vencimiento_y" 
  					value="<?php echo $datos[0]->vencimiento_y;?>"  required/>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>paguese </b>:    coordenada x</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="paguese_x" name="paguese_x" 
  					value="<?php echo $datos[0]->paguese_x;?>"  required/>
			</div>
			
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>paguese </b>:    coordenada y</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="paguese_y" name="paguese_y" 
  					value="<?php echo $datos[0]->paguese_y;?>"  required/>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>cantidad en letras </b>:    coordenada x</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="cant_letras_x" name="cant_letras_x" 
  					value="<?php echo $datos[0]->cant_letras_x;?>"  required/>
			</div>
			
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default"><b>cantidad en letras </b>:    coordenada y</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="cant_letras_y" name="cant_letras_y" 
  					value="<?php echo $datos[0]->cant_letras_y;?>"  required/>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group mb-3">					
  				<div class="input-group-prepend">
    				<span class="input-group-text" id="inputGroup-sizing-default">Distancia entre cheques</span>
  				</div>
  				<input type="tel" class="form-control" placeholder="ingrese coordenada" id="saltos" name="saltos" 
  					value="<?php echo $datos[0]->saltos;?>"  required/>
			</div>
			
			<button type="submit"  class="btn btn-primary">
				<i class="fas fa-pen"></i> Actualizar
			</button>
		</div>							
	<?=form_close();?>	<br><hr /><br><br>
</div>