<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>

<body id="page-top"> 

<div class="content-wrapper"></div>
  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="<?=base_url();?>">Sistema Bancario	</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href=<?=base_url();?>>
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      
      
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href=<?=base_url();?> id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <!--<span class="badge badge-danger">9+</span>-->
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/">Empresas</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_list/">Bancos</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cuentas/">Cuentas</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cheques/">Cheques</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href=<?=base_url();?> id="messagesDropdown" role="button" data-toggle="dropdown" 
			aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <!--<span class="badge badge-danger">7</span>-->
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">          
          <a class="dropdown-item" href=<?=base_url();?>>Manual del usuario</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="http://www.infrasoft.com.ar/" target="_blank">Desarrollo de sistemas</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href=<?=base_url();?> id="userDropdown" role="button"
        	 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="<?=base_url();?>index.php/seguridad/change_password/">Cambio de password</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/seguridad/">Actividad</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?=base_url();?>index.php/seguridad/logout/" >salir de sistema</a>
        </div>
      </li>
    </ul>

  </nav>

<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?=base_url();?>index.php/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Inicio</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href=<?=base_url();?> id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-wallet"></i>
          <span> Bancos</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Bancos</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_new/">Alta de Banco</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_list/">Lista de bancos</a>
          
          
          	<h6 class="dropdown-header">Cuentas</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cuentas/new_account/">Alta de Cuenta</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cuentas/">Lista de Cuentas</a>
          
          <h6 class="dropdown-header">Conceptos</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/conceptos/">lista de conceptos</a>
          
          <h6 class="dropdown-header">Movimientos</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/movimientos/">lista de Movimientos</a>
          
          <a class="dropdown-item" href="<?=base_url();?>index.php/movimientos/avanced_seach_form/">Busqueda Avanzada</a>          
          <!--<div class="dropdown-divider"></div> -->
          
          
          <h6 class="dropdown-header">Cheques:</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cheques/">Lista de cheques</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cheques/avanced_seach/">
          	Busqueda Avanzada
          </a> 
          <a class="dropdown-item" href="<?=base_url();?>index.php/cheques/check_conciliation/">
          	Conciliacion de Cheques
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/cheques_comprobantes/">
          	Facturas vinculadas 
          </a>                    
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href=<?=base_url();?> id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="far fa-building"></i>
          <span>Proveedores</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Empresas</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_list/proveedor/">Lista de Proveedores</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_new/proveedor">Alta de Proveedores</a>
         
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Comprobantes:</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/receipts_list/proveedor/1/lista_proveedor/">
          	Lista de Comprobantes
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/receipts_list/proveedor/avanced/">
          	Busqueda Avanzada
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/account_status/proveedor/">
          	Estados de cuenta
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/receipts_new/proveedor/">Registrar Comprobante</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Pagos:</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/orden_pago/new_payment_order/">
          	Nueva Orden de Pago
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/orden_pago/">
          	Lista de Ordenes Pago
          </a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href=<?=base_url();?> id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="far fa-smile"></i>
          <span>Clientes</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Empresas</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_list/cliente">Lista de Clientes</a>          
          <a class="dropdown-item" href="<?=base_url();?>index.php/empresas/company_new/cliente">Registrar Cliente</a>
         
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Comprobantes:</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/receipts_list/cliente/1/lista_cliente">
          	Lista de Comprobantes
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/receipts_list/cliente/avanced/">
          	Busqueda Avanzada
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/account_status/cliente/">
          	Estados de cuenta
          </a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/comprobantes/receipts_new/cliente/">Registrar Comprobante</a>
          
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Pagos:</h6>
          <a class="dropdown-item" href="<?=base_url();?>index.php/pagos/new_pay/">Registrar pago</a>
          <a class="dropdown-item" href="<?=base_url();?>index.php/pagos/">Lista de pagos</a>

        </div>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>index.php/deudas/list_company/">
          <i class="fas fa-people-carry"></i>
          <span>Deudas</span>
        </a>
                    
      </li>     
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>index.php/imprimir/set_print/">
          <i class="fas fa-fw fa-table"></i>
          <span>Configuracion</span>
        </a>
                    
      </li>
    </ul>
