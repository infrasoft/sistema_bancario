<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 *  Modelo para el manejo de categoria
 */
class Detalle_comprobante_mdl extends CI_Model 
{
	private $table = "detalle_comprobante";
	function __construct()
	{
		parent::__construct();	
	}
	
	//realiza una consulta en la lista de clientes
	public function consulta($data='',$campos="*")
	{		
		$this->db->select($campos);		
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}		
		$consulta = $this->db->get();
		$_SESSION["cant_reg"] = $consulta->num_rows();
		return $consulta->result(); 
	}
	
	public function alta($data=array())
	{		
      	return $this->db->insert($this->table, $data);
	}
	
	public function modifica($data=array(),$data_seach = array())
	{
		$this->db->where($data_seach);
		return	$this->db->update($this->table, $data);
	}
	
	public function elimina($data_seach = array())
	{
		$this->db->where($data_seach);
		return $this->db->delete($this->table);;
	}
}
?>