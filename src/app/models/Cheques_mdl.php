<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');


class Cheques_mdl extends CI_Model 
{
	private $table = "cheques";
	function __construct()
	{
		parent::__construct();	
	}
	
	//realiza una consulta en la lista de clientes
	public function consulta($data='',$campos="*")
	{		
		$this->db->select($campos);		
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}		
		$consulta = $this->db->get();
		$_SESSION["cant_reg"] = $consulta->num_rows();
		return $consulta->result(); 
	}
	
	//realiza una consulta a traves de la vista creada
	public function consulta_views($data='',$campos="*")
	{
		$this->db->select($campos);		
		$this->db->from("cheques_cuenta_empresa");
		if ($data!=null) 
		{
			$this->db->where($data);
		}		
		$consulta = $this->db->get();
		$_SESSION["cant_reg"] = $consulta->num_rows();
		return $consulta->result();
	}
	
	public function alta($data=array())
	{		
      	return $this->db->insert($this->table, $data);
	}
	
	public function modifica($data=array(),$cond = array())
	{
		$this->db->where($cond);
		return	$this->db->update($this->table, $data);
	}
	
	public function delete($data=array())
	{
		$this->db->where($data);
      return $this->db->delete($this->table);
	}
}
?>