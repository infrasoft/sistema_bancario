<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 *  Modelo para el manejo de categoria
 */
class Cuentas_mdl extends CI_Model 
{
	private $table = "cuenta";
	function __construct()
	{
		parent::__construct();	
	}
	
	//realiza una consulta 
	public function consulta($data='',$campos="*")
	{		
		$this->db->select($campos);		
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}		
		$consulta = $this->db->get();
		$_SESSION["cant_reg"] = $consulta->num_rows();
		return $consulta->result(); 
	}
	
	//realiza una consulta 
	public function consultavista($data='',$campos="*")
	{
		$this->table = "cuenta_empresa";		
		$this->db->select($campos);		
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}		
		$consulta = $this->db->get();
		$_SESSION["cant_reg"] = $consulta->num_rows();
		$resul = $consulta->result(); 
		$this->table = "cuenta";
		return $resul;
	}
	
	public function alta($data=array())
	{		
      	return $this->db->insert($this->table, $data);
	}
	
	public function modifica($data=array(), $cond ="")
	{
		$this->db->where($cond);
		return	$this->db->update($this->table, $data);
	}
	
	public function delete($data=array())
	{
		$this->db->where($data);
      return $this->db->delete($this->table);
	}
}
?>