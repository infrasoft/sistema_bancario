<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 *  Modelo para el manejo de categoria
 */
class Impresion_mdl extends CI_Model 
{
	private $table = "impresion";
	function __construct()
	{
		parent::__construct();	
	}
	
	//realiza una consulta en la lista de clientes
	public function consulta($data="",$campos="*")
	{		
		$this->db->select($campos);		
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}		
		$consulta = $this->db->get();
		$_SESSION["cant_reg"] = $consulta->num_rows();
		return $consulta->result(); 
	}
		
	public function alta($data=array())
	{		
      	$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	public function modifica($data=array(), $id= 0)
	{
		$this->db->where($id);
		return	$this->db->update($this->table, $data);
	}
	
	public function delete($data=array())
	{
		$this->db->where($data);
      return $this->db->delete($this->table);
	}
}
?>