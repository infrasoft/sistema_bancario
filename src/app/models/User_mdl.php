<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

/**
 *  Modelo para el manejo de usuarios
 */
class User_mdl extends CI_Model 
{
	private $table = "user";
	function __construct() 
	{
		parent::__construct();
	}
	
	//realiza la verificacion del login
	public function login($user="", $password= "")
	{
		if (is_null($user) OR (is_null($password))) 
		{				
			return FALSE;
		} else {
			$query = $this->consulta("nickname='".$user."'" , array("nickname","password","type_user"));
			//print_r($query);
			if ($query->num_rows() == 1) 
			{
				$row=$query->row();   //print_r($row);				
				$password = hash('sha512', $password ."descartes12432") ;	
				//echo $password."<br/>".$row->password;			
				if ($password == $row->password) 
				{
					$data=array('user_data'=>array(
                    'nickname'=>$row->nickname,
                    'type_user'=>$row->type_user,                    
                    'password'=>$row->password)
               		);
					$this->session->set_userdata($data);
					//print_r($data);
                	return true;
				}
				else {
					$this->session->unset_userdata('user_data');					
        			return false;
				}                
			}	
			 else {			 	
				return false;
			}				
		}		
	}
	
		
	//genera el alta
	public function alta($data = array())
	{
		$this->db->insert($this->table, $data);
      	return $this->db->insert_id();
	}
	
	//realiza la consulta
	public function consulta($data='',$campos="*")
	{
		$this->db->select($campos);		
		$this->db->from($this->table);
		if ($data!=null) 
		{
			$this->db->where($data);
		}
		$consulta = $this->db->get();
		return $consulta; 
	}
	
	//realiza la modificacion de la venta
	public function modificar($data=array(), $condicion)	
	{
		$this->db->where($condicion);
		return	$this->db->update($this->table, $data);
	}
}

?>
