<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//permite mostrar mensajes en la pantalla
if(!function_exists('mensajes'))
{
	function mensajes($text='',$alert = "alert-info", $head='¡Información!')
	{
		return '<div class="alert '.$alert.' alert-dismissable">
  					<button type="button" class="close" data-dismiss="alert">&times;</button>
  					<strong>'.$head.'</strong> '.$text.'.
				</div>';
	}	
}
/*******************************************
 *                 Fechas 
 * ***************************************/
//genera el codigo calendario
if (!function_exists('codigoCalendario')) 
{
	function codigoCalendario($input="", $boton="")
	{
		$cod = "<script type=\"text/javascript\"> 
                    Calendar.setup({ 
                        inputField : '$input',     // id del campo de texto 
                        ifFormat : '%d-%m-%Y',     // formato de la fecha que se escriba en el campo de texto 
                        button : '$boton'     // el id del botón que lanzará el calendario 
                    }); 
                    </script>";
		return $cod;
	}
}

// realiza la transformacion de la fecha
if (!function_exists('transforma_fecha')) 
{
	function transforma_fecha($date='')
	{
		$fecha = substr($date, 6,4)."-".substr($date, 3,2)."-".substr($date, 0,2);
		return $fecha;
	}
}

//convierte la fecha del sistema en fecha agradable para el usuario
if (!function_exists('invierte_fecha')) 
{
	function invierte_fecha($date='')
	{
		$fecha = substr($date, 8,2)."-".substr($date, 5,2)."-".substr($date, 0,4);
		return $fecha;
	}
}

//genera un boton para ocultar contenido
if (!function_exists('btn_ocultar')) 
{
	function btn_ocultar($title = "", $id = "", $contenido = "")
	{
		$a = "<a href=\"#".$id."\" class=\"btn btn-info\" data-toggle=\"collapse\">".$title."</a>";
		$div = "<div id=\"".$id."\" class=\"collapse\">".$contenido."</div>";
		return $a.$div;
	}
}

//muestras los estados de activo - cancelado - devuelto
if(!function_exists('estado'))
{
	function estado()
	{
		return '<label>Estado</label>
			<select class="form-control" name="estado" id="estado">
				<option>activo</option>
				<option>cancelado</option>
				<option>devuelto</option>
			</select>';
	}	
}

//genera la cantidad de espacios necesarios ingresados
if (!function_exists('espacios')) 
{
	function espacios($esp = 0)
	{
		$aux = "";
		for ($i=0; $i < $esp; $i++) 
		{ 
			$aux .= " ";
		}
		return $aux;
	}
}
?>
