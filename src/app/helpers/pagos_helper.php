<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//permite mostrar un select con los datos introducidos por el usuario
if(!function_exists('estado_detalles('))
{
	function estado_detalles($entrada='', $id=0)
	{
		$aux = '<select class="form-control" id="tipo'; $aux .= $id; 
				$aux .= ' name="tipo'; $aux .=$id." >
					<option ";
						 if($entrada == "efectivo") {$aux .= "selected";} $aux .= ">efectivo</option>
					<option "; if($entrada== "transferencia bancaria") {$aux .= "selected";} $aux .= ">transferencia bancaria</option>	
					<option "; if($entrada == "cheque") {$aux .= "selected";} $aux .= ">cheque</option>	
					<option "; if($entrada == "credito") {$aux .= "selected";} $aux .= ">credito</option>
					<option "; if($entrada == "otros") {$aux .= "selected";} $aux .= ">otros</option>				
		</select>";
		return $aux;
	}	
}	
?>