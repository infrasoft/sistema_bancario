<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');


class Empresas extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
				
	}
	
	//muestra la lista de empresas
	public function index()
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");			
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");			
			$data2["mensaje"]="";
			
			if (!is_null($buscar)) 
			{
				$data2["lista"] = $this->Empresas_mdl->consulta("(".$campo." LIKE \"%".$buscar."%\") ORDER BY id DESC limit 0,100");
				if (is_array($data2["lista"])) 
				{
					$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
				}				
			} else {				
				$data2["lista"] = $this->Empresas_mdl->consulta("1 ORDER BY id DESC limit 0,100");	
			}
			$data2["titulo"] = "Lista de empresas";
			$this->load->view("empresas/list",$data2);
			
			$data["footer"]="<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer", $data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	// registrar los datos de una empresa
	public function company_new($clase ="banco")
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$data["clase"] =$clase;	
			
			$this->load->view("header",$data);
			$this->load->view("menu");
			$this->load->view("empresas/submenu",$data);
			switch ($clase)
			{
				case 'banco':
					$data["titulo"] = "Registro Bancario";
					break;
				case 'cliente':
					$data["titulo"] = "Registro de Clientes";
					break;
				case 'proveedor':
					$data["titulo"] = "Registro de Proveedores";
					break;
				default:
					
					break;
			}
			
				
			$data["mensaje"] = "";	
			$vector = $this->input->post(array("cuit","tipo","razonSocial","actividad","clase","domicilio","telefono","telefono2",
						"celular","representante","tel_representante","otros"));
			
					
			if (($vector["cuit"] != "" ) AND ($vector["razonSocial"] != "" ) AND is_array($vector)) 
			{
				$id = $this->Empresas_mdl->alta($vector);
				if ($id) {
					$data["mensaje"] = mensajes("Datos Incorporados Correctamente");
					if ($clase == "banco") 
					{
						$this->load->model("Impresion_mdl");
						$vector = array("id"=>$id,
						"cant_x"=>0,"cant_y"=>0,"emision_x"=>0,"emision_y"=>0,
						"vencimiento_x"=>0,"vencimiento_y"=>0,"paguese_x"=>0,"paguese_y"=>0,
						"cant_letras_x"=>0,"cant_letras_y"=>0,"saltos"=>0);
						$this->Impresion_mdl->alta($vector);
					}
				} else {
					$data["mensaje"] = mensajes("Datos No ingresados. Por favor ingreselo nuevamente.","alert-danger");					
					
					if (($this->Empresas_mdl->consulta("cuit='".$vector["cuit"]."'","cuit")) != Array()) 
					{
						$data["mensaje"] .= mensajes("Cuit ya registrado.","alert-danger");
					}
					if($this->Empresas_mdl->consulta("razonSocial='".$vector["razonSocial"]."'","razonSocial") != Array())
					{
						$data["mensaje"] .= mensajes("Razon Social ya registrada.","alert-danger");
					}					
				}				
			} 
			
			$this->load->view("empresas/new",$data);
			
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	//muestra una lista con criterios mas avanzados
	public function company_list($clase="banco")
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$data["mensaje"] = "";
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");							
			
			if (!is_null($buscar)) 
			{
				$data["lista"] = $this->Empresas_mdl->consulta("(clase='".$clase."') AND ".
									$campo." LIKE \"%".$buscar."%\" ORDER BY id DESC limit 0,100");
				$_SESSION["cond"] = ""; $_SESSION["subtitle"] = "";
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] =mensajes("No se encontraron resultados");	
				}				
			}else {
				$vector = $this->input->post(array("otros","tel_representante",
								"representante","celular","telefono2","telefono",
								"domicilio","clase","actividad","tipo","razonSocial",
								"cuit","cant_reg","search"));
								 
				if(($vector != array()) AND ($vector["search"] =="true")) 
				{
					$cond = "";
					if ($vector["cuit"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(cuit LIKE '%".$vector["cuit"]."%')";
					}
					
					if ($vector["razonSocial"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(razonSocial LIKE '%".$vector["razonSocial"]."%')";
					}
					
					if ($vector["tipo"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(tipo ='".$vector["tipo"]."')";
					}
					
					if ($vector["actividad"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(actividad LIKE '%".$vector["actividad"]."%')";
					}
					
					if ($vector["clase"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(clase ='".$vector["clase"]."')";
					}
					
					if ($vector["domicilio"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(domicilio LIKE '%".$vector["domicilio"]."%')";
					}
					
					if ($vector["telefono"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(telefono LIKE '%".$vector["telefono"]."%')";
					}
					
					if ($vector["telefono2"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(telefono2 LIKE '%".$vector["telefono2"]."%')";
					}

					if ($vector["celular"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(celular LIKE '%".$vector["celular"]."%')";
					}
					
					if ($vector["representante"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(representante LIKE '%".$vector["representante"]."%')";
					}	
					
					if ($vector["tel_representante"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(tel_representante LIKE '%".$vector["tel_representante"]."%')";
					}
					
					if ($vector["tel_representante"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(tel_representante LIKE '%".$vector["tel_representante"]."%')";
					}
					
					if ($vector["otros"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(otros LIKE '%".$vector["otros"]."%')";
					}
						
					$data["lista"] = $this->Empresas_mdl->consulta($cond);
					$_SESSION["cond"] = $cond;
				} else {
					$data["lista"] = $this->Empresas_mdl->consulta("clase='".$clase.
														"' ORDER BY id DESC limit 0,100"); 
					$_SESSION["cond"] = "clase='".$clase."'";
				}
								
			}
			$_SESSION["lista"] = $data["lista"];
			$data["dir"] = "imprimir/list_company/";			
			$data["dir2"] = "csv/list_company/";
			
			switch ($clase) 
			{
				case 'banco':
					$data["mensaje"] .= mensajes("Lista de Bancos");
					$data["titulo"] = "Lista de Bancos";
				break;
				case 'proveedor':
					$data["mensaje"] .= mensajes("Lista de Proveedores");
					$data["titulo"] = "Lista de Proveedores";						
				break;
				case 'cliente':
					$data["mensaje"] .= mensajes("Lista de Clientes");
					$data["titulo"] = "Lista de Clientes";					
				break;
				case 'list':
					$data["mensaje"] .= mensajes("busqueda avanzada");
					$data["titulo"] = "Lista de General de empresas";
					$this->load->view("empresas/advanced_search",$data);
				break;
				default:
					$data["mensaje"] .= mensajes("Busqueda Avanzada");
					
				break;
			}
			
			
			if (isset($buscar)) 
			{
				$_SESSION["subtitle"][0] = "Criterio de busqueda: ";
				$_SESSION["subtitle"][1] = $campo." que contengan ".$buscar;
			}
			else {
				$_SESSION["subtitle"] = "";				
			}
						
			$_SESSION["title"] = $data["titulo"];
			$this->load->view("cheques/export_data", $data);
			if ($clase != "list") 
			{
				$this->load->view("empresas/list",$data);	
			} 
			
							
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer", $data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	public function company_update($id = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$data["vector"] = $this->Empresas_mdl->consulta("id=".$id);
			
			$data["clase"]= $data["vector"][0]->clase;
			$this->load->view("empresas/submenu",$data);
			$data["mensaje"] = "";
			
			if ($this->input->post("verificar") =="si") 
			{
				$vector = $this->input->post(array("cuit","tipo","razonSocial","actividad","clase","domicilio","telefono","telefono2",
						"celular","representante","tel_representante","otros"));
				
				if ($this->Empresas_mdl->modifica($vector,$id)) 
				{
					$data["mensaje"] =  mensajes("Cambios realizados correctamente");
				} else 
				{
					$data["mensaje"] =  mensajes("No se realizaron cambios");
				}			
			}
			
			if ($id == 0) 
			{
				$data["mensaje"] = "¿Desea registrar una empresa?";				
				$this->load->view("empresas/new",$data);
			} else {
				$data["vector"] = $this->Empresas_mdl->consulta("id=".$id);
				$this->load->view("empresas/update",$data);
			}
				
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
		
	}
	
	//realiza el resumen de datos
	public function company_reports($id_empresa=0) // sin implementar o terminar
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");		
			
			$data["mensaje"] = "";
			
			$this->load->view("empresas/reports",$data);
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	//elimina una empresa
	public function company_delete($id_empresa=0)
	{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");		
			
		$data["mensaje"] = "";
		if ($this->input->post("delete") == "delete") 
		{
			if ($this->Empresas_mdl->delete("id='".$id_empresa."'")) 
			{
				if ($this->Empresas_mdl->consulta("id='".$id_empresa."'")) 
				{
					if ($this->Empresas_mdl->delete(array("id"=>$id_empresa))) 
					{
						$data["mensaje"] = mensajes("Datos Eliminados correctamente");
					}
				} else {
					$data["mensaje"] = mensajes("Datos Eliminados correctamente");	
				}				
			} else {
				$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
			}				
		}
		$this->load->view("empresas/delete",$data);	
		
		$data["footer"] = "";
		$this->load->view("footer",$data);	
	}
}

?>