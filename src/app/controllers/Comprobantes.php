<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/


defined('BASEPATH') OR exit('Acceso no permitido');


class Comprobantes extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('Comprobantes_mdl');
		$this->load->model("Detalle_comprobante_mdl");
	}
	
	//muestra la lista de empresas
	public function index()
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");			
			$data2["mensaje"]="";
			$data2["clase"] = "proveedor";
			if (!is_null($buscar)) 
			{
				$data2["lista"] = $this->Comprobantes_mdl->consulta_views($campo." LIKE \"%".$buscar."%\" ORDER BY id_comprobante DESC limit 0,100");
				
				if (is_array($data2["lista"])) 
				{
					$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
				}				
			} else {				
				$data2["lista"] = $this->Comprobantes_mdl->consulta_views("1 ORDER BY id_comprobante DESC limit 0,100");	
			}
			$data2["title"] = "Lista de comprobantes";
			
			$this->load->view("comprobantes/list",$data2);
			
			$data["footer"]="<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer", $data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	// registrar los datos de una empresa
	public function receipts_new($clase = "cliente",$id_empresa=0) 
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script language='JavaScript' type='text/javascript' src='".
					base_url()."media/js/facturas.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			if ($clase == "cliente") 
			{
				$data["titulo"] = "Lista de Clientes - Operaciones con combrobantes";				
			} 
			elseif ($clase =="proveedor") 
			{
				$data["titulo"]  = "Lista de Proveedores - Operaciones con combrobantes";				
			}
			else {
				$data["titulo"] = "Lista de empresas";
			}	
			$data["mensaje"] = "";	
			
			if ($id_empresa == 0) 
			{// se genera lista de busqueda
				$data["mensaje"] = mensajes("Lista de ".$clase);
				$buscar = $this->input->post("buscar");
				$campo = $this->input->post("lista_venta");
				if (!is_null($buscar)) 
				{
					$data["lista"] = $this->Empresas_mdl->consulta("(clase='".$clase.
									"' OR clase='empresa' OR clase='otros') AND (".
									$campo." LIKE \"%".$buscar."%\") ORDER BY id DESC limit 0,100");
				}else {
					$data["lista"] = $this->Empresas_mdl->consulta("(clase='".$clase.
									"' OR clase='empresa' OR clase='otros') ORDER BY id DESC limit 0,100");
				}	
						
				$this->load->view("comprobantes/list_company",$data);
				
			} else 
			{
				$data["typo"] = $clase;			
				$vector = $this->input->post(array("id_empresa","tipo_comprobante","nro","fecha","iva","neto","iva_valor",
															"iva2","neto2","iva_valor2",
															"concep_no_grabados","imp_internos","total","observaciones","estado",
															"typo","pago","id_empresa_dest","saldo"));				
				//print_r ($vector);	
				if (($vector["total"] != "" ) AND is_array($vector)) 
				{
					if ($this->Comprobantes_mdl->consulta(array("id_empresa"=>$vector["id_empresa"],
																"tipo_comprobante"=>$vector["tipo_comprobante"],
																"nro"=>$vector["nro"]))) 
					{
						$data["mensaje"] = mensajes($vector["tipo_comprobante"]." cargado previamente");
					} else {
						$id = $this->Comprobantes_mdl->alta($vector);
						if ($id) 
						{
							$a=1; $id=$this->db->insert_id();
							/*while ($a <= 10) 
							{
								$detalle_comprobante = $this->input->post(array("cant".$a,"concep".$a,"precio".$a,"tot".$a));
								if (($detalle_comprobante["concep".$a] != null) AND ($detalle_comprobante["tot".$a] != null)) 
								{
									$this->Detalle_comprobante_mdl->alta(array("id_comprobante"=>$id,
																			"id_detalle"=>$a,
																			"cantidad"=>$detalle_comprobante["cant".$a],
																			"concepto"=>$detalle_comprobante["concep".$a],
																			"precio"=>$detalle_comprobante["precio".$a],
																			"total"=>$detalle_comprobante["tot".$a]));	
																		
								}
								$a++;	
							}			*/			
							$data["mensaje"] = mensajes("Datos Incorporados Correctamente");
						
						} else 
						{
							$data["mensaje"] = mensajes("Datos No ingresados. Por favor ingreselo nuevamente");
						}
						
					}						
				}
				$data["dir"] = "comprobantes/receipts_new/".$clase."/";
				$this->load->view("comprobantes/submenu",$data);
				$data["empresa"] = $this->Empresas_mdl->consulta("id='".$id_empresa."'");
				$this->load->model("Cuentas_mdl");
				$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																"razonSocial,id_empresa");	
				//print_r($data["cuentas"]);		
				$this->load->view("comprobantes/new",$data);
			}
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);		
			
		} else 
		{
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	//muestra una lista con criterios mas avanzados
	public function receipts_list($clase="cliente" , $buscar = "", $campo="id")
	{
		if (is_logged_in()) 
		{
			if ($buscar == "avanced") 
			{
				$data["header"] = "<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
			} else {
				$data["header"]="";
			}			
			
			$this->load->view("header",$data);
			$this->load->view("menu");
			$data["dir"]="imprimir/print_receipts/";
			$data["dir2"]="csv/print_receipts/";
			
			$data["mensaje"] = "";	
					
			$data["lista"] ="";
			$data["title"] = "";
			$data["clase"] = $clase;
			
			if ($clase == "cliente") 
			{
				$data["title"] = "Lista de comprobantes de Clientes";
			} 
			if($clase =="proveedor")
			{
				$data["title"]  = "Lista de comprobantes de Proveedores";
			}
			
			$data["typo"] = $clase;
			//busqueda
			$buscar_form = $this->input->post("buscar");
			$campo_form = $this->input->post("campo");
			
			$_SESSION["subtitle"] = array();
			$_SESSION["subtitle"][] = "Criterio de Busqueda";
			if (!is_null($buscar_form)) //buqueda de comprobantes
			{
				$cond = " ".$campo_form." LIKE '%".$buscar_form."%' AND ";
				$_SESSION["subtitle"][] = $campo_form." que contengan ".$buscar_form;
			} else 
			{
				$cond = ""; $subtitle= "";
				$_SESSION["subtitle"][] = "Listado completo";
			}
				
			
			switch ($campo) 
			{
				case 'id': // Para ver recibos binculados
					$data["lista"] = $this->Comprobantes_mdl->consulta_views("id_empresa = '".$buscar."'"); 
					$data["mensaje"] = mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					if ($data["lista"] != array ()) 
					{
						$data["empresa"] = $this->Empresas_mdl->consulta("id = '".$buscar."'");
						$data["mensaje"] .= mensajes("Empresa: <b>".$data["empresa"][0]->razonSocial.
												"</b> CUIT: <b>".$data["empresa"][0]->cuit.
												"</b> Actividad: <b>".$data["empresa"][0]->actividad."</b>",
												"alert-info", "");
					}
					
				break;
				
				case 'lista_proveedor':
					$_SESSION["subtitle"][] = "Lista Proveedor";
					$_SESSION["cond"] = $cond." NOT(typo='cliente')";
					$data["lista"] = $this->Comprobantes_mdl->consulta_views($_SESSION["cond"].
																			" ORDER BY id_comprobante DESC limit 0,100"); 
				break;
				
				case 'lista_cliente':
					$_SESSION["subtitle"][] = "Lista cliente";					
					$_SESSION["cond"] = $cond." NOT(typo='proveedor')";
					$data["lista"] = $this->Comprobantes_mdl->consulta_views($_SESSION["cond"].
																				" ORDER BY id_comprobante DESC limit 0,100"); 
				break;
				
				case 'limit': // ver la separacion de cliente y proveedor
				
					$vector = $this->input->post(array("razonSocial","id_empresa_dest","nro","tipo_comprobante","estado","pago","neto","iva",
														"fecha_desde","fecha_hasta","neto_desde","neto_hasta",
														"iva","iva_valor_deste","iva_valor_hasta","concep_no_grabados","imp_internos","observaciones",
														"total_desde","total_hasta","search","groupby"));
														
					if (($vector["search"] == "Buscar") 
					AND ($vector != array())) 
					{
						if ((!isset($_SESSION["cond"])) AND (!isset($_SESSION["subtitle"]))) 
						{
							$_SESSION["cond"] = "";
							$_SESSION["subtitle"] = "";
						}						
					}								
					//print_r($vector);
					if (($vector != array()) AND ($vector["search"] == "Buscar")) 
					{
						$subtitle = array("Criterio de Busqueda");
						$cond = "";
						if ($vector["razonSocial"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(razonSocial LIKE '%".$vector["razonSocial"]."%' )"; 
							$subtitle[] = strtoupper($clase)." : ".$vector["razonSocial"];
						}
						
						if ($clase == "proveedor") 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(typo ='".$clase."')";
							//$subtitle[] = "Comprobantes de  ".$clase;
						}
						
						if ($clase == "cliente") 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(typo ='".$clase."')";
							$subtitle[] = "Comprobantes de ".$clase;
						}
						
						if ($vector["id_empresa_dest"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(id_empresa_dest='".$vector["id_empresa_dest"]."' )"; 
							$empr = $this->Empresas_mdl->consulta("id='".$vector["id_empresa_dest"]."'");
							$_SESSION["empresa_dest"] = $empr[0]->razonSocial;
							//$subtitle[] = " Empresa: ".$empr[0]->razonSocial;
						}
						
						if ($vector["nro"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(nro LIKE '%".$vector["nro"]."%' )"; 
							$subtitle[] = " Nro Comprobante: ".$vector["nro"];
						}
						
						if ($vector["tipo_comprobante"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(tipo_comprobante LIKE '%".$vector["tipo_comprobante"]."%' )"; 
							$subtitle[] = " Tipo Comprobante: ".$vector["tipo_comprobante"];
						}
						
						if ($vector["estado"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(estado LIKE '%".$vector["estado"]."%' )"; 
							$subtitle[] = " Estado: ".$vector["estado"];
						}
						
						if ($vector["pago"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							if ($vector["pago"] == "consaldo") 
							{
								$cond .= "(saldo > 0)"; 
								$subtitle[] = " Pago: Con Saldo";
							} else {
								$cond .= "(pago LIKE '%".$vector["pago"]."%' )"; 
							$subtitle[] = " Pago: ".$vector["pago"];
							}			
						}
						
						if ($vector["fecha_desde"] != null AND $vector["fecha_hasta"] != null) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(fecha BETWEEN '".$vector["fecha_desde"]."' AND '".$vector["fecha_hasta"]."')"; 
							$subtitle[] = "Fechas Entre: ".$vector["fecha_desde"]." Y ".$vector["fecha_hasta"];
						}
						
						if ($vector["fecha_desde"] != null AND $vector["fecha_hasta"] == null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(fecha >= '".$vector["fecha_desde"]."' )"; 
							$subtitle[] = " Fechas: Desde ".$vector["fecha_desde"];
						}
						
						if ($vector["fecha_desde"] == null AND $vector["fecha_hasta"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(fecha <= '".$vector["fecha_hasta"]."' )"; 
							$subtitle[] = " Fecha: Hasta ".$vector["fecha_hasta"];
						}
						
												
						if ($vector["neto_desde"] != null AND $vector["neto_hasta"] != null) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(neto BETWEEN '".$vector["neto_desde"]."' AND '".$vector["neto_hasta"]."')"; 
							$subtitle[] = " Neto : Entre ".$vector["neto_desde"]." Y ".$vector["neto_hasta"];
						}
						
						if ($vector["neto_desde"] != null AND $vector["neto_hasta"] == null) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(neto >= '".$vector["neto_desde"]."')"; 
							$subtitle[] = " Neto : Desde ".$vector["neto_desde"];
						}
						
						if ($vector["neto_desde"] == null AND $vector["neto_hasta"] != null) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(neto <= '".$vector["neto_hasta"]."')"; 
							$subtitle[] = " Neto : Hasta ".$vector["neto_hasta"];
						}
						
						if ($vector["iva"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(iva LIKE '%".$vector["iva"]."%' )"; 
							$subtitle[] = " IVA: ".$vector["iva"];
						}
						
						if ($vector["iva_valor_deste"] != null AND $vector["iva_valor_hasta"] != null) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(iva_valor BETWEEN '".$vector["iva_valor_deste"]."' AND '".$vector["iva_valor_hasta"].")"; 
							$subtitle[] = " Iva : $".$vector["iva_valor_deste"]." AND $".$vector["iva_valor_hasta"];
						}
						
						if ($vector["iva_valor_deste"] != null AND $vector["iva_valor_hasta"] == null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(iva_valor >= '".$vector["iva_valor_deste"]."' )"; 
							$subtitle[] = " IVA: Desde $".$vector["iva_valor_deste"];
						}
						
						if ($vector["iva_valor_deste"] == null AND $vector["iva_valor_hasta"] != null) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(iva_valor <= '".$vector["iva_valor_hasta"]."' )"; 
							$subtitle[] = " IVA: Hasta $".$vector["iva_valor_hasta"];
						}
						
						if ($vector["concep_no_grabados"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(concep_no_grabados LIKE '%".$vector["concep_no_grabados"]."%' )"; 
							$subtitle[] = " Conceptos No Gravados: ".$vector["concep_no_grabados"];
						}
						
						if ($vector["imp_internos"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(imp_internos LIKE '%".$vector["imp_internos"]."%' )"; 
							$subtitle[] = " Impuestos Internos: ".$vector["imp_internos"];
						}
						
						if ($vector["total_desde"] != null AND $vector["total_hasta"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(total BETWEEN '".$vector["total_desde"]."' AND '".$vector["total_hasta"]."')"; 
							$subtitle[] = " Total: Entre $".$vector["total_desde"]." Y $".$vector["total_hasta"];
						}
						
						if ($vector["total_desde"] != null AND $vector["total_hasta"] == null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(total >= '".$vector["total_desde"]."' )"; 
							$subtitle[] = " Total : Desde $".$vector["total_desde"];
						}
						
						if ($vector["total_desde"] == null AND $vector["total_hasta"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(total <= '".$vector["total_hasta"]."' )"; 
							$subtitle[] = " Total: Hasta $".$vector["total_hasta"];
						}
						
						if ($vector["observaciones"] != null ) 
						{
							if ($cond != "") 
							{
								$cond .= " AND ";
							}
							$cond .= "(observaciones LIKE '%".$vector["iobservaciones"]."%' )"; 
							$subtitle[] = " Observaciones: ".$vector["observaciones"];
						}
						
						$_SESSION["subtitle"] = $subtitle;							
						$data["lista"] = $this->Comprobantes_mdl->consulta_views($cond." ORDER BY id_comprobante DESC ");					
						
					} else {
						$data["lista"] = $this->Comprobantes_mdl->consulta_views("1 ORDER BY id_comprobante DESC limit ");
					}
					
					if (is_array($data["lista"])) 
					{
						$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					} else {
						$data["mensaje"] =mensajes("No se encontraron resultados");	
					}					
					$_SESSION["cond"] = $cond; //print_r($_SESSION["cond"]);
				break;
				
				default: 
					$data["lista"] = $this->Comprobantes_mdl->consulta_views("1 ORDER BY id_comprobante DESC limit 0,100");				
				break;				
			}
			
			$_SESSION["comprobantes_list"] = $data["lista"]; 
		
			if ($buscar == "avanced") // busqueda avanzada
			{				
				$this->load->model("Cuentas_mdl");
				$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial","razonSocial,id_empresa");		
				$data["dir"] = "comprobantes/receipts_new/".$clase."/";
				$this->load->view("comprobantes/submenu",$data);
				$this->load->view("comprobantes/advanced_search",$data); 
				$this->load->library('ajax');
				$data["footer"] = $this->ajax->company_list("","razonSocial");
				
			} else 
			{
				$this->load->view("cheques/export_data",$data); //print_r($_SESSION["subtitle"]); print_r($_SESSION["cond"]);
				$this->load->view("comprobantes/list",$data);
				$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			}					
			
			$this->load->view("footer", $data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
	
	public function account_status($clase='cliente')
	{
		if (is_logged_in()) 
		{
		$data["header"] = "<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";	
		$this->load->view("header",$data);
		$this->load->view("menu");			
		$data = array("mensaje"=>"","typo"=>$clase);
		
		$vector = $this->input->post(array("typo","razonSocial","id_empresa_dest","tipo_comprobante","estado","pago",
						"fecha_desde","fecha_hasta","neto_desde","neto_hasta","total_desde","total_hasta"));
		if ($this->input->post("search")=="Buscar") 
		{
			if ((!isset($_SESSION["cond"])) AND (!isset($_SESSION["subtitle"]))) 
			{
				$_SESSION["cond"] = "";
				$_SESSION["subtitle"] = "";
			}						
					
			$subtitle[] = "Criterio de Busqueda";
			$cond = "";
			
			if ($vector["razonSocial"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(razonSocial LIKE '%".$vector["razonSocial"]."%' )"; 
				$subtitle[] = strtoupper($clase)." : ".$vector["razonSocial"];
			}
				
			if ($vector["id_empresa_dest"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(id_empresa_dest='".$vector["id_empresa_dest"]."' )"; 
				$empr = $this->Empresas_mdl->consulta("id='".$vector["id_empresa_dest"]."'");
				$_SESSION["empresa_dest"] = $empr[0]->razonSocial;
				$subtitle[] = " Empresa: ".$empr[0]->razonSocial;
			}
			
			if ($vector["tipo_comprobante"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(tipo_comprobante LIKE '%".$vector["tipo_comprobante"]."%' )"; 
				$subtitle[] = " Tipo Comprobante: ".$vector["tipo_comprobante"];
			}
						
			if ($vector["estado"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(estado LIKE '%".$vector["estado"]."%' )"; 
				$subtitle[] = " Estado: ".$vector["estado"];
			}
						
			if ($vector["pago"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				if ($vector["pago"] == "consaldo") 
				{
					$cond .= "(saldo > 0)"; 
					$subtitle[] = " Pago: Con Saldo";
				} else {
					$cond .= "(pago LIKE '%".$vector["pago"]."%' )"; 
					$subtitle[] = " Pago: ".$vector["pago"];
				}			
			}
			
			if ($vector["fecha_desde"] != null AND $vector["fecha_hasta"] != null) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(fecha BETWEEN '".$vector["fecha_desde"]."' AND '".$vector["fecha_hasta"]."'"; 
				$subtitle[] = "Fechas Entre: ".$vector["fecha_desde"]." Y ".$vector["fecha_hasta"];
			}
						
			if ($vector["fecha_desde"] != null AND $vector["fecha_hasta"] == null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(fecha >= '".$vector["fecha_desde"]."' )"; 
				$subtitle[] = " Fechas: Desde ".$vector["fecha_desde"];
			}
						
			if ($vector["fecha_desde"] == null AND $vector["fecha_hasta"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(fecha <= '".$vector["fecha_hasta"]."' )"; 
				$subtitle[] = " Fecha: Hasta ".$vector["fecha_hasta"];
			}
			
			if ($vector["neto_desde"] != null AND $vector["neto_hasta"] != null) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(neto BETWEEN '".$vector["neto_desde"]."' AND '".$vector["neto_hasta"]."')"; 
				$subtitle[] = " Neto : Entre ".$vector["neto_desde"]." Y ".$vector["neto_hasta"];
			}
						
			if ($vector["neto_desde"] != null AND $vector["neto_hasta"] == null) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(neto >= '".$vector["neto_desde"]."')"; 
				$subtitle[] = " Neto : Desde ".$vector["neto_desde"];
			}
						
			if ($vector["neto_desde"] == null AND $vector["neto_hasta"] != null) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(neto <= '".$vector["neto_hasta"]."')"; 
				$subtitle[] = " Neto : Hasta ".$vector["neto_hasta"];
			}
			
			if ($vector["total_desde"] != null AND $vector["total_hasta"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(total BETWEEN '".$vector["total_desde"]."' AND '".$vector["total_hasta"]."')"; 
				$subtitle[] = " Total: Entre $".$vector["total_desde"]." Y $".$vector["total_hasta"];
			}
						
			if ($vector["total_desde"] != null AND $vector["total_hasta"] == null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(total >= '".$vector["total_desde"]."' )"; 
				$subtitle[] = " Total : Desde $".$vector["total_desde"];
			}
						
			if ($vector["total_desde"] == null AND $vector["total_hasta"] != null ) 
			{
				if ($cond != "") 
				{
					$cond .= " AND ";
				}
				$cond .= "(total <= '".$vector["total_hasta"]."' )"; 
				$subtitle[] = " Total: Hasta $".$vector["total_hasta"];
			}	
			
			if ($cond != "") 
			{
				$cond .= " AND ";
			}
			$cond = $cond." (typo='".$clase."') GROUP BY razonSocial,facturacion,tipo_comprobante";		
			$data["lista"] = $this->Comprobantes_mdl->consulta_views($cond,
								"razonSocial,facturacion,tipo_comprobante,pago,SUM(total) AS total,SUM(saldo) AS saldo,".
								"id_empresa_dest,id_empresa");
			
			$_SESSION["comprobantes_list"] = $data["lista"];
			$_SESSION["subtitle"] = $subtitle;
			
			$data["title"] = "Resumen de cuenta - ".$clase;
			$data["clase"] = $clase;
			$data["dir"] ="imprimir/account_status/";	$data["dir2"] = "";
			
			$this->load->view("cheques/export_data",$data);
			$this->load->view("comprobantes/list_simple", $data);
			$data["footer"] = "";
		}else 
		{
			$this->load->model("Cuentas_mdl");
			$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY razonSocial ORDER BY razonSocial");		
		
			$this->load->view("comprobantes/account_status_search", $data);
		
			$this->load->library('ajax');
			$data["footer"] = $this->ajax->company_list("","razonSocial");
		}		
			
		$this->load->view("footer", $data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	public function receipts_update($id = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script language='JavaScript' type='text/javascript' src='".base_url()."media/js/facturas.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");			
			$data["mensaje"] = "";
			
			if ($id == 0) 
			{
				$data["mensaje"] = "¿Desea registrar una empresa?";		// verificar		
				$this->load->view("comprobantes/new",$data);
			} else 
			{				
				$post = $this->input->post(array("id_empresa","tipo_comprobante","nro","fecha","iva","neto","iva_valor",
															"iva2","neto2","iva_valor2",
															"concep_no_grabados","imp_internos","total","observaciones",
															"typo","estado","pago","id_empresa_dest","saldo"));	
				//print_r($post);					
				if (($this->input->post("total") != "" ) AND is_array($post))
				{					
					if ($this->Comprobantes_mdl->modifica($post,$id)) 
					{
						/*$a=1; 
						while ($a <= 10) 
						{
							$detalle_comprobante = $this->input->post(array("cant".$a,"concep".$a,"precio".$a,"tot".$a));
							$this->Detalle_comprobante_mdl->elimina(array("id_comprobante"=>$id,
																			"id_detalle"=>$a));
							if (($detalle_comprobante["concep".$a] != null) AND ($detalle_comprobante["tot".$a] != null)) 
							{
									$this->Detalle_comprobante_mdl->alta(array("id_comprobante"=>$id,
																			"id_detalle"=>$a,
																			"cantidad"=>$detalle_comprobante["cant".$a],
																			"concepto"=>$detalle_comprobante["concep".$a],
																			"precio"=>$detalle_comprobante["precio".$a],
																			"total"=>$detalle_comprobante["tot".$a]));																					
							}							
							$a++;	
						}*/							
						$data["mensaje"] = mensajes("Cambios realizados correctamente");
					} else 
					{
						$data["mensaje"] = mensajes("No se realizaron los cambios correctamente");
					}					
				} 
				
				$data["detalle"] = $this->Detalle_comprobante_mdl->consulta("id_comprobante='".$id."'");
				$data["vector"] = $this->Comprobantes_mdl->consulta("id_comprobante='".$id."'");
				$this->load->model("Cuentas_mdl");
				$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial","razonSocial,id_empresa");								
				$data["empresa"]=$this->Empresas_mdl->consulta("id='".$data["vector"][0]->id_empresa."'");				
				$this->load->view("comprobantes/update",$data);
			}	
			
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
		
	}
	
	//realiza la impresion de datos correspondientes
	public function receipts_delete($id_comprobante = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");	
			$data["mensaje"] = "";
			$this->Detalle_comprobante_mdl->elimina("id_comprobante ='".$id_comprobante."'");
			if ($this->Comprobantes_mdl->delete("id_comprobante ='".$id_comprobante."'"))
			{
				$data["mensaje"] = mensajes("Datos Eliminados correctamente");
			} else {
				$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
			}	
			$this->load->view("comprobantes/delete",$data);
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
	}
}

?>