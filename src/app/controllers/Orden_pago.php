<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase de registro de ordenes de pago
 */
 defined('BASEPATH') OR exit('Acceso no permitido');
 
 
 class Orden_pago extends CI_Controller 
 {
     
     function __construct() 
     {
         parent::__construct();
		
		$this->load->model('Orden_comprobante_mdl');
		$this->load->model('Orden_pago_mdl');
		$this->load->model('Temp_comprobantes_mdl');
		$this->load->model('Comprobantes_mdl');
		$this->load->model('Cheques_mdl');
		$this->load->model('Temp_conciliacion_mdl');
		$this->load->model('Cheques_Comprobantes_mdl');
		$this->load->model('Cuentas_mdl');
     }
	 
	 //lista de ordenes de pago
	 public function index()
	 {
		 if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");
			$data["mensaje"] = "" ;
			
			if (!is_null($buscar)) 
			{
				$data["lista"] = $this->Orden_pago_mdl->consulta(
									$campo." LIKE \"%".$buscar."%\") ORDER BY id_orden DESC limit 0,100");
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] .= mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] .= mensajes("No se encontraron resultados");	
				}
			}else {
				$data["lista"] = $this->Orden_pago_mdl->consulta("1 ORDER BY id_orden DESC limit 0,100");
			}
					
			$this->load->view("orden_pago/list",$data);
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	 }
	 
	 public function new_payment_order($procees=0, $company = 0,$op=0,$op2=0)
	 {
		 if (is_logged_in()) 
		{
			$data["header"]="<script language='JavaScript' type='text/javascript' src='".
					base_url()."media/js/orden_pago.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");			
			$data["mensaje"] = "" ; $data2["mensaje"] = "";
			
			switch ($procees) 
			{
				case '0': 
					
					$id_empresa_dest = $this->input->post("id_empresa_dest");					
					if (!isset($id_empresa_dest)) 
					{						
						$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																			"razonSocial,id_empresa");
						$this->load->view("deudas/billing_list",$data);
					} else {
						//seleccionar la empresa						
						if (!is_null($buscar)) 
						{
							$data["lista"] = $this->Empresas_mdl->consulta("(clase='proveedor".
									"' OR clase='empresa' OR clase='otros') AND (".
									$campo." LIKE \"%".$buscar."%\") ORDER BY id DESC limit 0,1000");
						}else {
							$data["lista"] = $this->Empresas_mdl->consulta("(clase='proveedor".
									"' OR clase='empresa' OR clase='otros') ORDER BY id DESC limit 0,1000");
						}
						$this->Temp_comprobantes_mdl->delete("1=1");	
						$this->load->view("orden_pago/list_companies",$data);
					}																		
					
				  	$_SESSION["id_empresa_dest"] = $id_empresa_dest;
					break;
				
				case '1'://seleccionar comprobantes		
													
					if (!is_null($buscar)) 
					{						
						$data2["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$company."') AND (".$campo
												." LIKE \"%".$buscar."%\") AND (id_empresa_dest='".$_SESSION["id_empresa_dest"].
									"' )AND (typo='proveedor') AND (pago <>'completado') ORDER BY id_comprobante DESC limit 0,100");
						
						if (is_array($data2["lista"])) 
						{
							$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						} else {
							$data2["mensaje"] =mensajes("No se encontraron resultados");	
						}				
					} else {				
						$data2["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$company."') AND (id_empresa_dest='".
											$_SESSION["id_empresa_dest"]. 
											"') AND (typo='proveedor') AND (pago <>'completado') ORDER BY id_comprobante DESC limit 0,100");	
					}
			
					if ($this->input->post("confirm")=="Agregar") 
					{						
						for ($i=0; $i < 100; $i++) 
						{							
							if ($this->input->post("check".$i)=="on") 
							{
								$this->Temp_comprobantes_mdl->alta(array("id_comprobante"=>$this->input->post("id_comprobante".$i),
																	 "tipo_comprobante"=>$this->input->post("tipo_comprobante".$i),
																	 "nro"=>$this->input->post("nro".$i),
																	 "fecha"=>$this->input->post("fecha".$i),
																	 "total"=>$this->input->post("total".$i)));
							} 
							
						}
						redirect('/orden_pago/new_payment_order/2/'.$company."/", 'location');
					}
					$this->Temp_conciliacion_mdl->delete("1=1");
					$data2["title"] = "Generar nueva orden de pago"; 
					$data2["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																				"razonSocial,id_empresa");
					$data2["empresa"] = $this->Empresas_mdl->consulta("id='".$company."'");
					$this->load->view("orden_pago/voucher_list",$data2);
					break;
			
				case '2': //seleccionar cheques		
					$_SESSION["id_empresa"] = $company;			
					if (!is_null($buscar)) 
					{
						$data["lista"] = $this->Cheques_mdl->consulta_views("(id_empresa='".$_SESSION["id_empresa_dest"].
																			"') AND (".$campo." LIKE \"%".$buscar.
																		"%\") ORDER BY emision DESC limit 0,5000");
											
						$_SESSION["cond"] = $campo." LIKE \"%".$buscar."%\"  ORDER BY emision DESC";
						$_SESSION["subtitle"][0] = "Criterio de busqueda: ";
						$_SESSION["subtitle"][1] = $campo." que contengan ".$buscar; 
						if (is_array($data["lista"])) 
						{
							$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						} else {
							$data["mensaje"] =mensajes("No se encontraron resultados");	
						}				
					} else {
						$data["lista"] = $this->Cheques_mdl->consulta_views( "(id_empresa='".$_SESSION["id_empresa_dest"].
											"')  ORDER BY emision DESC limit 0,5000");
					}
					
					if ($this->input->post("confirm")=="Agregar") 
					{						
						for ($i=0; $i < 100; $i++) 
						{							
							if ($this->input->post("check".$i)=="on") 
							{
								$this->Temp_conciliacion_mdl->alta(array("banco"=>$this->input->post("banco".$i),
																	 	"cuenta"=>$this->input->post("cuenta".$i),
																	 "nro"=>$this->input->post("nro".$i),
																	 "tipo"=>$this->input->post("tipo".$i),
																	 "paguese"=>$this->input->post("paguese".$i),
																	 "emision"=>$this->input->post("emision".$i),
																	 "vencimiento"=>$this->input->post("vencimiento".$i),
																	 "cantidad"=>$this->input->post("cantidad".$i)));
							} 
							
						}				
						
						redirect('/orden_pago/new_payment_order/3/', 'location');
					}
					$data["empresa"]=$this->Empresas_mdl->consulta("id='".$_SESSION["id_empresa"]."'");	
					$data["title"] = "Generar nueva orden de pago"; 
					$this->load->view("orden_pago/list_check",$data);
					break;
				
				case '3':// seleccionar otros medios de pago y generar orden					
					
					if (($company != 0) AND ($op==0) AND ($op2==0)) //eliminar comprobantes
					{
						if ($this->Temp_comprobantes_mdl->delete("id_comprobante='".$company."'")) 
						{
							$data["mensaje"] = mensajes("Comprobante eliminado de la orden de pago") ;
						} else {
							$data["mensaje"] = mensajes("Comprobante no eliminado de la orden de pago") ;
						}						
					}elseif (($company != 0) AND ($op!=0) AND ($op2!=0)) //eliminar cheques
					{
						if ($this->Temp_conciliacion_mdl->delete(array("banco"=>$company,"cuenta"=>$op,"nro"=>$op2))) 
						{
							$data["mensaje"] = mensajes("Cheque eliminado de la orden de pago");
						} else {
							$data["mensaje"] = mensajes("Cheque no eliminado de la orden de pago");
						}						
					}
					
					$data["lista_comprobantes"] = $this->Temp_comprobantes_mdl->consulta("");
					$data["lista_cheques"] = $this->Temp_conciliacion_mdl->consulta("");
					
					$vector = $this->input->post(array("pago_efectivo","pago_retenciones","nro_transferencia","pago_transferencia",
														"pago_nota","fecha","total","facturacion","pendiente"));
					
					if (($vector["fecha"] != "" ) AND is_array($vector)) 
					{
						$vector["id_empresa"] = $_SESSION["id_empresa"]; //poner las condiciones para gravar
					//	str_replace(",", ".", $subject);
						$id_orden = $this->Orden_pago_mdl->alta($vector);						
						if ($id_orden != 0) 
						{
							$data["mensaje"] = mensajes("Orden de pago creada correctamente");
							$total = $vector["pago_efectivo"] + $vector["pago_retenciones"] + 
										$vector["pago_transferencia"] + $vector["pago_nota"];
							$data["lista_comprobantes"] = $this->Temp_comprobantes_mdl->consulta("1 ORDER BY id_comprobante DESC");
							
							//comprobantes							
							$total_comp = 0; $vect_comp = array();
							foreach ($data["lista_comprobantes"] as $row) 
							{
								//$this->Comprobantes_mdl->modifica(array("pago"=>"completado"),$row->id_comprobante);				
								$this->Orden_comprobante_mdl->alta(array("id_orden"=>"".$id_orden,"id_comprobante"=>$row->id_comprobante,
																	"tipo_comprobante"=>$row->tipo_comprobante,"nro"=>$row->nro,
																	"fecha"=>$row->fecha,"total"=>$row->total)); 
								$this->Temp_comprobantes_mdl->delete("id_comprobante='".$row->id_comprobante."'");
								
								$vect_comp[] = $row->id_comprobante;
								
								if ($total_comp <= $vector["total"]) 
								{
									$this->Comprobantes_mdl->modifica(array("pago"=>"completado","saldo"=>0), $row->id_comprobante);
									 
								} else {
									$this->Comprobantes_mdl->modifica(array("pago"=>"parcial","saldo"=>$total_comp - $vector["total"]),
																 $row->id_comprobante);
								}						
																
								$total_comp += $row->total;
							}
														
							//cheques							
							$n = count($vect_comp);
							if (($n == 0))
							{
								$vect_comp[0] = 0;	
							}
							
							$total_cheques = 0; 
							foreach ($data["lista_cheques"] as $row) 
							{
								for ($i=0; $i < $n; $i++) 
								{ 
									$array_cheques_comprobantes = array("id_comprobante"=>$vect_comp[$i],"banco"=>$row->banco,
																	"cuenta"=>$row->cuenta,"nro"=>$row->nro,
																	"saldo"=>"0","observaciones"=>"Cheque cantidad:".$row->cantidad,
																	"id_orden"=>"".$id_orden);
									
									if ($this->Cheques_Comprobantes_mdl->alta($array_cheques_comprobantes)) 
									{
										$this->Temp_conciliacion_mdl->delete("banco='".$row->banco."' AND cuenta='".$row->cuenta.
																			"' AND nro='".$row->nro."'");
									}						
								}
								$total_cheques += $row->cantidad;
							}							
							
						} else {
							$data["mensaje"] = mensajes("Orden de pago no creada");
						}						
					} 
					$data["empresa"]=$this->Empresas_mdl->consulta("id='".$_SESSION["id_empresa"]."'");	
					$this->load->view("orden_pago/submenu",$data); 
					$this->load->view("orden_pago/new",$data);
					break;
			}
			
		
						
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else 
		{
			redirect('/seguridad/logout/', 'location');	
		}
	 }
	 
	 public function update_payment_order($id_orden = 0, $procees=0, $company = 0, $op=0, $op2=0)
	 {
		 if (is_logged_in()) 
		{
			if ($id_orden == 0) 
			{
				redirect('/orden_pago/new_payment_order/', 'location');
			} else {
				$data["header"]="<script language='JavaScript' type='text/javascript' src='".
					base_url()."media/js/orden_pago.js'></script>";
				$this->load->view("header",$data);
				$this->load->view("menu");
			
				$buscar = $this->input->post("buscar");
				$campo = $this->input->post("campo");
				$data["mensaje"] = "" ; $data["id_orden"] = $id_orden;
			
				$data["lista_comprobantes"] = $this->Orden_comprobante_mdl->consulta("id_orden='".$id_orden."'");
				$data["lista_cheques"] = $this->Cheques_Comprobantes_mdl->consulta("id_orden='".$id_orden.
											"' GROUP BY banco, cuenta, nro");
				$data["orden_pago"]	= $this->Orden_pago_mdl->consulta("id_orden='".$id_orden."'");				
				$_SESSION["id_empresa"] = $data["orden_pago"][0]->id_empresa;
				
				switch ($procees) 
				{
					case 0:
					if (($company != 0) AND ($op==0) AND ($op2==0)) //eliminar comprobantes
					{
						$array = array("id_comprobante"=>$company,"id_orden"=>$id_orden); 
						if ($this->Orden_comprobante_mdl->delete($array)) 
						{
							$data["mensaje"] = mensajes("Comprobante eliminado de la orden de pago") ;
						} else {
							$data["mensaje"] = mensajes("Comprobante no eliminado de la orden de pago") ;
						}						
					}elseif (($company != 0) AND ($op!=0) AND ($op2!=0)) //eliminar cheques
					{
						if ($this->Cheques_Comprobantes_mdl->delete(array("banco"=>$company,"cuenta"=>$op,"nro"=>$op2))) 
						{
							$data["mensaje"] = mensajes("Cheque eliminado de la orden de pago");
						} else {
							$data["mensaje"] = mensajes("Cheque no eliminado de la orden de pago");
						}						
					}
					$vector = $this->input->post(array("pago_efectivo","pago_retenciones","nro_transferencia","pago_transferencia",
														"pago_nota","fecha","estado","total","pendiente"));	
					//print_r($vector);
					$vector["id_empresa"] =	$_SESSION["id_empresa"];
					if (($vector["fecha"] != "" ) AND is_array($vector)) 
					{
						if ($this->Orden_pago_mdl->modifica($vector,$id_orden)) 
						{
							$data["mensaje"] = mensajes("Cambios realizados correctamente");
						} else {
							$data["mensaje"] = mensajes("No se realizaron los cambios correctamente");
						}						
					}
					
					$this->load->view("orden_pago/submenu",$data);												
					$data["orden_pago"]	= $this->Orden_pago_mdl->consulta("id_orden='".$id_orden."'");
					$data["dir"] = "update_payment_order/".$id_orden."/0/";
					$this->load->view("orden_pago/update",$data);	
						break;	
					
					case 1://Modificar comprobantes
					$data2["empresa"] = $this->Empresas_mdl->consulta("id='".$_SESSION["id_empresa"]."'");
					if (!is_null($buscar)) 
					{
						$data2["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$company."') AND (".$campo." LIKE \"%"
												.$buscar."%\") AND (id_empresa_dest = '".$_SESSION["id_empresa_dest"]
												."')  AND (typo = 'proveedor') AND (pago <>'completado')".
												"  ORDER BY id_comprobante DESC limit 0,1000");
						if (is_array($data2["lista"])) 
						{
							$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						} else {
							$data2["mensaje"] =mensajes("No se encontraron resultados");	
						}				
					} else {				
						$data2["lista"] = $this->Comprobantes_mdl->consulta_views("id_empresa=".$company.
											" AND (typo='proveedor') AND (pago <>'completado') ORDER BY id_comprobante DESC limit 0,1000");	
					}
			
					if ($this->input->post("confirm")=="Agregar") 
					{						
						for ($i=0; $i < 100; $i++) 
						{							
							if ($this->input->post("check".$i)=="on") 
							{
								$vect_orden = array("id_orden"=>$id_orden,
													"id_comprobante"=>$this->input->post("id_comprobante".$i),
													"tipo_comprobante"=>$this->input->post("tipo_comprobante".$i),
													"nro"=>$this->input->post("nro".$i),
													"fecha"=>$this->input->post("fecha".$i),
													"total"=>$this->input->post("total".$i));
								
								$this->Orden_comprobante_mdl->alta($vect_orden);								
							} 							
						}
						redirect('/orden_pago/update_payment_order/'.$id_orden."/", 'location');
					}
					
					$this->load->view("orden_pago/submenu",$data);
					$data2["title"] = "Modificar Orden de pago";
					$this->load->view("orden_pago/voucher_list",$data2);	
						break;
					
					case 2://modificar cheques - probar
					if (!is_null($buscar)) 
					{
						$data["lista"] = $this->Cheques_mdl->consulta_views("(id_empresa='".$_SESSION["id_empresa_dest"].
																		"') AND (".$campo." LIKE \"%".$buscar.
																		"%\") AND NOT (nro IN (SELECT nro FROM cheques_comprobantes
											))  ORDER BY emision DESC limit 0,5000");
																		
						$_SESSION["cond"] = $campo." LIKE \"%".$buscar."%\" clase = 'con factura' ORDER BY emision DESC";
						$_SESSION["subtitle"][0] = "Criterio de busqueda: ";
						$_SESSION["subtitle"][1] = $campo." que contengan ".$buscar; 
						if (is_array($data["lista"])) 
						{
							$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						} else {
							$data["mensaje"] =mensajes("No se encontraron resultados");	
						}	
				
					} else {// ver condiciones
						$data["lista"] = $this->Cheques_mdl->consulta_views("(id_empresa='".$_SESSION["id_empresa_dest"].
																			"') AND NOT (nro IN (SELECT nro FROM cheques_comprobantes
											))  ORDER BY emision DESC limit 0,5000");
					}
					
					if ($this->input->post("confirm")=="Agregar") 
					{						
						for ($i=0; $i < 100; $i++) 
						{							
							if ($this->input->post("check".$i)=="on") 
							{									
								if ($data["lista_comprobantes"] == Array()) 
								{
									$this->Cheques_Comprobantes_mdl->alta(array("id_comprobante"=>0,
																					"banco"=>$this->input->post("banco".$i),
																					"cuenta"=>$this->input->post("cuenta".$i),
																					"nro"=>$this->input->post("nro".$i),
																					"saldo"=>0,
																					"observaciones"=>"Cheque cantidad:".$this->input->post("nro".$i),
																					"id_orden"=>$id_orden));
								} else {
									foreach ($data["lista_comprobantes"] as $row_comp=>$id_comp) // corregir un error Aqui
									{
										$this->Cheques_Comprobantes_mdl->alta(array("id_comprobante"=>$id_comp->id_comprobante,
																					"banco"=>$this->input->post("banco".$i),
																					"cuenta"=>$this->input->post("cuenta".$i),
																					"nro"=>$this->input->post("nro".$i),
																					"saldo"=>0,
																					"observaciones"=>"Cheque cantidad:".$this->input->post("nro".$i),
																					"id_orden"=>$id_orden));										
									}
								}								
																
							}
						} 
						redirect('/orden_pago/update_payment_order/'.$id_orden.'/', 'location');
					}				
												
					$this->load->view("orden_pago/submenu",$data);
					$data["empresa"]=$this->Empresas_mdl->consulta("id='".$_SESSION["id_empresa"]."'");	
					$data["title"] = "Modificar orden de pago"; 
					$this->load->view("orden_pago/list_check",$data);	
						break;
					}				
				}					
			
				$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
				$this->load->view("footer",$data);			
			
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	 }
	
	//muestra la lista de pagos por empresas
	public function list_by_id($id_empresa = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");
			$row = $this->Empresas_mdl->consulta("id='".$id_empresa."'");
			$data["mensaje"] = "Razon Social: ".$row[0]->razonSocial." CUIT: ".$row[0]->cuit ;
			
			if (!is_null($buscar)) 
			{
				$data["lista"] = $this->Orden_pago_mdl->consulta("id_empresa='".$id_empresa."' ".
									$campo." LIKE \"%".$buscar."%\") ORDER BY id_orden DESC limit 0,100");
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] .= mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] .= mensajes("No se encontraron resultados");	
				}
			}else {
				$data["lista"] = $this->Orden_pago_mdl->consulta("id_empresa='".$id_empresa."' ORDER BY id_orden DESC limit 0,100");
			}
					
			$this->load->view("orden_pago/list",$data);
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
				$this->load->view("footer",$data);			
			
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}

	public function delete_payment($id_pago = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$data["mensaje"] = "";
			
			if ($this->input->post("delete") == "delete") 
			{
				//cambiar estado de facturas
				$list_orden = $this->Orden_comprobante_mdl->consulta("id_orden='".$id_pago."'");
				foreach ($list_orden as $row) 
				{
					$this->Comprobantes_mdl->consulta("id_comprobante='".$row->id_comprobante."'");
					if ($_SESSION["cant_reg"]>1) 
					{
						$this->Comprobantes_mdl->modifica(array("pago" => "parcial"),$row->id_comprobante);
					} else {
						$this->Comprobantes_mdl->modifica(array("pago" => "pendiente"),$row->id_comprobante);
					}
					$this->Orden_comprobante_mdl->delete(array("id_orden"=>$row->id_orden,"id_comprobante"=>$row->id_comprobante));			
				}				
				//eliminar cheques vinculados
				if ($this->Cheques_Comprobantes_mdl->delete("id_orden='".$id_pago."'")) 
				{
					$data["mensaje"] = mensajes("Cheques Desvinculados Correctamente");
				} else {
					$data["mensaje"] = mensajes("Fallo en la desvinculacion de cheques");
				}
				
				//elimina orden de pago
				if ($this->Orden_pago_mdl->delete(array("id_orden"=>$id_pago))) 
				{
					$data["mensaje"] = mensajes("Datos Eliminados correctamente");
				} else {
					$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
				}		
			}
			$this->load->view("orden_pago/delete",$data);
			
			$this->load->view("footer",$data);
			
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
 }
 
 ?>