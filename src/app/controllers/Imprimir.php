<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase de 
 */
class Imprimir extends CI_Controller 
{
	
	function __construct() 
	{
		parent::__construct();			
					
		$this->load->model("Cheques_mdl");
		$this->load->model("Cuentas_mdl");
		$this->load->model("Impresion_mdl");
	}
	
	
	//realiza los informes en pdf de cheques por cuenta
	public function cheques_por_cuenta($id=0,$cuenta=0,$operacion="lista")
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		if (!isset($_SESSION["cond"])) 
		{
			$_SESSION["cond"]="";
		}
		$cheques = $this->Cheques_mdl->consulta_views($_SESSION["cond"]);
		
		switch ($operacion) 
		{
			case "lista":
				$title = "Listados de cheques";//$_SESSION["title"];
				$datos_cuenta = $this->Cuentas_mdl->consultavista("id_banco='".$id
														."' AND nro_cuenta='".$cuenta."'");														
				
				$pdf->AddPage('P','Legal',0);
	 			//cebecera
     			$pdf->SetFont('Arial','B',10);
     			$pdf->Cell(15,0,'Banco:',0,0,'L');
	 
				 $pdf->SetFont('Arial','',10);
	 			$pdf->Cell(120,0,$datos_cuenta[0]->Banco,0,0,'L'); // poner banco
	 
	 			$pdf->SetFont('Arial','B',10);
     			$pdf->Cell(15,0,'Cuenta:',0,0,'L');
	 
	 			$pdf->SetFont('Arial','',10); //cuenta
     			$pdf->Cell(0,0,$datos_cuenta[0]->nro_cuenta,0,0,'L');
	 
	 			$pdf->Ln(5);
	 			$pdf->SetFont('Arial','B',10);
     			$pdf->Cell(15,0,'Titular:',0,0,'L');
	 
				$pdf->SetFont('Arial','',10); //titular
     			$pdf->Cell(0,0,$datos_cuenta[0]->razonSocial,0,0,'L');
	 
	 			$pdf->Ln(5);
	 			$pdf->SetFont('Arial','B',11);
     			$pdf->Cell(0,0,$title,0,0,'C');
				
				$pdf->Ln(5); //cabecera
				 
				$i = 0;
				if (!isset($_SESSION["subtitle"])) 
				{
					$_SESSION["subtitle"]="";
				}
				if (count($_SESSION["subtitle"])>1) 
				{
					foreach ($_SESSION["subtitle"] as $sub)
					{
						if ($i ==0) 
						{
							$pdf->SetFont('Arial','B',9);
							$pdf->Cell(0,0,$sub,0,0,'L');
						} else {
							$pdf->SetFont('Arial','',9);
							$pdf->Cell(0,0,"       ".$sub,0,0,'L');
						}
						$pdf->Ln(5);					
						$i++;
					}
				} 
	 			$pdf->SetFillColor(232,232,232);
				$pdf->SetFont('Arial','B',11);
				$pdf->Cell(20,5, utf8_decode('Cheque Nº'),1,0,'C',1);
				$pdf->Cell(22,5,'Emitido',1,0,'C',1);
				$pdf->Cell(22,5,'A pagar',1,0,'C',1);
				$pdf->Cell(110,5,'A la orden de',1,0,'C',1);
				$pdf->Cell(25,5,'Importe',1,0,'C',1);
				$pdf->Ln();
	
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',10);
				$total = 0; $conciliados = 0;
				foreach ($cheques as $row)
				{
					if ($row->estado == "conciliados") 
					{
						$pdf->SetFont('Arial','B',9);
						$pdf->SetFillColor(232,232,232);
						$conciliados += $row->cantidad;
					} else {
						$pdf->SetFont('Arial','',8);
						$pdf->SetFillColor(255,255,255);	
					}
					$pdf->Cell(20,4, $row->nro,1,0,'C',1);
					$pdf->Cell(22,4,invierte_fecha($row->emision),1,0,'C',1);
					$pdf->Cell(22,4,invierte_fecha($row->vencimiento),1,0,'C',1);
					$pdf->Cell(110,4,utf8_decode($row->paguese),1,0,'C',1);
					$pdf->Cell(25,4,number_format($row->cantidad,2,",","."),1,0,'R',1);
					$pdf->Ln();
					$total += $row->cantidad;
				}
				$pdf->Ln();
				$pdf->SetFont('Arial','B',14);
				$pdf->Cell(0,0,'Total : $ '.number_format($total,2,",","."),0,0,'R');$pdf->Ln(5);
				$pdf->SetFont('Arial','B',12); $total = $total - $conciliados;
				$pdf->Cell(0,0,'Total  No Conciliados : $ '.number_format($total,2,",",".")."       ",0,0,'R');$pdf->Ln(5);
				$pdf->Cell(0,0,'Total  Conciliados : $ '.number_format($conciliados,2,",",".")."       ",0,0,'R');$pdf->Ln(5);
				$pdf->Cell(0,0,'Total  Cheques nuevos : $                           ',0,0,'R');$pdf->Ln(5);
				$pdf->Cell(0,0,'Total  Cheques que pasan : $                         ',0,0,'R');
			break;
			
			case 'general': //muestra la convinacion de varias cuentas
				$title = "Listados de cheques de Varias Cuentas";
				
				$pdf->AddPage('L','Legal',0);
				$pdf->Ln(5);
				$pdf->SetFillColor(232,232,232);
				$pdf->SetFont('Arial','B',16);
				$pdf->Cell(0,0,$title,0,0,'C');
				$pdf->Ln(5); //cabecera
				$i = 0;
				if (count($_SESSION["subtitle"])>1) 
				{
					foreach ($_SESSION["subtitle"] as $sub)
					{
						if ($i ==0) 
						{
							$pdf->SetFont('Arial','B',9);
							$pdf->Cell(0,0,$sub,0,0,'L');
						} else {
							$pdf->SetFont('Arial','',9);
							$pdf->Cell(0,0,"       ".$sub,0,0,'L');
						}
						$pdf->Ln(5);					
						$i++;
					}
				}
				
				
	 			$pdf->SetFillColor(232,232,232);
				$pdf->SetFont('Arial','B',10);
				$pdf->Cell(50,5,'Banco',1,0,'C',1);
				$pdf->Cell(80,5,'Titular',1,0,'C',1);
				$pdf->Cell(35,5, utf8_decode('Cheque Nº'),1,0,'C',1);
				$pdf->Cell(28,5,'Emitido',1,0,'C',1);
				$pdf->Cell(28,5,'A pagar',1,0,'C',1);
				$pdf->Cell(90,5,'A la orden de',1,0,'C',1);
				$pdf->Cell(30,5,'Importe',1,0,'C',1);
				$pdf->Ln();
				
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',10);
				$total = 0; $conciliados = 0;
				foreach ($cheques as $row) 
				{
					if ($row->estado == "conciliados") 
					{
						$pdf->SetFont('Arial','B',9);
						$pdf->SetFillColor(232,232,232);
						$conciliados += $row->cantidad;
					} else {
						$pdf->SetFont('Arial','',8);
						$pdf->SetFillColor(255,255,255);	
					}
					
					$pdf->Cell(50,4,$row->Banco,1,0,'L',1);
					$pdf->Cell(80,4,$row->razonSocial,1,0,'L',1);
					$pdf->Cell(35,4, $row->nro,1,0,'C',1);
					$pdf->Cell(28,4,invierte_fecha($row->emision),1,0,'C',1);
					$pdf->Cell(28,4,invierte_fecha($row->vencimiento),1,0,'C',1);
					$pdf->Cell(90,4,utf8_decode($row->paguese),1,0,'L',1);
					$pdf->Cell(30,4,number_format($row->cantidad,2,",","."),1,0,'R',1);
					$total += $row->cantidad;
					$pdf->Ln();
				}
				$pdf->Ln();
				$pdf->SetFont('Arial','B',14);
				$pdf->Cell(0,0,'Total : $ '.number_format($total,2,",",".")."       ",0,0,'R');$pdf->Ln(5);
				$pdf->SetFont('Arial','B',12); $total = $total - $conciliados;
				$pdf->Cell(0,0,'Total  No Conciliados: $ '.number_format($total,2,",",".")."       ",0,0,'R');$pdf->Ln(5);
				$pdf->Cell(0,0,'Total  Conciliados : $ '.number_format($conciliados,2,",",".")."       ",0,0,'R');$pdf->Ln(5);
				$pdf->Cell(0,0,'Total  Cheques nuevos : $                           ',0,0,'R');$pdf->Ln(5);
				$pdf->Cell(0,0,'Total  Cheques que pasan : $                         ',0,0,'R');
			break;
		}	 
	
	
	$pdf->AutoPrint();
    $pdf->Output('cheques.pdf' , 'I' );
	}

	private function generate_check($id=0,$cuenta=0,$nro =0, $cheque = array(),$pdf,$vector)
	{		
		
		if ($vector == array()) 
		{
			$vector = array("id"=>$id,
						"cant_x"=>0,"cant_y"=>0,"emision_x"=>0,"emision_y"=>0,
						"vencimiento_x"=>0,"vencimiento_y"=>0,"paguese_x"=>0,"paguese_y"=>0,
						"cant_letras_x"=>0,"cant_letras_y"=>0,"saltos"=>0);
			$this->Impresion_mdl->alta($vector);
			$vector = $this->Impresion_mdl->consulta("id='".$id."'");
			$vector = $vector[0]; 
		}else
		{	
			$vector = $vector[0]; 	
		}	 
		
		//cantidad
		$pdf->Cell(125 + $vector->cant_x,-5+ $vector->cant_y ,"",0,0,'C',0);
		$pdf->SetFont("Arial","B",12);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetFillColor(0, 0, 0);		
		$pdf->Cell(45 ,-5  ,"$ ".number_format( $cheque->cantidad, 2, ",", ".") ,1,0,"C",TRUE);
		
		//fechas
		$fecha_actual = date("d-m-Y");
		setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
		
		$pdf->Ln(5);
		$pdf->SetFont("Arial","B",10);
		$pdf->SetTextColor(0,0,0);
		$esp = (10 - strlen(strftime("%B", strtotime($cheque->emision))));
		$pdf->Cell(110 + $vector->emision_x,6 + $vector->emision_y,
					strtoupper(strftime("%d     ".espacios($esp)."     %B                       %Y", strtotime($cheque->emision))),0,0,'R');
		$pdf->Ln(5);
		$esp = (10 - strlen(strftime("%B", strtotime($cheque->vencimiento))));
		$pdf->Cell(100+ $vector->vencimiento_x,6+ $vector->vencimiento_y,
					strtoupper(strftime("%d     ".espacios($esp)."     %B                        %Y",strtotime($cheque->vencimiento))),0,0,'R');	
		
		//Paguese a
		$pdf->Ln(8);	
		if($cheque->tipo == "A")
		{	
			$paguese = strlen($cheque->paguese);			
			if($paguese >= 35)
			{
				$pos = strrpos($cheque->paguese," ",25); 
				$pdf->Cell(40+ $vector->paguese_x ,0+ $vector->paguese_y,	"",0,0,'R');
				$pdf->Cell(40+$vector->paguese_x +  $pos,0+ $vector->paguese_y, 
					"**".substr(strtoupper(utf8_decode($cheque->paguese)), 0,$pos),0,0,'L');
				$pdf->Ln(4);
				$pdf->Cell(40  ,0 + $vector->cant_letras_y,"",0,0,'C',0);
				$pdf->Cell(80+ $vector->paguese_x + $paguese- $pos,0+ $vector->paguese_y, 
					substr(strtoupper(utf8_decode($cheque->paguese)), $pos)."**",0,0,'L');$pdf->Ln(4);
			}
			else
			{
				$pdf->Cell(40+ $vector->paguese_x ,0+ $vector->paguese_y,	"",0,0,'R');
				$pdf->Cell(40+ $vector->paguese_x ,0+ $vector->paguese_y, "**".strtoupper(utf8_decode($cheque->paguese))."**",0,0,'L');$pdf->Ln(8);
			}
		}
		else
		{//120
			$pdf->Cell(120+ $vector->paguese_x,0+ $vector->paguese_y,"                      ",0,0,'R');$pdf->Ln(8);
		}		
		
		//cantidad letras
		$pdf->Ln(10);	
		$letras = strlen(nros_letras($cheque->cantidad));
		
		if($letras>45)
		{
			$aux = nros_letras($cheque->cantidad);
			$pos = strpos($aux," ",35);
			$pdf->Cell(20 + $vector->cant_letras_x,0 + $vector->cant_letras_y,"",0,0,'C',0);
			$pdf->Cell(80 + $pos + $vector->cant_letras_x ,0+ $vector->cant_letras_y,
						"** ".substr(strtoupper(utf8_decode(nros_letras($cheque->cantidad))), 0,$pos),0,0,'L');
			$pdf->Ln(4);
			$pdf->Cell(40  ,0 + $vector->cant_letras_y,"",0,0,'C',0);
			$pdf->Cell(80 + $letras - $pos + $vector->cant_letras_x , 0 + $vector->cant_letras_y, 
						substr(strtoupper(utf8_decode(nros_letras($cheque->cantidad))), $pos)." **",0,0,'L');
		}
		else {
			$pdf->Cell(20 + $vector->cant_letras_x,0 + $vector->cant_letras_y,"",0,0,'C',0);
			$pdf->Cell(60 + $letras  , 0 + $vector->cant_letras_y,
						"** ".strtoupper(utf8_decode(nros_letras($cheque->cantidad)))." **",0,0,'L');$pdf->Ln(4);
		}		
	}
	
	//genera la impresion de un cheque
	public function print_cheque($id=0,$cuenta=0,$nro =0)
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$this->load->helper("nros");	
		
		$pdf = new PDF_JavaScript();
		$cheque = $this->Cheques_mdl->consulta("banco='".$id
											."' AND cuenta='".$cuenta
											."' AND nro='".$nro."'");
		$pdf->AddPage('P','cheque',0);
				
		$vector = $this->Impresion_mdl->consulta("id='".$id."'");
		
		
		$this->generate_check($id=0, $cuenta=0, $nro =0, $cheque[0],$pdf,$vector);
		//print_r($vector[0]);
		if ($vector == null) 
		{
			$vector = array("saltos"=>0);
			$pdf->Ln(47.5 + $vector[0]->saltos);
		} 
		else {
			$pdf->Ln(47.5 + $vector[0]->saltos);
		}
				
		$pdf->AutoPrint();		
		$pdf->Output();
	}
	
	//genera la impresion de cheques pendientes de impresion
	public function print_pending($id=0,$cuenta=0)
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$this->load->helper("nros");		
				
		$vector = $this->Impresion_mdl->consulta("id='".$id."'");
		$pdf = new PDF_JavaScript();
		$list_check = $this->Cheques_mdl->consulta("banco='".$id
											."' AND cuenta='".$cuenta
											."' AND impreso='false'");
		//print_r($list_check);
		$i=0;	
		
		$cond = array("banco"=>$id,"cuenta"=>$cuenta);
		$this->Cheques_mdl->modifica(array("impreso"=>"true"),$cond);	
		$pdf->AddPage('P','cheque',0);	
		$lista = count ($list_check);
		foreach ($list_check as $row ) 
		{
			if($lista > $i)
			{
				$this->generate_check($id=0, $cuenta=0, $row->nro, $list_check[$i],$pdf,$vector);;
				$pdf->Ln(47.5+$vector[0]->saltos); 
				if((($i+1) % 4 == 0) AND ($i !=0) AND ($i+1 != $lista))
				{
					$pdf->AddPage('P','cheque',0);
				}
			}				
			$i++;
		}
		
		$pdf->AutoPrint();			
		$pdf->Output();
	}
	
	//imprime los movimientos
	public function print_movements()
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$this->load->model("Movimientos_mdl");
		$this->load->model("Concepto_mdl");
		$movimientos = $this->Movimientos_mdl->consulta_views($_SESSION["cond"]);
				
		$pdf = new PDF_JavaScript();
		$title = "Listados de movimientos bancarios ";
		
		if (isset($_SESSION["subtitle"])) 
		{
			$_SESSION["subtitle"]= array();
		}
				
		$pdf->AddPage('P','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(0,0,$title,0,0,'C');
		$pdf->Ln(10); //cabecera
		
		$i = 0;
				if (count($_SESSION["subtitle"])>1) 
				{//print_r($_SESSION["subtitle"]);					
					foreach ($_SESSION["subtitle"] as $sub)
					{
						if ($i ==0) 
						{
							$pdf->SetFont('Arial','B',14);
							$pdf->Cell(0,0,$sub,0,0,'L');
						} else {
							$pdf->SetFont('Arial','',14);
							$pdf->Cell(0,0,"       ".$sub,0,0,'L');
						}
						$pdf->Ln(8);					
						$i++;
					}
				}
		
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',12);
		/*$pdf->Cell(60,6,'Banco',1,0,'C',1);
		$pdf->Cell(90,6,'Titular',1,0,'C',1);
		$pdf->Cell(60,6, utf8_decode('Cuenta Nº'),1,0,'C',1);*/		
		$pdf->Cell(25,6,'Fecha',1,0,'C',1);
		$pdf->Cell(60,6,'Movimiento',1,0,'C',1);			
		$pdf->Cell(80,6,'Otros Datos',1,0,'C',1);
		$pdf->Cell(30,6,'Valor',1,0,'C',1);	
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',10);
		$total = 0;		
		foreach ($movimientos as $row) 
		{
			/*$pdf->Cell(60,6,utf8_decode($row->Banco),1,0,'C',1);
			$pdf->Cell(90,6,utf8_decode($row->razonSocial),1,0,'C',1);
			$pdf->Cell(60,6,$row->cuenta,1,0,'C',1);*/			
			$row->valor = round($row->valor,2);
			$concept = $this->Concepto_mdl->consulta("id_concepto='".$row->concepto."'","concepto");
			$pdf->Cell(25,6,invierte_fecha($row->fecha),1,0,'C',1);
			$pdf->Cell(60,6,$concept[0]->concepto,1,0,'C',1);
			$pdf->Cell(80,6,utf8_decode($row->observaciones),1,0,'C',1);
			$pdf->Cell(30,6,number_format($row->valor,2,",","."),1,0,'R',1);		
			
			$pdf->Ln();
			$total += $row->valor;
		}		
				
		$pdf->Ln();
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,'Total $:  '.number_format($total,2,",","."),0,0,'R');
				
		$pdf->AutoPrint();
    $pdf->Output('movimientos.pdf' , 'I' );
	}

	//imprime la conciliacion de cheques
	public function print_reconciliation($id_banco =0, $cuenta="")
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = "Conciliacion de Cheques";
		$datos_cuenta = $this->Cuentas_mdl->consultavista("id_banco='".$id_banco
														."' AND nro_cuenta='".$cuenta."'");
		
				$pdf->AddPage('P','Legal',0);
	 			//cebecera
     			$pdf->SetFont('Arial','B',11);
     			$pdf->Cell(15,0,'Banco:',0,0,'L');
	 
				 $pdf->SetFont('Arial','',11);
	 			$pdf->Cell(140,0,$datos_cuenta[0]->Banco,0,0,'L'); // poner banco
	 
	 			$pdf->SetFont('Arial','B',11);
     			$pdf->Cell(15,0,'Cuenta:',0,0,'L');
	 
	 			$pdf->SetFont('Arial','',11); //cuenta
     			$pdf->Cell(0,0,$datos_cuenta[0]->nro_cuenta,0,0,'L');
	 
	 			$pdf->Ln(5);
	 			$pdf->SetFont('Arial','B',11);
     			$pdf->Cell(15,0,'Titular:',0,0,'L');
	 
				$pdf->SetFont('Arial','',11); //titular
     			$pdf->Cell(0,0,$datos_cuenta[0]->razonSocial,0,0,'L');
	 
	 			$pdf->Ln(5);
	 			$pdf->SetFont('Arial','B',11);
     			$pdf->Cell(0,0,$title,0,0,'C');
				
				$pdf->Ln(5); //cabecera
				 
				$pdf->SetFillColor(232,232,232);
				$pdf->SetFont('Arial','B',12);
				$pdf->Cell(35,6, utf8_decode('Cheque Nº'),1,0,'C',1);
				$pdf->Cell(30,6,'Emitido',1,0,'C',1);
				$pdf->Cell(30,6,'A pagar',1,0,'C',1);
				$pdf->Cell(70,6,'A la orden de',1,0,'C',1);
				$pdf->Cell(30,6,'Importe',1,0,'C',1);
				$pdf->Ln();
				
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',12);
				$total = 0;
								
				foreach ($_SESSION["conciliacion"] as $row)
				{
					$pdf->Cell(35,5, $row->nro,1,0,'C',1);
					$pdf->Cell(30,5,invierte_fecha($row->emision),1,0,'C',1);
					$pdf->Cell(30,5,invierte_fecha($row->vencimiento),1,0,'C',1);
					$pdf->Cell(70,5,$row->paguese,1,0,'C',1);
					$pdf->Cell(30,5,number_format($row->cantidad,2,",","."),1,0,'R',1);
					$pdf->Ln();
					$total += $row->cantidad;
				}
				$pdf->Ln();
				$pdf->SetFont('Arial','B',14);
				$pdf->Cell(0,0,'Total $:  '.$total,0,0,'R'); 
		
		$pdf->AutoPrint();			
		$pdf->Output();
	}
	
	// realiza la configuracion de cheques
	public function set_print($idBanco=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		$data["mensaje"]="";
		
		if ($idBanco==0) 
		{
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");
			if (!is_null($buscar)) 
			{
				$data["lista"] = $this->Empresas_mdl->consulta("(clase='banco') AND ".
															$campo." LIKE \"%".$buscar."%\" ORDER BY id DESC limit 0,100");
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] =mensajes("No se encontraron resultados");	
				}				
			} else {				
				$data["lista"] = $this->Empresas_mdl->consulta("(clase='banco') ORDER BY id DESC limit 0,100");	
			}
			
			$this->load->view("impresion/list-bank",$data);
			
			$data["footer"]="<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
		} else {
			
			
			$vector = $this->input->post(array(
						"cant_x","cant_y","emision_x","emision_y","vencimiento_x","vencimiento_y","paguese_x","paguese_y",
						"cant_letras_x","cant_letras_y","saltos")); 
						//print_r($vector);
			if ($vector["cant_x"] != "" AND is_array($vector)) 
			{
				if ($this->Impresion_mdl->modifica($vector,"id='".$idBanco."'")) 
				{
					$data["mensaje"] =mensajes("Cambios realizados correctamente");	
				} else {
					$data["mensaje"] =mensajes("Error al actualizar los datos");
				}				
			} 
			
			$data["datos"] = $this->Impresion_mdl->consulta("id='".$idBanco."'");
			//print_r($data["datos"]);
			if ($data["datos"] == array()) 
			{
				$this->Impresion_mdl->alta(array("id"=>$idBanco,
						"cant_x"=>0,"cant_y"=>0,"emision_x"=>0,"emision_y"=>0,
						"vencimiento_x"=>0,"vencimiento_y"=>0,"paguese_x"=>0,"paguese_y"=>0,
						"cant_letras_x"=>0,"cant_letras_y"=>0,"saltos"=>0));
				$data["datos"] = $this->Impresion_mdl->consulta("id='".$idBanco."'");				
			}			
			$data["Banco"] = $this->Empresas_mdl->consulta("id='".$idBanco."'");			
			$this->load->view("Impresion/new",$data);
			$data["footer"] = "";
		}	
				
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}

	//imprime la lista de compañias
	public function list_company()
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = "Lista de empresas";
				
		$pdf->AddPage('L','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(0,0,$title,0,0,'C');
		$pdf->Ln(10); //cabecera
		
		$i = 0;
				if (count($_SESSION["subtitle"])>1) 
				{//print_r($_SESSION["subtitle"]);					
					foreach ($_SESSION["subtitle"] as $sub)
					{
						if ($i ==0) 
						{
							$pdf->SetFont('Arial','B',14);
							$pdf->Cell(0,0,$sub,0,0,'L');
						} else {
							$pdf->SetFont('Arial','',14);
							$pdf->Cell(0,0,"       ".$sub,0,0,'L');
						}
						$pdf->Ln(7);					
						$i++;
					}
				}
		
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(100,5,'Razon Social',1,0,'C',1);
		$pdf->Cell(50,5,'Actividad',1,0,'C',1);
		$pdf->Cell(90,5, "Domicilio",1,0,'C',1);		
		$pdf->Cell(40,5,'Telefono',1,0,'C',1);
		$pdf->Cell(50,5,'celular',1,0,'C',1);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',10);
		if (!isset($_SESSION["cond"])) 
		{
			$_SESSION["cond"]="";
		}
		$empresas = $this->Empresas_mdl->consulta($_SESSION["cond"]);	
		foreach ($empresas as $row) 
		{
			$pdf->Cell(100,4,utf8_decode($row->razonSocial),1,0,'C',1);
			$pdf->Cell(50,4,$row->actividad,1,0,'C',1);
			$pdf->Cell(90,4,utf8_decode($row->domicilio),1,0,'C',1);
			$pdf->Cell(40,4,$row->telefono,1,0,'C',1);
			$pdf->Cell(50,4,$row->celular,1,0,'C',1);
			
			$pdf->Ln();			
		}		
				
		$pdf->Ln();
		$pdf->SetFont('Arial','B',12);
						
		$pdf->AutoPrint();
    	$pdf->Output('movimientos.pdf' , 'I' );
	}

	//genera la oden de pago de la empresa
	public function pay_order($id_orden=0)
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = utf8_decode("Recibo de Pago");
		
		$this->load->model("Orden_pago_mdl");		
		$orden = $this->Orden_pago_mdl->consulta("id_orden='".$id_orden."'");
		$empresa = $this->Empresas_mdl->consulta("id='".$orden[0]->id_empresa."'");
		$facturacion = $this->Empresas_mdl->consulta("id='".$orden[0]->facturacion."'");
		
		$pdf->AddPage('P','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',18);
		$pdf->Cell(0,0,strtoupper($facturacion[0]->razonSocial),0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(0,0,$title,0,0,'C'); 
		$pdf->Ln(10); //cabecera
		
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(0,0,"Fecha: ".invierte_fecha($orden[0]->fecha)."                        ".utf8_decode("Nº")." Orden: ".$id_orden,0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,$empresa[0]->razonSocial,0,0,'L');
		$pdf->Ln(10);
		
		$pdf->Cell(0,0, "Apliquese A" ,0,0,'L'); $pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(40,5,'Fecha',1,0,'C',1);
		$pdf->Cell(100,5,'Fact/Doc',1,0,'C',1);
		$pdf->Cell(40,5, "Importe",1,0,'C',1);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
		
		//comprobantes
		$this->load->model("Orden_comprobante_mdl");
		$comprobantes = $this->Orden_comprobante_mdl->consulta("id_orden='".$id_orden."'");		
		$total=0;
		foreach ($comprobantes as $row) 
		{
			$pdf->Cell(40,4,invierte_fecha($row->fecha),1,0,'C',1);
			$pdf->Cell(100,4,$row->tipo_comprobante."  ".$row->nro,1,0,'C',1);
			$pdf->Cell(40,4,number_format($row->total,2,",","."),1,0,'R',1);			
			$pdf->Ln();	
			$total += $row->total;		
		}
		$pdf->Ln();
		$pdf->Cell(0,0,"Total: $".number_format($total,2,",","."),0,0,'L');
		$pdf->Ln(10);
		
		$pdf->Ln();
		$pdf->SetFont('Arial','B',14);
		
		//cheques
		$this->load->model("Cheques_Comprobantes_mdl");
		$cheques = $this->Cheques_Comprobantes_mdl->consulta("id_orden='".$id_orden."' GROUP BY nro");
		$pdf->Cell(0,0, "Valores remitidos" ,0,0,'L'); $pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(40,5,'Vencimiento',1,0,'C',1);
		$pdf->Cell(50,5,'Doc/Cheque',1,0,'C',1);
		$pdf->Cell(50,5,'Banco',1,0,'C',1);
		$pdf->Cell(40,5, "Importe",1,0,'C',1);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
		//$total=0;				
		foreach ($cheques as $row) 
		{
			$data_check = $this->Cheques_mdl->consulta_views(array("id_banco"=>$row->banco,"cuenta"=>$row->cuenta,"nro"=>$row->nro));						
			
			$pdf->Cell(40,4,invierte_fecha($data_check[0]->vencimiento),1,0,'C',1);
			$pdf->Cell(50,4,$row->cuenta."  ".$row->nro,1,0,'C',1);
			$pdf->Cell(50,4,$data_check[0]->Banco,1,0,'C',1);
			$pdf->Cell(40,4,number_format($data_check[0]->cantidad,2,",","."),1,0,'R',1);			
			$pdf->Ln();	
			//$total += $data_check[0]->cantidad;
		}
		if ($orden[0]->pago_efectivo != "") 
		{
			$pdf->Ln(5);
			$pdf->Cell(0,0,"Pago en efectivo: $".number_format($orden[0]->pago_efectivo,2,",","."),0,0,'L');
		}
		if ($orden[0]->pago_retenciones != "") 
		{
			$pdf->Ln(5);
			$pdf->Cell(0,0,"Retenciones: $".number_format($orden[0]->pago_retenciones,2,",","."),0,0,'L');
		}
		if ($orden[0]->pago_transferencia != "") 
		{
			$pdf->Ln(5);
			$pdf->Cell(0,0,"Transferencia Nro ".$orden[0]->nro_transferencia.": $"
						.number_format($orden[0]->pago_transferencia,2,",","."),0,0,'L');
		}
		if ($orden[0]->pago_nota != "") 
		{
			$pdf->Ln(5);
			$pdf->Cell(0,0,"Nota: $".number_format($orden[0]->pago_nota,2,",","."),0,0,'L');
		}
				
		$pdf->Ln(5);
		$pdf->Cell(0,0,"Total: $".number_format($orden[0]->total,2,",","."),0,0,'L');
		$pdf->Ln(20);
		$pdf->Cell(0,0, "______________              _________________               ____________________                      ___________________                      " ,0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0, "Confecciono                                   Autorizo                                  Contabilizo                                      Recibi Conforme                      " ,0,0,'R');$pdf->Ln();
						
		$pdf->AutoPrint();
    	$pdf->Output('ordenes.pdf' , 'I' );
	}

	//imprime los pagos
	public function pay($id_pago = 0)
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = utf8_decode("Recibo de pago");
		
		$this->load->model("Pagos_mdl");
		$pagos = $this->Pagos_mdl->consulta("id_pago='".$id_pago."'");
		$this->load->model("Detalle_pagos_mdl");
		$detalles = $this->Detalle_pagos_mdl->consulta("id_pago='".$id_pago."'");
		$empresa = $this->Empresas_mdl->consulta("id='".$pagos[0]->id_empresa."'");
		$facturacion = $this->Empresas_mdl->consulta("id='".$pagos[0]->facturacion."'");
		
		$pdf->AddPage('P','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',18);
		$pdf->Cell(0,0,strtoupper($facturacion[0]->razonSocial),0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(0,0,$title,0,0,'C');
		$pdf->Ln(10); //cabecera
		
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(0,0,"   Fecha: ".invierte_fecha($pagos[0]->fecha).
						"                   ".utf8_decode("Nº")." Comprobante de Pago: ".$id_pago,0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,"Cliente:",0,0,'L');
		$pdf->Ln(5);
		$pdf->Cell(0,0,$empresa[0]->razonSocial,0,0,'L');
		$pdf->Ln(10);
		
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(40,5,'Fecha',1,0,'C',1);
		$pdf->Cell(120,5,'Fact/Doc',1,0,'C',1);
		$pdf->Cell(40,5, "Importe",1,0,'C',1);
				
		$pdf->Ln();
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
		
		//comprobantes
		$this->load->model("Pagos_comprobantes_mdl");
		$comprob = $this->Pagos_comprobantes_mdl->consulta("id_pago='".$id_pago."'");
		$this->load->model("Comprobantes_mdl");
		foreach ($comprob as $row ) 
		{			
			$fact = $this->Comprobantes_mdl->consulta("id_comprobante='".$row->id_comprobante."'"); //print_r($fact[0]->total);
			$pdf->Cell(40,4,invierte_fecha($fact[0]->fecha),1,0,'C',1);
			$pdf->Cell(120,4,utf8_decode($fact[0]->tipo_comprobante)."  ".$fact[0]->nro,1,0,'C',1);
			$pdf->Cell(40,4,number_format($fact[0]->total,2,",","."),1,0,'R',1);			
			$pdf->Ln();	
			//$total += $row->total;	
		}
		
		$pdf->Ln(20);
		
		$total=0;
		
		//$pdf->Cell(0,0, "Apliquese A" ,0,0,'L'); $pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(20,5,'Id',1,0,'C',1);
		$pdf->Cell(60,5,'Tipo',1,0,'C',1);
		$pdf->Cell(80,5, "Detalle",1,0,'C',1);		
		$pdf->Cell(40,5, "Cantidad",1,0,'C',1);
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
		
		foreach ($detalles as $row) 
		{
			$pdf->Cell(20,4,$row->id_detalle,1,0,'C',1);
			$pdf->Cell(60,4,$row->tipo,1,0,'C',1);
			$pdf->Cell(80,4,$row->detalle,1,0,'C',1);
			$pdf->Cell(40,4,number_format($row->cantidad,2,",","."),1,0,'R',1);			
			$pdf->Ln();	
			$total += $row->cantidad;
		}
		
		$pdf->Ln();
		$pdf->Cell(0,0,"Total: $".number_format($total,2,",","."),0,0,'L');
		$pdf->Ln(20);
		$pdf->Cell(0,0, "______________              _________________               ____________________                      ___________________                      " ,0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0, "Confecciono                                   Autorizo                                  Contabilizo                                      Recibi Conforme                      " ,0,0,'R');$pdf->Ln();
						
		$pdf->AutoPrint();
    	$pdf->Output('pagos.pdf' , 'I' );
	}

	//imprime los comprobantes  de las diferentes empresas
	public function print_receipts()
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = utf8_decode("Resumen de Cuenta");
		
		//$this->load->model("Comprobantes_mdl");
		//$this->Comprobantes_mdl->consulta_views("");
		
		
		$pdf->AddPage('P','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		if (isset($_SESSION["empresa_dest"])) 
		{
			$pdf->SetFont('Arial','B',18);
			$pdf->Cell(0,0,$_SESSION["empresa_dest"],0,0,'C');
			$pdf->Ln(5);
		}
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,$title,0,0,'C');
		$pdf->Ln(10); //cabecera
		/*if (isset($_SESSION["razonSocial"])) 
		{
			$pdf->SetFont('Arial','B',16);
			$pdf->Cell(0,0,$_SESSION["razonSocial"],0,0,'L');
		}*/
		$i = 0;
				if (count($_SESSION["subtitle"])>1) 
				{//print_r($_SESSION["subtitle"]);					
					foreach ($_SESSION["subtitle"] as $sub)
					{
						if ($i ==0) 
						{
							$pdf->SetFont('Arial','B',12);
							$pdf->Cell(0,0,$sub,0,0,'L');
						} else {
							$pdf->SetFont('Arial','',14);
							$pdf->Cell(0,0,"       ".$sub,0,0,'L');
						}
						$pdf->Ln(7);					
						$i++;
					}
				}
		
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Razon Social',1,0,'C',1);
		$pdf->Cell(30,5,'Tipo',1,0,'C',1);
		$pdf->Cell(30,5, "Nro",1,0,'C',1);		
		$pdf->Cell(18,5, "Pago",1,0,'C',1);
		$pdf->Cell(18,5, "Fecha",1,0,'C',1);
		$pdf->Cell(23,5, "Total",1,0,'C',1);
		$pdf->Cell(23,5, "Saldo",1,0,'C',1);
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
			
		
		$total =0; $total_pagado = 0; $total_pendiente = 0; $total_parcial=0; $total_n_credito=0;
		foreach ($_SESSION["comprobantes_list"] as $row)
		{
			$row->total = round($row->total,2);
			$pdf->Cell(60,5, utf8_decode($row->razonSocial),1,0,'C',1);
			$pdf->Cell(30,5,utf8_decode($row->tipo_comprobante),1,0,'C',1);
			$pdf->Cell(30,5,$row->nro,1,0,'C',1);
			$pdf->Cell(18,5,$row->pago,1,0,'C',1);
			$pdf->Cell(18,5,invierte_fecha($row->fecha),1,0,'C',1);
			$pdf->Cell(23,5,number_format($row->total,2,",","."),1,0,'C',1);
			$pdf->Cell(23,5,number_format($row->saldo,2,",","."),1,0,'C',1);
			$pdf->Ln();
			
			if ($row->tipo_comprobante=="Notas de crédito") 
			{
				$total_n_credito += $row->total;
			} else {
				if ($row->pago == "completado" ) 
				{
					$total_pagado += $row->total;
				} 
				 if($row->pago == "pendiente")
				{
					$total_pendiente += $row->saldo;
				}
				 if($row->pago == "parcial")
				{
					$total_parcial += $row->saldo;
				}
				$total =$total + $row->total;
			}			
		}
		$total = $total - $total_n_credito;
		$pdf->Ln();
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,'Total Pago completado :  $'.number_format($total_pagado,2,",","."),0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0,'Total Pago parcial :  $'.number_format($total_parcial,2,",","."),0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0,'Total Pago Pendiente :  $'.number_format($total_pendiente,2,",","."),0,0,'R');$pdf->Ln(5);
		if ($total_n_credito != 0) 
		{
			$pdf->Cell(0,0,'Total Pago Notas de credito :  $'.number_format($total_n_credito,2,",","."),0,0,'R');$pdf->Ln(5);
		}
		$pdf->Cell(0,0,'Total $:  '.number_format($total,2,",","."),0,0,'R'); 
		
		$pdf->AutoPrint();
    	$pdf->Output( 'comprobantes.pdf' , 'I' );
	}

	//imprime los comprobantes  realizando un resumen de cuenta
	public function account_status()
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = utf8_decode("Resumen de Cuenta");
		
		//$this->load->model("Comprobantes_mdl");
		//$this->Comprobantes_mdl->consulta_views("");
		
		
		$pdf->AddPage('P','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		if (isset($_SESSION["empresa_dest"])) 
		{
			$pdf->SetFont('Arial','B',18);
			$pdf->Cell(0,0,$_SESSION["empresa_dest"],0,0,'C');
			$pdf->Ln(5);
		}
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,$title,0,0,'C');
		$pdf->Ln(10); //cabecera
		/*if (isset($_SESSION["razonSocial"])) 
		{
			$pdf->SetFont('Arial','B',16);
			$pdf->Cell(0,0,$_SESSION["razonSocial"],0,0,'L');
		}*/
		$i = 0;
				if (count($_SESSION["subtitle"])>1) 
				{//print_r($_SESSION["subtitle"]);					
					foreach ($_SESSION["subtitle"] as $sub)
					{
						if ($i ==0) 
						{
							$pdf->SetFont('Arial','B',12);
							$pdf->Cell(0,0,$sub,0,0,'L');
						} else {
							$pdf->SetFont('Arial','',14);
							$pdf->Cell(0,0,"       ".$sub,0,0,'L');
						}
						$pdf->Ln(7);					
						$i++;
					}
				}
		
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Razon Social',1,0,'C',1);
		$pdf->Cell(35,5,'Tipo',1,0,'C',1);				
		$pdf->Cell(20,5, "Pago",1,0,'C',1);		
		$pdf->Cell(25,5, "Total",1,0,'C',1);
		$pdf->Cell(25,5, "Saldo",1,0,'C',1);
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
			
		
		$total =0; $total_pagado = 0; $total_pendiente = 0; $total_parcial=0;
		foreach ($_SESSION["comprobantes_list"] as $row)
		{
			$row->total = round($row->total,2);
			$pdf->Cell(60,5, utf8_decode($row->razonSocial),1,0,'C',1);
			$pdf->Cell(35,5,utf8_decode($row->tipo_comprobante),1,0,'C',1);		
			$pdf->Cell(20,5,$row->pago,1,0,'C',1);			
			$pdf->Cell(25,5,number_format($row->total,2,",","."),1,0,'C',1);
			$pdf->Cell(25,5,number_format($row->saldo,2,",","."),1,0,'C',1);
			$pdf->Ln();
			if ($row->pago == "completado" ) 
			{
				$total_pagado += $row->total;
			} 
			 if($row->pago == "pendiente")
			{
				$total_pendiente += $row->total;
			}
			 if($row->pago == "parcial")
			{
				$total_parcial += $row->total;
			}
			$total = $total + $row->total;
		}
		$pdf->Ln();
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(0,0,'Total Pago completado :  $'.number_format($total_pagado,2,",","."),0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0,'Total Pago parcial :  $'.number_format($total_parcial,2,",","."),0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0,'Total Pago Pendiente :  $'.number_format($total_pendiente,2,",","."),0,0,'R');$pdf->Ln(5);
		$pdf->Cell(0,0,'Total $:  '.number_format($total,2,",","."),0,0,'R'); 
		
		$pdf->AutoPrint();
    	$pdf->Output( 'comprobantes.pdf' , 'I' );
	}

	public function print_debts()
	{
		require_once APPPATH.'third_party/fpdf/pdf_js.php';
		$pdf = new PDF_JavaScript();
		$title = utf8_decode("Deudas");
		
		$pdf->AddPage('P','Legal',0);
		$pdf->Ln(5);
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(0,0,$title,0,0,'C');
		$pdf->Ln(10); //cabecera
		
		$pdf->SetFillColor(232,232,232);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(60,5,'Razon Social',1,0,'C',1);
		$pdf->Cell(35,5,'Tipo',1,0,'C',1);
		$pdf->Cell(40,5, "Nro",1,0,'C',1);		
		$pdf->Cell(20,5, "Pago",1,0,'C',1);
		$pdf->Cell(20,5, "Fecha",1,0,'C',1);
		$pdf->Cell(25,5, "Total",1,0,'C',1);
		$pdf->Ln();
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',8);
		
		$pdf->AutoPrint();
    	$pdf->Output( 'comprobantes.pdf' , 'I' );
	}
}
?>