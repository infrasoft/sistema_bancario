<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase de registro de cuentas
 */
 defined('BASEPATH') OR exit('Acceso no permitido');
 
class Cuentas extends CI_Controller 
{
	
	function __construct() 
	{
		parent::__construct();
		$this->load->model("Cuentas_mdl");
	}
	
	//genera la lista de cuentas
	public function index()
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$buscar = $this->input->post("buscar");
		$campo = $this->input->post("lista_venta");			
		$data2["mensaje"]="";
		
		if (!is_null($buscar)) 
		{
			$data2["lista"] = $this->Cuentas_mdl->consultavista($campo." LIKE \"%".$buscar."%\" ORDER BY nro_cuenta DESC limit 0,100");
			if (is_array($data2["lista"])) 
			{
				$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
			} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
			}	
		} else 
		{
			$data2["lista"] = $this->Cuentas_mdl->consultavista("1 ORDER BY nro_cuenta DESC limit 0,100");
		}		
		
		$this->load->view("cuentas/list",$data2);
		
		$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	//registra una nueva cuenta
	public function new_account()
	{
		if (is_logged_in()) 
		{
		$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
		$this->load->view("header",$data);
		$this->load->view("menu");
		$this->load->view("cuentas/submenu");
		$data["mensaje"] = "";	$mje = "";		
			
		$vector = $this->input->post(array("banco","id_banco","titular","id_empresa","nro_cuenta","tipo","cbu","sucursal","saldo",
											"ultimo_cheque","observaciones","estado"));
		//print_r($vector);									
		if (($vector["nro_cuenta"] != "" ) AND is_array($vector)) 
		{
			$aux = $this->Empresas_mdl->consulta("razonSocial='".$vector["banco"]."'","id"); 
			
			if ($aux != NULL) 
			{
				$vector["id_banco"] = $aux[0]->id;								
			} else {
				$vector["id_banco"] = $this->Empresas_mdl->alta(array("razonSocial"=>$vector["banco"],
																			"clase"=>"banco",
																			"tipo"=>"Responsable Inscripto"));
				if ($vector["id_banco"] == 0) 
				{
					redirect(base_url().'/index/cuentas/new_account/', 'location');
				}
				$mje = "Se registro entidad Banco, por favor completar los datos <br>";
			}
			 unset($vector["banco"]);
			
			$aux = $this->Empresas_mdl->consulta("razonSocial='".$vector["titular"]."'","id");
			
			if ($aux != NULL) 
			{
				$vector["id_empresa"] = $aux[0]->id;	
			} else {
				$vector["id_empresa"] = $this->Empresas_mdl->alta(array("razonSocial"=>$vector["titular"],
																			"clase"=>"Otros",
																			"tipo"=>"otros"));
				if ($vector["id_empresa"] == 0) 
				{
					redirect(base_url().'/index/cuentas/new_account/', 'location');
				}
				$mje .= "Se registro entidad del titular, por favor completar los datos <br>";	
			}
			unset($vector["titular"]);
			
			$vector["nro_cuenta"] = str_replace("/", "-", $vector["nro_cuenta"]) ;
			$vector["nro_cuenta"] = str_replace(" ", "", $vector["nro_cuenta"]) ;
			$vector["cbu"] = str_replace("/", "-", $vector["cbu"]);
			
			if ($this->Cuentas_mdl->alta($vector)) 
			{
				$data["mensaje"] = mensajes($mje."Datos Incorporados Correctamente");
			} else 
			{
				$data["mensaje"] = mensajes($mje."Datos No ingresados. Por favor ingreselo nuevamente");
			}			
		}
		
		
		$this->load->view("cuentas/new",$data);
		$this->load->library('ajax');		
		$data["footer"] = $this->ajax->bank_list().$this->ajax->company_list("","titular","lista2");
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	public function update_account($id = 0, $cuenta=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
		$this->load->view("header",$data);
		$this->load->view("menu");
		$this->load->view("cuentas/submenu");
		$data["mensaje"] = "";		
		$vector = $this->input->post(array("id_banco","id_empresa","nro_cuenta","tipo","cbu","sucursal","saldo",
											"ultimo_cheque","observaciones","estado"));
		if (($vector["id_banco"] != "" ) AND ($vector["nro_cuenta"] != "" ) AND is_array($vector)) 
		{
			$vector["nro_cuenta"] = str_replace("/", "-", $vector["nro_cuenta"]) ;
			$vector["nro_cuenta"] = str_replace(" ", "", $vector["nro_cuenta"]) ;
			$vector["cbu"] = str_replace("/", "-", $vector["cbu"]);
			
			if ($this->Cuentas_mdl->modifica($vector,"id_banco='".$id."' AND nro_cuenta='".$cuenta."'")) 
			{
				$data["mensaje"] = mensajes("Datos Modificados Correctamente");
			} else 
			{
				$data["mensaje"] = mensajes("Datos No modificados. Por favor intentelo nuevamente");
			}			
		}
		$data["vector"] = $this->Cuentas_mdl->consultavista("id_banco='".$id."' AND nro_cuenta='".$cuenta."'");
		
		$this->load->view("cuentas/update",$data);
		
		$this->load->library('ajax');
		$data["footer"] = $this->ajax->bank_list().$this->ajax->company_list("1","titular","lista2");
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}

	//realiza una busqueda avanzada por criterio
	public function avanced_seach($id = "")
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");		
		
		$buscar = $this->input->post("buscar");
		$campo = $this->input->post("lista_venta");			
		$data2["mensaje"]="";			
		
		
		if (!is_null($buscar)) 
		{
			$data2["lista"] = $this->Cuentas_mdl->consultavista($campo." LIKE \"%".$buscar."%\" ORDER BY nro_cuenta DESC limit 0,100");
			if (is_array($data2["lista"])) 
			{
				$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
			} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
			}	
		} else 
		{
			if ($id != "") 
			{
				$data2["lista"] = $this->Cuentas_mdl->consultavista("id_banco='".$id."' ORDER BY nro_cuenta DESC  limit 0,100");
			} else {
				$data2["lista"] = $this->Cuentas_mdl->consultavista(" ORDER BY nro_cuenta DESC limit 0,100");
			}
			
			
		}		
		
		$this->load->view("cuentas/list",$data2);
		
		$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	//this method is for eliminate account bank
	public function delete_account($id_banco=0, $cuenta="")
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");	
		
		$data["mensaje"]="";
		if ($this->input->post("delete") == "delete") 
		{
			if ($this->Cuentas_mdl->delete(array("id_banco" => $id_banco, "nro_cuenta" => $cuenta))) 
			{
				if ($this->Cuentas_mdl->consulta(array("id_banco" => $id_banco, "nro_cuenta" => $cuenta))) 
				{
					if ($this->Cuentas_mdl->delete("id_banco='".$id_banco."' AND nro_cuenta='".$cuenta."'")) 
					{
						$data["mensaje"] = mensajes("Datos Eliminados correctamente");
					}
				} else {
					$data["mensaje"] = mensajes("Datos Eliminados correctamente");
				}
					
			} else {
				$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
			}
			
		}
		
		$this->load->view("cuentas/delete",$data);
		
		$data["footer"] = "";
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
}

?>