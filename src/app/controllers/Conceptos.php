<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 *  Opera con los conceptos
 */
class Conceptos extends CI_Controller 
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model("Concepto_mdl");
	}
	
	//muestra la lista de conceptos
	public function index()
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$buscar = $this->input->post("buscar");
		$campo = $this->input->post("lista_venta");			
		$data["mensaje"]="";
		
		if (!is_null($buscar)) 
		{
			$data["lista"] = $this->Concepto_mdl->consulta($campo." LIKE \"%".$buscar."%\" ORDER BY id_concepto DESC limit 0,100");
			if (is_array($data["lista"])) 
			{
				$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
			} else {
					$data["mensaje"] =mensajes("No se encontraron resultados");	
			}	
		} else 
		{
			$data["lista"] = $this->Concepto_mdl->consulta("1 ORDER BY id_concepto DESC limit 0,100");
		}
		
		$this->load->view("conceptos/list",$data);
		
		$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";	
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	public function new_concept()
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$data["mensaje"]="";
		$vector = $this->input->post(array("concepto","codigo","descripcion"));
		
		if (($vector["concepto"] != "" ) AND is_array($vector)) 
		{
			if ($this->Concepto_mdl->alta($vector)) 
			{
				$data["mensaje"] = mensajes("Datos Incorporados Correctamente");	
			} else {
				$data["mensaje"] = mensajes("Datos No ingresados. Por favor ingreselo nuevamente");
			}				
		}
		
		$this->load->view("conceptos/new",$data);
		
		$data["footer"] = "";	
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	public function update_concept($id_concepto=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$data["mensaje"]="";
		$vector = $this->input->post(array("concepto","codigo","descripcion"));
		
		if (($vector["concepto"] != "" ) AND is_array($vector)) 
		{
			if ($this->Concepto_mdl->modifica($vector, $id_concepto)) 
			{
				$data["mensaje"] = mensajes("Datos Modificados Correctamente");	
			} else {
				$data["mensaje"] = mensajes("Datos No modificados. Por favor intentelo nuevamente");
			}				
		}
		$data["vector"] = $this->Concepto_mdl->consulta("id_concepto='".$id_concepto."'");
		$this->load->view("conceptos/update",$data);
		
		$data["footer"] = "";	
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	public function delete_concept($id_concepto=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$data["mensaje"]="";	
		if ($this->input->post("delete") == "delete") 
		{
			if ($this->Concepto_mdl->delete(array("id_concepto"=>$id_concepto))) 
			{
				$data["mensaje"] = mensajes("Datos Eliminados correctamente");
			} else {
				$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
			}			
		}
		$this->load->view("conceptos/delete",$data);
			
		$data["footer"] = "";	
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
}

?>