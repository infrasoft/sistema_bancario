<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase de movimientos bancarios 
 */
class Movimientos extends CI_Controller 
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model("Movimientos_mdl");
		$this->load->model("Concepto_mdl");
		$this->load->model("Cuentas_mdl");
	}
	
	
	public function index()
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$data2["titulo"] = "Lista de movimientos bancarios";
			$data2["mensaje"] = "";
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");	
			
			if (!is_null($buscar)) 
			{
				$data2["lista"] = $this->Movimientos_mdl->consulta_views($campo." LIKE \"%".$buscar."%\" ORDER BY id DESC limit 0,100");
				
				$_SESSION["cond"] = $campo." LIKE \"%".$buscar."%\" ORDER BY id DESC";
				$_SESSION["subtitle"][0] = "Criterio de busqueda:";
				$_SESSION["subtitle"][1] = $campo.": ".$buscar;
				
				if (is_array($data2["lista"])) 
				{
					$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
				}
				
			} else {
				$data2["lista"] = $this->Movimientos_mdl->consulta_views("1 ORDER BY id DESC limit 0,100");
				$_SESSION["cond"] = "1 ORDER BY id DESC"; 
				$_SESSION["subtitle"] = "";
			}			
			
			$data2["dir"] = "imprimir/print_movements/";
			$data2["dir2"] = "csv/list_movements/";
			$data2["dir3"] = "data_enter/movements/";
			$this->load->view("movimientos/export_data",$data2);
			$this->load->view("movimientos/list",$data2);
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}		
		
	}

	public function avanced_seach($banco=0, $cuenta=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$datos_cuenta =$this->Cuentas_mdl->consultavista("id_banco='".$banco."' AND nro_cuenta='".$cuenta."'",
												"razonSocial,Banco");
			//print_r($datos_cuenta[0]);
			$data2["titulo"] = "Banco: ".$datos_cuenta[0]->Banco." - Titular: ".$datos_cuenta[0]->razonSocial;
			$data2["mensaje"] = "";
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");	
			
			if (!is_null($buscar)) 
			{
				$data2["lista"] = $this->Movimientos_mdl->consulta_views("id_banco='".$banco."' AND cuenta='".$cuenta."' AND ".
															$campo." LIKE \"%".$buscar."%\" ORDER BY id DESC limit 0,100");
				
				$_SESSION["cond"] = $campo." LIKE \"%".$buscar."%\" ORDER BY id DESC";
				$_SESSION["subtitle"][0] = "Criterio de busqueda:";
				$_SESSION["subtitle"][1] = $campo.": ".$buscar;
				
				if (is_array($data2["lista"])) 
				{
					$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
				}
				
			} else {
				
				$data2["lista"] = $this->Movimientos_mdl->consulta_views("(id_banco='".$banco."' AND cuenta='".$cuenta."')  " 
																			." ORDER BY id DESC limit 0,100");
				$_SESSION["cond"] = "(id_banco=".$banco." AND cuenta=".$cuenta.")  ORDER BY id DESC"; 
				$_SESSION["subtitle"][0] = "Banco: ".$datos_cuenta[0]->Banco." - Titular: ".$datos_cuenta[0]->razonSocial;
				$_SESSION["subtitle"][1] = "Criterio de busqueda:";
				$_SESSION["subtitle"][2] = $campo.": ".$buscar;
			}			
			
			$data2["dir"] = "imprimir/print_movements/";
			$data2["dir2"] = "csv/list_movements/";
			$this->load->view("movimientos/export_data",$data2);
			$this->load->view("movimientos/advanced_search.php",$data2);
				
			
		$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";	
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}	
	}
	
	// se genera un nuevo movimiento a traves de la cuenta
	public function movements_new_list($banco=0, $cuenta=0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$this->load->view("movimientos/submenu");
			
			$data["titulo"] = "Registrar movimiento bancario";
			$data["mensaje"] = "";
			
			$vector = $this->input->post(array("id_banco","cuenta","movimiento","concepto","valor","fecha","tipo","observaciones"));
			if (($vector["id_banco"] != "" ) AND is_array($vector)) // mas condiciones y verificar
			{
				
				$concepto =	substr($vector["concepto"] ,strpos($vector["concepto"], "|")+1 );
				if ((!is_numeric($concepto)) OR (strpos($vector["concepto"], "|")==0)) 
				{
					$concepto = 0;
				}
				$aux_concepto = $this->Concepto_mdl->consulta("id_concepto='".$concepto."'");
								
				if ($aux_concepto != array()) 
				{
					$vector["concepto"] = $concepto;
				}		
				else {
					$vector["concepto"] = $this->Concepto_mdl->alta(array("concepto"=>$vector["concepto"]));
				}
				
				if ($this->Movimientos_mdl->alta($vector)) 
				{
					$saldo = $this->input->post("saldo");
					if ($vector["tipo"] == "debito") 
					{
						$sum = $saldo - $vector["valor"];
					} else {
						$sum = $saldo + $vector["valor"];	
					}
					$this->Cuentas_mdl->modifica(array("saldo"=>$sum), array("id_banco"=>$banco, "nro_cuenta"=>$cuenta));
					$data["mensaje"] = mensajes("Datos Incorporados Correctamente");
				} else 
				{
					$data["mensaje"] = mensajes("Datos No ingresados. Por favor ingreselo nuevamente");
				}
			} 
			
			$data["cuenta"] = $this->Cuentas_mdl->consultavista("id_banco='".$banco
														."' AND nro_cuenta='".$cuenta."'");
														
			$this->load->view("movimientos/new_complete",$data);
			
			$this->load->library('ajax');
			$data["footer"] = $this->ajax->conceptos();
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');
		}		
	}
	
	public function movements_new()
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$data["titulo"] = "Registrar movimiento bancario";
			$data["mensaje"] = "";
			
			$vector = $this->input->post(array("id_banco","cuenta","movimiento","concepto","valor","fecha","tipo","observaciones"));
			if (($vector["id_banco"] != "" ) AND is_array($vector)) 
			{				
				$id_banco = substr($vector["id_banco"] ,strpos($vector["id_banco"], "|") +1,strpos($vector["id_banco"], "#") +1);
			
				$vector["cuenta"] = (int) substr($id_banco ,0, strpos($id_banco, "#") );				
				$vector["id_banco"] = (int) substr($id_banco , strpos($id_banco, "#") +1);
				
				$concepto =	substr($vector["concepto"] ,strpos($vector["concepto"], "|")+1 );
				
				$this->load->model("Concepto_mdl");
				$aux_concepto = $this->Concepto_mdl->consulta("id_concepto='".$concepto."'");				
				if ($aux_concepto != array()) 
				{
					$vector["concepto"] = $concepto;
				}		
				else {
					$vector["concepto"] = $this->Concepto_mdl->alta(array("concepto"=>$vector["concepto"]));
				}  
				
				if ($this->Movimientos_mdl->alta($vector)) 
				{
					$saldo = $this->Cuentas_mdl->consulta(array("id_banco"=>$vector["id_banco"],"nro_cuenta"=>$vector["cuenta"]),"saldo" ); 													 
					if ($vector["tipo"] == "egreso") 
					{
						$sum = $saldo[0]->saldo - $vector["valor"];
					} else {
						$sum = $saldo[0]->saldo + $vector["valor"];	
					}
					$this->Cuentas_mdl->modifica(array("saldo"=>$sum), array("id_banco"=>$vector["id_banco"],"nro_cuenta"=>$vector["cuenta"]));			
					
					$data["mensaje"] = mensajes("Datos Incorporados Correctamente");
				} else 
				{
					$data["mensaje"] = mensajes("Datos No ingresados. Por favor ingreselo nuevamente");
				}
			}
			$this->load->view("movimientos/new",$data);
			
			$this->load->library('ajax');
			$data["footer"] = $this->ajax->account_list("id_banco").$this->ajax->conceptos();
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');
		}
		
	}

	private function actualizar_saldo($banco=0, $cuenta=0)
	{
		$consulta = $this->Movimientos_mdl->consulta(array("id_banco"=>$banco,"cuenta"=>$cuenta),"tipo,valor");
		$suma = 0;
		foreach ($consulta as $row) 
		{
			if ($row->tipo == "ingreso") 
			{
				$suma += $row->valor;
		    } else {
				$suma -= $row->valor;
			}				
		}
		return $this->Cuentas_mdl->modifica(array("saldo"=>$suma), array("id_banco"=>$banco,"nro_cuenta"=>$cuenta));
	}
	
	public function movements_update($id = "")
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$this->load->view("movimientos/submenu");
		
			$data["titulo"] = "Modificar movimiento bancario";
			$data["mensaje"] = "";
			
			$vector = $this->input->post(array("id","movimiento","concepto","valor","fecha","tipo","observaciones"));
		
			if (($vector["id"] != "" ) AND is_array($vector))
			{
				$concepto =	substr($vector["concepto"] ,strpos($vector["concepto"], "|")+1 );
				
				$this->load->model("Concepto_mdl");
				$aux_concepto = $this->Concepto_mdl->consulta("id_concepto='".$concepto."'");				
				if ($aux_concepto != array()) 
				{
					$vector["concepto"] = $concepto;
				}		
				else {
					$vector["concepto"] = $this->Concepto_mdl->alta(array("concepto"=>$vector["concepto"]));
				}
											
				if ($this->Movimientos_mdl->modifica($vector,"id='".$id."'")) 
				{
					$data["mensaje"] = mensajes("Datos Modificados Correctamente");
					
				} else 
				{
					$data["mensaje"] = mensajes("Datos No modificados. Por favor intentelo nuevamente");
				}
								
			}
			
			$data["vector"] = $this->Movimientos_mdl->consulta("id='".$id."'");
			$this->actualizar_saldo($data["vector"][0]->id_banco,$data["vector"][0]->cuenta);
			$consulta = "id_banco='".$data["vector"][0]->id_banco
														."' AND nro_cuenta='".$data["vector"][0]->cuenta."'";	
			$data["cuenta"] = $this->Cuentas_mdl->consultavista($consulta);				
			$data["concepto"] = $this->Concepto_mdl->consulta("id_concepto='".$data["vector"][0]->concepto."'");
			$this->load->view("movimientos/update",$data);
		
			$this->load->library('ajax');
			$data["footer"] = $this->ajax->conceptos();
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');
		}	
	}

	//realiza la busqueda avanzada de los movimientos
	
	public function movements_delete($id = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$this->load->view("movimientos/submenu");
			
			$data["mensaje"] = "";
			
			if ($this->input->post("delete") == "delete") 
			{
				if ($this->Movimientos_mdl->delete(array("id"=>$id))) 
				{
					$data["mensaje"] = mensajes("Datos Eliminados correctamente");
				} else {
					$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
				}			
			}
			$this->load->view("movimientos/delete",$data);
		
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');
		}
	}
	
	//busqueda avanzada por criterios
	public function avanced_seach_form($op = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$data["mensaje"] = "";
						
			$vector = $this->input->post(array("id_cuenta","fecha_desde","fecha_hasta","movimiento","concepto","valor_desde","valor_hasta",
												"fecha","tipo","observaciones","search"));
						
			if (($vector["search"] != "" ) AND is_array($vector)) 
			{
				$data["titulo"] = "Lista de movimientos bancarios";
				
				if ((!isset($_SESSION["cond"])) AND (!isset($_SESSION["subtitle"]))) 
				{
					$_SESSION["cond"] = "";
					$_SESSION["subtitle"] = "Condiciones";
				}
				$cond = ""; $subtitle[] = "Condiciones";
				
				if ($vector["id_cuenta"] != null) 
				{
					if ($cond != "") 
						{
							$cond .= " AND ";
						}	
						$banco = substr($vector["id_cuenta"], 0 ,strpos($vector["id_cuenta"], "#"));
						$cuenta = substr($vector["id_cuenta"], strpos($vector["id_cuenta"], "#")+1);					
						$cond .= "id_banco='".$banco."' AND cuenta='".$cuenta."'"; 
						$banco_nombre = $this->Empresas_mdl->consulta("id='".$banco."'","razonSocial");
						$subtitle[] = "Banco: ".$banco_nombre[0]->razonSocial." Cuenta: ".$cuenta;
				}
				
				if ($vector["fecha_desde"] != null AND $vector["fecha_hasta"] != null) 
				{
					if ($cond != "") 
						{
							$cond .= " AND ";
						}						
						$cond .= "(fecha BETWEEN '".$vector["fecha_desde"].
								"' AND '".$vector["fecha_hasta"]."')"; 
						$subtitle[] = " Fecha de  Desde: ".invierte_fecha($vector["fecha_desde"]).
												" Hasta :".invierte_fecha($vector["fecha_hasta"]);
				}
				
				if ($vector["fecha_desde"] != null AND $vector["fecha_hasta"] == null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "(fecha >=".$vector["fecha_desde"].")";
					$subtitle[] = "Fechas mayores que ".$vector["fecha_desde"];
				}
				
				if ($vector["fecha_desde"] == null AND $vector["fecha_hasta"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "(fecha <=".$vector["fecha_hasta"].")";
					$subtitle[] = "Fechas menores que ".$vector["fecha_hasta"];
				}
				
				if ($vector["concepto"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$concepto =	substr($vector["concepto"] ,strpos($vector["concepto"], "|")+1 );
					$cond .= "concepto ='".$concepto."'";
					$subtitle[] = "Concepto : ".$vector["concepto"];
				}
				
				if ($vector["movimiento"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "movimiento ='".$vector["movimiento"]."'";
					$subtitle[] = "Movimiento: ".$vector["movimiento"];
				}
				
				if ($vector["valor_desde"] != null AND $vector["valor_hasta"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					//$vector["valor_desde"] = " ".$vector["valor_desde"];
					$cond .= "(valor BETWEEN '".$vector["valor_desde"]."' AND '".$vector["valor_hasta"]."')";
					$subtitle[] = "Valores entre : $".$vector["valor_desde"]." y  $".$vector["valor_hasta"];
				}
				
				if ($vector["valor_desde"] != null AND $vector["valor_hasta"] == null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "valor >=".$vector["valor_desde"];
					$subtitle[] = "Valores mayores que ".$vector["valor_desde"];
				}
				
				if ($vector["valor_desde"] == null AND $vector["valor_hasta"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "valor <= ".$vector["valor_hasta"];
					$subtitle[] = "Valores menores que ".$vector["valor_hasta"];
				}
				
				if ($vector["tipo"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "tipo='".$vector["tipo"]."'";
					$subtitle[] = "tipo = ".$vector["tipo"];
				}
				
				if ($vector["observaciones"] != null) 
				{
					if ($cond != "") 
					{
						$cond .= " AND ";
					}
					$cond .= "observaciones='".$vector["observaciones"]."'";
					$subtitle[] = "observaciones :".$vector["observaciones"];
				}
				
				$_SESSION["subtitle"] = $subtitle;	$_SESSION["cond"] = $cond;			
				
				$data["lista"] = $this->Movimientos_mdl->consulta_views($cond);
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] =mensajes("No se encontraron resultados");	
				}			
				$data2["dir"] = "imprimir/print_movements/";
				$data2["dir2"] = "csv/list_movements/";
				$this->load->view("movimientos/export_data",$data2);
				$this->load->view("movimientos/list",$data);
			
				$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
				
			} else {
				$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 ORDER BY razonSocial",
													"razonSocial,id_banco,Banco,nro_cuenta");
				$this->load->view("movimientos/form_advanced_search",$data);
				$this->load->library('ajax');
				$data["footer"] = $this->ajax->conceptos();
				$this->load->view("footer",$data);
			}				
		} else {
			redirect('/seguridad/logout/', 'location');
		}	
	}	
}

?>