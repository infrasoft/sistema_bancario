<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');


class Pagos extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model("Pagos_mdl");
		$this->load->model("Pagos_comprobantes_mdl");
		$this->load->model("Detalle_pagos_mdl");
		$this->load->model("Cuentas_mdl");
		$this->load->model('Comprobantes_mdl');		
	}
	
	//muestra la lista de pagos
	public function index()
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");			
			$data["mensaje"]="";
		
			if (!is_null($buscar)) 
			{
				$data["lista"] = $this->Pagos_mdl->consulta_views($campo." LIKE \"%".$buscar."%\" ORDER BY id_pago DESC limit 0,100");
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] =mensajes("No se encontraron resultados");	
				}	
			} else 
			{
				$data["lista"] = $this->Pagos_mdl->consulta_views("1 ORDER BY id_pago DESC limit 0,1000");
			}
			
			$this->load->view("pago/list",$data);			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}	
	}
	
	//registra un nuevo pago
	public function new_pay($id_empresa = 0, $op=0)
	{
		if (is_logged_in()) 
		{						
			 //registro de pago
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$data["mensaje"] = "";
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");
			
			if ($id_empresa == 0) 
			{			
				if (!is_null($buscar)) 
				{
					$data["lista"] = $this->Empresas_mdl->consulta("(".$campo." LIKE \"%".$buscar.
											"%\") AND (not(clase='proveedor' OR clase='banco')) ORDER BY id DESC limit 0,100");
					
					$_SESSION["cond"] = "(".$campo." LIKE \"%".$buscar."%\") AND (not(clase='proveedor' OR clase='banco')) ORDER BY id DESC";
					$_SESSION["subtitle"][0] = "Criterio de busqueda:";
					$_SESSION["subtitle"][1] = $campo.": ".$buscar;
				
					if (is_array($data["lista"])) 
					{
						$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					} else {
						$data2["mensaje"] =mensajes("No se encontraron resultados");	
					}
				} else {
					$data["lista"] = $this->Empresas_mdl->consulta("not(clase='proveedor' OR clase='banco') ORDER BY id DESC limit 0,100");
					$_SESSION["cond"] = "not(clase='proveedor' OR clase='banco') ORDER BY id DESC"; 
					$_SESSION["subtitle"] = "";
				}
				$this->load->view("pago/list_company",$data);
				$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";				
								
			} else {
					
				switch ($op) 
				{
					case '0': //seleccionar la facturacion
						$id_empresa_dest = $this->input->post("id_empresa_dest");					
						if (!isset($id_empresa_dest)) 
						{						
							$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																				"razonSocial,id_empresa");
							$this->load->view("deudas/billing_list",$data);
						}
						else {
							$_SESSION["id_empresa_dest"] = $id_empresa_dest;
							redirect('/pagos/new_pay/'.$id_empresa."/1/", 'location');
						}
						$data["footer"] = "";
					break;
					
					case "1":// Selecconar las Facturas
						$data2["mensaje"] = ""; 
						$lista = array();
						
						if ($this->input->post("confirm")=="Agregar") 
						{					
							for ($i=0; $i < 100; $i++) 
							{							
								if ($this->input->post("check".$i)=="on") 
								{
									$id_comprobante = $this->input->post("id_comprobante".$i);
									$comprobantes = $this->Comprobantes_mdl->consulta("id_comprobante='".$id_comprobante."'");
									
									if ($comprobantes) 
									{
										$lista[] = array("id_comprobante"=>$comprobantes[0]->id_comprobante,
														 "tipo_comprobante"=>$comprobantes[0]->tipo_comprobante,
														 "nro"=>$comprobantes[0]->nro,
														 "fecha"=>$comprobantes[0]->fecha,
														 "total"=>$comprobantes[0]->total,
														 "saldo"=>$comprobantes[0]->saldo);
									}									
																	
								}							
							}
							$_SESSION["lista"] = $lista;
							redirect('/pagos/new_pay/'.$id_empresa."/2/", 'location');
						}
						
						if (!is_null($buscar)) 
						{						
								$data2["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$id_empresa."') AND (".$campo
												." LIKE \"%".$buscar."%\") AND (id_empresa_dest='".$_SESSION["id_empresa_dest"].
										"' )AND (typo='cliente') AND (pago <>'completado') ORDER BY id_comprobante DESC limit 0,1000");
						
							if (is_array($data2["lista"])) 
							{
								$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
							} else {
								$data2["mensaje"] =mensajes("No se encontraron resultados");	
							}				
						} else {				
							$data2["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$id_empresa."') AND (id_empresa_dest='".
											$_SESSION["id_empresa_dest"]. 
											"') AND (typo='cliente') AND (pago<>'completado') ORDER BY id_comprobante DESC limit 0,100");	
						}
						$data2["title"] = "Registrar Pago en comprobantes"; 
						$data2["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																				"razonSocial,id_empresa");
						$data2["empresa"] = $this->Empresas_mdl->consulta("id='".$id_empresa."'");
						$this->load->view("orden_pago/voucher_list",$data2);
						
						$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
						
					break;
					
					case "2":	//formas de pago
				
				$data["empresa"] = $this->Empresas_mdl->consulta("id='".$id_empresa."'");
				$fecha = $this->input->post("fecha");
				$observaciones = $this->input->post("observaciones");
				$facturacion = $this->input->post("id_empresa_dest");
				
				$vector = array("id_empresa"=>$id_empresa,"fecha"=>$fecha,"observaciones"=>$observaciones,
								"facturacion"=>$facturacion);
				if(($vector["fecha"] != "" ) AND is_array($vector))
				{
					$id_pago = $this->Pagos_mdl->alta($vector);
					$total = 0;
					for ($i=0; $i < 10; $i++) 
					{
						$detalle = $this->input->post("detalle".$i);
						$cantidad = $this->input->post("cantidad".$i); 
						$tipo = $this->input->post("tipo".$i); 
						if ((($detalle != "") AND ($cantidad != "")) OR ($tipo = "efectivo" AND $cantidad != "")) 
						{
							$vector = array("id_pago"=>$id_pago,"id_detalle"=>$i,
											"tipo"=>$tipo,"detalle"=>$detalle,"cantidad"=>$cantidad);
							$total = $total + $cantidad;						
							$this->Detalle_pagos_mdl->alta($vector);												
						}
					}
					
					if ($this->Pagos_mdl->modifica(array("total"=>$total),$id_pago)) 
					{
						$data["mensaje"] .= mensajes("Datos Insertados correctamente");	
						//redirect('/pagos/new_pay/'.$id_empresa."/1/", 'location');
					} else {
						$data["mensaje"] .= mensajes("Falla en la insercion de datos");	
					}
				 	
					
					foreach ($_SESSION["lista"] as $row) 
					{
						
						if ($total>=$row["saldo"]) 
						{
							$vector = array("saldo"=>0, "pago"=>"completado");
							$total = $total - $row["saldo"];
						} else {
							$vector = array("saldo"=>$row["saldo"] - $total,
														"pago"=>"parcial");
							$total = 0;
						}
						$vector_vinculo = array("id_pago"=>$id_pago,"id_comprobante"=>$row["id_comprobante"],"estado"=>"activo");				
						$this->Pagos_comprobantes_mdl->alta($vector_vinculo);				
						if ($this->Comprobantes_mdl->modifica($vector, $row["id_comprobante"]))										
						{
							$data["mensaje"] .= mensajes("Comprobante Nº ".$row["id_comprobante"]
																	." Modificado correctamente");										
						} else {
							$data["mensaje"] .= mensajes("Error en la Modificacion de datos: Comprobante Nº"
																						.$row["id_comprobante"]);
						}
					}
										
				}				
				
				$this->load->view("pago/submenu",$data);				
				$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																			"razonSocial,id_empresa");
				$this->load->view("pago/new",$data);		
				
				$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
						
				break;
										
				}
						
			} 
			$this->load->view("footer",$data);
		
		}else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	//modifican los datos de los pagos realizados
	public function update_pay($id_pago=0, $procees = "update_pay", $id_comprobante = 0)// sin terminar
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$data["mensaje"] = "";			
			$data["id_pago"] = $id_pago;
			$this->load->helper("pagos");
			
			switch ($procees) 
			{
				case "new_facture":
					$data["title"] = "Lista de Facturas";
					$data["pago"] = $this->Pagos_mdl->consulta("id_pago='".$id_pago."'"); 
					$data["empresa"] = $this->Empresas_mdl->consulta("id='".$data["pago"][0]->id_empresa."'");
					$buscar = $this->input->post("buscar");
					$campo = $this->input->post("campo");
					if (!is_null($buscar)) 
					{						
						$data["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$data["pago"][0]->id_empresa.
														"') AND (".$campo." LIKE \"%".$buscar."%\") AND (id_empresa_dest='".
														$data["pago"][0]->facturacion.
									"' )AND (typo='cliente') AND (pago <>'completado') AND (id_comprobante NOT IN (
																								SELECT id_comprobante 
																								FROM pagos_comprobantes 
																								WHERE id_pago ='".$id_pago.
																			"')) ORDER BY id_comprobante DESC limit 0,1000");
						
						if (is_array($data2["lista"])) 
						{
							$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						} else {
							$data["mensaje"] =mensajes("No se encontraron resultados");	
						}				
					} else 
					{				
						$data["lista"] = $this->Comprobantes_mdl->consulta_views("(id_empresa='".$data["pago"][0]->id_empresa.
													"') AND (id_empresa_dest='".$data["pago"][0]->facturacion. 
											"') AND (typo='cliente') AND (pago<>'completado') AND (id_comprobante NOT IN (
																								SELECT id_comprobante 
																								FROM pagos_comprobantes 
																								WHERE id_pago ='".$id_pago.
																			"')) ORDER BY id_comprobante DESC limit 0,1000");	
					}
					
					if ($this->input->post("confirm")=="Agregar") 
					{						
						for ($i=0; $i < 100; $i++) 
						{							
							if ($this->input->post("check".$i)=="on") 
							{								
								$this->Pagos_comprobantes_mdl->alta(array("id_pago"=>$id_pago,
																		  "id_comprobante"=>$this->input->post("id_comprobante".$i),
																		  "estado"=>"activo"));
							} 
							
						}
						redirect('/pagos/update_pay/'.$id_pago."/", 'location');
					}
					$this->load->view("orden_pago/voucher_list",$data);
					
					break;
				
				case 'update_pay': // faltan eliminar comprobantes y realizar actualizaciones
				
					//eliminar comprobante
					$id_empresa = $this->input->post("id_empresa");
					$fecha = $this->input->post("fecha");
					$observaciones = $this->input->post("observaciones");
					$estado = $this->input->post("estado");				
					
					$vector = array("id_empresa"=>$id_empresa,"fecha"=>$fecha,"observaciones"=>$observaciones,"estado"=>$estado);	
					$total = 0;
					$n_detall = $this->Detalle_pagos_mdl->consulta("id_pago='".$id_pago."'","MAX(id_detalle) AS id_detalle");
					$j=1;	
					
					if(($vector["fecha"] != "" ) AND is_array($vector))
					{		
						for ($i=0; $i <= $n_detall[0]->id_detalle; $i++) 
						{
							$detalle = $this->input->post("detalle".$i);
							$cantidad = $this->input->post("cantidad".$i); 
							$tipo = $this->input->post("tipo".$i); 
					
							if ((($detalle != "") AND ($cantidad != "")) OR ($tipo = "efectivo" AND $cantidad != "")) 
							{
								$vector_detalle = array("id_pago"=>$id_pago,"id_detalle"=>$j,
														"tipo"=>$tipo,"detalle"=>$detalle,"cantidad"=>$cantidad);	
								$j++;
								$this->Detalle_pagos_mdl->delete(array("id_pago"=>$id_pago,"id_detalle"=>$i));
								//print_r($vector_detalle);
								$this->Detalle_pagos_mdl->alta($vector_detalle );						
								if (isset($cantidad)) 
								{
									$total += $cantidad;
								}											
							}
						}
				
						for ($i=4; $i <= 10; $i++) 
						{ 
							$detalle = $this->input->post("detalle10".$i);
							$cantidad = $this->input->post("cantidad10".$i); 
							$tipo = $this->input->post("tipo10".$i); 
				
							if ((($detalle != "") AND ($cantidad != "")) OR ($tipo = "efectivo" AND $cantidad != "")) 
							{
								$j++;
								$vector_detalle = array("id_pago"=>$id_pago,"id_detalle"=>$j,
														"tipo"=>$tipo,"detalle"=>$detalle,"cantidad"=>$cantidad);	
								//print_r($vector_detalle);
								$this->Detalle_pagos_mdl->alta($vector_detalle );						
								if (isset($vector["cantidad"])) 
								{
									$total += $cantidad;
								}											
							}
						}
				
						$vector["total"] = $total; 
						if($this->Pagos_mdl->modifica($vector,$id_pago))
						{
							$data["mensaje"] .= mensajes("Datos Modificados correctamente");	
						} else {
							$data["mensaje"] .= mensajes("Falla en la Modificacion de datos");	
						}
						
						//modificamos comprobantes
						$data["pagos_comprobantes"] = $this->Pagos_comprobantes_mdl->consulta("id_pago='".$id_pago."'");
						foreach ($data["pagos_comprobantes"] as $row) 
						{
							$comprobante = $this->Comprobantes_mdl->consulta("id_comprobante='".$row->id_comprobante."'");
							if ($comprobante[0]->saldo <= $total) 
							{
								$this->Comprobantes_mdl->modifica(array("pago"=>"completado","saldo"=>0), $row->id_comprobante);									 
							} else 
							{
								$this->Comprobantes_mdl->modifica(array("pago"=>"parcial","saldo"=>$total - $comprobante[0]->saldo),
																 $row->id_comprobante);
							}															
							$total = $total - $comprobante[0]->saldo;
						}				
					}		
						
					$this->load->view("pago/submenu",$data);
					$data["pago"] = $this->Pagos_mdl->consulta("id_pago='".$id_pago."'"); 
					$data["empresa"] = $this->Empresas_mdl->consulta("id='".$data["pago"][0]->id_empresa."'");
					$data["detalle"] = $this->Detalle_pagos_mdl->consulta("id_pago='".$id_pago."'");
					$data["pagos_comprobantes"] = $this->Pagos_comprobantes_mdl->consulta("id_pago='".$id_pago."'");
					$this->load->view("pago/update_list",$data);
					break;
					
					case 'delete_facture':
						$data = array("title"=>"Eliminar comprobante");
						if ($this->Pagos_comprobantes_mdl->delete("id_pago='".$id_pago."' AND id_comprobante='".$id_comprobante."'")) 
						{
							$data["mensaje"] = mensajes("Datos Eliminados Correctamente");	
						} else {
							$data["mensaje"] = mensajes("Error en la eliminacion de Datos");
						}
						$data["mensaje"] .= mensajes("Para volver al pago, por favor debe hacer click 
													<a href='".base_url()."index.php/pagos/update_pay/".$id_pago."/'>aqui</a>");
						$this->load->view("message",$data);
						redirect('/pagos/update_pay/'.$id_pago.'/', 'location');
						break;
			}			
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}

	//elimina un detalle de pago
	public function delete_pay($id_pago = 0, $id_detalle = 0)
	{
		if ($this->Detalle_pagos_mdl->delete(array("id_pago"=>$id_pago,"id_detalle"=>$id_detalle))) 
		{
			redirect('/pagos/update_pay/'.$id_pago."/", 'location');
		} 	
	}
	
	//elimina todo el pago
	public function delete_pay_complete($id_pago = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$data["mensaje"] = "";
			
			if ($this->input->post("delete") == "delete") 
			{				
				if ($this->Pagos_mdl->delete(array("id_pago"=>$id_pago))) 
				{
					$data["mensaje"] = mensajes("Datos Eliminados correctamente");
				} else {
					$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
				}			
			}
			$this->load->view("pago/delete",$data);
			
			$this->load->view("footer",$data);
			
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
}