<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 *  Opera con la tabla cheques_comprobantes
 */

 class Cheques_Comprobantes extends CI_Controller
 {
     
     function __construct()
	 {
     	parent::__construct(); 
		
		$this->load->model("Cheques_mdl");
		$this->load->model("Comprobantes_mdl"); 
		$this->load->model("Cheques_Comprobantes_mdl");   
     }
	 
	 public function index() //lista de cheques y recibos vinculados
	 {
		 if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");			
			$data2["mensaje"]="";
			
			if (!is_null($buscar)) 
			{
				$data2["lista"] = $this->Cheques_Comprobantes_mdl->consulta_view($campo." LIKE \"%".$buscar
																			."%\" ORDER BY id_comprobante DESC limit 0,100");
				if (is_array($data2["lista"])) 
				{
					$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data2["mensaje"] =mensajes("No se encontraron resultados");	
				}				
			} else {				
				$data2["lista"] = $this->Cheques_Comprobantes_mdl->consulta_view("1 ORDER BY id_comprobante DESC limit 0,100");	
			}
			$data2["title"] = "Lista de comprobantes y cheques vinculados";
			$this->load->view("cheques_comprobantes/list",$data2);
			
			$data["footer"]="<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer", $data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	 }
	 
	 //vincula un cheque a un cheque
	 public function link_checks_to_receipts($id_comprobante = 0, $id=0, $cuenta=0, $nro=0, $op=0)
	 {
		 if (is_logged_in()) 
		{
		
		if ($op ==1)
		{
			$data["header"]='<script>
function showCustomer(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (this.readyState==4 && this.status==200)
    {
    document.getElementById("txtHint").innerHTML=this.responseText;
    }
  }
xmlhttp.open("GET","'.base_url().'index.php/ajax/orden_pago/"+str,true);
xmlhttp.send();
}
</script>';
		} else {
			$data["header"]="";
		}
		
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$data["mensaje"] = "";
		$buscar = $this->input->post("buscar");
		$campo = $this->input->post("campo");
		
		
		$data["id_comprobante"] = $id_comprobante;
		
		switch ($op) 
		{
			case 0://seleccionar un cheque
				if (!is_null($buscar)) 
				{
					$data["lista"] = $this->Cheques_mdl->consulta_views($campo." LIKE \"%".$buscar.
																		"%\" AND  ORDER BY nro DESC limit 0,100");
					$_SESSION["cond"] = $campo." LIKE \"%".$buscar."%\" clase = 'con factura' ORDER BY nro DESC";
					$_SESSION["subtitle"][0] = "Criterio de busqueda: ";
					$_SESSION["subtitle"][1] = $campo." que contengan ".$buscar; 
					if (is_array($data["lista"])) 
					{
						$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					} else {
						$data["mensaje"] =mensajes("No se encontraron resultados");	
					}
				
				} else {
					$data["lista"] = $this->Cheques_mdl->consulta_views("clase = 'con factura' ORDER BY nro DESC limit 0,100");
				}
				
				$this->load->view("cheques_comprobantes/list_check",$data);
			break;
			
			case 1://confirmar vinculacion
				$data["cheque"] = $this->Cheques_mdl->consulta_views("id_banco='".$id."' AND cuenta='".$cuenta."' AND nro='".$nro."'");
				$data["comprobante"] = $this->Comprobantes_mdl->consulta_views("id_comprobante='".$id_comprobante."'"); 
				
				$vector = $this->input->post(array("saldo","observaciones"));
				if (($vector["saldo"] != "" ) AND is_array($vector)) 
				{
					$orden_pago = $this->input->post("orden_pago");
					switch ($orden_pago) 
					{
						case 'Realizar':
							$this->load->model("Orden_pago_mdl");
							$vect_orden = $this->input->post("pago_efectivo","pago_retenciones","pago_transferencia","pago_nota");
							$id_orden = $this->Orden_pago_mdl->alta($vect_orden);
							
							$vect_cheq_comp = array(
														"id_comprobante"=>$id_comprobante,
														"banco"=>$id,
														"cuenta"=>$cuenta,
														"nro"=>$nro,
														"saldo"=>$vector["saldo"],
														"observaciones"=>$vector["observaciones"],
														"id_orden"=>$id_orden);						
							break;
						default:
							$vect_cheq_comp = array(
														"id_comprobante"=>$id_comprobante,
														"banco"=>$id,
														"cuenta"=>$cuenta,
														"nro"=>$nro,
														"saldo"=>$vector["saldo"],
														"observaciones"=>$vector["observaciones"]);	
						break;						
					}
					if ($this->Cheques_Comprobantes_mdl->alta($vect_cheq_comp)) 
					{
						$data["mensaje"] = mensajes("Datos Incorporados Correctamente");
					}
					elseif ($this->Cheques_Comprobantes_mdl->consulta("id_comprobante='".$id_comprobante.
											"' AND banco='".$id."' AND cuenta='".$cuenta
											."' AND nro='".$nro."'")) 
					{
						$data["mensaje"] = mensajes("Datos ya ingresados en la base de datos");
					}else{
						$data["mensaje"] = mensajes("Datos No ingresados. Por favor ingreselo nuevamente");
					}
				}
				$data["lista_comprobantes"] = $this->Cheques_Comprobantes_mdl->consulta(
														"banco='".$id."' AND cuenta='".$cuenta."' AND nro='".$nro."' GROUP BY id_comprobante");				
				$data["lista_cheques"] = $this->Cheques_Comprobantes_mdl->consulta("id_comprobante='".$id_comprobante."'");
				//print_r($data["lista_cheques"]);
				$this->load->view("cheques_comprobantes/confirm",$data);
			break;
			
			case 2: // seleccionar un comprobante
				$data["id_banco"] = $id; $data["cuenta"] = $cuenta; $data["nro"] = $nro;   
				if (!is_null($buscar)) 
				{
					$data["lista"] = $this->Comprobantes_mdl->consulta_views($campo." LIKE \"%".$buscar."%\" ORDER BY id_comprobante DESC limit 0,100");
					if (is_array($data2["lista"])) 
					{
						$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					} else {
						$data["mensaje"] =mensajes("No se encontraron resultados");	
					}				
				} else {				
					$data["lista"] = $this->Comprobantes_mdl->consulta_views("1 ORDER BY id_comprobante DESC limit 0,100");	
				}
				$this->load->view("cheques_comprobantes/list_receipts",$data);
			break;			
		}
				
		$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	 }

	public function delete_vinculation($id_comprobante = "", $banco = "", $cuenta = "", $nro = "")
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			if ($this->Cheques_Comprobantes_mdl->delete(array("id_comprobante"=>$id_comprobante, 
																"banco" => $banco, 
																"cuenta" => $cuenta,
																"nro" => $nro))) 
			{
				$data["mensaje"] = mensajes("Datos Eliminados correctamente");
			} else {
				$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
			}
			$data["dir"] = "cheques_comprobantes/";
			$this->load->view("cheques_comprobantes/delete", $data);
			
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
 }
 
 ?>
