<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Clase para el manejo de csv
 */

 class Csv extends CI_Controller 
 {     
     function __construct() 
     {
         parent::__construct();
		 
		 $this->load->helper('file');
     }
	 
	 public function list_check($titulo = "")
	 {
		 $this->load->dbutil();
		 echo $titulo."\r\n";
		 if (!isset($_SESSION["cond"])) 
		 {
			 $query = $this->db->query("SELECT * FROM cheques_cuenta_empresa;");
		 } else {
			 $query = $this->db->query("SELECT * FROM cheques_cuenta_empresa WHERE ".$_SESSION["cond"].";");
			 if (!isset($_SESSION["subtitle"])) {
				foreach ($_SESSION["subtitle"] as $row) 
			 	{
					 echo $row."\r\n";
			 	}
			 }
			 
		 }		 
		
		$test = $this->dbutil->csv_from_result($query);
		$test = str_replace('"', "", $test);
		$test = str_replace(',', ";", $test);
		$test = str_replace('.', ",", $test);
		echo $test;			
	 }
	 
	 public function list_company()
	 {
		 $this->load->dbutil();
		 echo "Lista de empresas\r\n";
		 if (!isset($_SESSION["cond"])) 
		 {
			 $query = $this->db->query("SELECT * FROM empresas;");
		 } else {
			 $query = $this->db->query("SELECT * FROM empresas WHERE ".$_SESSION["cond"].";");
			 foreach ($_SESSION["subtitle"] as $row) 
			 {
				 echo $row."\r\n";
			 }
		 }		 
		
		$test = $this->dbutil->csv_from_result($query);
		$test = str_replace('"', "", $test);
		$test = str_replace(',', ";", $test);
		$test = str_replace('.', ",", $test);
		echo $test;	
	 }
	 
	 //lista de comprobantes
	 public function print_receipts()// verificar resultados
	 {
		 $this->load->dbutil();
		 echo "Lista de comprobantes\r\n";
		 //echo $_SESSION["cond"];		 
		 if (isset($_SESSION["comprobantes_list"])) 
		 {
			 $query = $this->db->query("SELECT * FROM empresa_comprobante;");
			 $test = $this->dbutil->csv_from_result($query);
		 } else {
			 $test = $this->dbutil->csv_from_result("SELECT * FROM empresa_comprobante WHERE ".$_SESSION["cond"]);
		 }
		 $test = str_replace('"', "", $test);
		 $test = str_replace(',', ";", $test);
		 $test = str_replace('.', ",", $test);
		 echo $test;
	 }
	 
	 //lista de movimientos
	 public function list_movements()
	 {
		 $this->load->dbutil();
		 echo "Lista de movimientos\r\n";
		 if (isset($_SESSION["cond"])) 
		 {
		 	 $query = $this->db->query("SELECT * FROM movimiento_cuenta_empresa;");
			 $test = $this->dbutil->csv_from_result($query);			 
		 } else {
			 $test = $this->dbutil->csv_from_result("SELECT * FROM movimiento_cuenta_empresa WHERE ".$_SESSION["cond"]);
		 }
		 $test = str_replace('"', "", $test);
		 $test = str_replace(',', ";", $test);
		 $test = str_replace('.', ",", $test);
		 echo $test;
	 }
 }
 
