<?php 
/***************************************************
           https://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: https://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

// realiza la importacion de datos de diferentes entidades
class Importar extends CI_Controller
{
    function __construct() 
	{
		parent::__construct();

        $this->load->library("csvreader");
        
	}
    
    public function index()//menu list
    {
        if (is_logged_in()) 
		{
            $data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");

            $data["mensaje"] = "";
            $this->load->view("importar/list_menu", $data);

            $data["footer"] = "";
            $this->load->view("footer", $data);
        }
        else
        {
            redirect('/seguridad/logout/', 'location');
        }
    }

    public function data_enter($type ="cheques",$site_on="false") //1 process to upload data
    {
        if (is_logged_in()) 
		{
		    $this->load->model("Cheques_mdl");
		    
            $data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
            
            $data["mensaje"] = "";
            			
            $data["title"] = "Importar Archivos"; 
            $data["session"] = $type;
            if(!isset($arch) or ($site_on == "true"))
            {
                $data["mensaje"] = mensajes("Ingresando a importar datos");
                $this->load->view("importar/form",$data);
                $data["footer"] = '<script>
                                        $("#send-data").on( "click", function() {
                                            $(this).html("<span id=\"none\" class=\"spinner-border spinner-border-sm text-light\"></span> ¡¡¡Cargando!!!");}
                                        );
                                    </script>';
            }else
            {                 
                $data = $this->process_upload($type);
            }
            
            $this->load->view("footer", $data);
        }
        else
        {
            redirect('/seguridad/logout/', 'location');
        }   
    }
    
    public function process_upload( $seccion = "cheques") //2 upload the archive and show the datas
    {
        $data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
        
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'csv';
        $config['max_size']             = 2048;
                
        $mje = "";
        $arch = $this->input->post("file_upload");
        $_SESSION["comment"] = $this->input->post("comment");

        if(isset($_FILES['file_upload']))
        {
            $name_file = $config['upload_path'].$seccion."/import_".$seccion.time().$_FILES['file_upload']['name'];
            $_SESSION["name_data"] = "import_".$seccion.time().$_FILES['file_upload']['name'];
            $_SESSION["name_file"] = $name_file;
            if(move_uploaded_file($_FILES['file_upload']['tmp_name'], $name_file))
            {
                $data["mensaje"] = mensajes("Fichero subido correctamente: ");
                $data["lista"] = $this->csvreader->parse_file($name_file); //print_r($data["lista"]); 
                if(!is_null($data["lista"]))
                {
                    switch ($seccion) {
                        case "cheques":
                            $this->load->view("importar/list_check", $data); 
                        break;
                        case "movements": //movimientos
                            $this->load->view("importar/move-list", $data);
                        break;
                        case "companies": //empresas
                            $this->load->view("importar/list_companies", $data);
                        break;
                        case "vouchers": //facturas
                            $this->load->view("importar/list_vouchers", $data);
                        break;
                    }    
                    
                    $_SESSION["list"] = $data["lista"];
                }
                    
            }else{
                $data["title"] = "Error";
                $data["mensaje"] =  mensajes("Error en la subida de archivos", "alert-danger");
                $this->load->view("message",$data);
            }
        
                $data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.datatables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/datatables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>

<script>
    $(\"#none\").css(\"display\", \"none\");
	$(\"#dataTable\").removeClass(\"d-none\");;
</script>";
        }else
        {
            $_SESSION["list"] = null;
            
            redirect('/importar/data_enter/'.$seccion."/true/", 'location');      
            $data["footer"] = "";
        }
        $this->load->view("footer", $data);
    }
    
    public function process_upload_data($type="cheques") //3 data base update
    {
        if (is_logged_in()) 
		{
            $data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
            $this->load->model("Import_data_mdl");

            $data["title"] = "Proceso de Importacion de datos";
            $data["mensaje"] = mensajes("Cargando la base de datos");
            $vector = array(); $aux = null;
            switch ($type) {
                case "cheques":
                    $this->load->model("Cheques_mdl");
                    foreach ($_SESSION["list"]  as $row) 
                	{
                	    $n=count($row)-1; //echo "limite ".$n;
                		for($i=0; $i <= $n; $i++)
                		{
                		   $vector = array("banco"=>$row[$i][4], "cuenta"=>$row[$i][5], "nro"=>$row[$i][6], "tipo"=>$row[$i][7], 
                		                    "paguese"=>$row[$i][8], "estado"=>$row[$i][9], "emision"=>$row[$i][10], "vencimiento"=>$row[$i][11],
                		                    "conciliacion"=>$row[$i][12], "cantidad"=>$row[$i][13], "confecciono"=>$row[$i][14], 
                		                    "clase"=>$row[$i][15], "impreso"=>$row[$i][16], "responsable"=>$row[$i][17], "observaciones"=>$row[$i][18]);
                                                 // print_r($vector); echo " $i <br/>";
                           
                           $vector["emision"] = str_replace("/","-",$vector["emision"]);                      
                           if ($vector["emision"][2] == "-") //encontrar el orden 
                           {
                                $vector["emision"] = transforma_fecha($vector["emision"]);
                           }
                           
                           $vector["vencimiento"] = str_replace("/","-",$vector["vencimiento"]);                      
                           if ($vector["vencimiento"][2] == "-") //encontrar el orden 
                           {
                                $vector["vencimiento"] = transforma_fecha($vector["vencimiento"]);
                           }
                           
                           $vector["conciliacion"] = str_replace("/","-",$vector["conciliacion"]);                      
                           if (!isset($vector["conciliacion"]) And $vector["conciliacion"][2] == "-") //encontrar el orden 
                           {
                                $vector["conciliacion"] = transforma_fecha($vector["conciliacion"]);
                           }

                		   if($row[$i] != null)
                		   {
                		       if($this->Cheques_mdl->alta($vector))
                		       {
                		            $data["mensaje"] .=   mensajes($i." Cargado Exitosamente: cuenta: ".$vector["cuenta"]." Nro: ".$vector["nro"].
                		                                    " Tipo: ".$vector["tipo"]."Paguese: ".$vector["paguese"]." Estado: ".$vector["estado"].
                		                                " Emision: ".$vector["emision"]." Vencimiento: ".$vector["vencimiento"]." Conciliacion: ".$vector["conciliacion"].
                		                                " Cantidad: ".$vector["cantidad"]." Confecciono: ".$vector["confecciono"].
                		                                " Impreso: ".$vector["impreso"]." Responsable".$vector["responsable"].
                		                                " Observaciones: ".$vector["observaciones"]);                                    
                                    
                		        }
                		        else
                		        {
                		            $data["mensaje"] .= mensajes($i." Error en la carga de datos: cuenta: ".$vector["cuenta"]." Nro: ".$vector["nro"].
                		                                    " Tipo: ".$vector["tipo"]."Paguese: ".$vector["paguese"]." Estado: ".$vector["estado"].
                		                                " Emision: ".$vector["emision"]." Vencimiento: ".$vector["vencimiento"]." Conciliacion: ".$vector["conciliacion"].
                		                                " Cantidad: ".$vector["cantidad"]." Confecciono: ".$vector["confecciono"].
                		                                " Impreso: ".$vector["impreso"]." Responsable".$vector["responsable"].
                		                                " Observaciones: ".$vector["observaciones"],"alert-danger"); 
                		        }
                		   }
                		   
                		}                            
                	}
                                   	
                break;

                case "movements": //movimientos bancarios
                    $this->load->model("Movimientos_mdl");
                    foreach ($_SESSION["list"]  as $row)
                    {
                        $n=count($row)-1; //echo "limite ".$n;
                		for($i=0; $i <= $n; $i++)
                        {
                            $vector = array("id"=>$row[$i][0], "id_banco"=>$row[$i][1], "cuenta"=>$row[$i][2],
                                    "movimiento"=>$row[$i][3],  "concepto"=>$row[$i][4], "valor"=>$row[$i][5], "fecha"=>$row[$i][6],
                                     "tipo"=>$row[$i][7], "observaciones"=>$row[$i][8] );
                    
                            if($row[$i] != null)
                            {
                                if($this->Movimientos_mdl->alta($vector))
                                {
                                    $data["mensaje"] .=   mensajes($i." cuenta".$vector["cuenta"].
                                                                " concepto:".$vector["concepto"]." fecha:".$vector["fecha"].
                                                        " tipo:".$vector["tipo"]." observaciones:".$vector["observaciones"]);
                                }
                                else
                                {
                                    $data["mensaje"] .=   mensajes($i." cuenta".$vector["cuenta"].
                                                        " concepto:".$vector["concepto"]." fecha:".$vector["fecha"].
                                                        " tipo:".$vector["tipo"]." observaciones:".$vector["observaciones"],
                                                        "alert-danger");
                                }
                            } 
                        }
                    }
                   
                break;

                case "vouchers":// pagos y comprobantes
                    $this->load->model("Comprobantes_mdl");
                    foreach ($_SESSION["list"]  as $row)
                    {
                        $n=count($row)-1; //echo "limite ".$n;
                		for($i=0; $i <= $n; $i++)
                        {//verificar los datos exportador por las tablas
                            $vector = array("id_comprobante"=>$row[$i][0], "pago"=>$row[$i][1], "id_empresa"=>$row[$i][2],
                                    "id_empresa_dest"=>$row[$i][3],"tipo_comprobante"=>$row[$i][5],"nro"=>$row[$i][6],
                                    "fecha"=>$row[$i][7],"iva"=>$row[$i][8],"iva_valor"=>$row[$i][9],"iva2"=>$row[$i][10],
                                    "iva_valor2"=>$row[$i][11],"neto"=>$row[$i][12],"neto2"=>$row[$i][13],
                                    "concep_no_grabados"=>$row[$i][14],"total"=>$row[$i][15],"saldo"=>$row[$i][16],
                                    "observaciones"=>$row[$i][17],"estado"=>$row[$i][18],"typo"=>$row[$i][19]);
                            if($row[$i] != null)
                            {
                                if ($this->Comprobantes_mdl->alta($vector)) {
                                    $data["mensaje"] .=   mensajes($i." Nro comprobante: ".$row["nro"]." Tipo: ".
                                                            $row["tipo_comprobante"]." Fecha: ".$row["fecha"]." Total: ".$row["total"].
                                                            " Saldo: ".$row["saldo"]." Estado: ".$row["estado"]);
                                } else {
                                    $data["mensaje"] .=   mensajes($i." Nro comprobante: ".$row["nro"]." Tipo: ".
                                                            $row["tipo_comprobante"]." Fecha: ".$row["fecha"]." Total: ".$row["total"].
                                                            " Saldo: ".$row["saldo"]." Estado: ".$row["estado"],"alert-danger");
                                }                                
                            }
                            
                        }
                    }
                       
                break;

                case "companies": //clientes y proveedores
                    $this->load->model("Empresas_mdl");
                    foreach ($_SESSION["list"]  as $row)
                    {
                        $n=count($row)-1; //echo "limite ".$n;
                		for($i=0; $i <= $n; $i++)
                        {
                            $vector = array("id"=>$row[$i][0],"cuit"=>$row[$i][1],"tipo"=>$row[$i][2],
                            "razonSocial"=>$row[$i][3],"actividad"=>$row[$i][4],"clase"=>$row[$i][5],
                            "domicilio"=>$row[$i][6],"telefono"=>$row[$i][7],"telefono2"=>$row[$i][8],
                            "celular"=>$row[$i][9],"domicilio"=>$row[$i][10],"representante"=>$row[$i][11],
                            "tel_representante"=>$row[$i][12],"otros"=>$row[$i][13]);
                            if($row[$i] != null)
                            {
                                if ($this->Empresas_mdl->alta($vector)) {
                                    $data["mensaje"] .=   mensajes($i." Cuit: ".$row["cuit"].
                                                        " Razon Social: ".$row["razonSocial"]." Tipo: ".$row["tipo"].
                                                        " Actividad: ".$row["actividad"]);
                                } else {
                                    $data["mensaje"] .=   mensajes($i." Cuit: ".$row["cuit"].
                                                        " Razon Social: ".$row["razonSocial"]." Tipo: ".$row["tipo"].
                                                        " Actividad: ".$row["actividad"],"alert-danger");
                                }                                 
                            }
                        }
                    }
                    
                break;    
            }
            $vector2 = array("name_arch"=>$_SESSION["name_data"], "comment"=>$_SESSION["comment"],
                             "datatype"=>$type, "date"=>date("d-m-Y"), "time_data"=>time());
            if ($this->Import_data_mdl->alta($vector2)) {
                $data["mensaje"] .=   mensajes("Carga de base de datos completa");
            } else {
                $data["mensaje"] .=   mensajes("Fallida la carga de datos","alert-danger");
            }        
           
         //   print_r($data["mensaje"]);
            $this->load->view("message",$data); 
            
            $data["footer"] = "";
            $this->load->view("footer", $data);
        }
        else
        {
            redirect('/seguridad/logout/', 'location');
        }   
    }
 
    public function list_upload()
    {
        $data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
        
        $this->load->view("footer", $data);
    }
}

?>