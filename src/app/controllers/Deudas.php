<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Manejan las deudas de clientes y proveedores
 */
class Deudas extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model("Deudas_mdl");
	}
	
	public function debt_list($id_empresa = 0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");			
			$data["mensaje"]="";
			
			$id_empresa_dest = $this->input->post("id_empresa_dest");
			if (!isset($id_empresa_dest)) 
			{
				$this->load->model("Cuentas_mdl");
				$data["cuentas"] = $this->Cuentas_mdl->consultavista("1 GROUP BY id_empresa ORDER BY razonSocial",
																			"razonSocial,id_empresa");
				$this->load->view("deudas/billing_list",$data);
			} else {
				$_SESSION["id_empresa_dest"] = $id_empresa_dest;
				$data["empresa"]=$this->Empresas_mdl->consulta("id='".$id_empresa."'");	
				$_SESSION["cond"] = "(id_empresa='".$id_empresa."') AND (facturacion='".
										$id_empresa_dest."')  ORDER BY fecha ASC";
				$data["lista"] = $this->Deudas_mdl->consulta($_SESSION["cond"]);
				
				/*$data2["dir"] = "imprimir/print_movements/";
				$data2["dir2"] = "csv/list_movements/";
				$this->load->view("movimientos/export_data",$data2);*/
			
				$this->load->view("deudas/list",$data);
			}	
			
			$data["footer"] = "";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}			
	}
	
	public function list_company()
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("lista_venta");			
			$data["mensaje"]="";
			
			if (!is_null($buscar)) 
			{
				$data["lista"] = $this->Empresas_mdl->consulta($campo." LIKE \"%".$buscar.
										"%\" AND clase <> 'banco' ORDER BY id DESC limit 0,100");
				if (is_array($data["lista"])) 
				{
					$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
				} else {
					$data["mensaje"] =mensajes("No se encontraron resultados");	
				}
				
			}else 
			{
				$data["lista"] = $this->Empresas_mdl->consulta(" clase <> 'banco' ORDER BY id DESC limit 0,100");
			}
						
			$this->load->view("deudas/list_companies",$data);
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.dataTables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/dataTables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
}

?>