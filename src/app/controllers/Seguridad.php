<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Clase para el control de acceso al sistema
 */
class Seguridad extends CI_Controller 
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->helper("vistas");
		
		$this->load->library('session');
	}
	
	//controla el logueo del usuario
	public function index()
	{
		$user = $this->input->post("loginname");
		$pass = $this->input->post("password");
		$data["header"] = "";
		$this->load->view('header',$data);
		$data["mensaje"] = "";
		if ($this->User_mdl->login($user,$pass)) 
		{
			$data = $this->session->userdata();
			$this->load->model("Cheques_mdl");
			$this->Cheques_mdl->modifica(array("impreso"=>"true"));
			//print_r($data["user_data"]["nickname"]);
			redirect('/Cuentas/', 'location');
		} else {
			
			if ($user != "" OR $pass != "" )
			{
				$data["mensaje"] =  mensajes("Login Incorrecto");
			}
			
			$this->load->view('seguridad/login',$data);
		}	
		
			$data["footer"] = "";
			$this->load->view("footer",$data);
	}
	
	//sale del sistema
	public function logout()
	{
		session_destroy();
		$data["header"] = "";
		$this->load->view('header',$data);
		$this->load->view('seguridad/logout');
		
		$data["footer"] = "";
		$this->load->view("footer",$data);
	}
	
	//olvido su password
	public function forgot()
	{
		$data["header"] = "";
		$this->load->view('header',$data);
		$this->load->view('forgot');
		$data["footer"] = "";
		$this->load->view("footer",$data);
		
	}
	
	//verifica si la seccion esta activa o no
	public function change_password()
	{
		$data["header"] = "";
		$this->load->view('header',$data);
		$this->load->view('menu');
		$data["mensaje"] = "";
		
		$vector = $this->input->post(array("old","new","verified"));
		if (($vector["old"]!= "") AND ($vector["new"]!= "") AND is_array($vector)) 
		{
			if ($vector["new"] == $vector["verified"]) 
			{
				$vector["new"] = hash('sha512', $vector["new"] ."descartes12432") ;				
				if ($this->User_mdl->modificar(array("password"=>$vector["new"]),
							"nickname='".$_SESSION["user_data"]["nickname"]."'")) 
				{
					$data["mensaje"] =mensajes("Cambios realizados correctamente");
				} else {
					$data["mensaje"] =mensajes("Error al actualizar los datos","alert-warning");
				}
				
			} else {
				$data["mensaje"] =mensajes("El password y su verificacion no son correctas.","alert-warning");
			}
			
		}
		$this->load->view('seguridad/change_password',$data);
		
		$data["footer"] = "";
		$this->load->view("footer",$data);
	}
}


?>