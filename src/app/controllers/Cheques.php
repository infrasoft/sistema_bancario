<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase de manejo de cheques
 */
 
class Cheques extends CI_Controller 
{
	
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model("Cuentas_mdl");
		$this->load->model("Cheques_mdl");
	}
	
	//lista y busqueda de cheques
	public function index()
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$data2["mensaje"]=""; $_SESSION["subtitle"] = "";
		
		$buscar = $this->input->post("buscar");
		$campo = $this->input->post("campo");	
		
		if (!is_null($buscar)) 
		{
			$data2["lista"] = $this->Cheques_mdl->consulta_views($campo." LIKE \"%".$buscar.
													"%\" AND NOT(estado='cancelado' OR estado='anulado') ORDER BY nro DESC limit 0,100");
			$_SESSION["cond"] = $campo." LIKE \"%".$buscar."%\" ORDER BY nro DESC";
			$_SESSION["subtitle"][0] = "Criterio de busqueda: ";
			$_SESSION["subtitle"][1] = $campo." que contengan ".$buscar; //print_r($_SESSION["subtitle"]);
			if (is_array($data2["lista"])) 
			{
				$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
			} else {
				$data2["mensaje"] =mensajes("No se encontraron resultados");	
			}
				
		} else {
				$data2["lista"] = $this->Cheques_mdl->consulta_views("NOT(estado='cancelado' OR estado='anulado') ORDER BY nro DESC limit 0,1000");
				//$_SESSION["subtitle"]="";
			} //print_r($data2["lista"]);
		$data2["dir"] = "imprimir/cheques_por_cuenta/0/0/general/";
		$data2["dir2"] = "csv/list_check/cheques/";
		$data2["dir3"] = "data_enter/cheques/";
		$this->load->view("cheques/export_data",$data2);
			
		$this->load->view("cheques/list",$data2);
		
		$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.datatables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/datatables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}
	
	public function new_check($id=0,$cuenta=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
		$this->load->view("header",$data);
		//$this->load->view("menu");
		$this->load->view("cheques/submenu",$data);
		$data["mensaje"] = "";
		
		$cant = $this->Cheques_mdl->consulta("banco='".$id
											."' AND cuenta='".$cuenta.
											"' AND impreso='false'");
		if ($_SESSION["cant_reg"] != 0) 
		{
			$data["mensaje"] .= mensajes("Existen ".$_SESSION["cant_reg"]." registros en cola");
		}									
		//agregar el cheque
		$vector = $this->input->post(array("banco","cuenta","nro","tipo","paguese",
											"emision","vencimiento","cantidad",
											"confecciono","clase","responsable","observaciones"));
												
		if (($vector["cantidad"] != "" ) AND is_array($vector)) 
		{
			if ($vector["emision"]<$vector["vencimiento"]) 
			{				
				if ($this->Cheques_mdl->alta($vector)) 
				{					
					$this->Cuentas_mdl->modifica(array("ultimo_cheque"=>$vector["nro"]),
									"id_banco='".$id."' AND nro_cuenta='".$cuenta."'"); 
				
				 	// verificar ingreso de datos
					$_SESSION["confecciono"] = $vector["confecciono"];
					$_SESSION["responsable"] = $vector["responsable"];		
					
					/*$this->load->model("Movimientos_mdl");
					$this->Movimientos_mdl->alta(array("id_banco"=>$id_banco,"cuenta"=>$nro_sucursal,"concepto"=>4,
													"valor"=>$row->cantidad,"fecha"=>$fecha_actual,"tipo"=>"egreso",
													"observaciones"=>"Cheque Nº".$row->nro ));
					//$saldo = $data["titulo"]							
					$this->Cuentas_mdl->modifica(array("saldo"=>($data["titulo"][0]->saldo - $row->cantidad)), 
														array("id_banco"=>$id_banco, "nro_cuenta"=>$nro_sucursal));	*/
												
				} else 
				{
					$data["mensaje"] .= mensajes("Datos No ingresados. Por favor ingreselo nuevamente", "alert-danger");
					if ($this->Cheques_mdl->consulta("banco='".$vector["banco"]
											."' AND cuenta='".$vector["cuenta"].
											"' AND nro='".$vector["nro"]."'") != Array()) 
					{
						$data["mensaje"] .= mensajes("Nro de Cheque ya cargado", "alert-danger");	
					}				
				}
			} else {
				$data["mensaje"] .= mensajes("La fecha de emision es mayor a la fecha de vencimiento", "alert-danger");
			}
									
			
		}		
		
		if ($this->input->post("print") == "Lote") // en caso de presionar imprimir lote
		{			
			$data["mensaje"] .= '<script language=javascript> 
											function ventanaSecundaria (URL){ 
   												window.open(URL,"ventana1","width=400,height=600,scrollbars=NO") 
												}
											ventanaSecundaria("'.base_url().'index.php/imprimir/print_pending/'.$id.'/'.$cuenta.'/"); 
										</script>';			
		}
		$data["cuenta"] = $this->Cuentas_mdl->consultavista("id_banco='".$id
														."' AND nro_cuenta='".$cuenta."'");		
		if (!isset($_SESSION["confecciono"])) 
		{
			$_SESSION["confecciono"] = "";	
		}
		
		if (!isset($_SESSION["responsable"])) 
		{
			$_SESSION["responsable"] = "";	
		}
		$this->load->view("cheques/new_complete",$data);
		
		$this->load->library('ajax');
								
		$data["footer"] =  $this->ajax->company_list("","paguese");
		$this->load->view("footer",$data);
		} else {
			redirect('/seguridad/logout/', 'location');	
		}
	}

	public function update_check($id=0,$cuenta=0,$nro=0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="<script type=\"text/javascript\" src='".base_url()."media/js/autocomplete.js'></script>";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$data["mensaje"] = "";
		
		$vector = $this->input->post(array("banco","cuenta","nro","tipo","paguese","estado",
											"emision","vencimiento","conciliacion" ,"cantidad","impreso",
											"confecciono","clase","responsable","observaciones"));
											
		if (($vector["cantidad"] != "" ) AND is_array($vector)) 
		{
			$vector["vencimiento"] = $vector["vencimiento"];
			$vector["emision"] = $vector["emision"];
			if ($this->Cheques_mdl->modifica($vector,array("banco" => $vector["banco"],
															"cuenta" => $vector["cuenta"],
															"nro" => $vector["nro"]))) 
			{
				$data["mensaje"] = mensajes("Datos Modificados Correctamente");
				
				if ($this->input->post("print") != null) 
				{
					$data["mensaje"] .='<script language=javascript> 
											function ventanaSecundaria (URL){ 
   												window.open(URL,"ventana1","width=400,height=600,scrollbars=NO") 
												}
											ventanaSecundaria("'.base_url().'index.php/imprimir/print_cheque/'.$id.'/'.$cuenta.'/'.$nro.'/"); 
										</script>';
				}
			} else {
				$data["mensaje"] = mensajes("Datos No modificados. Por favor intentelo nuevamente", "alert-danger");
			}			
		}									
		
		$data["cuenta"]=$this->Cuentas_mdl->consultavista("id_banco='".$id."' AND nro_cuenta='".$cuenta."'");		
		$data["cheques"] = $this->Cheques_mdl->consulta("banco='".$id
														."' AND cuenta='".$cuenta
														."' AND nro='".$nro."'");
				
		$this->load->view("cheques/submenu",$data);
												
		$this->load->helper("nros"); 
		$data["cant_nro"] = nros_letras($data["cheques"][0]->cantidad);									
		$this->load->view("cheques/update",$data);
											
		$this->load->library('ajax');	
		$data["footer"] = $this->ajax->company_list("","paguese");
		$this->load->view("footer",$data);
		}
		else {
			redirect('/seguridad/logout/', 'location');	
		}	
	}

	//busqueda avanzada de cheques
	public function avanced_seach($op=0,$id_banco=0,$cuenta=0)//sin terminar
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		
		$buscar = $this->input->post("buscar");
		$campo = $this->input->post("lista_venta");			
		$data2["mensaje"]="";
		
		switch ($op) 
		{
			case 0:// seleccionar las cuentas
				if (!is_null($buscar)) 
				{
					$data2["lista"] = $this->Cuentas_mdl->consultavista(
											$campo." LIKE \"%".$buscar.
											"%\" ORDER BY nro_cuenta DESC limit 0,100");
					if (is_array($data2["lista"])) 
					{
						$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					} else {
						$data2["mensaje"] =mensajes("No se encontraron resultados");	
					}	
				} else 
				{
					$data2["lista"] = $this->Cuentas_mdl->consultavista("1 ORDER BY nro_cuenta DESC limit 0,100");
				}
				$data2["link"] = "avanced_seach/2/";
				$this->load->view("cheques/list_account",$data2);
				$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.datatables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/datatables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			break;
			
			case 1:// seleccionar todas las cuentas
				$data2["mensaje"] =mensajes("Buscar en todas las cuentas ");
			
				$vector = $this->input->post(array("tipo_fecha","desde","hasta",
													"desde_cantidad","hasta_cantidad",
													"fecha_menor","fecha_mayor",
													"nro","tipo","paguese","estado","conciliacion","no_conciliados",
													"clase","responsable","observaciones",
													"search","save","Banco","razonSocial","tipo_cuenta"));
				if (($vector["save"] == "Buscar") 
				AND ($vector != array())) 
				{
					if ((!isset($_SESSION["cond"])) AND (!isset($_SESSION["subtitle"]))) 
					{
						$_SESSION["cond"] = "";
						$_SESSION["subtitle"] = "";
					}
					$_SESSION["aux_cond"] = $_SESSION["cond"]; 
					$_SESSION["aux_subtitle"] = $_SESSION["subtitle"];
				}
				
				//print_r($vector);
				$subtitle[]  = "Criterio de busqueda: ";
				if(($vector["search"] == "Buscar") 
				AND ($vector != array())) 
				{
					$cond = "";
					if ($vector["desde"] != null AND $vector["hasta"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}						
						$cond .= "(".$vector["tipo_fecha"]." BETWEEN '".$vector["desde"].
								"' AND '".$vector["hasta"]."')"; 
						$subtitle[] = " Fecha de ".$vector["tipo_fecha"]
									." Desde: ".invierte_fecha($vector["desde"])." Hasta :".invierte_fecha($vector["hasta"]);
					} 
					
					if ($vector["desde"] != null AND $vector["hasta"] == null) //desde
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(".$vector["tipo_fecha"]." >= '".$vector["desde"]."' )"; 
						$subtitle[] = " Fecha de ".$vector["tipo_fecha"]
									." Desde: ".invierte_fecha($vector["desde"]);
					}
					
					if ($vector["desde"] == null AND $vector["hasta"] != null) //hasta
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(".$vector["tipo_fecha"]." <= '".$vector["hasta"]."')"; 
						$subtitle[] = " Fecha de ".$vector["tipo_fecha"]
									." Hasta : ".invierte_fecha($vector["hasta"]);
					}
					
					if ($vector["fecha_menor"] != null AND $vector["fecha_mayor"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(emision <= '".$vector["fecha_menor"]."' AND vencimiento >= '".$vector["fecha_mayor"]."')";
						$subtitle[] = "Fecha de emision menor igual a : ".$vector["fecha_menor"].
									". Fecha de cobro mayor igual a :".$vector["fecha_mayor"];	
					}
					
					if ($vector["desde_cantidad"] != null AND $vector["hasta_cantidad"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND (cantidad BETWEEN  '".$vector["desde_cantidad"].
									"' AND '".$vector["hasta_cantidad"]."')";
						} else {
							$cond .= " (cantidad BETWEEN  '".$vector["desde_cantidad"].
									"' AND '".$vector["hasta_cantidad"]."')";
						}						
						$subtitle[] = "Montos entre $".$vector["desde_cantidad"]." hasta $".$vector["hasta_cantidad"];						
					}
					
					if ($vector["Banco"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(Banco LIKE '%".$vector["Banco"]."%')";						
						$subtitle[] = "Banco que contenga: ".$vector["Banco"];
					}
					
					if ($vector["razonSocial"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(razonSocial LIKE '%".$vector["razonSocial"]."%')";						
						$subtitle[] = "Titular :".$vector["razonSocial"];
					}  
					
					if ($vector["tipo_cuenta"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(tipo_cuenta='".$vector["tipo_cuenta"]."')";
						
						$subtitle[] = "Tipo de cuenta : ".$vector["tipo_cuenta"];
					}
					
					if ($vector["nro"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(nro=".$vector["nro"].")";				
						$subtitle[] = "Nro de cheque :".$vector["nro"];
					} 
					
					if ($vector["tipo"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(tipo='".$vector["tipo"]."')";					
						$subtitle[] = " Tipo :".$vector["tipo"];
					}
					
					if ($vector["paguese"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(paguese LIKE '%".$vector["paguese"]."%')";						
						$subtitle[] = " Paguese a :".$vector["paguese"];
					}
					
					if ($vector["estado"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(estado='".$vector["estado"]."')";						
						$subtitle[] = " Estado : ".$vector["estado"];
					}
					
					if ($vector["conciliacion"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$subtitle[] = " Fecha de conciliacion: ".invierte_fecha($vector["conciliacion"]); 
						if ($vector["no_conciliados"] == "on") 
						{
							$cond .= "(conciliacion LIKE '".$vector["conciliacion"]."') OR (estado='activo')";
							$subtitle[] = "Incluye cheques no conciliados";
						} else {
							$cond .= "(conciliacion LIKE '".$vector["conciliacion"]."')";
						}				
					}
					
					if ($vector["clase"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(clase='".$vector["clase"]."')";						
						$subtitle[] = " Clase: ".$vector["clase"];
					}
					
					if ($vector["responsable"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(responsable LIKE '%".$vector["responsable"]."%')";						
						$subtitle[] = "Responsable : ".$vector["responsable"];
					}
					
					if ($vector["observaciones"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(observaciones LIKE '%".$vector["observaciones"]."%')";						
						$subtitle[] = "observaciones: ".$vector["observaciones"];
					}
					$_SESSION["cond"] = $cond; 
					$_SESSION["subtitle"] = $subtitle; 
					if ($vector["save"] == "Buscar")  
					{
						if ($_SESSION["aux_cond"] == "") 
						{
							$_SESSION["aux_cond"] = $_SESSION["cond"]; 
							$_SESSION["aux_subtitle"] = $_SESSION["subtitle"];
						} else {
							$_SESSION["cond"] = $_SESSION["aux_cond"];						
							$_SESSION["subtitle"] = $_SESSION["aux_subtitle"] ;
						}						
					} 
					
					$data2["lista"]=$this->Cheques_mdl->consulta_views($_SESSION["cond"]);
					//print_r($_SESSION["cond"]);
					if (is_array($data2["lista"])) 
					{
						$data2["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
					} else {
						$data2["mensaje"] =mensajes("No se encontraron resultados");	
					}
					
					$data2["dir"] = "imprimir/cheques_por_cuenta/0/0/general/";
					$data2["dir2"] = "csv/list_check/cheques/";
					$this->load->view("cheques/export_data",$data2);
					$this->load->view("cheques/list",$data2);
					
					$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.datatables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/datatables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
				}
				else {
					$this->load->view("cheques/open_form",$data2);
					$this->load->view("cheques/account_data");
					$this->load->view("cheques/date_search");
					$this->load->view("cheques/between_dates");
					$this->load->view("cheques/money_search");
					$this->load->view("cheques/other_input");
					$this->load->view("cheques/close_form");
					$data["footer"] = "";
				}	
				
								
				
			break;
			
			case 2: //seleccionar una cuenta en particular
				$data_cuentas = $this->Cuentas_mdl->consultavista("id_banco = '".$id_banco."' AND nro_cuenta = '".$cuenta."'",
																	"Banco, razonSocial"); 
				
				$data2["mensaje"] =mensajes("Buscar en: Banco: <b>".$data_cuentas[0]->Banco.
																"</b> Titular: <b>".$data_cuentas[0]->razonSocial."</b>
																cuenta Nª <b>".$cuenta."</b>" );
			
				$vector = $this->input->post(array("tipo_fecha","desde","hasta",
													"desde_cantidad","hasta_cantidad",
													"fecha_menor","fecha_mayor",
													"nro","tipo","paguese","estado","conciliacion","no_conciliados","clase",
													"responsable","observaciones","search","save"));
				if (($vector["save"] == "Buscar") 
				AND ($vector != array())) 
				{
					if ((!isset($_SESSION["cond"])) AND (!isset($_SESSION["subtitle"]))) 
					{
						$_SESSION["cond"] = "";
						$_SESSION["subtitle"] = "";
					}
					$_SESSION["aux_cond"] = $_SESSION["cond"]; 
					$_SESSION["aux_subtitle"] = $_SESSION["subtitle"];
				}									
													
				$subtitle[]  = "Criterio de busqueda: ";
				//print_r($vector);
				if((($vector["search"] == "Buscar") OR ($vector["save"] == "Buscar")) 
				AND ($vector != array())) 
				{
					$cond = "(id_banco='".$id_banco."' AND cuenta='".$cuenta."')";
					if ($vector["desde"] != null AND $vector["hasta"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}						
						$cond .= " (".$vector["tipo_fecha"]." BETWEEN '".$vector["desde"]."' AND '".$vector["hasta"]."')";						
						$subtitle[] = " Fecha de ".$vector["tipo_fecha"]
									." Desde: ".invierte_fecha($vector["desde"])." Hasta :".invierte_fecha($vector["hasta"]);
					}
					
					if ($vector["desde"] != null AND $vector["hasta"] == null) //desde
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(".$vector["tipo_fecha"]." >= '".$vector["desde"]."' )"; 
						$subtitle[] = " Fecha de ".$vector["tipo_fecha"]
									." Desde: ".invierte_fecha($vector["desde"]);
					}
					
					if ($vector["desde"] == null AND $vector["hasta"] != null) //hasta
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(".$vector["tipo_fecha"]." <= '".$vector["hasta"]."')"; 
						$subtitle[] = " Fecha de ".$vector["tipo_fecha"]
									." Hasta : ".invierte_fecha($vector["hasta"]);
					}
					
					if ($vector["fecha_menor"] != null AND $vector["fecha_mayor"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}
						$cond .= "(emision <= '".$vector["fecha_menor"]."' AND vencimiento >= '".$vector["fecha_mayor"]."')";
						$subtitle[] = "Fecha de emision menor igual a : ".$vector["fecha_menor"].
									". Fecha de cobro mayor igual a :".$vector["fecha_mayor"];	
					} 
					
					if ($vector["desde_cantidad"] != null AND $vector["hasta_cantidad"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND (cantidad BETWEEN  '".$vector["desde_cantidad"].
									"' AND '".$vector["hasta_cantidad"]."')";
						} else {
							$cond .= " (cantidad BETWEEN  '".$vector["desde_cantidad"].
									"' AND '".$vector["hasta_cantidad"]."')";
						}						
						$subtitle[] = "Montos entre $".$vector["desde_cantidad"]." hasta $".$vector["hasta_cantidad"];						
					}
					
					if ($vector["nro"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(nro=".$vector["nro"].")";					
						$subtitle[] = "Nro de cheque :".$vector["nro"];
					} 
					
					if ($vector["tipo"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(tipo='".$vector["tipo"]."')";						
						$subtitle[] = " Tipo :".$vector["tipo"];
					}
					
					if ($vector["paguese"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(paguese LIKE '%".$vector["paguese"]."%')";						
						$subtitle[] = " Paguese a :".$vector["paguese"];
					}
					
					if ($vector["estado"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(estado='".$vector["estado"]."')";						
						$subtitle[] = " Estado : ".$vector["estado"];
					}
					
					if ($vector["conciliacion"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						}											
						$subtitle[] = " Fecha de conciliacion: ".invierte_fecha($vector["conciliacion"]);
						if ($vector["no_conciliados"] == "on") 
						{
							$cond .= "((conciliacion LIKE '".$vector["conciliacion"]."') OR (estado='activo' AND vencimiento<= '".$vector["conciliacion"]."'))";
							$subtitle[] = "Incluye cheques no conciliados";
						} else {
							$cond .= "(conciliacion LIKE '".$vector["conciliacion"]."')";
						}	print_r($vector["no_conciliados"]);
					}
					
					if ($vector["clase"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(clase='".$vector["clase"]."')";					
						$subtitle[] = " Clase: ".$vector["clase"];
					}
					
					if ($vector["responsable"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(responsable LIKE '%".$vector["responsable"]."%')";						
						$subtitle[] = "Responsable : ".$vector["responsable"];
					}
					
					if ($vector["observaciones"] != null) 
					{
						if ($cond != "") 
						{
							$cond .= " AND ";
						} 
						$cond .= "(observaciones LIKE '%".$vector["observaciones"]."%')";						
						$subtitle[] = "observaciones: ".$vector["observaciones"];
					}
					
					$_SESSION["cond"] = $cond; 
					$_SESSION["subtitle"] = $subtitle; 
										
					if ($vector["save"] == "Buscar")  
					{
						if ($_SESSION["aux_cond"] == "") 
						{
							$_SESSION["aux_cond"] = $_SESSION["cond"]; 
							$_SESSION["aux_subtitle"] = $_SESSION["subtitle"];
						} else {
							$_SESSION["cond"] = $_SESSION["aux_cond"];						
							$_SESSION["subtitle"] = $_SESSION["aux_subtitle"] ;
						}						
					} 
					
					$data2["lista"]=$this->Cheques_mdl->consulta_views($_SESSION["cond"]);
					//print_r($_SESSION["cond"]);
					if (is_array($data2["lista"])) 
					{						
						$data2["mensaje"] = mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");						
					} else {
						$data2["mensaje"] =mensajes("No se encontraron resultados");	
					}
					$data2["dir"] = "imprimir/cheques_por_cuenta/".$id_banco."/".$cuenta."/";
					$data2["dir2"] = "csv/list_check/cheques/";
					$this->load->view("cheques/export_data",$data2);
					$data2["link"] = "avanced_seach/2/";					
					$this->load->view("cheques/list",$data2);
					
					$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.datatables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/datatables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
				}
				else {
					$this->load->view("cheques/open_form",$data2);
					$this->load->view("cheques/date_search");
					$this->load->view("cheques/between_dates");
					$this->load->view("cheques/money_search");
					$this->load->view("cheques/other_input");
					$this->load->view("cheques/close_form");
					$data["footer"] = "";
				}				
								
			break;			
		}
				
		$this->load->view("footer",$data);	
		} else {
			redirect('/seguridad/logout/', 'location');
		}
		
	}

	//limpia el lote de los cheques no impresos
	public function clean_checks($id=0,$cuenta = 0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");
		$this->load->view("cheques/submenu");	
		$data["mensaje"]="";	
		
		if ($this->Cheques_mdl->delete(array("banco"=>$id,"cuenta"=>$cuenta,"impreso"=>"false"))) 
		{
			$data["mensaje"]=mensajes("Limpieza de lote realizada correctamente");
		} else {
			$data["mensaje"]=mensajes("La limpieza de lote no se pudo realizar correctamente, por favor intentelo de nuevo",
									"alert-danger");
		}
		
		$this->load->view("cheques/clean_checks",$data);
		
		$data["footer"] = "";
		$this->load->view("footer",$data);	
		}
		else {
			redirect('/seguridad/logout/', 'location');
		}
	}
	
	//realiza la eliminacion de la base de datos de un cheque
	public function delete_check($id=0,$cuenta=0,$nro=0)
	{
		if (is_logged_in()) 
		{
		$data["header"]="";
		$this->load->view("header",$data);
		$this->load->view("menu");	
		//$this->load->view("cheques/submenu",$data);
		$data["mensaje"] = "";
		if ($this->input->post("delete") == "delete") 
		{
			if ($this->Cheques_mdl->delete(array("banco" => $id,"cuenta"=>$cuenta,"nro"=>$nro))) 
			{				
				if ($this->Cheques_mdl->consulta(array("banco" => $id,"cuenta"=>$cuenta,"nro"=>$nro))) 
				{
					if ($this->Cheques_mdl->delete("banco='".$id."' AND cuenta='".$cuenta."' AND nro='".$nro."'")) 
					{
						$data["mensaje"] = mensajes("Datos Eliminados correctamente");
					}
				} else {
					$data["mensaje"] = mensajes("Datos Eliminados correctamente");
				}
				
			} else {
				$data["mensaje"] = mensajes("Fallo en la eliminacion de datos");
			}						
		} 
		
		$this->load->view("cheques/delete_check",$data);
		
		$data["footer"] = "";
		$this->load->view("footer",$data);	
		}
		else {
			redirect('/seguridad/logout/', 'location');
		}
	}
	
	public function check_conciliation($id_banco =0, $nro_sucursal=0)
	{
		if (is_logged_in()) 
		{
			$data["header"]="";
			$this->load->view("header",$data);
			$this->load->view("menu");
			$this->load->view("cheques/submenu");	
			
			$data["mensaje"]="";
			$data["lista"] = array();
			$data["list_conciliacion"] = array();
			
			$buscar = $this->input->post("buscar");
			$campo = $this->input->post("campo");
			
			if (($id_banco ==0) AND ($nro_sucursal == 0)) 
			{				
				if (!is_null($buscar)) 
				{
					$data["lista"] = $this->Cuentas_mdl->consultavista("(".$campo." LIKE \"%".$buscar.
								"%\") AND (conciliados <> 'conciliados') ORDER BY nro_cuenta DESC limit 0,100");
					if (is_array($data["lista"])) 
					{
						$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						
					} else {
						$data["mensaje"] =mensajes("No se encontraron resultados");				
					}	
				} else 
				{
					$data["lista"] = $this->Cuentas_mdl->consultavista("1 ORDER BY nro_cuenta DESC limit 0,100");
				}
				$data["link"] = "check_conciliation/"; 
				//print_r($data["lista"]);
				$this->load->view("cheques/list_reconciliation",$data);
			} else {
				
				$this->load->model("Temp_conciliacion_mdl");
				if (!is_null($buscar)) 
				{
					$aux = "((banco='".$id_banco."' AND cuenta='".$nro_sucursal.
										"') AND (".$campo." LIKE \"%".$buscar.
										"%\")) AND (estado = 'activo') ORDER BY nro DESC limit 0,100"; 
					$data["lista"] = $this->Cheques_mdl->consulta($aux,"banco,cuenta,nro,emision,vencimiento,tipo,paguese,cantidad");
					//print_r($data["lista"]);		
					
								
					if ($data["lista"] != Array()) 
					{
						$data["mensaje"] =mensajes("Se encontraron ".$_SESSION["cant_reg"]." resultados");
						//aqui debo mejorar
						
						foreach ($data["lista"] as $row ) 
						{
							if($row->vencimiento > date("Y-m-d"))
							{
								$data["mensaje"] = mensajes("El cheque entro antes de fecha de pago");
							}else
							{
								$this->Temp_conciliacion_mdl->alta( array(
															"banco"=>$row->banco,
															"cuenta"=>$row->cuenta,
															"nro"=>$row->nro,
															"emision"=>$row->emision,
															"vencimiento"=>$row->vencimiento,
															"tipo"=>$row->tipo,
															"paguese"=>$row->paguese,
															"cantidad"=>$row->cantidad));
							}							
						}
						
					} else {
						$aux = "((banco='".$id_banco."' AND cuenta='".$nro_sucursal.
										"') AND (".$campo." LIKE \"%".$buscar.
										"%\")) ORDER BY nro DESC limit 0,100"; 
						$consulta = $this->Cheques_mdl->consulta($aux,"estado,conciliacion");
						//print_r($consulta);
						if ($consulta) 
						{							
							if ($consulta[0]->estado == "conciliados") 
							{
								$data["mensaje"] =mensajes("Cheque conciliado el : <b>".invierte_fecha($consulta[0]->conciliacion)."</b>");
							} else 
							{
								$data["mensaje"] =mensajes("Cheque en estado : <b>".$consulta[0]->estado."</b>");
							}
							
								
						} else {
							$data["mensaje"] =mensajes("No se encontraron resultados");	
						}				
						
					}				
				}
				$data["list_conciliacion"] = $this->Temp_conciliacion_mdl->consulta(array("banco='".$id_banco.
															"' AND cuenta='".$nro_sucursal."'"));							
				$data["titulo"] = $this->Cuentas_mdl->consultavista("id_banco='".$id_banco.
																"' AND nro_cuenta='".$nro_sucursal."'");
				//print_r($data["titulo"]);
				$conciliacion = $this->input->post("conciliacion");
				
				if ($conciliacion == "ok")  
				{	
					$fecha_actual = $this->input->post("fecha");
					foreach ($data["list_conciliacion"] as $row) 
					{
						if ($row->vencimiento > $fecha_actual ) 
						{
							$data["mensaje"] =mensajes("El cheque Nª ".$row->nro." entro antes de fecha de pago");
						} else 
						{
							if ($this->Cheques_mdl->modifica(array("estado"=>"conciliados",															
															"conciliacion"=>$fecha_actual),
											array("banco" => $id_banco,
												"cuenta" => $nro_sucursal,
												"nro"=>$row->nro))) 
							{
								$data["mensaje"] .= mensajes("Cheques Nª ".$row->nro." conciliados correctamente");
								$this->load->model("Movimientos_mdl");							
								if ($this->Movimientos_mdl->alta(array("id_banco"=>$id_banco,"cuenta"=>$nro_sucursal,"concepto"=>4,
													"valor"=>$row->cantidad,"fecha"=>$fecha_actual,"tipo"=>"egreso",
													"observaciones"=>"Nº".$row->nro." ".$row->paguese ))) 
								{
									$data["mensaje"] .= mensajes("Se agrego nuevo movimiento bancario");
									if ($this->Cuentas_mdl->modifica(array("saldo"=>($data["titulo"][0]->saldo - $row->cantidad)), 
														array("id_banco"=>$id_banco, "nro_cuenta"=>$nro_sucursal))) 
									{
										$data["mensaje"] .= mensajes("Se modifico correctamente el saldo");
									} else {
										$data["mensaje"] .= mensajes("No se modifico el saldo");	
									}									
								}
								else {
									$data["mensaje"] .= mensajes("No se genero movimiento bancario");
								}
								//$saldo = $data["titulo"]							
							}
						}											
						
					}
					    
						//$_SESSION["conciliacion"] = $data["list_conciliacion"];
						/*$data["mensaje"] .='<script language=javascript> 
											function ventanaSecundaria (URL){ 
   												window.open(URL,"ventana1","width=400,height=600,scrollbars=NO") 
												}
											ventanaSecundaria("'.base_url().'index.php/imprimir/print_reconciliation/'.$id_banco.'/'.$nro_sucursal.'/"); 
										</script>';*/
					
					
					$this->Temp_conciliacion_mdl->delete(array("banco" => $id_banco,
												"cuenta" => $nro_sucursal));
					
				}				
				$this->load->view("cheques/reconciliation",$data);
			}
			
			
			$data["footer"] = "<!-- Page level plugin JavaScript-->
  <script src=\"".base_url()."media/vendor/datatables/jquery.datatables.js\"></script> 
  <script src=\"".base_url()."media/vendor/datatables/datatables.bootstrap4.js\"></script>  

  <!-- Demo scripts for this page-->
  <script src=\"".base_url()."media/js/demo/datatables-demo.js\"></script>

	<script type=\"text/javascript\">
    $(document).ready(function() {
        $('#dataTable').dataTable( {
        	\"destroy\":true,
            \"language\": {
                \"url\": \"https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json\"
            }
        } );
    } );
</script>";
			$this->load->view("footer",$data);	
		}
		else {
			redirect('/seguridad/logout/', 'location');	
		}
	}

	// elimina un elemento de la conciliacion
	public function delete_conciliation($id_banco =0, $nro_sucursal=0, $nro=0)
	{
		$this->load->model("Temp_conciliacion_mdl");
		if ($this->Temp_conciliacion_mdl->delete(array("banco"=>$id_banco,"cuenta"=>$nro_sucursal,"nro"=>$nro))) 
		{
			
			redirect("/cheques/check_conciliation/".$id_banco."/".$nro_sucursal."/", 'location');
		} else {
			$this->Temp_conciliacion_mdl->delete("banco='".$id_banco."' AND cuenta='".$nro_sucursal." AND nro='".$nro."'");
			redirect('/index/', 'location');	
		}	
	}
}
?>