<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Clase para el ajax de diferentes necesidades
 */
class Ajax extends CI_Controller
{
	
	function __construct() 
	{
		parent::__construct();
	}
	
	//funciones para transformar nros a letras
	public function nros_a_letras($nro)
	{
		$this->load->helper("nros");
		echo nros_letras($nro);
	}
	
	//transforma una fecha en formato letras
	public function fecha_a_texto($fecha_actual = "")
	{
		//$fecha_actual = date("d-m-Y");
		setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
		echo strftime("%d de %B de %Y", strtotime($fecha_actual));
	}
	
	//cambia las opciones en la vinculacion de cheques a comprobantes
	public function orden_pago($op=0)
	{
		switch ($op) 
		{
			case "realizar":
				echo '<h3 class="form-group col">Formas de pago</h3>
			
			
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Efectivo </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_efectivo" name="pago_efectivo" step="0.01"
  					/>
				</div>
			</div>
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Retenciones </span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_retenciones" name="retenciones" step="0.01"
  					/>
				</div>
			</div>
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Tr.</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Nro Transferencia" id="nro_transferencia" name="nro_transferencia" step="0.01"
  					/>
					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_transferencia" name="pago_transferencia" step="0.01"
  					/>
				</div>
			</div>
			<div class="form-group col">	
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">N C</span>
  					</div>
  					<input type="tel" class="form-control" placeholder="Ingrese la cantidad" id="pago_nota" name="pago_nota" step="0.01"
  					/>
					<input type="hidden" id="tipo_nota" name="tipo_nota" value=""/>
				</div>
			</div>';
				break;
			case 'ya_realizada':
				echo '			
			<div class="form-group col">
				<div class="input-group mb-3">
  					<div class="input-group-prepend">
    					<span class="input-group-text" id="inputGroup-sizing-default">Orden de Pago </span>
  					</div>
  					<input type="number" class="form-control" placeholder="orden" id="id_orden" name="id_orden" />
				</div>';
				break;
			default:
				
				break;
		}
	}

	//realiza la consulta de cheques ya conciliados
	public function ya_conciliados($date='',$id_banco=0,$nro_cuenta="")
	{
		$this->load->model("Cheques_mdl");
		if ($date != "") 
		{
			echo "<table class='table table-striped'>";
			$lista = $this->Cheques_mdl->consulta("banco='".$id_banco."' AND cuenta='".$nro_cuenta."' AND conciliacion='".$date."'");
			foreach ($lista as $row) 
			{
				echo "<tr>									
									<td>".$row->nro."</td>									
									<td>".$row->emision."</td>
									<td>".$row->vencimiento."</td>
									<td>".$row->paguese."</td>
									<td>".number_format($row->cantidad,2,",",".")."</td>									
									<td>
										<a href='".base_url()."index.php/cheques/update_check/".
																				$row->banco."/".
																				$row->cuenta."/".
																				$row->nro."/'
											  title='Modificar dato de cheque NRO:".$row->nro."'>
											<i class='fas fa-money-check'></i>
										</a> 
									</td>
							</tr>";	
			}
			echo "</table>";
		}
	}
}
