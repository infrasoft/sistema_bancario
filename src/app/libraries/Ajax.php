<?php 
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');
/**
 * 
 */
class Ajax
{	
	protected $ci;
   
   function __construct(){
      $this->ci =& get_instance();
   }	
	//lista de empresas
	public function company_list($cond="",$name="empresa",$lista="list")
	{
		$aux="<script>
		/*An array containing all the list:*/
var ".$lista." =";
		$this->ci->load->model('Empresas_mdl'); 
		$consulta = $this->ci->Empresas_mdl->consulta($cond);
		if ($_SESSION["cant_reg"]>0) 
		{
			foreach ($consulta  as $row ) 
			{
				$array[] = $row->razonSocial;
			}
			$aux.= json_encode($array);
		}			
		
		$aux.= ";
/*initiate the autocomplete function */
autocomplete(document.getElementById('".$name."'), ".$lista.");
</script>";
		return $aux;
	}
	
	//lista de bancos
	public function bank_list()
	{
		$aux="<script>
		/*An array containing all the list:*/
var list =";
		$this->ci->load->model('Empresas_mdl'); 
		$consulta = $this->ci->Empresas_mdl->consulta("clase='banco'");
		if ($_SESSION["cant_reg"]>0) 
		{
			foreach ($consulta  as $row ) 
			{
				$array[] = $row->razonSocial;
			}
			$aux.= json_encode($array);
		}			
		
		$aux.= ";
/*initiate the autocomplete function */
autocomplete(document.getElementById('banco'), list);
</script>";
		return $aux;
	}
	
	//lista de cuentas
	public function account_list($name="cuenta",$lista="list")
	{
		$aux="<script>
		/*An array containing all the list:*/
var ".$lista." =";
		$this->ci->load->model("Cuentas_mdl");
		$consulta = $this->ci->Cuentas_mdl->consultavista("estado='activa'");
		if ($_SESSION["cant_reg"]>0) 
		{
			foreach ($consulta  as $row ) 
			{
				$array[] = $row->razonSocial." - ".$row->Banco."|".$row->nro_cuenta."#".$row->id_banco;
			}
			$aux.= json_encode($array);
		}
		$aux.= ";
/*initiate the autocomplete function */
autocomplete(document.getElementById('".$name."'), ".$lista.");
</script>";
		return $aux;
	}
	
	//muestra la lista de conceptos
	public function conceptos($name="concepto",$lista="lista2")
	{
		$aux="<script>
		/*An array containing all the list:*/
var ".$lista." =";
		$this->ci->load->model("Concepto_mdl");
		$consulta = $this->ci->Concepto_mdl->consulta();
		if ($_SESSION["cant_reg"]>0) 
		{
			foreach ($consulta  as $row ) 
			{
				$array[] = $row->concepto." - ".$row->codigo."|".$row->id_concepto;
			}
			$aux.= json_encode($array);
		}
		$aux.= ";
/*initiate the autocomplete function */
autocomplete(document.getElementById('".$name."'), ".$lista.");
</script>";
		return $aux;
	}
}
