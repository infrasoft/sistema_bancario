/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
	
	//calcula el total del comprobante
	function total_facturas() 
		{
		  var 	neto = parseFloat(document.getElementById("neto").value);
		  if(neto == null) 
		  {
			  neto = 0;
		  }  
		  var 	neto2 = parseFloat(document.getElementById("neto2").value);
		  if(neto2 == null)
		  {
			  neto2 = 0;
		  } 
		  var 	iva = parseFloat(document.getElementById("iva_valor").value);
		  if(iva == null)
		  {
			  iva = 0;
		  }
		  var 	iva2 = parseFloat(document.getElementById("iva_valor2").value);
		  if(iva2 == null)
		  {
			  iva2 = 0;
		  }
  		  var	concept = parseFloat(document.getElementById("concep_no_grabados").value);
  		  var	imp = parseFloat(document.getElementById("imp_internos").value);
  		  var	total = parseFloat(neto + iva + neto2 + iva2 + concept + imp);
  			document.getElementById("total").value = total.toFixed(2);
  			document.getElementById("saldo").value = total.toFixed(2);
		}
		
	//calcula el iva
	function calculo_iva () 
	{
		var iva = parseFloat(document.getElementById("iva").value);
		if(iva == null)
		  {
			  iva = 0;
		  }
		var neto = parseFloat(document.getElementById("neto").value);
		 if(neto == null) 
		  {
			  neto = 0;
		  }   
		var total_iva = (neto * iva )/ 100;
		if (isNaN(total_iva)) 
		{
			total_iva = 0;
		}
		document.getElementById("iva_valor").value = total_iva.toFixed(2);
	}
	
	//calcula el iva
	function calculo_iva2 () 
	{
		var iva = parseFloat(document.getElementById("iva2").value);
		if(iva == null)
		  {
			  iva = 0;
		  }
		var neto = parseFloat(document.getElementById("neto2").value);
		if(neto == null)
		  {
			  neto = 0;
		  }  
		var total_iva = (neto * iva )/ 100;
		if(isNaN(total_iva))
		{
			total_iva = 0
		}
		document.getElementById("iva_valor2").value = total_iva.toFixed(2);
	}
	
	//verifica el estado del saldo y las condiciones
	function saldo_estado () 
	{
	  var saldo = parseFloat(document.getElementById("saldo").value);
	  var total = parseFloat(document.getElementById("total").value);
	  
	  if (total == saldo)  
	  {
	  	document.getElementById("pago").value = "pendiente";
	  }else
	  {
	  	if(saldo == 0) 
	  	{
			 document.getElementById("pago").value = "completado";	  	 
	  	}
	  	else
	  	{
	  		if(saldo != 0) 
	  		{		
		 		document.getElementById("pago").value = "parcial";	  	 
	  		}	
	  	}
	  } 
	  
	   
	}
	
	//calcula el iva y el total
	function iva_total() 
	{	  	
	  	calculo_iva ();
	  	calculo_iva2 ();
	  	total_facturas();   
	}
	
	function verificar_saldo() 
	{
	  var saldo = parseFloat(document.getElementById("saldo").value);
	  var total = parseFloat(document.getElementById("total").value);
	  var pago = document.getElementById("pago").value;
	  
	  if ((pago == "completado") && (saldo==0)) 
	  {
	  	 submit();
	  } else
	  {
	  	if ((pago=="pendiente") && (saldo==total)) 
	  	{
	  		submit();
	  	} else
	  	{
	  		if ((pago =="parcial") && (saldo!=total) && (saldo!=0)) 
	  		{
	  			submit(); 
	  		}else
	  		{
	  			alert("Por favor Verifique el estado de Pago y Saldo");
	  		}
	  	}
	  }
	 
	}