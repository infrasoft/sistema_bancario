/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

//calcula el total de las ordenes de pago
function total_orden_pago () 
{
	var total_facturas = document.getElementById("total_facturas").value;
		total_facturas = parseFloat(total_facturas.replace(",","."));
	if (isNaN(total_facturas)) {total_facturas = 0;};
	
	var total_cheques = document.getElementById("total_cheques").value;
		total_cheques = parseFloat(total_cheques.replace(",","."));
	if (isNaN(total_cheques)) {total_cheques = 0;};
	
	var pago_efectivo =document.getElementById("pago_efectivo").value;
		pago_efectivo = parseFloat(pago_efectivo.replace(",","."));
	if (isNaN(pago_efectivo)) {pago_efectivo = 0;};
	
	var pago_retenciones = document.getElementById("pago_retenciones").value;
		pago_retenciones = parseFloat(pago_retenciones.replace(",","."));
	if (isNaN(pago_retenciones)) {pago_retenciones = 0;};
	
	var pago_transferencia = document.getElementById("pago_transferencia").value;
		pago_transferencia = parseFloat(pago_transferencia.replace(",","."));
	if (isNaN(pago_transferencia)) {pago_transferencia = 0;};
	
	var pago_nota = document.getElementById("pago_nota").value;
		pago_nota = parseFloat(pago_nota.replace(",","."));
	if (isNaN(pago_nota)) {pago_nota = 0;};
	
	var	total = parseFloat(total_cheques + pago_efectivo + pago_retenciones + pago_transferencia + pago_nota);
  	document.getElementById("total").value = total.toFixed(2);
  	document.getElementById("pendiente").value = (total - total_facturas).toFixed(2);
}